/* eslint-disable @typescript-eslint/no-var-requires */
/** @type {import('tailwindcss').Config} */
const plugin = require('./plugin/index.cjs');

module.exports = {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  plugins: [plugin],
};
