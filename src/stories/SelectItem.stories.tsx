import { Meta, StoryFn } from '@storybook/react';

import { SelectItem } from '../lib/components/SelectItem';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Inputs/Select/SelectItem',
  component: SelectItem,
} as Meta<typeof SelectItem>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof SelectItem> = (args) => <SelectItem {...args} />;

export const _SelectItem = Template.bind({});

_SelectItem.args = {
  children: 'jF',
  disabled: false,
  selected: true,
  value: 'jF',
};
