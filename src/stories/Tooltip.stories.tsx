import { Meta, StoryFn } from '@storybook/react';

import { Tooltip } from '../lib/components/Tooltip';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Tooltip',
  component: Tooltip,
} as Meta<typeof Tooltip>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof Tooltip> = (args) => {
  return (
    <Tooltip {...args}>
      <div>Hover to view tooltip</div>
    </Tooltip>
  );
};

export const _Tooltip = Template.bind({});
_Tooltip.args = {
  content: 'Tooltip content',
  placement: 'bottom',
};
