import { Meta, StoryFn } from '@storybook/react';

import { Typography } from '../lib/components/Typography';
// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Typography',
  component: Typography,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {
    children: { control: { type: 'text' } },
  },
} as Meta<typeof Typography>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof Typography> = (args) => <Typography {...args} />;

export const _Typography = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
_Typography.args = {
  variant: 'h1',
  children: 'Type Some Thing',
};
