import { Meta, StoryFn } from '@storybook/react';

import FieldInputControlledProvider from '@/lib/components/FieldInput/Provider';
import InputRange from '@/lib/components/InputRange/InputRange';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Inputs/InputRange/InputRange',
  component: InputRange,
} as Meta<typeof InputRange>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof InputRange> = (args) => {
  return (
    <FieldInputControlledProvider>
      <InputRange {...args} />
    </FieldInputControlledProvider>
  );
};

export const _InputRange = Template.bind({});

_InputRange.args = {};
