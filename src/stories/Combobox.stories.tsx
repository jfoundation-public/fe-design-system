import { Meta, StoryFn } from '@storybook/react';
import clsx from 'clsx';
import { useState } from 'react';

import provinceOptions from '@/demos/province-options';

import { Combobox } from '../lib/components/Combobox';

const options = provinceOptions.map((p) => ({
  ...p,
  id: p.value,
  plus: Math.floor(Math.random() * 1000),
}));

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Combobox',
  component: Combobox,
} as Meta<typeof Combobox>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof Combobox> = (args) => {
  const [value, setValue] = useState('');

  const provinceSelected = options.find((opt) => opt.value === value);

  return (
    <Combobox
      {...args}
      options={options}
      value={value}
      onChange={(e: any) => setValue(e.target.value)}
      renderInput={({ onOpenChange }) => (
        <button
          type='button'
          className='w-full text-left'
          onClick={() => {
            onOpenChange((p) => !p);
          }}
        >
          {provinceSelected ? (
            <div className='flex gap-3'>
              {provinceSelected?.label} - {provinceSelected?.plus}
            </div>
          ) : (
            'Select option'
          )}
        </button>
      )}
      renderOption={({ option, onSelected, isSelected }) => (
        <div
          key={option.id}
          className={clsx(isSelected && 'text-text-error')}
          onClick={() => {
            onSelected(option.id);
            setValue(option.value);
          }}
          role='presentation'
        >
          <h5>{option.label}</h5>
          <p>{option.plus}</p>
        </div>
      )}
    />
  );
};

export const _Combobox = Template.bind({});
_Combobox.args = {
  disabled: false,
  required: true,
  label: 'jF',
  labelIcon: '3dot',
};
