import { Meta, StoryFn } from '@storybook/react';

import { Notification } from '../lib/components/Notification';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Notification',
  component: Notification,
} as Meta<typeof Notification>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof Notification> = (args) => <Notification {...args} />;

export const _Notification = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args

_Notification.args = {
  iconName: 'download',
  iconVariant: 'solid',
  title: 'Download jF Design System successfully',
  description: 'Thank you',
};
