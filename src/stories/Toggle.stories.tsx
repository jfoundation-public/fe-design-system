import { Meta, StoryFn } from '@storybook/react';
import { useState } from 'react';

import { Toggle } from '../lib/components/Toggle';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Inputs/Toggle/Toggle',
  component: Toggle,
} as Meta<typeof Toggle>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof Toggle> = (args) => {
  const [checked, setChecked] = useState(false);
  return (
    <Toggle
      {...args}
      checked={checked || !!args.checked}
      onChange={(e) => setChecked(!e.target.checked)}
    />
  );
};

export const _Toggle = Template.bind({});
_Toggle.args = {
  disabled: false,
  readOnly: false,
  checked: false,
};
