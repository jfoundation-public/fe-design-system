import { Meta, StoryFn } from '@storybook/react';

import ModalHeader from '../lib/components/Modal/ModalHeader';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Modal/ModalHeader',
  component: ModalHeader,
} as Meta<typeof ModalHeader>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof ModalHeader> = (args) => <ModalHeader {...args} />;

export const _ModalHeader = Template.bind({});

_ModalHeader.args = {
  title: 'Modal header',
};
