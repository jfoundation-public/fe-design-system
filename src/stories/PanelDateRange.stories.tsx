import { Meta, StoryFn } from '@storybook/react';
import { useEffect, useState } from 'react';

import PanelDateRange from '../lib/components/DateTimePicker/PanelDateRange';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Pickers/_SubComponent_/PanelDateRange',
  component: PanelDateRange,
} as Meta<typeof PanelDateRange>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof PanelDateRange> = (args) => {
  const [fromDate, setFromDate] = useState<Date>();
  const [toDate, setToDate] = useState<Date>();
  const [selecting, setSelecting] = useState<'fromDate' | 'toDate'>('fromDate');

  useEffect(() => {
    if (fromDate) {
      setSelecting('toDate');
    }
  }, [fromDate]);

  useEffect(() => {
    if (toDate) {
      setSelecting('fromDate');
    }
  }, [toDate]);

  return (
    <PanelDateRange
      {...args}
      fromDateSelected={fromDate}
      toDateSelected={toDate}
      onFromDateSelected={setFromDate}
      onToDateSelected={setToDate}
      selecting={selecting}
    />
  );
};

export const _PanelDateRange = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args

_PanelDateRange.args = {
  viewDate: new Date(),
};
