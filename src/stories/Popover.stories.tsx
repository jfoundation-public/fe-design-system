import { Meta, StoryFn } from '@storybook/react';

import { Button, Popover, usePopover } from '@/lib';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Popover',
  component: Popover,
} as Meta<typeof Popover>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof Popover> = (args) => {
  const { open, onOpen, onClose, anchorEl } = usePopover();
  return (
    <>
      <Button onClick={onOpen}>Open Popover</Button>
      <Popover {...args} open={open} onClose={onClose} anchorEl={anchorEl}>
        <div className='p-4 bg-green-100'>Popover content</div>
      </Popover>
    </>
  );
};

export const _Popover = Template.bind({});

_Popover.args = {
  open: false,
};
