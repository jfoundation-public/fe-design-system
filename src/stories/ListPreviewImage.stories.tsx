import { Meta, StoryFn } from '@storybook/react';

import { PreviewImgType, Upload } from '@/lib';

import ListPreviewImage from '../lib/components/PreviewImage/ListPreviewImage';

const listImg: PreviewImgType[] = [
  {
    src: 'https://jfun.co/wp-content/uploads/2021/01/logo.png',
  },
  {
    src: 'https://jfun.co/wp-content/uploads/2021/01/logo.png',
  },
  {
    src: 'https://jfun.co/wp-content/uploads/2021/01/logo.png',
  },
  {
    src: 'https://jfun.co/wp-content/uploads/2021/01/logo.png',
  },
  {
    src: 'https://jfun.co/wp-content/uploads/2021/01/logo.png',
  },
  {
    src: 'https://jfun.co/wp-content/uploads/2021/01/logo.png',
  },
  {
    src: 'https://jfun.co/wp-content/uploads/2021/01/logo.png',
  },
  {
    src: 'https://jfun.co/wp-content/uploads/2021/01/logo.png',
  },
  {
    src: 'https://jfun.co/wp-content/uploads/2021/01/logo.png',
  },
  {
    src: 'https://jfun.co/wp-content/uploads/2021/01/logo.png',
  },
  {
    src: 'https://jfun.co/wp-content/uploads/2021/01/logo.png',
  },
  {
    src: 'https://jfun.co/wp-content/uploads/2021/01/logo.png',
  },
];

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/PreviewImage/ListPreviewImage',
  component: ListPreviewImage,
} as Meta<typeof ListPreviewImage>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof ListPreviewImage> = (args) => <ListPreviewImage {...args} />;

export const _ListPreviewImage = Template.bind({});

_ListPreviewImage.args = {
  listImg,
  maxListImg: 5,
  renderEndContent: <Upload layout='horizontal' name='uploadFile' multiple />,
};
