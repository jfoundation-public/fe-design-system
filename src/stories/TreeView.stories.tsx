import { Meta, StoryFn } from '@storybook/react';
import { useDeferredValue, useEffect, useState } from 'react';

import { IconButton, TreeItemCore, TreeViewExpandedState, TreeViewSelectedState } from '@/lib';

import TreeView from '../lib/components/TreeView/TreeView';

const options = [
  {
    id: '1',
    label: 'Furniture',
    items: [
      {
        id: '1.1',
        label: 'Tables & Chairs',
      },
      {
        id: '1.2',
        label: 'Sofas',
      },
      {
        id: '1.3',
        label: 'Occasional Furniture',
      },
    ],
  },
  {
    id: '2',
    label: 'Decor',
    items: [
      {
        id: '2.1',
        label: 'Bed Linen',
        items: [
          {
            id: '2.1.1',
            label: 'B',
            items: [
              {
                id: '2.1.1.1',
                label: 'Tables & Chairs',
              },
              {
                id: '2.1.1.2',
                label: 'Sofas',
              },
              {
                id: '2.1.1.3',
                label: 'Occasional Furniture',
              },
            ],
          },
        ],
      },
      {
        id: '2.2',
        label: 'Curtains & Blinds',
      },
      {
        id: '2.3',
        label: 'Carpets',
      },
    ],
  },
];

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/TreeView',
  component: TreeView,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
} as Meta<typeof TreeView>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof TreeView> = (args) => {
  const [expanded, setExpanded] = useState<TreeViewExpandedState>({});
  return <TreeView {...args} expanded={expanded} onExpand={setExpanded} />;
};

const TemplateWithCustomItem: StoryFn<typeof TreeView> = (args) => {
  const [expanded, setExpanded] = useState<TreeViewExpandedState>({});
  return <TreeView {...args} expanded={expanded} onExpand={setExpanded} />;
};

function RenderTreeItem({ label, treeId }: TreeItemCore<{ other?: string }>) {
  return (
    <div className='text-primary flex items-center'>
      {label}
      <IconButton
        className='ml-2'
        isAdornment
        iconName='edit'
        iconVariant='outline'
        onClick={() => {
          // eslint-disable-next-line no-console
          console.log('>>>>treeId', treeId);
        }}
      />
    </div>
  );
}

const TemplateWithCheckbox: StoryFn<typeof TreeView> = (args) => {
  const [expanded, setExpanded] = useState<TreeViewExpandedState>({});
  const [selected, setSelected] = useState<TreeViewSelectedState>({});
  return (
    <TreeView
      {...args}
      expanded={expanded}
      onExpand={setExpanded}
      selected={selected}
      onSelected={setSelected}
    />
  );
};

const TemplateWithRadio: StoryFn<typeof TreeView> = (args) => {
  const [expanded, setExpanded] = useState<TreeViewExpandedState>({});
  const [checked, setChecked] = useState({});
  return (
    <TreeView
      {...args}
      expanded={expanded}
      onExpand={setExpanded}
      checked={checked}
      onChecked={setChecked}
    />
  );
};

const TemplateWithSearch: StoryFn<typeof TreeView> = (args) => {
  const [treeData, setTreeData] = useState(options);
  const [expanded, setExpanded] = useState<TreeViewExpandedState>({});
  const [checked, setChecked] = useState({});
  const [search, setSearch] = useState('');

  const searchCached = useDeferredValue(search);

  useEffect(() => {
    setTreeData(
      options.filter((t) => t.label.toLocaleLowerCase().includes(searchCached.toLocaleLowerCase())),
    );
  }, [searchCached]);
  return (
    <TreeView
      {...args}
      data={treeData}
      expanded={expanded}
      onExpand={setExpanded}
      checked={checked}
      onChecked={setChecked}
      searchProps={{
        noOutline: true,
        name: 'tree-search',
        value: search,
        onChange: (e) => {
          setSearch(e.target.value);
        },
      }}
    />
  );
};

export const _TreeView = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
_TreeView.args = {
  data: options,
};

export const _TreeViewWithCustomTreeItem = TemplateWithCustomItem.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
_TreeViewWithCustomTreeItem.args = {
  data: options,
  renderTreeItem: RenderTreeItem,
};

export const _TreeViewWithCheckbox = TemplateWithCheckbox.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
_TreeViewWithCheckbox.args = {
  data: options,
};

export const _TreeViewWithRadio = TemplateWithRadio.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
_TreeViewWithRadio.args = {
  data: options,
};

export const _TreeViewWithSearch = TemplateWithSearch.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
_TreeViewWithSearch.args = {
  data: options,
};
