import { Meta, StoryFn } from '@storybook/react';

import { Radio } from '../lib/components/Radio';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Inputs/Radio/Radio',
  component: Radio,
} as Meta<typeof Radio>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof Radio> = (args) => <Radio {...args} />;

export const _Radio = Template.bind({});
_Radio.args = {
  tooltipProps: {
    content: "'Tooltip is here'",
    placement: 'bottom',
  },
};
