import { Meta, StoryFn } from '@storybook/react';
import { useState } from 'react';

import PanelMonth from '../lib/components/DateTimePicker/PanelMonth';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Pickers/_SubComponent_/PanelMonth',
  component: PanelMonth,
} as Meta<typeof PanelMonth>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof PanelMonth> = (args) => {
  const [selectedMonth, setSelectedMonth] = useState(new Date());
  return <PanelMonth {...args} monthSelected={selectedMonth} onSelected={setSelectedMonth} />;
};

export const _PanelMonth = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args

_PanelMonth.args = {
  locale: 'vi',
  viewDate: new Date(),
};
