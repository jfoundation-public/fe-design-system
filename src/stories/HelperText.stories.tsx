import { Meta, StoryFn } from '@storybook/react';

import { HelperText } from '../lib/components/HelperText';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Inputs/HelperText',
  component: HelperText,
} as Meta<typeof HelperText>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof HelperText> = (args) => <HelperText {...args} />;

export const _HelperText = Template.bind({});

_HelperText.args = {
  children: 'jF',
  state: 'active',
};
