import { Meta, StoryFn } from '@storybook/react';
import { useState } from 'react';

import { RenderMarkProps, Slider } from '../lib';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Slider',
  component: Slider,
} as Meta<typeof Slider>;

function RenderMark(props: RenderMarkProps) {
  const { isActive, onClick, value } = props;
  return (
    <div
      role='presentation'
      className={isActive ? 'text-text-primary' : 'text-text-error'}
      onClick={onClick}
    >
      {value}
    </div>
  );
}

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof Slider> = (args) => {
  const [rangeValue, setRangeValue] = useState(100);
  return <Slider {...args} range={false} value={rangeValue} onChange={(v) => setRangeValue(v)} />;
};

const TemplateRange: StoryFn<typeof Slider> = (args) => {
  const [rangeValue, setRangeValue] = useState<[number, number]>([100, 250]);
  return <Slider {...args} range={true} value={rangeValue} onChange={(v) => setRangeValue(v)} />;
};

export const _Slider = Template.bind({});
_Slider.args = {
  max: 260,
  step: 0.1,
};

const marks = {
  100: RenderMark,
  200: RenderMark,
};

export const _SliderWithRange = TemplateRange.bind({});
_SliderWithRange.args = {
  max: 260,
  step: 0.1,
};

export const _SliderWithMarks = TemplateRange.bind({});
_SliderWithMarks.args = {
  max: 260,
  step: 0.1,
  marks: marks,
};
