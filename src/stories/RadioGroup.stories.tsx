import { Meta, StoryFn } from '@storybook/react';

import { RadioGroup } from '@/lib';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Inputs/Radio/RadioGroup',
  component: RadioGroup,
} as Meta<typeof RadioGroup>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof RadioGroup> = (args) => <RadioGroup {...args} />;

export const _RadioGroup = Template.bind({});
_RadioGroup.args = {
  options: [
    { label: 'Male', value: 'male', disabled: true },
    {
      label: 'Female',
      value: 'female',
      tooltipProps: {
        content: 'Tooltip description',
        placement: 'left',
      },
    },
  ],
};
