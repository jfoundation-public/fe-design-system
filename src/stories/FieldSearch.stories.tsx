import { Meta, StoryFn } from '@storybook/react';

import { FieldSearch } from '../lib/components/FieldSearch';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Inputs/FieldSearch',
  component: FieldSearch,
} as Meta<typeof FieldSearch>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof FieldSearch> = (args) => <FieldSearch {...args} />;

export const _FieldSearch = Template.bind({});

_FieldSearch.args = {
  children: 'jF',
};
