import { Meta, StoryFn } from '@storybook/react';
import { useEffect, useState } from 'react';

import { Badge, noop, SelectSubMenuType, SidebarList } from '@/lib';

import Sidebar from '../lib/components/Sidebar/Sidebar';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Sidebar/Sidebar',
  component: Sidebar,
} as Meta<typeof Sidebar>;

const sidebars: SidebarList[] = [
  {
    id: 'cms',
    icon: 'cms',
    title: 'DESIGN SYSTEM 1',
    listItems: [
      {
        id: '1',
        name: 'Raider',
        url: '/raider',
      },
      {
        id: '2',
        name: 'Raider2 lorem ipsum dolor sit amet',
        url: '/raider',
        endAdornment: <Badge>999+</Badge>,
      },
    ],
  },
  {
    id: 'dashboard',
    icon: 'dashboard',
    title: 'DESIGN SYSTEM 2',
    tabMenu: [
      {
        id: 'dashboard new',
        tabTitle: 'dashboard new',
        tabSubMenu: [
          {
            id: '25',
            title: 'dashboard',
            listSubMenu: [
              { id: '26', name: 'Raider', url: '/raider' },
              { id: '27', name: 'Forbidden Rite', url: '/forbidden-rite' },
              {
                id: '28',
                name: 'Sub Menu 1',
                url: '/sub-menu-1',
              },
            ],
          },
          {
            id: '29',
            title: 'dashboard 2',
            listSubMenu: [
              { id: '30', name: 'Sub Menu 2', url: '/sub-menu-2' },
              { id: '31', name: 'Sub Menu 3', url: '/sub-menu-3' },
              { id: '32', name: 'Sub Menu 4', url: '/sub-menu-4' },
            ],
          },
          {
            id: '33',
            title: 'Menu 3',
            listSubMenu: [
              { id: '34', name: 'Sub Menu 5', url: '/sub-menu-5' },
              { id: '35', name: 'Sub Menu 6', url: '/sub-menu-6' },
              { id: '36', name: 'Sub Menu 7', url: '/sub-menu-7' },
              { id: '37', name: 'Sub Menu 5', url: '/sub-menu-5' },
              { id: '38', name: 'Sub Menu 6', url: '/sub-menu-6' },
              { id: '39', name: 'Sub Menu 7', url: '/sub-menu-7' },
            ],
          },
        ],
      },
      {
        id: 'language 2',
        tabTitle: 'language',
        listItem: [
          { id: '37', name: 'English', url: '/english' },
          { id: '38', name: 'Vietnamese', url: '/vietnam' },
          { id: '39', name: 'Chinese', url: '/chinese' },
        ],
      },
      {
        id: '40',
        tabTitle: 'option',
        //   listItem: ['English', 'Vietnamese', 'Indian'],
      },
    ],
  },
];

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof Sidebar> = (args) => {
  const [expanded, setExpanded] = useState(false);

  const [selectedSideBar, setSelectedSideBar] = useState<SidebarList>();

  const handleOnExpand = () => {
    setExpanded((prev) => !prev);
  };

  const handleOnClickSideBar = (item: SidebarList) => {
    setSelectedSideBar(item);
  };

  const handleLogoClick = noop;

  const handleClickMenu = (data: SelectSubMenuType) => {
    // eslint-disable-next-line no-console
    console.log(data, 'SelectSubMenuType');
  };

  useEffect(() => {
    if (selectedSideBar) {
      setExpanded(true);

      return;
    }
    return setExpanded(false);
  }, [setExpanded, selectedSideBar]);

  useEffect(() => {
    setSelectedSideBar(sidebars[0]);
  }, []);
  return (
    <Sidebar
      {...args}
      sidebars={sidebars}
      defaultIndexTab={0}
      selectedSideBar={selectedSideBar}
      expanded={expanded}
      onLogoClick={handleLogoClick}
      onExpand={handleOnExpand}
      onClickSideBar={handleOnClickSideBar}
      onClickMenu={handleClickMenu}
      logo={() => <img src='/favicon.svg' alt='Logo' />}
      defaultSidebarMenuItem={{
        id: '1',
        name: 'Raider',
        url: '/raider',
      }}
    />
  );
};

export const _Sidebar = Template.bind({});
_Sidebar.args = {};
