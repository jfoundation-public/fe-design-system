import { ComponentMeta, ComponentStory } from '@storybook/react';

import { IconButton } from '../lib/components/Button';
// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/IconButton',
  component: IconButton,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {
    disabled: { control: { type: 'boolean' } },
    size: { options: ['sm', 'md', 'lg'], control: { type: 'radio' } },
    variant: {
      options: ['primary', 'positive', 'negative', 'secondary', 'blank', 'link'],
      control: { type: 'radio' },
    },
  },
} as ComponentMeta<typeof IconButton>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof IconButton> = (args) => <IconButton {...args} />;

export const Primary = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
Primary.args = {
  variant: 'primary',
  size: 'md',
  children: 'Button',
  iconName: 'profile-user',
};
