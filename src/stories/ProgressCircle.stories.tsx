import { Meta, StoryFn } from '@storybook/react';

import ProgressCircle from '@/lib/components/Progress/ProgressCircle';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Progress/ProgressCircle',
  component: ProgressCircle,
} as Meta<typeof ProgressCircle>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof ProgressCircle> = (args) => {
  return <ProgressCircle {...args} />;
};

export const _ProgressCircle = Template.bind({});

_ProgressCircle.args = {
  percent: 70,
  hideInfo: false,
  isError: false,
};
