import { Meta, StoryFn } from '@storybook/react';

import CheckboxLabel from '../lib/components/Checkbox/CheckboxLabel';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Inputs/Checkbox/CheckboxLabel',
  component: CheckboxLabel,
} as Meta<typeof CheckboxLabel>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof CheckboxLabel> = (args) => <CheckboxLabel {...args} />;

export const _CheckboxLabel = Template.bind({});
_CheckboxLabel.args = {
  children: 'jF',
};
