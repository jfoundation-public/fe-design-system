import { Meta, StoryFn } from '@storybook/react';

import WeekHeader from '../lib/components/Schedules/WeekHeader';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Schedules/WeekHeader',
  component: WeekHeader,
} as Meta<typeof WeekHeader>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof WeekHeader> = (args) => <WeekHeader {...args} />;

export const _WeekHeader = Template.bind({});
_WeekHeader.args = {
  locale: 'vi',
  date: new Date(),
};
