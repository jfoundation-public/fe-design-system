import { Meta, StoryFn } from '@storybook/react';
import { useState } from 'react';

import { Accordion } from '../lib/components/Accordion';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Accordion/Accordion',
  component: Accordion,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
} as Meta<typeof Accordion>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const AccordionSingleTemplate: StoryFn<typeof Accordion> = (args) => {
  const [expand, setExpand] = useState<Record<string, boolean>>({});

  const handleExpand = (id: string) => {
    setExpand((prev) => ({
      [id]: !prev[id],
    }));
  };

  return (
    <Accordion {...args}>
      <Accordion.Item
        title='Today - Sep 09, 2022'
        onExpand={handleExpand}
        accordionId='1'
        expand={expand['1']}
        hasBorderHeader
      >
        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Unde praesentium provident minus
        optio distinctio asperiores quisquam modi voluptas quis tenetur iste ad, facilis numquam
        sequi pariatur dicta inventore odit placeat.
      </Accordion.Item>
      <Accordion.Item
        title='Today - Sep 10, 2022'
        onExpand={handleExpand}
        accordionId='2'
        expand={expand['2']}
        hasBorderHeader
      >
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto adipisci repellendus est
        impedit doloribus aspernatur rem obcaecati? Aliquid ab sequi modi, quod doloribus adipisci
        quisquam, atque aliquam, minima quaerat veritatis.
      </Accordion.Item>
      <Accordion.Item
        title='Today - Sep 11, 2022'
        onExpand={handleExpand}
        accordionId='3'
        expand={expand['3']}
        hasBorderHeader
      >
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Exercitationem magni delectus
        laudantium? Repellat at repudiandae architecto! Suscipit, earum? Nostrum alias officiis
        facilis temporibus beatae asperiores magnam eos sunt nulla illo?
      </Accordion.Item>
      <Accordion.Item
        title='Today - Sep 12, 2022'
        onExpand={handleExpand}
        accordionId='4'
        expand={expand['4']}
        hasBorderHeader
      >
        Lorem ipsum dolor sit amet consectetur, adipisicing elit. At est, sed cupiditate omnis atque
        libero eligendi architecto possimus earum natus vero fugiat unde hic saepe corrupti, dolorem
        enim molestiae blanditiis.
      </Accordion.Item>
    </Accordion>
  );
};

export const _Accordion = AccordionSingleTemplate.bind({});
