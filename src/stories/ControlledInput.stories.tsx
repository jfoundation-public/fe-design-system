import { Meta, StoryFn } from '@storybook/react';

import FieldInputControlledProvider from '@/lib/components/FieldInput/Provider';

import { ControlledInput } from '../lib/components/ControlledInput';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Inputs/ControlledInput',
  component: ControlledInput,
  argTypes: {
    type: {
      options: ['password', 'text', 'search'],
      control: { type: 'radio' },
    },
    format: {
      type: 'function',
    },
    parse: {
      type: 'function',
    },
  },
} as Meta<typeof ControlledInput>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof ControlledInput> = (args) => {
  return (
    <FieldInputControlledProvider>
      <ControlledInput {...args} />
    </FieldInputControlledProvider>
  );
};

export const _ControlledInput = Template.bind({});
_ControlledInput.args = {
  disabled: false,
  required: true,
};
