import { Meta, StoryFn } from '@storybook/react';

import { Progress } from '@/lib';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Progress/Progress',
  component: Progress,
} as Meta<typeof Progress>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof Progress> = (args) => {
  return <Progress {...args} />;
};

export const _Progress = Template.bind({});

_Progress.args = {
  percent: 70,
  hideInfo: false,
  isError: false,
};
