import { Meta, StoryFn } from '@storybook/react';
import clsx from 'clsx';
import { useState } from 'react';

import { Checkbox, IconButton, Typography } from '@/lib';
import { useDataGrid, usePagination } from '@/lib/components/DataGrid/hooks';

import { ColumnDef, DataGrid, DataGridProps } from '../lib/components/DataGrid';

type Data = {
  id: number;
  lastName: string;
  firstName: string | null;
  age: number | null;
  subRows?: Data[];
};

const rawData: Data[] = [
  {
    id: 1,
    lastName: 'Snow',
    firstName: 'Jon',
    age: 35,
    subRows: [
      { id: 2, lastName: 'Snow', firstName: 'Jon', age: 35 },
      {
        id: 3,
        lastName: 'Lannister',
        firstName: 'Cersei',
        age: 42,
        subRows: [
          { id: 4, lastName: 'Lannister', firstName: 'Cersei', age: 42 },
          {
            id: 5,
            lastName: 'Lannister',
            firstName: 'Jaime',
            age: 45,
            subRows: [
              { id: 6, lastName: 'Lannister', firstName: 'Cersei', age: 42 },
              { id: 7, lastName: 'Lannister', firstName: 'Jaime', age: 45 },
              { id: 8, lastName: 'Stark', firstName: 'Arya', age: 16 },
            ],
          },
          { id: 9, lastName: 'Stark', firstName: 'Arya', age: 16 },
        ],
      },
      { id: 10, lastName: 'Lannister', firstName: 'Jaime', age: 45 },
      { id: 11, lastName: 'Stark', firstName: 'Arya', age: 16 },
    ],
  },
  { id: 2, lastName: 'Lannister', firstName: 'Cersei', age: 42 },
  { id: 3, lastName: 'Lannister', firstName: 'Jaime', age: 45 },
  { id: 4, lastName: 'Stark', firstName: 'Arya', age: 16 },
  { id: 5, lastName: 'Targaryen', firstName: 'Daenerys', age: null },
  { id: 6, lastName: 'Melisandre', firstName: null, age: 150 },
  { id: 7, lastName: 'Clifford', firstName: 'Ferrara', age: 44 },
  { id: 8, lastName: 'Frances', firstName: 'Rossini', age: 36 },
  { id: 9, lastName: 'Roxie', firstName: 'Harvey', age: 65 },
];

const HEADER_NAMES = {
  select: 'Select',
  firstName: 'First name',
  id: 'ID',
  lastName: 'Last Name',
  age: 'Age',
  fullName: 'Full name',
  action: 'Action',
};

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/DataGrid',
  component: DataGrid,
} as Meta<typeof DataGrid>;

const columns: ColumnDef<Data>[] = [
  {
    id: 'select',
    header: ({ table }) => (
      <Checkbox
        className='flex items-center'
        checked={table.getIsAllRowsSelected()}
        indeterminate={table.getIsSomeRowsSelected()}
        onChange={table.getToggleAllRowsSelectedHandler()}
      />
    ),
    cell: ({ row }) => (
      <Checkbox
        className='flex items-center'
        checked={row.getIsSelected()}
        indeterminate={row.getIsSomeSelected()}
        onChange={() => row.toggleSelected()}
      />
    ),
    meta: {
      headerClassName: 'w-[56px]',
    },
  },
  {
    id: 'id',
    accessorKey: 'id',
    header: HEADER_NAMES['id'],
  },
  {
    accessorKey: 'firstName',
    header: HEADER_NAMES['firstName'],
    cell: ({ row, getValue }) => {
      return (
        <div className='relative flex items-center'>
          {row.depth >= 1 && (
            <>
              <p
                className={clsx(
                  'absolute left-3 h-1 w-1',
                  'before:absolute before:bottom-[8px] before:left-0  before:w-[1px] before:bg-[#9A9DB2]',
                  row.index === 0 && row.depth === 1 ? 'before:h-[1220%]' : 'before:h-[1607%]',
                )}
              />
              <div
                className={clsx(
                  'relative h-1',
                  row.depth >= 2 &&
                    'before:absolute before:-left-[52px] before:bottom-[2px] before:h-[52px] before:border-l before:border-[#9A9DB2]',
                )}
                style={{
                  marginLeft: `${row.depth * 32}px`,
                }}
              />
            </>
          )}
          {row.getCanExpand() && (
            <IconButton
              iconName={row.getIsExpanded() ? 'minus-square' : 'add-square'}
              iconVariant='outline'
              isAdornment
              iconSize={24}
              className={clsx(
                'relative mr-2 z-10 bg-white rounded-lg',
                row.depth >= 1 &&
                  'before:absolute before:-left-5 before:-top-[43px] before:h-[57px] before:w-4 before:rounded-bl-md before:border-b before:border-l before:border-[#9A9DB2]',
              )}
              onClick={row.getToggleExpandedHandler()}
            />
          )}
          <Typography
            variant='body'
            className={clsx(
              !row.getCanExpand() &&
                row.depth >= 1 &&
                'relative before:absolute before:-left-5 before:w-4 before:rounded-bl-md before:border-b before:border-l before:border-[#9A9DB2]',
              row.index === 0
                ? 'before:-top-[35px] before:h-[200%]'
                : 'before:-top-[60px] before:h-[310%]',
            )}
          >
            {getValue<string>()}
          </Typography>
        </div>
      );
    },
  },
  {
    id: 'lastName',
    accessorKey: 'lastName',
    header: HEADER_NAMES['lastName'],
    // header: ({ column }) => {
    //   column.columnDef.header = HEADER_NAMES['lastName'];
    //   // header.getContext().setColumnVisibility('lastName', false);
    // },
    cell: ({ row }) => {
      return <>{row.original.lastName}</>;
    },
  },
  {
    accessorKey: 'age',
    header: HEADER_NAMES['age'],
    meta: { align: 'center' },
  },
  {
    id: 'fullName',
    accessorFn: (row) => `${row.firstName} ${row.lastName}`,
    header: HEADER_NAMES['fullName'],
  },
  {
    id: 'action',
    header: () => <>Action</>,
    cell: () => <></>,
    meta: { align: 'right', headerClassName: '' },
  },
];

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<DataGridProps<Data>> = (args: DataGridProps<Data>) => {
  const {
    columnOrder,
    setColumnOrder,
    setColumnVisibility,
    columnVisibility,
    sorting,
    expanded,
    rowSelection,
    setExpanded,
    setRowSelection,
    setSorting,
    setTable,
  } = useDataGrid<Data>();

  const [data, setData] = useState(rawData);

  const { onPageChange, onPageSizeChange, page, pageSize, total } = usePagination();

  return (
    <div className='p-8'>
      <DataGrid<Data>
        {...args}
        data={data}
        rowSelection={rowSelection}
        expanded={expanded}
        onRowSelectionChange={setRowSelection}
        sorting={sorting}
        onSortingChange={setSorting}
        onExpandedChange={setExpanded}
        columnVisibility={columnVisibility}
        onColumnVisibilityChange={setColumnVisibility}
        columnOrder={columnOrder}
        onColumnOrderChange={setColumnOrder}
        pagination={{
          total: total,
          page,
          pageSize,
          onPageChange,
          onPageSizeChange,
        }}
        onTableStateChange={setTable as any}
        onDataStateChange={(data) => setData(data)}
      />
    </div>
  );
};

export const _DataGrid = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
_DataGrid.args = {
  data: rawData,
  columns: columns,
};

export const _DataGridWithSubRows = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
_DataGridWithSubRows.args = {
  data: rawData,
  columns: columns,
  getSubRows: (row) => row.subRows,
};

export const _DataGridDraggable = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
_DataGridDraggable.args = {
  data: rawData,
  columns: columns,
  isDraggable: true,
};

export const _DataGridDraggableWithSubRows = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
_DataGridDraggable.args = {
  data: rawData,
  columns: columns,
  isDraggable: true,
  getSubRows: (row) => row.subRows,
};
