import { Meta, StoryFn } from '@storybook/react';

import { AvatarType, GroupAvatar } from '@/lib';

const avatars: AvatarType[] = [
  {
    id: '1',
    type: 'img',
    size: 40,
    name: 'Yuna',
    src: 'https://i.ytimg.com/vi/G7F1eEKhvrs/maxresdefault.jpg',
  },
  {
    id: '2',
    type: 'img',
    size: 40,
    name: 'Yuna',
    src: 'https://i.ytimg.com/vi/G7F1eEKhvrs/maxresdefault.jpg',
  },
  {
    id: '3',
    type: 'img',
    size: 40,
    name: 'Yuna',
    src: 'https://i.ytimg.com/vi/G7F1eEKhvrs/maxresdefault.jpg',
  },
  { id: '4', type: 'text', size: 40, name: 'Terra Branford' },
  { id: '5', type: 'text', size: 40, name: 'Rikku' },
  { id: '6', type: 'text', size: 40, name: 'Celes Chere' },
  { id: '7', type: 'text', size: 40, name: 'Aerith Gainsborough' },
  { id: '8', type: 'text', size: 40, name: 'Aerith Gainsborough' },
  { id: '9', type: 'text', size: 40, name: 'Aerith Gainsborough' },
  {
    id: '10',
    type: 'img',
    size: 40,
    name: 'Yuna',
    src: 'https://i.ytimg.com/vi/G7F1eEKhvrs/maxresdefault.jpg',
  },
  {
    id: '11',
    type: 'img',
    size: 40,
    name: 'Yuna',
    src: 'https://i.ytimg.com/vi/G7F1eEKhvrs/maxresdefault.jpg',
  },
  {
    id: '12',
    type: 'img',
    size: 40,
    name: 'Yuna',
    src: 'https://i.ytimg.com/vi/G7F1eEKhvrs/maxresdefault.jpg',
  },
];

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Avatar/GroupAvatar',
  component: GroupAvatar,
} as Meta<typeof GroupAvatar>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof GroupAvatar> = (args) => <GroupAvatar {...args} />;

export const _GroupAvatar = Template.bind({});
_GroupAvatar.args = {
  avatars: avatars,
};
