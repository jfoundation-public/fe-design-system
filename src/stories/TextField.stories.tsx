import { Meta, StoryFn } from '@storybook/react';

import { TextField } from '../lib/components/TextField';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Inputs/TextField',
  component: TextField,
} as Meta<typeof TextField>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof TextField> = (args) => <TextField {...args} />;

export const _TextField = Template.bind({});

_TextField.args = {
  label: 'jF',
};
