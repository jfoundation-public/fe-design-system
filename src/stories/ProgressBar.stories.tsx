import { Meta, StoryFn } from '@storybook/react';

import ProgressBar from '@/lib/components/Progress/ProgressBar';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Progress/ProgressBar',
  component: ProgressBar,
} as Meta<typeof ProgressBar>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof ProgressBar> = (args) => {
  return <ProgressBar {...args} />;
};

export const _ProgressBar = Template.bind({});

_ProgressBar.args = {
  percent: 70,
  hideInfo: false,
  isError: false,
};
