import { Meta, StoryFn } from '@storybook/react';

import { TextArea } from '../lib';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Inputs/TextArea',
  component: TextArea,
} as Meta<typeof TextArea>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof TextArea> = (args) => <TextArea {...args} />;

export const _TextArea = Template.bind({});
_TextArea.args = {
  label: 'Text area',
};
