import { Meta, StoryFn } from '@storybook/react';
import { useState } from 'react';

import { ViewNav, ViewOption } from '@/lib';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Schedules/ ViewNav',
  component: ViewNav,
} as Meta<typeof ViewNav>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof ViewNav> = (args) => {
  const [selectedView, setSelectedView] = useState<ViewOption>('week');
  return <ViewNav {...args} view={selectedView} onChange={setSelectedView} />;
};

export const _ViewNav = Template.bind({});

_ViewNav.args = {};
