import { Meta, StoryFn } from '@storybook/react';

import { Icon } from '../lib/components/Icon';
// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Icon',
  component: Icon,
} as Meta<typeof Icon>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof Icon> = (args) => <Icon {...args} />;

export const _Icon = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
_Icon.args = {
  iconName: 'profile-user',
};
