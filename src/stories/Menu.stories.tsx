import { Meta, StoryFn } from '@storybook/react';

import Menu from '../lib/components/Menu/Menu';

const menuData = {
  id: '1',
  listSubMenu: [
    { id: '1234', name: 'Menu sub item 1', url: '/asdf' },
    { id: '12345', name: 'Menu sub item 2', url: '/asdfg' },
  ],
  title: 'Menu item 1',
};

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Menu',
  component: Menu,
} as Meta<typeof Menu>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof Menu> = (args) => <Menu {...args} />;

export const _Menu = Template.bind({});

_Menu.args = {
  data: menuData,
  selectedMenu: { id: '1234', name: 'Menu sub item 1', url: '/asdf' },
};
