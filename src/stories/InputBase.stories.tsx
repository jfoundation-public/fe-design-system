import { Meta, StoryFn } from '@storybook/react';

import { InputBase } from '../lib/components/InputBase';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Inputs/InputBase',
  component: InputBase,
  argTypes: {
    endAdornment: { description: 'ReactElement' },
    parse: { type: 'function', description: 'onChange parser' },
    format: { type: 'function', description: 'value formater' },
  },
} as Meta<typeof InputBase>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof InputBase> = (args) => <InputBase {...args} />;

export const _InputBase = Template.bind({});

_InputBase.args = {};
