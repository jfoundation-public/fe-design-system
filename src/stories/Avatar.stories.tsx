import { Meta, StoryFn } from '@storybook/react';

import { Avatar } from '../lib/components/Avatar';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Avatar/Avatar',
  component: Avatar,
} as Meta<typeof Avatar>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof Avatar> = (args) => <Avatar {...args} />;

export const _IconAvatar = Template.bind({});
_IconAvatar.args = {
  type: 'icon',
  icon: 'calendar',
  name: 'Tien Bui',
  subName: 'Software Engineer',
};

export const _ImageAvatar = Template.bind({});
_ImageAvatar.args = {
  type: 'img',
  size: 40,
  name: 'Yuna',
  src: 'https://i.ytimg.com/vi/G7F1eEKhvrs/maxresdefault.jpg',
};

export const _TextAvatar = Template.bind({});
_TextAvatar.args = {
  type: 'text',
  size: 40,
  name: 'Terra Branford',
};
