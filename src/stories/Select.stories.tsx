import { Meta, StoryFn } from '@storybook/react';
import clsx from 'clsx';
import { useState } from 'react';

import { Select } from '../lib/components/Select';

const options = [
  { label: 'Male', value: 'male' },
  { label: 'Female', value: 'female' },
  { label: 'Other', value: 'other' },
  { label: 'Male2', value: 'male2' },
  { label: 'Female2', value: 'female2' },
  { label: 'Other2', value: 'other2' },
];

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Inputs/Select/Select',
  component: Select,
} as Meta<typeof Select>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof Select> = (args) => {
  const [selected, setSelected] = useState<string>('');
  return <Select {...args} value={selected} onChange={(e) => setSelected(e.target.value)} />;
};

export const _Select = Template.bind({});
_Select.args = {
  options,
};

export const _SelectMultiple = Template.bind({});
_SelectMultiple.args = {
  options,
  isMultiple: true,
  selectedTagsProps: {
    maxTagsDisplayLength: 5,
  },
};

export const _SelectWithCustomOption = Template.bind({});
_SelectWithCustomOption.args = {
  options,
  renderOption: ({ option, onSelected, isSelected }) => (
    <li
      role='presentation'
      onClick={() => onSelected(option.value)}
      className={clsx(
        'flex flex-col m-2 p-2 hover:bg-surface-primary4 rounded-md',
        isSelected && 'text-text-error',
      )}
    >
      <div className='subtitle1'>{option.label}</div>
    </li>
  ),
};
