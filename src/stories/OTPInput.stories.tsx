import { Meta, StoryFn } from '@storybook/react';

import OTPInput from '../lib/components/OTPInput/OTPInput';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/OTPInput/OTPInput',
  component: OTPInput,
} as Meta<typeof OTPInput>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof OTPInput> = (args) => <OTPInput {...args} />;

export const _OTPInput = Template.bind({});

_OTPInput.args = {};
