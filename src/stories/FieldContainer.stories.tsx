import { Meta, StoryFn } from '@storybook/react';

import { FieldContainer } from '../lib/components/FieldContainer';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Inputs/FieldContainer/FieldContainer',
  component: FieldContainer,
} as Meta<typeof FieldContainer>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof FieldContainer> = (args) => <FieldContainer {...args} />;

export const _FieldContainer = Template.bind({});

_FieldContainer.args = {
  label: 'jF',
};
