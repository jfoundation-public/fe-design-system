import { Meta, StoryFn } from '@storybook/react';

import ModalFooter from '../lib/components/Modal/ModalFooter';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Modal/ModalFooter',
  component: ModalFooter,
} as Meta<typeof ModalFooter>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof ModalFooter> = (args) => <ModalFooter {...args} />;

export const _ModalFooter = Template.bind({});

_ModalFooter.args = {
  children: 'Hello jF',
};
