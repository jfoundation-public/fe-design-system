import { Meta, StoryFn } from '@storybook/react';
import { noop } from '@tanstack/react-table';

import { DataTransferCheckBox, Transfer, useTransferCheckBox } from '../lib/components/Transfer';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Transfer',
  component: Transfer,
} as Meta<typeof Transfer>;

const leftSourceData: DataTransferCheckBox[] = [
  { checked: false, id: '112', text: 'example8' },
  { checked: false, id: '113', text: 'example9' },
  { checked: false, id: '114', text: 'example10' },
  { checked: false, id: '115', text: 'example11' },
  { checked: false, id: '116', text: 'example12' },
  { checked: false, id: '117', text: 'example13' },
  { checked: false, id: '118', text: 'example14' },
];
const rightSourceData: DataTransferCheckBox[] = [
  { checked: false, id: '100', text: 'example_1' },
  { checked: false, id: '101', text: 'example_2' },
  { checked: false, id: '102', text: 'example_3' },
  { checked: false, id: '103', text: 'example_4' },
  { checked: false, id: '104', text: 'example_5' },
  { checked: false, id: '105', text: 'example_6' },
  { checked: false, id: '106', text: 'example-7' },
  { checked: false, id: '107', text: 'example-8' },
  { checked: false, id: '108', text: 'example-9' },
  { checked: false, id: '109', text: 'example-10' },
  { checked: false, id: '110', text: 'example-11' },
  { checked: false, id: '111', text: 'example-12' },
];

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof Transfer> = () => {
  const [checkBox, handleChangeSearch, handleCheckAll, handleTransfer] = useTransferCheckBox({
    dataL: leftSourceData,
    dataR: rightSourceData,
    dataCheckBoxTitleL: 'Example Left',
    dataCheckBoxTitleR: 'Example Right',
    paginationL: {
      page: 1,
      pageSize: 10,
      total: 10,
      onPageChange: noop,
      onPageSizeChange: noop,
    },
    paginationR: {
      page: 1,
      pageSize: 10,
      total: 10,
      onPageChange: noop,
      onPageSizeChange: noop,
    },
  });

  return (
    <Transfer
      // {...args}
      type='checkbox'
      checkBox={checkBox}
      onSearch={handleChangeSearch}
      onCheckAll={handleCheckAll}
      onTransfer={handleTransfer}
    />
  );
};

export const _TransferCheckbox = Template.bind({});
_TransferCheckbox.args = { type: 'checkbox' };
