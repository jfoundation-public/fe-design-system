import { Placement } from '@floating-ui/react-dom-interactions';
import { Meta, StoryFn } from '@storybook/react';

import DatePicker from '../lib/components/DateTimePicker/DatePicker';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Pickers/DatePicker',
  component: DatePicker,
  argTypes: {
    pickerPlacement: {
      options: [
        'bottom',
        'bottom-end',
        'bottom-start',
        'left',
        'left-end',
        'left-start',
        'right',
        'right-end',
        'right-start',
        'top',
        'top-end',
        'top-start',
      ] as Placement[],
      control: {
        type: 'radio',
      },
    },
  },
} as Meta<typeof DatePicker>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof DatePicker> = (args) => <DatePicker {...args} />;

export const _DatePicker = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args

_DatePicker.args = {
  label: 'jF',
  required: true,
  readOnly: false,
  disabled: false,
  disableFuture: false,
  disablePast: false,
  pickerPlacement: 'bottom',
  isOnChangeFocusInput: false,
  isError: false,
  isSuccess: false,
  isWarning: false,
  dateformat: 'yyyy-MM-dd',
};
