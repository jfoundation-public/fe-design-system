import { Meta, StoryFn } from '@storybook/react';

import { Chips } from '../lib/components/Chips';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Chips',
  component: Chips,
} as Meta<typeof Chips>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof Chips> = (args) => <Chips {...args} />;

export const _Chips = Template.bind({});
_Chips.args = {
  tagName: 'jF',
  selected: true,
  disabled: false,
  hideSelectedIndicator: false,
  onRemove: undefined,
};

export const _ChipsWithRemoveHandler = Template.bind({});
_ChipsWithRemoveHandler.args = {
  tagName: 'jF',
  selected: true,
  disabled: false,
  hideSelectedIndicator: false,
};
