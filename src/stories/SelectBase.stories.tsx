import { Meta, StoryFn } from '@storybook/react';
import { useState } from 'react';

import { SelectBase } from '../lib/components/SelectBase';

const options = [
  { label: 'Male', value: 'male' },
  { label: 'Female', value: 'female' },
  { label: 'Other', value: 'other' },
  { label: 'Male2', value: 'male2' },
  { label: 'Female2', value: 'female2' },
  { label: 'Other2', value: 'other2' },
];

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Inputs/Select/SelectBase',
  component: SelectBase,
} as Meta<typeof SelectBase>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof SelectBase> = (args) => {
  const [selected, setSelected] = useState<string>('');
  return <SelectBase {...args} value={selected} onChange={(e) => setSelected(e.target.value)} />;
};

export const _SelectBase = Template.bind({});
_SelectBase.args = {
  options,
};
