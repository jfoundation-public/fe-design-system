import { Meta, StoryFn } from '@storybook/react';
import clsx from 'clsx';
import { useMemo } from 'react';

import {
  Checkbox,
  ColumnDef,
  ColumnDisplayedProps,
  DataGrid,
  IconButton,
  Popover,
  SelectItem,
  Typography,
  usePopover,
} from '@/lib';
import { useDataGrid } from '@/lib/components/DataGrid/hooks';

import ColumnDisplayed from '../lib/components/DataGrid/ColumnDisplayed';

const HEADER_NAMES = {
  select: 'Select',
  firstName: 'First name',
  id: 'ID',
  lastName: 'Last Name',
  age: 'Age',
  fullName: 'Full name',
  action: 'Action',
};

type Data = {
  id: number;
  lastName: string;
  firstName: string | null;
  age: number | null;
  subRows?: Data[];
};

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/ColumnDisplayed',
  component: ColumnDisplayed,
} as Meta<
  Pick<
    ColumnDisplayedProps<any>,
    'columnNames' | 'className' | 'label' | 'hideColumnsId' | 'startIcon'
  >
>;

function MenuAction({
  onSelect,
  rowId,
}: {
  rowId: number;
  onSelect: (rowId: number) => (selected: string) => void;
}) {
  const { onOpen, ...popoverProps } = usePopover();
  const handleSelect = (v: string) => {
    onSelect(rowId)(v);
    popoverProps.onClose();
  };
  return (
    <>
      <IconButton iconName='3dot' isAdornment iconVariant='outline' onClick={onOpen} />
      <Popover {...popoverProps}>
        <div className='paper'>
          {['View', 'Edit', 'Delete'].map((item) => (
            <SelectItem value={item} onSelect={handleSelect} key={item}>
              {item}
            </SelectItem>
          ))}
        </div>
      </Popover>
    </>
  );
}

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof ColumnDisplayed> = (args) => {
  const {
    table,
    setTable,
    columnVisibility,
    setColumnOrder,
    setColumnVisibility,
    columnOrder,
    rowSelection,
    setRowSelection,
    expanded,
    setExpanded,
  } = useDataGrid();

  const handleAction = (row: number) => (action: string) => {
    // eslint-disable-next-line no-console
    console.log('>>>>>>', row, action);
  };

  const columns = useMemo<ColumnDef<Data>[]>(() => {
    const baseColumns: ColumnDef<Data>[] = [
      {
        id: 'select',
        header: ({ table }) => (
          <Checkbox
            className='flex items-center'
            checked={table.getIsAllRowsSelected()}
            indeterminate={table.getIsSomeRowsSelected()}
            onChange={table.getToggleAllRowsSelectedHandler()}
          />
        ),
        cell: ({ row }) => (
          <Checkbox
            className='flex items-center'
            checked={row.getIsSelected()}
            indeterminate={row.getIsSomeSelected()}
            onChange={() => row.toggleSelected()}
          />
        ),
        meta: {
          headerClassName: 'w-[56px]',
        },
      },
      {
        id: 'id',
        accessorKey: 'id',
        header: HEADER_NAMES['id'],
      },
      {
        accessorKey: 'firstName',
        header: HEADER_NAMES['firstName'],
        cell: ({ row, getValue }) => {
          return (
            <div className='relative flex items-center'>
              {row.depth >= 1 && (
                <>
                  <p
                    className={clsx(
                      'absolute left-3 h-1 w-1',
                      'before:absolute before:bottom-[8px] before:left-0  before:w-[1px] before:bg-[#9A9DB2]',
                      row.index === 0 && row.depth === 1 ? 'before:h-[1220%]' : 'before:h-[1607%]',
                    )}
                  />
                  <div
                    className={clsx(
                      'relative h-1',
                      row.depth >= 2 &&
                        'before:absolute before:-left-[52px] before:bottom-[2px] before:h-[52px] before:border-l before:border-[#9A9DB2]',
                    )}
                    style={{
                      marginLeft: `${row.depth * 32}px`,
                    }}
                  />
                </>
              )}
              {row.getCanExpand() && (
                <IconButton
                  iconName={row.getIsExpanded() ? 'minus-square' : 'add-square'}
                  iconVariant='outline'
                  isAdornment
                  iconSize={24}
                  className={clsx(
                    'relative mr-2 z-10 bg-white rounded-lg',
                    row.depth >= 1 &&
                      'before:absolute before:-left-5 before:-top-[43px] before:h-[57px] before:w-4 before:rounded-bl-md before:border-b before:border-l before:border-[#9A9DB2]',
                  )}
                  onClick={row.getToggleExpandedHandler()}
                />
              )}
              <Typography
                variant='body'
                className={clsx(
                  !row.getCanExpand() &&
                    row.depth >= 1 &&
                    'relative before:absolute before:-left-5 before:w-4 before:rounded-bl-md before:border-b before:border-l before:border-[#9A9DB2]',
                  row.index === 0
                    ? 'before:-top-[35px] before:h-[200%]'
                    : 'before:-top-[60px] before:h-[310%]',
                )}
              >
                {getValue<string>()}
              </Typography>
            </div>
          );
        },
      },
      {
        id: 'lastName',
        accessorKey: 'lastName',
        header: HEADER_NAMES['lastName'],
        // header: ({ column }) => {
        //   column.columnDef.header = HEADER_NAMES['lastName'];
        //   // header.getContext().setColumnVisibility('lastName', false);
        // },
        cell: ({ row }) => {
          return <>{row.original.lastName}</>;
        },
      },
      {
        accessorKey: 'age',
        header: HEADER_NAMES['age'],
        meta: { align: 'center' },
      },
      {
        id: 'fullName',
        accessorFn: (row) => `${row.firstName} ${row.lastName}`,
        header: HEADER_NAMES['fullName'],
      },
      {
        id: 'action',
        header: () => <>Action</>,
        cell: ({ row }) => <MenuAction rowId={row.original.id} onSelect={handleAction} />,
        meta: { align: 'right', headerClassName: '' },
      },
    ];

    return baseColumns;
  }, []);

  return (
    <div className='p-8'>
      <div className='flex items-center'>
        <ColumnDisplayed
          {...args}
          columnNames={HEADER_NAMES}
          table={table}
          columnVisibility={columnVisibility}
          onColumnVisibilityChange={setColumnVisibility}
          columnOrder={columnOrder}
          onColumnOrderChange={setColumnOrder}
          hideColumnsId={['action', 'select']}
        />
      </div>
      <DataGrid
        data={[]}
        columns={columns}
        rowSelection={rowSelection}
        expanded={expanded}
        onRowSelectionChange={setRowSelection}
        onExpandedChange={setExpanded}
        columnVisibility={columnVisibility}
        onColumnVisibilityChange={setColumnVisibility}
        columnOrder={columnOrder}
        onColumnOrderChange={setColumnOrder}
        onTableStateChange={setTable}
      />
    </div>
  );
};

export const _ColumnDisplayed = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args

_ColumnDisplayed.args = {
  columnNames: HEADER_NAMES,
};
