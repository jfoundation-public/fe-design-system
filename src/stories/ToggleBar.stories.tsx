import { Meta, StoryFn } from '@storybook/react';
import { useState } from 'react';

import { ViewOption } from '@/lib';

import { ToggleBar } from '../lib/components/ToggleBar';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Inputs/ToggleBar',
  component: ToggleBar,
} as Meta<typeof ToggleBar>;

const VIEW_OPTIONS: { value: ViewOption; label: string }[] = [
  { label: 'Day', value: 'day' },
  { label: 'Week', value: 'week' },
  { label: 'Month', value: 'month' },
  { label: 'Year', value: 'year' },
]; // @TO_DO: translation

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof ToggleBar> = (args) => {
  const [viewOption, setViewOption] = useState<ViewOption>('day');
  return <ToggleBar {...args} options={VIEW_OPTIONS} value={viewOption} onChange={setViewOption} />;
};

export const _ToggleBar = Template.bind({});
_ToggleBar.args = {};
