import { Meta, StoryFn } from '@storybook/react';
import { useState } from 'react';

import PanelTime from '../lib/components/DateTimePicker/PanelTime';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Pickers/_SubComponent_/PanelTime',
  component: PanelTime,
} as Meta<typeof PanelTime>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof PanelTime> = (args) => {
  const [time, setTime] = useState<string | null>();
  return <PanelTime {...args} onChange={setTime} value={time} />;
};

export const _PanelTime = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args

_PanelTime.args = {};
