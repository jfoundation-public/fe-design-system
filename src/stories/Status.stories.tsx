import { Meta, StoryFn } from '@storybook/react';

import { Status } from '../lib';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Status',
  component: Status,
} as Meta<typeof Status>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof Status> = (args) => <Status {...args} />;

export const _Status = Template.bind({});
_Status.args = {
  variant: 'success',
  size: 'md',
  children: 'Success',
  stroke: false,
  iconName: 'download',
  iconType: 'solid',
};
