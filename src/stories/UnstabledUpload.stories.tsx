import { Meta, StoryFn } from '@storybook/react';
import { useRef } from 'react';

import { Icon, PreviewImage, UploadProgress, useUpload, uuid } from '@/lib';

import Upload from '../lib/components/UnstableUpload/Upload';
// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Upload',
  component: Upload,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {
    children: { control: { type: 'text' } },
  },
} as Meta<typeof Upload>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof Upload> = (args) => {
  const signalRef = useRef(new AbortController());
  const handleUpload = (setProgress: React.Dispatch<React.SetStateAction<UploadProgress>>) => {
    return async (file: File | undefined) => {
      if (file) {
        const fileId = file.name;
        try {
          setProgress((prv) => ({ ...prv, [fileId]: 100 }));

          setPreviews((p) => [
            ...p,
            {
              id: uuid(),
              name: file.name,
              type: 'image',
            },
          ]);
        } catch (err) {
          // console.log(err, 'err');
        }
      }
    };
  };

  const {
    progress,
    errors,
    previews,
    previewFile,
    setPreviews,
    handleChange,
    handleDrag,
    cancelRequest,
    handleRemoveFile,
    handleRemoveErrorFile,
    handlePreviewFile,
    handleClosePreviewFile,
  } = useUpload(handleUpload, signalRef);

  if (previewFile) {
    return (
      <div className='flex flex-col'>
        <Icon
          className='cursor-pointer'
          onClick={handleClosePreviewFile}
          iconName='circleCross'
          variant='solid'
        />
        <PreviewImage ratio='1:1' src={previewFile} width={600} />
      </div>
    );
  }
  return (
    <Upload
      {...args}
      onCancelUpload={cancelRequest}
      previews={previews}
      name='uploadFile'
      progress={progress}
      errors={errors}
      onChange={handleChange}
      onRemoveFile={handleRemoveFile}
      onPreviewFile={handlePreviewFile}
      onRemoveErrorFile={handleRemoveErrorFile}
      multiple
      onDragUpload={handleDrag}
      // wrapperClassName='w-1/2'
      className='!w-[300px]'
    />
  );
};

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const TemplateDrag: StoryFn<typeof Upload> = (args) => {
  const signalRef = useRef(new AbortController());
  const handleUpload = (setProgress: React.Dispatch<React.SetStateAction<UploadProgress>>) => {
    return async (file: File | undefined) => {
      if (file) {
        const fileId = file.name;
        try {
          setProgress((prv) => ({ ...prv, [fileId]: 100 }));

          setPreviews((p) => [
            ...p,
            {
              id: uuid(),
              name: file.name,
              type: 'image',
            },
          ]);
        } catch (err) {
          // console.log(err, 'err');
        }
      }
    };
  };

  const {
    progress,
    errors,
    previews,
    previewFile,
    setPreviews,
    handleDrag,
    cancelRequest,
    handleRemoveFile,
    handleRemoveErrorFile,
    handlePreviewFile,
    handleClosePreviewFile,
  } = useUpload(handleUpload, signalRef);

  if (previewFile) {
    return (
      <div className='flex flex-col'>
        <Icon
          className='cursor-pointer'
          onClick={handleClosePreviewFile}
          iconName='circleCross'
          variant='solid'
        />
        <PreviewImage ratio='1:1' src={previewFile} width={600} />
      </div>
    );
  }
  return (
    <Upload
      {...args}
      isDrag
      layout='horizontal'
      onCancelUpload={cancelRequest}
      previews={previews}
      errors={errors}
      name='uploadFile'
      progress={progress}
      onRemoveErrorFile={handleRemoveErrorFile}
      onRemoveFile={handleRemoveFile}
      onPreviewFile={handlePreviewFile}
      onDragUpload={handleDrag}
      multiple
    />
  );
};

export const _Upload = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
_Upload.args = {
  layout: 'vertical',
  isDrag: false,
};

export const _DragUpload = TemplateDrag.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
_DragUpload.args = {
  layout: 'vertical',
  isDrag: true,
  dragText: {
    btnText: 'Button Text',
    mainText: 'Main Text',
  },
};
