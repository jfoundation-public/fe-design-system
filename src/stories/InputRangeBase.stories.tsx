import { Meta, StoryFn } from '@storybook/react';

import FieldInputControlledProvider from '@/lib/components/FieldInput/Provider';
import InputRangeBase from '@/lib/components/InputRange/InputRangeBase';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Inputs/InputRange/InputRangeBase',
  component: InputRangeBase,
  argTypes: {
    value: {
      description: '[number, number]',
    },
    placeholder: {
      description: '[string, string]',
    },
  },
} as Meta<typeof InputRangeBase>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof InputRangeBase> = (args) => {
  return (
    <FieldInputControlledProvider>
      <InputRangeBase {...args} />
    </FieldInputControlledProvider>
  );
};

export const _InputRangeBase = Template.bind({});

_InputRangeBase.args = {};
