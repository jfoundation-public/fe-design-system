import { Meta, StoryFn } from '@storybook/react';

import { BaseScheduleEvent, EventItem } from '@/lib';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Schedules/EventItem',
  component: EventItem,
} as Meta<typeof EventItem<BaseScheduleEvent>>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof EventItem<BaseScheduleEvent>> = (args) => {
  return <EventItem {...args} containerClassnames='w-[300px]' />;
};

export const _EventItem = Template.bind({});

_EventItem.args = {
  event: {
    id: '12',
    title: 'Small sale Mid-Autumn Festival 11-53',
    startEventDate: new Date('2023-09-06T10:47:22.341Z'),
    time: [new Date('2023-09-05T18:20:22.341Z'), new Date('2023-09-05T20:30:22.341Z')],
    priority: 4,
  },
  nestedEvents: [
    {
      id: '12',
      title: 'Small sale Mid-Autumn Festival 11-53',
      startEventDate: new Date('2023-09-06T10:47:22.341Z'),
      time: [new Date('2023-09-05T18:20:22.341Z'), new Date('2023-09-05T20:30:22.341Z')],
      priority: 4,
    },
  ],
  badge: 1,
  isTilted: true,
};
