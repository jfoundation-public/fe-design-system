import { Meta, StoryFn } from '@storybook/react';

import { FieldContainer } from '../lib/components/FieldContainer';
import { FieldControlled } from '../lib/components/FieldControlled';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Inputs/FieldControlled',
  component: FieldControlled,
  subcomponents: {
    FieldContainer,
  },
} as Meta<typeof FieldControlled>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof FieldControlled> = (args) => {
  return <FieldControlled {...args} />;
};

export const _FieldControlled = Template.bind({});

_FieldControlled.args = {
  label: 'jF',
};
