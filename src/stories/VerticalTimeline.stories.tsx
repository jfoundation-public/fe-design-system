import { Meta, StoryFn } from '@storybook/react';

import VerticalTimeLine from '@/lib/components/Timeline/VerticalTimeLine';

import { TimeLineBottomInfo, TimeLineItems, TimeLineTopInfo } from '../lib';

const data: TimeLineItems[] = [
  {
    id: '1',
    icon: 'output-up',
    iconVariant: 'solid',
    bottomInfo: {
      detail: { value: 'something bottom 1' },
      info: { value: 'something info bottom' },
    },
    topInfo: { detail: { value: 'something top' }, time: { value: 'time' } },
  },
  {
    id: '2',
    icon: 'add-circle',
    iconVariant: 'outline',
    iconColor: 'error',
    bottomInfo: {
      detail: { value: 'something bottom 2' },
      info: { value: 'something info bottom' },
    },
    topInfo: { detail: { value: 'something top' }, time: { value: 'time' } },
  },
  {
    id: '3',
    icon: 'eye',
    iconColor: 'information',
    bottomInfo: {
      detail: { value: 'something bottom 3' },
      info: { value: 'something info bottom' },
    },
    topInfo: { detail: { value: 'something top' }, time: { value: 'time' } },
  },
  {
    id: '4',
    icon: 'eye-slash',
    iconColor: 'success',
    bottomInfo: {
      detail: { value: 'something bottom 4' },
      info: { value: 'something info bottom' },
    },
    topInfo: { detail: { value: 'something top' }, time: { value: 'time' } },
  },
];

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Timeline/VerticalTimeLine',
  component: VerticalTimeLine,
} as Meta<typeof VerticalTimeLine>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof VerticalTimeLine> = (args) => {
  return <VerticalTimeLine {...args} />;
};

export const _Timeline = Template.bind({});
_Timeline.args = {
  hasLastLine: true,
  items: data,
  renderTop: (data: TimeLineTopInfo, _id: string) => {
    // console.log(data, 'top section');
    // console.log(id, 'id');

    return <div>{data.detail.value}</div>;
  },
  renderBottom: (data: TimeLineBottomInfo, id: string) => {
    // console.log(data, 'bottom section');
    // eslint-disable-next-line no-console
    console.log(id, 'id');

    return <div>{data.detail.value}</div>;
  },
};
