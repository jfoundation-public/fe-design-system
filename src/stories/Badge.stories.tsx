import { Meta, StoryFn } from '@storybook/react';

import { Badge } from '../lib/components/Badge';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Badge',
  component: Badge,
} as Meta<typeof Badge>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof Badge> = (args) => <Badge {...args} />;

export const _DotBadge = Template.bind({});
_DotBadge.args = {
  type: 'dot',
  value: 'New',
};
export const _TextBadge = Template.bind({});
_TextBadge.args = {
  type: 'text',
  value: 'New',
};

export const _CustomChildrenBadge = Template.bind({});
_CustomChildrenBadge.args = {
  children: <span>999+</span>,
};
