import { Placement } from '@floating-ui/react-dom-interactions';
import { Meta, StoryFn } from '@storybook/react';

import DateRangePicker from '../lib/components/DateTimePicker/DateRangePicker';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Pickers/DateRangePicker',
  component: DateRangePicker,
  argTypes: {
    pickerPlacement: {
      options: [
        'bottom',
        'bottom-end',
        'bottom-start',
        'left',
        'left-end',
        'left-start',
        'right',
        'right-end',
        'right-start',
        'top',
        'top-end',
        'top-start',
      ] as Placement[],
      control: {
        type: 'radio',
      },
    },
  },
} as Meta<typeof DateRangePicker>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof DateRangePicker> = (args) => {
  return <DateRangePicker {...args} />;
};

export const _TimePicker = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args

_TimePicker.args = {
  label: 'jF',
  required: true,
  readOnly: false,
  disabled: false,
  disableFuture: false,
  disablePast: false,
  pickerPlacement: 'bottom',
  isOnChangeFocusInput: false,
  isError: false,
  isSuccess: false,
  isWarning: false,
  dateformat: 'yyyy-MM-dd',
};
