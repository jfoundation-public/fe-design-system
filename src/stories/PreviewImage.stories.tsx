import { Meta, StoryFn } from '@storybook/react';

import { PreviewImage } from '../lib/components/PreviewImage';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/PreviewImage/PreviewImage',
  component: PreviewImage,
} as Meta<typeof PreviewImage>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof PreviewImage> = (args) => <PreviewImage {...args} />;

export const _PreviewImage = Template.bind({});

_PreviewImage.args = {
  src: 'https://jfun.co/wp-content/uploads/2021/01/logo.png',
};
