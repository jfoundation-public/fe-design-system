import { Meta, StoryFn } from '@storybook/react';
import { useState } from 'react';

import FieldInputControlledProvider from '@/lib/components/FieldInput/Provider';

import TreeSelectBase from '../lib/components/TreeSelect/TreeSelectBase';

const options = [
  {
    id: '1',
    label: 'Furniture',
    items: [
      {
        id: '1.1',
        label: 'Tables & Chairs',
      },
      {
        id: '1.2',
        label: 'Sofas',
      },
      {
        id: '1.3',
        label: 'Occasional Furniture',
      },
    ],
  },
  {
    id: '2',
    label: 'Decor',
    items: [
      {
        id: '2.1',
        label: 'Bed Linen',
        items: [
          {
            id: '2.1.1',
            label: 'B',
            items: [
              {
                id: '2.1.1.1',
                label: 'Tables & Chairs',
              },
              {
                id: '2.1.1.2',
                label: 'Sofas',
              },
              {
                id: '2.1.1.3',
                label: 'Occasional Furniture',
              },
            ],
          },
        ],
      },
      {
        id: '2.2',
        label: 'Curtains & Blinds',
      },
      {
        id: '2.3',
        label: 'Carpets',
      },
    ],
  },
];

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/TreeSelect/TreeSelectBase',
  component: TreeSelectBase,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
} as Meta<typeof TreeSelectBase>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof TreeSelectBase> = (args) => {
  const [treeSelected, setTreeSelected] = useState<string>('');
  return (
    <FieldInputControlledProvider>
      <TreeSelectBase
        {...args}
        onChange={(e: React.ChangeEvent<HTMLInputElement>) => setTreeSelected(e.target.value)}
        value={treeSelected}
      />
    </FieldInputControlledProvider>
  );
};

export const _TreeSelect = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
_TreeSelect.args = {
  options,
  placeholder: 'Choose',
};
