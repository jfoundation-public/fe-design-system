import { Meta, StoryFn } from '@storybook/react';

import { FieldInput } from '../lib/components/FieldInput';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Inputs/FieldContainer/FieldInput',
  component: FieldInput,
} as Meta<typeof FieldInput>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof FieldInput> = (args) => <FieldInput {...args} />;

export const _FieldInput = Template.bind({});

_FieldInput.args = {};
