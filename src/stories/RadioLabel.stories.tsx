import { Meta, StoryFn } from '@storybook/react';

import RadioLabel from '../lib/components/Radio/RadioLabel';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Inputs/Radio/RadioLabel',
  component: RadioLabel,
} as Meta<typeof RadioLabel>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof RadioLabel> = (args) => <RadioLabel {...args} />;

export const _RadioLabel = Template.bind({});
_RadioLabel.args = {
  children: 'jF',
};
