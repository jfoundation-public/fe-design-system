import { Meta, StoryFn } from '@storybook/react';
import { useState } from 'react';

import { Step, StepItem, StepStatus, Typography } from '../lib';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Step',
  component: Step,
} as Meta<typeof Step>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof Step> = (args) => {
  const [selected, setSelected] = useState(0);
  const data: StepItem[] = [
    { id: 'step1', title: <Typography variant='subtitle1'>Hello</Typography> },
    {
      id: 'step2',
      title: 'Header Content',
      description: 'Something',
    },
    { id: 'step3', title: 'Example 3' },
    { id: 'step4', title: 'Example 4' },
    {
      id: 'step5',
      title: 'Example 5',
    },
  ];

  const mockStatus: StepStatus = {
    step1: 'default',
    step2: 'default',
    step3: 'finish',
    step4: 'error',
    step5: 'disable',
  };

  return (
    <Step
      {...args}
      direction={args?.direction ?? 'horizontal'}
      className='p-8'
      onSelect={(id) => setSelected(id)}
      steps={data}
      current={selected}
      status={mockStatus}
    />
  );
};

export const _Step = Template.bind({});
_Step.args = {
  type: 'dot',
  direction: 'horizontal',
};
