import { Meta, StoryFn } from '@storybook/react';

import { FieldLabel } from '../lib/components/FieldLabel';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Inputs/FieldContainer/FieldLabel',
  component: FieldLabel,
} as Meta<typeof FieldLabel>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof FieldLabel> = (args) => <FieldLabel {...args} />;

export const _FieldLabel = Template.bind({});

_FieldLabel.args = {
  children: 'jF',
};
