import { Meta, StoryFn } from '@storybook/react';

import DigitInput from '@/lib/components/OTPInput/DigitInput';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/OTPInput/DigitInput',
  component: DigitInput,
} as Meta<typeof DigitInput>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof DigitInput> = (args) => <DigitInput {...args} />;

export const _DigitInput = Template.bind({});

_DigitInput.args = {
  isError: false,
};
