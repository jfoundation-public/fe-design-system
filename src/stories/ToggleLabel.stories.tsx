import { Meta, StoryFn } from '@storybook/react';

import ToggleLabel from '../lib/components/Toggle/ToggleLabel';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Inputs/Toggle/ToggleLabel',
  component: ToggleLabel,
} as Meta<typeof ToggleLabel>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof ToggleLabel> = (args) => <ToggleLabel {...args} />;

export const _ToggleLabel = Template.bind({});
_ToggleLabel.args = {
  children: 'jF',
};
