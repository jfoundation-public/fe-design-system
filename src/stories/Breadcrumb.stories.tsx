import { Meta, StoryFn } from '@storybook/react';

import { Breadcrumb } from '../lib/components/Breadcrumb';

const breadcrumbs = [
  { id: 'home', name: 'home', url: '/' },
  { id: 'feature', name: 'feature', url: '/feature' },
  { id: 'option', name: 'option', url: '/sth/option' },
  { id: 'hello', name: 'hello', url: '/sth/hello' },
  { id: 'ola', name: 'ola', url: '/sth/ola' },
];

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Breadcrumb',
  component: Breadcrumb,
} as Meta<typeof Breadcrumb>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof Breadcrumb> = (args) => <Breadcrumb {...args} />;

export const _Breadcrumb = Template.bind({});
_Breadcrumb.args = {
  breadcrumbs,
};
