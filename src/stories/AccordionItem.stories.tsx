/* eslint-disable react/prop-types */
import { Meta, StoryFn } from '@storybook/react';
import { useState } from 'react';

import { Icon } from '@/lib';
import AccordionItem from '@/lib/components/Accordion/AccordionItem';

import { Accordion } from '../lib/components/Accordion';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Accordion/AccordionItem',
  component: AccordionItem,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes,
} as Meta<typeof AccordionItem>;

const AccordionItemTemplate: StoryFn<typeof AccordionItem> = (props) => {
  const [open, setOpen] = useState(false);
  return (
    // eslint-disable-next-line react/prop-types
    <Accordion.Item
      {...props}
      expand={open || props.expand}
      onExpand={() => setOpen(!(open || props.expand))}
    />
  );
};

export const _AccordionItem = AccordionItemTemplate.bind({});
_AccordionItem.args = {
  title: 'Accordion',
  hasBorderHeader: true,
  children: 'Eiusmod do fugiat officia ea cupidatat consequat nulla sint eu culpa elit.',
};

export const _AccordionWithCustomIcon = AccordionItemTemplate.bind({});
_AccordionWithCustomIcon.args = {
  title: 'Accordion',
  hasBorderHeader: true,
  children: 'Eiusmod do fugiat officia ea cupidatat consequat nulla sint eu culpa elit.',

  collapseIcon: <Icon iconName='add-circle' />,
  expandIcon: <Icon iconName='minus-square' variant='outline' />,
};
