import { Meta, StoryFn } from '@storybook/react';

import { Button } from '@/lib';

import { Alert } from '../lib/components/Alert';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Alert',
  component: Alert,
} as Meta<typeof Alert>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof Alert> = (args) => <Alert {...args} />;

export const _Alert = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args

_Alert.args = {
  title: 'This is an alert',
  iconName: 'profile-user',
  action: (
    <Button size='sm' variant='blank'>
      Undo
    </Button>
  ),
};
