import { Meta, StoryFn } from '@storybook/react';
import { useState } from 'react';

import PanelDate from '../lib/components/DateTimePicker/PanelDate';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Pickers/_SubComponent_/PanelDate',
  component: PanelDate,
} as Meta<typeof PanelDate>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof PanelDate> = (args) => {
  const [selectedDate, setSelectedDate] = useState(new Date());
  return <PanelDate {...args} dateSelected={selectedDate} onSelected={setSelectedDate} />;
};

export const _PanelDate = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args

_PanelDate.args = {
  disableFuture: false,
  disablePast: false,
  locale: 'vi',
  viewDate: new Date(),
};
