import { Meta, StoryFn } from '@storybook/react';

import { Autocomplete } from '../lib/components/Autocomplete';

const options = [
  { label: 'Hà Nội', value: 'Hà Nội' },
  { label: 'Huế', value: 'Huế' },
  { label: 'Đà Nẵng', value: 'Đà Nẵng' },
  { label: 'TPHCM', value: 'TPHCM' },
];

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Inputs/Autocomplete',
  component: Autocomplete,
} as Meta<typeof Autocomplete>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof Autocomplete> = (args) => (
  <Autocomplete {...args} options={options} />
);

export const _Autocomplete = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
