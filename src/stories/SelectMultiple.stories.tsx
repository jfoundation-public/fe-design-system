import { Meta, StoryFn } from '@storybook/react';
import { useState } from 'react';

import SelectMultiple from '../lib/components/SelectBase/SelectMutiple';

const options = [
  { label: 'Male', value: 'male' },
  { label: 'Female', value: 'female' },
  { label: 'Other', value: 'other' },
  { label: 'Male2', value: 'male2' },
  { label: 'Female2', value: 'female2' },
  { label: 'Other2', value: 'other2' },
];

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Inputs/Select/SelectMultiple',
  component: SelectMultiple,
} as Meta<typeof SelectMultiple>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof SelectMultiple> = (args) => {
  const [selected, setSelected] = useState<string>('');
  return (
    <SelectMultiple {...args} value={selected} onChange={(e) => setSelected(e.target.value)} />
  );
};

export const _SelectMultiple = Template.bind({});
_SelectMultiple.args = {
  options,
};
