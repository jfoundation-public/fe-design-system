import { Meta, StoryFn } from '@storybook/react';
import { useState } from 'react';

import { Badge, SidebarList } from '@/lib';

import SidebarItem from '../lib/components/Sidebar/SidebarItem';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Sidebar/SidebarItem',
  component: SidebarItem,
} as Meta<typeof SidebarItem>;

const sidebars: SidebarList[] = [
  {
    id: 'cms',
    icon: 'cms',
    title: 'DESIGN SYSTEM 1',
    listItems: [
      {
        id: '1',
        name: 'Raider',
        url: '/raider',
      },
      {
        id: '2',
        name: 'Raider2 lorem ipsum dolor sit amet',
        url: '/raider',
        endAdornment: <Badge>999+</Badge>,
      },
    ],
  },
  {
    id: 'dashboard',
    icon: 'dashboard',
    title: 'DESIGN SYSTEM 2',
    tabMenu: [
      {
        id: 'dashboard new',
        tabTitle: 'dashboard new',
        tabSubMenu: [
          {
            id: '25',
            title: 'dashboard',
            listSubMenu: [
              { id: '26', name: 'Raider', url: '/raider' },
              { id: '27', name: 'Forbidden Rite', url: '/forbidden-rite' },
              {
                id: '28',
                name: 'Sub Menu 1',
                url: '/sub-menu-1',
              },
            ],
          },
          {
            id: '29',
            title: 'dashboard 2',
            listSubMenu: [
              { id: '30', name: 'Sub Menu 2', url: '/sub-menu-2' },
              { id: '31', name: 'Sub Menu 3', url: '/sub-menu-3' },
              { id: '32', name: 'Sub Menu 4', url: '/sub-menu-4' },
            ],
          },
          {
            id: '33',
            title: 'Menu 3',
            listSubMenu: [
              { id: '34', name: 'Sub Menu 5', url: '/sub-menu-5' },
              { id: '35', name: 'Sub Menu 6', url: '/sub-menu-6' },
              { id: '36', name: 'Sub Menu 7', url: '/sub-menu-7' },
              { id: '37', name: 'Sub Menu 5', url: '/sub-menu-5' },
              { id: '38', name: 'Sub Menu 6', url: '/sub-menu-6' },
              { id: '39', name: 'Sub Menu 7', url: '/sub-menu-7' },
            ],
          },
        ],
      },
      {
        id: 'language 2',
        tabTitle: 'language',
        listItem: [
          { id: '37', name: 'English', url: '/english' },
          { id: '38', name: 'Vietnamese', url: '/vietnam' },
          { id: '39', name: 'Chinese', url: '/chinese' },
        ],
      },
      {
        id: '40',
        tabTitle: 'option',
        //   listItem: ['English', 'Vietnamese', 'Indian'],
      },
    ],
  },
];

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof SidebarItem> = () => {
  const [selectedSideBar, setSelectedSideBar] = useState<SidebarList>(sidebars[0]);

  const handleOnClickSideBar = (item: SidebarList) => {
    setSelectedSideBar(item);
  };
  return (
    <>
      {sidebars.map((sidebar: SidebarList) => (
        <SidebarItem
          key={sidebar.icon}
          data={sidebar}
          onClickSideBar={handleOnClickSideBar}
          selectedSideBar={selectedSideBar}
        />
      ))}
    </>
  );
};

export const _SidebarItem = Template.bind({});
_SidebarItem.args = {
  data: {
    id: 'dashboard',
    icon: 'dashboard',
    title: 'DESIGN SYSTEM 2',
    tabMenu: [
      {
        id: 'dashboard new',
        tabTitle: 'dashboard new',
        tabSubMenu: [
          {
            id: '25',
            title: 'dashboard',
            listSubMenu: [
              {
                id: '26',
                name: 'Raider',
                url: '/raider',
              },
              {
                id: '27',
                name: 'Forbidden Rite',
                url: '/forbidden-rite',
              },
              {
                id: '28',
                name: 'Sub Menu 1',
                url: '/sub-menu-1',
              },
            ],
          },
          {
            id: '29',
            title: 'dashboard 2',
            listSubMenu: [
              {
                id: '30',
                name: 'Sub Menu 2',
                url: '/sub-menu-2',
              },
              {
                id: '31',
                name: 'Sub Menu 3',
                url: '/sub-menu-3',
              },
              {
                id: '32',
                name: 'Sub Menu 4',
                url: '/sub-menu-4',
              },
            ],
          },
          {
            id: '33',
            title: 'Menu 3',
            listSubMenu: [
              {
                id: '34',
                name: 'Sub Menu 5',
                url: '/sub-menu-5',
              },
              {
                id: '35',
                name: 'Sub Menu 6',
                url: '/sub-menu-6',
              },
              {
                id: '36',
                name: 'Sub Menu 7',
                url: '/sub-menu-7',
              },
              {
                id: '37',
                name: 'Sub Menu 5',
                url: '/sub-menu-5',
              },
              {
                id: '38',
                name: 'Sub Menu 6',
                url: '/sub-menu-6',
              },
              {
                id: '39',
                name: 'Sub Menu 7',
                url: '/sub-menu-7',
              },
            ],
          },
        ],
      },
      {
        id: 'language 2',
        tabTitle: 'language',
        listItem: [
          {
            id: '37',
            name: 'English',
            url: '/english',
          },
          {
            id: '38',
            name: 'Vietnamese',
            url: '/vietnam',
          },
          {
            id: '39',
            name: 'Chinese',
            url: '/chinese',
          },
        ],
      },
      {
        id: '40',
        tabTitle: 'option',
      },
    ],
  },
  selectedSideBar: {
    id: 'dashboard',
    icon: 'dashboard',
    title: 'DESIGN SYSTEM 2',
    tabMenu: [
      {
        id: 'dashboard new',
        tabTitle: 'dashboard new',
        tabSubMenu: [
          {
            id: '25',
            title: 'dashboard',
            listSubMenu: [
              {
                id: '26',
                name: 'Raider',
                url: '/raider',
              },
              {
                id: '27',
                name: 'Forbidden Rite',
                url: '/forbidden-rite',
              },
              {
                id: '28',
                name: 'Sub Menu 1',
                url: '/sub-menu-1',
              },
            ],
          },
          {
            id: '29',
            title: 'dashboard 2',
            listSubMenu: [
              {
                id: '30',
                name: 'Sub Menu 2',
                url: '/sub-menu-2',
              },
              {
                id: '31',
                name: 'Sub Menu 3',
                url: '/sub-menu-3',
              },
              {
                id: '32',
                name: 'Sub Menu 4',
                url: '/sub-menu-4',
              },
            ],
          },
          {
            id: '33',
            title: 'Menu 3',
            listSubMenu: [
              {
                id: '34',
                name: 'Sub Menu 5',
                url: '/sub-menu-5',
              },
              {
                id: '35',
                name: 'Sub Menu 6',
                url: '/sub-menu-6',
              },
              {
                id: '36',
                name: 'Sub Menu 7',
                url: '/sub-menu-7',
              },
              {
                id: '37',
                name: 'Sub Menu 5',
                url: '/sub-menu-5',
              },
              {
                id: '38',
                name: 'Sub Menu 6',
                url: '/sub-menu-6',
              },
              {
                id: '39',
                name: 'Sub Menu 7',
                url: '/sub-menu-7',
              },
            ],
          },
        ],
      },
      {
        id: 'language 2',
        tabTitle: 'language',
        listItem: [
          {
            id: '37',
            name: 'English',
            url: '/english',
          },
          {
            id: '38',
            name: 'Vietnamese',
            url: '/vietnam',
          },
          {
            id: '39',
            name: 'Chinese',
            url: '/chinese',
          },
        ],
      },
      {
        id: '40',
        tabTitle: 'option',
      },
    ],
  },
};
