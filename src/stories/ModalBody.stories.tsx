import { Meta, StoryFn } from '@storybook/react';

import ModalBody from '../lib/components/Modal/ModalBody';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Modal/ModalBody',
  component: ModalBody,
} as Meta<typeof ModalBody>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof ModalBody> = (args) => <ModalBody {...args} />;

export const _ModalBody = Template.bind({});

_ModalBody.args = {
  children: 'Hello jF',
};
