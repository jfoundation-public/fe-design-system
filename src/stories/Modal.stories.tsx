import { Meta, StoryFn } from '@storybook/react';
import { useState } from 'react';

import { Button, Icon } from '@/lib';

import { Modal, ModalBody, ModalFooter, ModalHeader } from '../lib/components/Modal';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Modal/Modal',
  component: Modal,
} as Meta<typeof Modal>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof Modal> = (args) => {
  const [openModal, setOpenModal] = useState(false);
  return (
    <>
      <Button onClick={() => setOpenModal(true)}>Open modal</Button>
      <Modal {...args} open={openModal} onClose={() => setOpenModal(false)}>
        <ModalHeader
          title={<p className='font-semibold text-text-warning'>Warning</p>}
          icon={<Icon iconName='warning' variant='solid' color='warning' />}
          onClose={() => setOpenModal(false)}
        />
        <ModalBody>All information will be discarded. Are you sure to cancel?</ModalBody>
        <ModalFooter>
          <div className='flex'>
            <Button
              type='button'
              variant='secondary'
              className='h-12'
              onClick={() => setOpenModal(false)}
            >
              Reject
            </Button>
            <Button
              type='button'
              variant='primary'
              className='h-12'
              onClick={() => setOpenModal(false)}
            >
              Confirm
            </Button>
          </div>
        </ModalFooter>
      </Modal>
    </>
  );
};

export const _Modal = Template.bind({});

_Modal.args = {
  open: false,
};
