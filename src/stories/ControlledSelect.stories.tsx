import { Meta, StoryFn } from '@storybook/react';
import { useState } from 'react';

import FieldInputControlledProvider from '@/lib/components/FieldInput/Provider';

import ControlledSelect from '../lib/components/SelectBase/ControlledSelect';

const options = [
  { label: 'Male', value: 'male' },
  { label: 'Female', value: 'female' },
  { label: 'Other', value: 'other' },
  { label: 'Male2', value: 'male2' },
  { label: 'Female2', value: 'female2' },
  { label: 'Other2', value: 'other2' },
];

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Inputs/Select/ControlledSelect',
  component: ControlledSelect,
} as Meta<typeof ControlledSelect>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof ControlledSelect> = (args) => {
  const [selected, setSelected] = useState('');
  return (
    <FieldInputControlledProvider>
      <ControlledSelect {...args} value={selected} onChange={(e) => setSelected(e.target.value)} />
    </FieldInputControlledProvider>
  );
};

export const _ControlledSelect = Template.bind({});
_ControlledSelect.args = {
  options,
};
