import { Meta, StoryFn } from '@storybook/react';
import { useState } from 'react';

import TreeSelect from '../lib/components/TreeSelect/TreeSelect';

const options = [
  {
    id: '1',
    label: 'Furniture',
    items: [
      {
        id: '1.1',
        label: 'Tables & Chairs',
      },
      {
        id: '1.2',
        label: 'Sofas',
      },
      {
        id: '1.3',
        label: 'Occasional Furniture',
      },
    ],
  },
  {
    id: '2',
    label: 'Decor',
    items: [
      {
        id: '2.1',
        label: 'Bed Linen',
        items: [
          {
            id: '2.1.1',
            label: 'B',
            items: [
              {
                id: '2.1.1.1',
                label: 'Tables & Chairs',
              },
              {
                id: '2.1.1.2',
                label: 'Sofas',
              },
              {
                id: '2.1.1.3',
                label: 'Occasional Furniture',
              },
            ],
          },
        ],
      },
      {
        id: '2.2',
        label: 'Curtains & Blinds',
      },
      {
        id: '2.3',
        label: 'Carpets',
      },
    ],
  },
];

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/TreeSelect/TreeSelect',
  component: TreeSelect,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
} as Meta<typeof TreeSelect>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof TreeSelect> = (args) => {
  const [treeSelected, setTreeSelected] = useState<string>('');
  return (
    <TreeSelect
      {...args}
      onChange={(e: React.ChangeEvent<HTMLInputElement>) => setTreeSelected(e.target.value)}
      value={treeSelected}
    />
  );
};

export const _TreeSelect = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
_TreeSelect.args = {
  options,
  placeholder: 'Choose from tree...',
  readOnly: false,
  disabled: false,
  label: 'jF',
  required: true,
};
