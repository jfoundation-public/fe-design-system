import { Meta, StoryFn } from '@storybook/react';
import { useState } from 'react';

import { CountInput } from '../lib/components/CountInput';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Components/Inputs/CountInput',
  component: CountInput,
} as Meta<typeof CountInput>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof CountInput> = (args) => {
  const [value, setValue] = useState(1);
  return (
    <CountInput
      {...args}
      value={value}
      onChange={(e) => setValue(parseInt(e.target.value))}
      onIncrease={() => setValue(value + 1)}
      onDecrease={() => setValue(value - 1)}
    />
  );
};

export const _CountInput = Template.bind({});
_CountInput.args = {
  disabled: false,
  required: true,
  increaseDisabled: false,
  decreaseDisabled: false,
};
