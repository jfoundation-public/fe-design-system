import { SelectOption } from '@/lib/types';

import { FieldControlledProps } from '../FieldControlled/types';
import { SelectBaseProps } from '../SelectBase/types';

export interface SelectProps<T extends SelectOption>
  extends FieldControlledProps,
    SelectBaseProps<T> {
  value: string | string[];
  name: string;
}
