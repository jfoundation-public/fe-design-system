import { SelectOption } from '@/lib/types';

import { FieldControlled } from '../FieldControlled';
import { ControlledSelect } from '../SelectBase';
import { SelectProps } from './types';

export default function Select<T extends SelectOption>(props: SelectProps<T>) {
  const {
    label,
    labelIcon,
    helperText,
    startAdornment,
    endAdornment,
    isWarning,
    isSuccess,
    isError,
    Container,
    ...restProps
  } = props;

  return (
    <FieldControlled
      label={label}
      required={restProps.required}
      labelIcon={labelIcon}
      helperText={helperText}
      startAdornment={startAdornment}
      endAdornment={endAdornment}
      isWarning={isWarning}
      isSuccess={isSuccess}
      isError={isError}
      {...Container}
    >
      <ControlledSelect {...restProps} fullWidth />
    </FieldControlled>
  );
}
