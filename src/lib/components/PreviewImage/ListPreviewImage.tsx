import clsx from 'clsx';
import { useState } from 'react';

import { uuid } from '@/lib';

import PreviewImage from './PreviewImage';
import { ListPreviewImageProps, PreviewImgType } from './types';

const ListPreviewImage = (props: ListPreviewImageProps) => {
  const {
    listImg = [],
    disabledViewMore,
    className,
    imgClassName,
    maxListImg = 5,
    renderEndContent,
  } = props;

  const [expand, setExpand] = useState<boolean>(false);
  const dynamicListImg = expand ? listImg : listImg.slice(0, maxListImg);
  const _classNames = clsx('list-preview-img flex flex-wrap flex-row gap-4', className);
  const _imgClassName = clsx('preview-img rounded-lg', imgClassName);

  const handleClickExpand = (disabled: boolean) => {
    if (disabled) return;
    setExpand((prev) => !prev);
  };

  const renderRemainListImg = () => {
    const remaining = listImg.length - maxListImg;

    if (remaining < 1) return null;

    return (
      <div
        role={'presentation'}
        onClick={disabledViewMore ? undefined : () => handleClickExpand(expand)}
        className={clsx(disabledViewMore ? 'list-img-remaining-disabled' : 'list-img-remaining')}
      >
        <span className='list-img-remaining-count'>{`+${remaining}`}</span>
        <PreviewImage
          imgClassName={_imgClassName}
          className='brightness-50'
          src={listImg[maxListImg].src}
          ratio={dynamicListImg[0].ratio}
          width={dynamicListImg[0].width}
        />
      </div>
    );
  };

  return (
    <div className={_classNames}>
      {dynamicListImg.map((img: PreviewImgType) => (
        <div key={`${uuid()}-${img.src}`} className='list-img-item'>
          <PreviewImage
            onClick={() => handleClickExpand(!expand)}
            imgClassName={_imgClassName}
            src={img.src}
            ratio={img.ratio}
            width={img.width}
          />
        </div>
      ))}
      {!expand && renderRemainListImg()}
      {renderEndContent}
    </div>
  );
};

export default ListPreviewImage;
