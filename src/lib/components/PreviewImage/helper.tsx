import { radioCalculatorType } from './types';

export const ratioCalculator = (width: number, ratio: string): radioCalculatorType => {
  let value: radioCalculatorType = { width: 0, height: 0 };
  switch (ratio) {
    case '1:1':
      value = {
        width: width,
        height: width,
      };
      break;
    case '4:3':
      value = {
        width: width,
        height: width * (3 / 4),
      };
      break;
    case '3:2':
      value = {
        width: width,
        height: width * (2 / 3),
      };
      break;
    case '16:10':
      value = {
        width: width,
        height: width * (10 / 16),
      };
      break;
    case '16:9':
      value = {
        width: width,
        height: width * (9 / 16),
      };
      break;
    case '2:1':
      value = {
        width: width,
        height: width * (1 / 2),
      };
      break;
    case '5:2':
      value = {
        width: width,
        height: width * (2 / 5),
      };
      break;
    case '3:1':
      value = {
        width: width,
        height: width * (1 / 3),
      };
      break;
    case '2:3':
      value = {
        width: width,
        height: width * (3 / 2),
      };
      break;
    case '3:4':
      value = {
        width: width,
        height: width * (4 / 3),
      };
      break;
  }
  return value;
};
