import clsx from 'clsx';

import { ratioCalculator } from './helper';
import { PreviewImageProps } from './types';

const PreviewImage = (props: PreviewImageProps) => {
  const {
    className,
    imgClassName,
    src,
    width: _width = 100,
    ratio = '1:1',
    onClick: _onClick,
    isSEO = true,
    alt = 'preview-img',
    type = 'image',
  } = props;

  const { width, height } = ratioCalculator(_width, ratio);

  const _className = clsx('preview-img', className);
  return (
    <div
      role={'presentation'}
      onClick={_onClick}
      className={_className}
      style={{ width: width, height: height }}
    >
      {type === 'video' ? (
        // eslint-disable-next-line jsx-a11y/media-has-caption
        <video
          autoPlay={true}
          muted
          controls={false}
          className={imgClassName}
          width={width}
          height={height}
          loop={true}
        >
          <source src={src} />
        </video>
      ) : isSEO ? (
        <img className={imgClassName} width={width} height={height} src={src} alt={alt} />
      ) : (
        <div
          className={clsx('w-full bg-center h-full bg-no-repeat bg-cover', imgClassName)}
          style={{ backgroundImage: `url(${src})` }}
        />
      )}
    </div>
  );
};

export default PreviewImage;
