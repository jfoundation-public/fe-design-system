export interface radioCalculatorType {
  width: number;
  height: number;
}

export type PreviewImageSourceType = 'video' | 'image';

export interface PreviewImageProps {
  src: string;
  ratio?: '1:1' | '4:3' | '3:2' | '16:10' | '16:9' | '2:1' | '5:2' | '3:1' | '2:3' | '3:4';
  className?: string;
  imgClassName?: string;
  width?: number;
  onClick?: () => void;
  isSEO?: boolean;
  alt?: string;
  type?: PreviewImageSourceType;
}

export type PreviewImgType = {
  src: string;
  ratio?: '1:1' | '4:3' | '3:2' | '16:10' | '16:9' | '2:1' | '5:2' | '3:1' | '2:3' | '3:4';
  width?: number;
};

export interface ListPreviewImageProps {
  listImg: PreviewImgType[];
  className?: string;
  imgClassName?: string;
  maxListImg?: number;
  renderEndContent?: React.ReactNode;
  disabledViewMore?: boolean;
}
