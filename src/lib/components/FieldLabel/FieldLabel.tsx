import clsx from 'clsx';

import { Icon } from '../Icon';
import { getFieldLabelClass } from './helper';
import { FieldLabelProps } from './types';

export default function FieldLabel(props: FieldLabelProps) {
  const { iconName, className, children, required, iconVariant, ...restProps } = props;
  const classes = getFieldLabelClass({ required });
  return (
    <label className={clsx(classes, className)} {...restProps}>
      <span className='caption1'>{children}</span>
      {!!iconName && <Icon size={16} iconName={iconName} className='ml-1' variant={iconVariant} />}
    </label>
  );
}
