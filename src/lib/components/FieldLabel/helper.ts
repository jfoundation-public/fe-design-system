import clsx from 'clsx';

function getFieldLabelClass({ required }: { required?: boolean }) {
  return clsx('field-label', required && 'field-label--required');
}

export { getFieldLabelClass };
