import { ComponentPropsWithRef } from 'react';

import { IconName, IconVariant } from '../Icon/types';

export interface FieldLabelProps extends ComponentPropsWithRef<'label'> {
  iconName?: IconName;
  iconVariant?: IconVariant;
  required?: boolean;
}
