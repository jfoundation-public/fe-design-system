import { FieldState } from '@/lib/types';

import { TypographyProps } from '../Typography/types';

export interface HelperTextProps extends Omit<TypographyProps, 'variant'> {
  state: FieldState;
}
