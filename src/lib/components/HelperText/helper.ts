import clsx from 'clsx';

import { FieldState } from '@/lib/types';

const HELPER_TEXT_STATE_CLASSES = {
  default: 'helper-text-state-default',
  hover: '',
  focused: '',
  error: 'helper-text-state-error',
  active: '',
  'read-only': '',
  'read-only-focused': '',
  disabled: '',
  success: 'helper-text-state-success',
  warning: 'helper-text-state-warning',
};

function getHelperTextClasses(state: FieldState) {
  return clsx('helper-text', HELPER_TEXT_STATE_CLASSES[state]);
}

export { getHelperTextClasses };
