import clsx from 'clsx';

import { Typography } from '../Typography';
import { getHelperTextClasses } from './helper';
import { HelperTextProps } from './types';

export default function HelperText(props: HelperTextProps) {
  const { state, className, children, ...restProps } = props;
  const classes = getHelperTextClasses(state);
  return (
    <Typography variant='caption2' className={clsx(classes, className)} {...restProps}>
      {children}
    </Typography>
  );
}
