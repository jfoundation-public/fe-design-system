import clsx from 'clsx';
import { useEffect } from 'react';

import { Badge, Icon } from '@/lib';

import { TabProps } from './types';

const Tab = (props: TabProps) => {
  const {
    title,
    selected,
    disabled = false,
    onSelect,
    tabId,
    leftIcon,
    rightIcon,
    status,
    tabClassName,
    tabTitleClassName,
  } = props;
  const isSelected = selected === tabId;

  useEffect(() => {
    //unselect when disabled
    if (disabled && selected === tabId) onSelect('');
  }, [disabled, selected, onSelect, tabId]);

  return (
    <div
      role='presentation'
      className={clsx(
        'tab-item',
        isSelected && !disabled && 'border-b-2 border-primary',
        disabled && 'text-icon-disable cursor-none pointer-events-none',
        tabClassName,
      )}
      onClick={() => onSelect(tabId)}
    >
      {leftIcon && (
        <Icon
          className={clsx(
            'tab-item-left-icon',
            isSelected && !disabled && '!text-palette-primary800',
            disabled && '!text-icon-disable',
            leftIcon?.className,
          )}
          iconName={leftIcon.iconName}
          variant={leftIcon.variant}
          size={leftIcon.size || 16}
        />
      )}
      <span
        className={clsx(
          'tab-title',
          isSelected && !disabled && '!text-palette-primary800',
          disabled && '!text-text-disable',
          tabTitleClassName,
        )}
      >
        {title}
      </span>
      {rightIcon && (
        <Icon
          className={clsx(
            'tab-item-left-icon',
            isSelected && !disabled && '!text-palette-primary800',
            disabled && '!text-icon-disable',
            rightIcon?.className,
          )}
          iconName={rightIcon.iconName}
          variant={rightIcon.variant}
          size={rightIcon.size || 16}
        />
      )}

      {status && (
        <Badge
          type='text'
          value={status.content}
          className={clsx('tab-badge', disabled && 'bg-text-disable')}
        />
      )}
    </div>
  );
};

export default Tab;
