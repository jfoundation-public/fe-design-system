import { IconName } from '../Icon/types';

export interface TabProps {
  title: string;
  selected: string;
  onSelect: (id: string) => void;
  tabId: string;
  leftIcon?: IconType;
  rightIcon?: IconType;
  status?: statusType;
  disabled?: boolean;
  tabClassName?: string;
  tabTitleClassName?: string;
}

export interface TabDataProps {
  title: string;
  tabId: string;
  leftIcon?: IconType;
  rightIcon?: IconType;
  status?: statusType;
  disabled?: boolean;
  tabClassName?: string;
}

export interface TabsProps {
  tabs: TabDataProps[];
  tabClassName?: string;
  tabTitleClassName?: string;
  className?: string;
  onSelect: (item: string) => void;
  selected: string;
}

export interface statusType {
  content: string;
  type: 'dot' | 'text';
  className?: string;
}

export interface IconType {
  iconName: IconName;
  variant: 'outline' | 'solid';
  className?: string;
  size?: number;
}
