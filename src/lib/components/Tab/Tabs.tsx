import clsx from 'clsx';
import { useCallback } from 'react';

import Tab from './Tab';
import { TabDataProps, TabsProps } from './types';

const Tabs = (props: TabsProps) => {
  const { tabs, className, selected, onSelect, tabClassName, ...restProps } = props;

  const handleOnSelect = useCallback(
    (tabId: string) => {
      onSelect && onSelect(tabId);
    },
    [onSelect],
  );
  return (
    <div className={clsx('tabs', className)}>
      {tabs.map((item: TabDataProps) => (
        <Tab
          key={item.tabId}
          leftIcon={item.leftIcon}
          rightIcon={item.rightIcon}
          tabId={item.tabId}
          onSelect={handleOnSelect}
          selected={selected}
          status={item.status}
          title={item.title}
          disabled={item.disabled}
          tabClassName={tabClassName}
          {...restProps}
        />
      ))}
    </div>
  );
};
export default Tabs;
