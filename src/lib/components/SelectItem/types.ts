import { ComponentPropsWithRef, ReactElement } from 'react';

export interface SelectItemProps extends Omit<ComponentPropsWithRef<'li'>, 'onSelect'> {
  value: string;
  onSelect: (id: string) => void;
  disabled?: boolean;
  selected?: boolean;
  withCheck?: boolean;
  children?: string | ReactElement;
}
