import clsx from 'clsx';

import { noop } from '@/lib/utils';

import { Checkbox } from '../Checkbox';
import { Icon } from '../Icon';
import { SelectItemProps } from './types';

export default function SelectItem(props: SelectItemProps) {
  const { children, value, onSelect, disabled, selected, withCheck, className, ...restProps } =
    props;
  return (
    <li
      className={clsx('select-item', disabled && 'select-item-disabled', className)}
      role='menuitem'
      onKeyDown={noop}
      {...restProps}
      onClick={() => {
        !withCheck && !disabled && !!onSelect && onSelect(value);
      }}
    >
      {withCheck ? (
        <Checkbox
          checked={selected}
          onChange={() => {
            !disabled && !!onSelect && onSelect(value);
          }}
        >
          {children}
        </Checkbox>
      ) : (
        children
      )}
      {!withCheck && selected && (
        <Icon iconName='tick' variant='outline' className='text-primary' />
      )}
    </li>
  );
}
