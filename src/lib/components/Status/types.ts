import { ComponentPropsWithRef } from 'react';

import { IconName } from '../Icon';

export type StatusSize = 'lg' | 'md' | 'sm';
export type StatusVariant = 'success' | 'warning' | 'error' | 'done' | 'canceled' | 'primary';

export interface StatusProps extends ComponentPropsWithRef<'span'> {
  variant?: StatusVariant;
  size?: StatusSize;
  stroke?: boolean;
  iconName?: IconName;
  iconType?: 'solid' | 'outline';
}
