import clsx from 'clsx';

import { getIconSize } from '../Chips/helper';
import { Icon } from '../Icon';
import { getStatusClasses } from './helper';
import { StatusProps } from './types';

export default function Status(props: StatusProps) {
  const {
    variant = 'primary',
    size = 'md',
    stroke = false,
    className,
    iconName,
    iconType = 'solid',
    children,
    ...restProps
  } = props;

  const classes = getStatusClasses({ variant, size, stroke });
  const iconSize = getIconSize(size);

  return (
    <span className={clsx(classes, className)} {...restProps}>
      {children}
      {!!iconName && <Icon iconName={iconName} size={iconSize} type={iconType} />}
    </span>
  );
}
