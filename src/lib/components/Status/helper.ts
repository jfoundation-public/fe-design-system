import clsx from 'clsx';

import { StatusProps, StatusSize } from './types';

const VARIANT_CLASSES = {
  success: 'status-success',
  warning: 'status-warning',
  error: 'status-error',
  done: 'status-done',
  canceled: 'status-canceled',
  primary: 'status-primary',
};

const SIZE_CLASSES = {
  sm: 'status-sm',
  md: 'status-md',
  lg: 'status-lg',
};

const ICON_SIZE = {
  sm: 12,
  md: 16,
  lg: 20,
};

const STROKE_CLASSES = {
  stroke: 'status-stroke',
  'none-stroke': 'status-none-stroke',
};

function getStatusClasses({
  variant,
  size,
  stroke,
}: Required<Pick<StatusProps, 'variant' | 'size' | 'stroke'>>) {
  return clsx(
    'status',
    VARIANT_CLASSES[variant],
    SIZE_CLASSES[size],
    STROKE_CLASSES[stroke ? 'stroke' : 'none-stroke'],
  );
}

function getStatusIconSize(size: StatusSize) {
  return ICON_SIZE[size];
}

export { getStatusClasses, getStatusIconSize };
