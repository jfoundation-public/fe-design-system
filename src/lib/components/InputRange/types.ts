import { ComponentPropsWithoutRef, ReactElement } from 'react';

import { FieldControlledProps } from '../FieldControlled';

export interface InputRangeBaseProps
  extends Omit<ComponentPropsWithoutRef<'input'>, 'value' | 'placeholder'> {
  value: Array<number | null>;
  placeholder?: [string, string];
}

export type InputRangeProps = FieldControlledProps &
  InputRangeBaseProps & {
    name: string;
    startAdornment?: ReactElement;
    endAdornment?: ReactElement;
  };
