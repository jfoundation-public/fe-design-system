import { FieldControlled } from '../FieldControlled';
import InputRangeBase from './InputRangeBase';
import { InputRangeProps } from './types';

export default function InputRange(props: InputRangeProps) {
  const {
    label,
    labelIcon,
    helperText,
    startAdornment,
    endAdornment,
    isWarning,
    isSuccess,
    isError,
    Container,
    ...restProps
  } = props;

  return (
    <FieldControlled
      label={label}
      labelIcon={labelIcon}
      helperText={helperText}
      endAdornment={endAdornment}
      startAdornment={startAdornment}
      isWarning={isWarning}
      isSuccess={isSuccess}
      isError={isError}
      {...Container}
    >
      <InputRangeBase {...restProps} />
    </FieldControlled>
  );
}
