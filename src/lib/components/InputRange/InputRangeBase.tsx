import { ChangeEvent, useEffect, useRef, useState } from 'react';

import { createCustomInputEvent, isEqual, isNumber } from '@/lib/utils';

import { IconButton } from '../Button';
import { withControlled } from '../FieldControlled';
import { Icon } from '../Icon';
import { InputBase } from '../InputBase';
import { InputRangeBaseProps } from './types';

function getValidValue(valStr: string) {
  if (valStr && isNumber(+valStr)) {
    return +valStr;
  }
  return null;
}

function InputRangeBase(props: InputRangeBaseProps) {
  const { value: valueProp, placeholder, disabled, readOnly, name, onChange, ...restProps } = props;
  const prevValueProp = useRef(valueProp);
  const [fromValue, setFromValue] = useState(String(valueProp?.[0] ?? ''));
  const [toValue, setToValue] = useState(String(valueProp?.[1] ?? ''));

  function handleChangeFromValue(e: ChangeEvent<HTMLInputElement>) {
    setFromValue(e.target.value);
  }

  function handleChangeToValue(e: ChangeEvent<HTMLInputElement>) {
    setToValue(e.target.value);
  }

  useEffect(() => {
    const fromValueNumber = getValidValue(fromValue);
    const toValueNumber = getValidValue(toValue);
    if (onChange) {
      onChange(createCustomInputEvent({ name, value: [fromValueNumber, toValueNumber] }));
    }
  }, [fromValue, name, onChange, toValue]);

  useEffect(() => {
    (function syncValueProp() {
      if (!isEqual(prevValueProp.current, valueProp)) {
        prevValueProp.current = valueProp;
        setFromValue(String(valueProp?.[0] ?? ''));
        setToValue(String(valueProp?.[1] ?? ''));
      }
    })();
  }, [valueProp]);

  const [fromPlaceholder, toPlaceholder] = placeholder ?? ['', ''];

  function getShowRemoveIcon(value: string) {
    if (!value) return false;
    return !(disabled || readOnly);
  }

  return (
    <div className='input-range'>
      <InputBase
        type='number'
        placeholder={fromPlaceholder}
        value={fromValue}
        onChange={handleChangeFromValue}
        disabled={disabled}
        readOnly={readOnly}
        className='input-range__from-input'
        endAdornment={
          getShowRemoveIcon(fromValue) ? (
            <IconButton
              type='button'
              isAdornment
              onMouseDown={(e) => {
                e.preventDefault();
                e.stopPropagation();
              }}
              onMouseUp={(e) => {
                e.preventDefault();
                e.stopPropagation();
                setFromValue('');
              }}
              iconName='failed-remove'
              iconVariant='solid'
              className='ml-2'
            />
          ) : undefined
        }
        {...restProps}
      />
      <Icon color='default' iconName='arrow_right' variant='outline' />
      <InputBase
        type='number'
        placeholder={toPlaceholder}
        value={toValue}
        onChange={handleChangeToValue}
        disabled={disabled}
        readOnly={readOnly}
        className='input-range__to-input'
        endAdornment={
          getShowRemoveIcon(toValue) ? (
            <IconButton
              type='button'
              isAdornment
              onMouseDown={(e) => {
                e.preventDefault();
                e.stopPropagation();
              }}
              onMouseUp={(e) => {
                e.preventDefault();
                e.stopPropagation();
                setToValue('');
              }}
              iconName='failed-remove'
              iconVariant='solid'
              className='ml-2'
            />
          ) : undefined
        }
        {...restProps}
      />
    </div>
  );
}

export default withControlled<Array<number | null>, InputRangeBaseProps>(InputRangeBase);
