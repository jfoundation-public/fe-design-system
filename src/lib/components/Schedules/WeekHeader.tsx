import { Locale } from '../DateTimePicker';
import { formatHeaderCellDate } from './utils';

interface WeekHeaderProps {
  date: Date;
  locale: Locale;
}

export default function WeekHeader(props: WeekHeaderProps) {
  const { date, locale } = props;
  const [headerDay, headerDate] = formatHeaderCellDate(date, locale);

  return (
    <div>
      <span className='subheading text-text-500'>{headerDay}</span>
      <h4 className='text-text-700 mt-2'>{headerDate}</h4>
    </div>
  );
}
