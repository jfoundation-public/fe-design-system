import { ComponentPropsWithoutRef, ReactElement } from 'react';
import { Options, WeekdayStr } from 'rrule';

import { Locale } from '../DateTimePicker';

export type ViewOption = 'day' | 'week' | 'month' | 'year';
export type Frequency = 'daily' | 'weekly' | 'monthly' | 'yearly';

export interface BaseScheduleEvent {
  id: string;
  startEventDate: Date;
  time: [Date, Date];
  title: string;
  recurring?: {
    frequency: Frequency;
    interval: number;
    count?: number;
    weekDay?: WeekdayStr[];
    endDate?: Date;
  } & Partial<Options>;
  recurringString?: string;
  priority: number;
}

export interface SchedulesProps<T extends BaseScheduleEvent>
  extends ComponentPropsWithoutRef<'div'> {
  events: T[];
  viewOption?: ViewOption;
  locale?: Locale;
  renderHeader: (props: { date: Date; locale: Locale }) => ReactElement;
  renderEventItems: (events: T[]) => ReactElement;
  handleEditEvent: (events: T) => void;
  handleDeleteEvent: (events: T) => void;
  handleDeleteEvents: (events: T[]) => void;
  handleAddEvent: () => void;
  handleEventClick: (eventClick: EventClick<BaseScheduleEvent>) => void;
  activeCell?: boolean;
  setActiveCell?: (value: boolean) => void;
}

export interface EventClick<T extends BaseScheduleEvent> {
  event?: T;
  time: [Date, Date];
}
