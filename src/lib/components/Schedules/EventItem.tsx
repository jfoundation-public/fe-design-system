import clsx from 'clsx';
import { ComponentPropsWithoutRef, useRef, useState } from 'react';

import { IconButton } from '../Button';
import { Icon } from '../Icon';
import { Popover, usePopover } from '../Popover';
import { Typography } from '../Typography';
import { BaseScheduleEvent, EventClick } from './types';
import { formatTimeStr, getFrequency, getTime } from './utils';

interface EventItemProps<T extends BaseScheduleEvent> extends ComponentPropsWithoutRef<'div'> {
  event: T;
  badge?: number;
  nestedEvents: T[];
  onEditEvent?: (events: T) => void;
  onDeleteEvent?: (events: T) => void;
  onDeleteEvents?: (events: T[]) => void;
  onEventClick: (eventClick: EventClick<T>) => void;
  isTilted?: boolean;
  containerClassnames?: string;
}

const CELL_HEIGHT = 60;

function calcMinutes(hours: number, minutes: number) {
  return hours * 60 + minutes;
}

export default function EventItem<T extends BaseScheduleEvent>(props: EventItemProps<T>) {
  const {
    event,
    badge = 0,
    nestedEvents,
    onEditEvent,
    onDeleteEvent,
    onDeleteEvents,
    onEventClick: _,
    isTilted,
    containerClassnames,
  } = props;

  const [currentEvent, setCurrentEvent] = useState<T>(event);

  const {
    onOpen: onOpenEventList,
    onClose: onCloseEventList,
    open: openEventList,
    ...popoverEventListProps
  } = usePopover('left');

  const {
    onOpen: onOpenEventDetails,
    onClose: onCloseEventDetails,
    open: openEventDetails,
    ...popoverEventDetailsProps
  } = usePopover('left');

  const {
    title,
    time: [fromTime, toTime],
    priority,
  } = event;
  const eventRef = useRef<HTMLDivElement>(null);

  const [fromHours, fromMinutes] = getTime(fromTime);
  const [toHours, toMinutes] = getTime(toTime);

  const fromTimeFormatted = formatTimeStr(fromHours, fromMinutes);
  const toTimeFormatted = formatTimeStr(toHours, toMinutes);

  const {
    time: [currentFromTime, currentToTime],
  } = currentEvent;
  const [currentFromHours, currentFromMinutes] = getTime(currentFromTime);
  const [currentToHours, currentToMinutes] = getTime(currentToTime);

  const currentFromTimeFormatted = formatTimeStr(currentFromHours, currentFromMinutes);
  const currentToTimeFormatted = formatTimeStr(currentToHours, currentToMinutes);

  const tiltSpacing = calcMinutes(fromHours, fromMinutes) / 60;

  const top = `${tiltSpacing * CELL_HEIGHT}px`;
  const height = `${
    ((calcMinutes(toHours, toMinutes) - calcMinutes(fromHours, fromMinutes)) * CELL_HEIGHT) / 60 - 1
  }px`;

  const left = `${isTilted ? (tiltSpacing % 1) * CELL_HEIGHT : 0}px`;
  const right = `-${isTilted ? (tiltSpacing % 1) * CELL_HEIGHT : 0}px`;

  function viewEvent(event: T) {
    setCurrentEvent(event);
    onCloseEventList();
    onOpenEventDetails(eventRef.current);
  }

  return (
    <div>
      <div
        className={clsx(
          'schedules__event-item',
          openEventDetails && 'bg-palette-primary1000 text-white',
          containerClassnames,
        )}
        style={{ top, height, left, right }}
        onClick={(e) => {
          if (!e.isDefaultPrevented()) {
            viewEvent(event);
          }
        }}
        role='presentation'
        ref={eventRef}
      >
        <div className='gap-2.5 w-full flex items-center relative'>
          <div className='absolute top-0 left-0 h-8 w-8 flex justify-center items-center rounded-full bg-surface-white text-primary caption1'>
            {priority}
          </div>
          <div className='w-full pl-10'>
            <Typography variant='caption1' className='text-inherit truncate'>
              {title}
            </Typography>
            <Typography variant='caption2' className='text-inherit truncate'>
              {fromTimeFormatted} - {toTimeFormatted}
            </Typography>
          </div>
        </div>
        {badge > 1 && (
          <span
            className={clsx('schedules__item-badge', openEventList && 'bg-palette-primary1000')}
            role='presentation'
            onClick={(e) => {
              e.preventDefault();
              onOpenEventList(eventRef.current);
            }}
          >
            {badge < 9 ? badge : `9+`}
          </span>
        )}
      </div>

      <Popover open={openEventList} onClose={onCloseEventList} {...popoverEventListProps}>
        <div className='schedules__event-list'>
          <div className='flex items-center justify-between'>
            <Typography variant='h5'>List schedule detail</Typography>
            <div className='space-x-2'>
              {onDeleteEvents && (
                <IconButton
                  isAdornment
                  iconName='delete'
                  iconVariant='outline'
                  onClick={() => onDeleteEvents(nestedEvents)}
                  className='rounded p-1 hover:bg-surface-primary4'
                />
              )}
              <IconButton
                isAdornment
                iconName='close-small'
                iconVariant='outline'
                onClick={onCloseEventList}
                className='rounded p-1 hover:bg-surface-primary4'
              />
            </div>
          </div>

          <div className='mt-6 space-y-4 max-h-[364px] scroll-bar'>
            {nestedEvents.map((event) => {
              const {
                id,
                time: [fromTime, toTime],
                title,
                priority,
              } = event;
              const [fromHours, fromMinutes] = getTime(fromTime);
              const [toHours, toMinutes] = getTime(toTime);

              const fromTimeFormatted = formatTimeStr(fromHours, fromMinutes);
              const toTimeFormatted = formatTimeStr(toHours, toMinutes);

              return (
                <div
                  key={id}
                  className='gap-2 w-full flex items-center cursor-pointer bg-surface-primary4 px-3 py-3.5 rounded hover:bg-surface-primary10'
                  role='presentation'
                  onClick={() => viewEvent(event)}
                >
                  <div className='h-8 w-8 flex justify-center items-center rounded-full bg-surface-white text-primary caption1'>
                    {priority}
                  </div>
                  <div className='max-w-[266px]'>
                    <Typography variant='caption1' className='text-primary truncate'>
                      {title}
                    </Typography>
                    <Typography variant='caption2' className='text-primary truncate'>
                      {fromTimeFormatted} - {toTimeFormatted}
                    </Typography>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </Popover>

      <Popover open={openEventDetails} onClose={onCloseEventDetails} {...popoverEventDetailsProps}>
        <div className='schedules__event-list'>
          <div className='flex items-center justify-between'>
            <Typography variant='h5' className='max-w-[208px] truncate'>
              {currentEvent.title}
            </Typography>
            <div className='space-x-2'>
              {onEditEvent && (
                <IconButton
                  isAdornment
                  iconName='edit'
                  iconVariant='outline'
                  onClick={() => onEditEvent(currentEvent)}
                  className='rounded p-1 hover:bg-surface-primary4'
                />
              )}
              {onDeleteEvent && (
                <IconButton
                  isAdornment
                  iconName='delete'
                  iconVariant='outline'
                  onClick={() => onDeleteEvent(currentEvent)}
                  className='rounded p-1 hover:bg-surface-primary4'
                />
              )}
              <IconButton
                isAdornment
                iconName='close-small'
                iconVariant='outline'
                onClick={onCloseEventDetails}
                className='rounded p-1 hover:bg-surface-primary4'
              />
            </div>
          </div>

          <div className='space-y-6'>
            <div className='gap-4 w-full flex items-center rounded mt-6'>
              <div className='h-8 w-8 flex justify-center items-center rounded-full text-primary caption1 bg-surface-primary4'>
                {currentEvent.priority}
              </div>

              <Typography variant='body' className='truncate'>
                Priority
              </Typography>
            </div>

            {(currentEvent.recurring || currentEvent.recurringString) && (
              <div className='gap-4 w-full flex items-center rounded'>
                <div className='h-8 w-8 flex justify-center items-center rounded-full text-primary caption1 bg-surface-primary4'>
                  <Icon iconName='calendar' size={16} />
                </div>
                <div>
                  <Typography
                    variant='subheading'
                    className='text-primary w-[280px] paragraph-clean line-2'
                  >
                    {getFrequency(currentEvent)}
                  </Typography>
                </div>
              </div>
            )}

            <div className='gap-4 w-full flex items-center rounded'>
              <div className='h-8 w-8 flex justify-center items-center rounded-full text-primary caption1 bg-surface-primary4'>
                <Icon iconName='time-clock' size={16} />
              </div>
              <div>
                <Typography variant='body' className='truncate'>
                  {currentFromTimeFormatted} - {currentToTimeFormatted}
                </Typography>
              </div>
            </div>
          </div>
        </div>
      </Popover>
    </div>
  );
}
