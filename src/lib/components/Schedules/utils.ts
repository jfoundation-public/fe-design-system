import { addDays, getDate, getDay, getHours, getMinutes, isSameDay, setDay } from 'date-fns';
import * as LocaleFns from 'date-fns/locale';
import { RRule } from 'rrule';

import { addLeadingZeros, ascend, prop, range, sort } from '@/lib/utils';

import { Locale } from '../DateTimePicker';
import { DAY_PER_WEEK, WEEK_START_DAY } from '../DateTimePicker/constants';
import { BaseScheduleEvent } from './types';

function getWeekStartDay(locale: Locale) {
  return WEEK_START_DAY[locale] ?? WEEK_START_DAY['enUS'];
}

function getStartDateOfWeek(viewDate: Date, locale: Locale) {
  const weekStartDay = getWeekStartDay(locale);
  return setDay(viewDate, weekStartDay, { weekStartsOn: weekStartDay });
}

function getWeekDates(viewDate: Date, locale: Locale) {
  const weekStartDate = getStartDateOfWeek(viewDate, locale);
  return range(0, DAY_PER_WEEK).map((diffDay) => {
    return addDays(weekStartDate, diffDay);
  });
}

function formatHeaderCellDate(date: Date, locale: Locale) {
  const fnsLocal = LocaleFns[locale];
  return [fnsLocal.localize?.day(getDay(date), { width: 'abbreviated' }), getDate(date)];
}

function get24hRange() {
  return range(0, 24).map((h) => `${addLeadingZeros(2)(h)}:00`);
}

function getEventOnDate<T extends BaseScheduleEvent>(events: T[]) {
  return (date: Date) => {
    const filtered = events.filter((evt) => {
      if (evt.recurring) {
        const { frequency, interval, count, endDate, weekDay, ...others } = evt.recurring;
        const FREQUENCIES = {
          daily: RRule.DAILY,
          weekly: RRule.WEEKLY,
          monthly: RRule.MONTHLY,
          yearly: RRule.YEARLY,
        };
        const rule = new RRule({
          freq: FREQUENCIES[frequency],
          interval,
          count,
          until: endDate,
          dtstart: evt.startEventDate,
          byweekday: weekDay,
          ...others,
        });
        return rule.all().some((d) => isSameDay(d, date));
      }
      if (evt.recurringString) {
        const rule = RRule.fromString(evt.recurringString);
        return rule.all().some((d) => isSameDay(d, date));
      }
      return isSameDay(evt.startEventDate, date);
    });
    return sort(ascend(prop('priority')), filtered);
  };
}

function getFrequency<T extends BaseScheduleEvent>(event: T): string {
  if (event.recurring) {
    const { frequency, interval, count, endDate, weekDay, ...other } = event.recurring;
    const FREQUENCIES = {
      daily: RRule.DAILY,
      weekly: RRule.WEEKLY,
      monthly: RRule.MONTHLY,
      yearly: RRule.YEARLY,
    };

    const rule = new RRule({
      freq: FREQUENCIES[frequency],
      interval,
      count,
      until: endDate,
      dtstart: event.startEventDate,
      byweekday: weekDay,
      ...other,
    });
    return rule.toText();
  }
  if (event.recurringString) {
    const rule = RRule.fromString(event.recurringString);

    return rule.toText();
  }

  return '';
}

function getTime(date: Date) {
  return [getHours(date), getMinutes(date)] as const;
}

function formatTimeStr(hours: number, minutes: number) {
  const addLeadingZero = addLeadingZeros(2);
  return `${addLeadingZero(hours)}:${addLeadingZero(minutes)}`;
}

export {
  formatHeaderCellDate,
  formatTimeStr,
  get24hRange,
  getEventOnDate,
  getFrequency,
  getStartDateOfWeek,
  getTime,
  getWeekDates,
};
