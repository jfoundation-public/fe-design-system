import clsx from 'clsx';
import { addWeeks, format, getWeekOfMonth } from 'date-fns';
import { useState } from 'react';

import { Button, IconButton } from '../Button';
import EventItem from './EventItem';
import { BaseScheduleEvent, SchedulesProps } from './types';
import ViewNav from './ViewNav';
import WeekView from './WeekView';

export default function Schedules<T extends BaseScheduleEvent>(props: SchedulesProps<T>) {
  const {
    events,
    viewOption = 'week',
    locale = 'enUS',
    renderHeader,
    className,
    handleEditEvent,
    handleDeleteEvent,
    handleDeleteEvents,
    handleEventClick,
    activeCell,
    ...restProps
  } = props;

  const [viewDate, setViewDate] = useState(new Date());

  function onNext() {
    switch (viewOption) {
      case 'week':
        setViewDate((prevDate) => addWeeks(prevDate, 1));

        break;

      default:
        break;
    }
  }

  function onPrev() {
    switch (viewOption) {
      case 'week':
        setViewDate((prevDate) => addWeeks(prevDate, -1));
        break;

      default:
        break;
    }
  }

  function onToday() {
    setViewDate(new Date());
  }

  return (
    <div className={clsx('schedules', className)} {...restProps}>
      <div className='flex justify-between mb-6 gap-4'>
        <ViewNav className='flex-1' view='week' />
        <div className='flex items-center gap-2'>
          <IconButton
            iconName='arrow-left'
            iconVariant='outline'
            variant='secondary'
            size='sm'
            className='border-lineBorder-borderDefault text-lineBorder-borderHover w-8 h-8'
            onClick={onPrev}
          />
          <p>
            Week {getWeekOfMonth(viewDate)} - {format(viewDate, 'MMMM y')}
          </p>
          <IconButton
            iconName='arrow-right'
            iconVariant='outline'
            variant='secondary'
            size='sm'
            className='border-lineBorder-borderDefault text-lineBorder-borderHover w-8 h-8'
            onClick={onNext}
          />
        </div>
        <div className='flex items-center'>
          <Button onClick={onToday} size='sm' variant='secondary' className='min-w-[40px]'>
            Today
          </Button>
        </div>
      </div>
      {viewOption === 'week' && (
        <WeekView
          activeCell={activeCell}
          onEventClick={handleEventClick}
          events={events}
          locale={locale}
          renderHeader={renderHeader}
          renderEventItems={(evts) => {
            return (
              <>
                {evts.map((evt) => {
                  const nestedEvent = evts.filter(
                    ({ time }) =>
                      time[0] >= evt.time[0] &&
                      time[0] <= evt.time[1] &&
                      time[1] >= evt.time[0] &&
                      time[1] <= evt.time[1],
                  );

                  const overlayEvents = evts.filter(
                    ({ time }) =>
                      time[0] <= evt.time[0] && time[1] <= evt.time[1] && time[1] > evt.time[0],
                  );

                  return (
                    <EventItem
                      key={evt.id}
                      event={evt}
                      nestedEvents={nestedEvent}
                      badge={nestedEvent.length}
                      isTilted={overlayEvents.length - 1 > 0}
                      onEditEvent={handleEditEvent}
                      onEventClick={handleEventClick}
                      onDeleteEvent={handleDeleteEvent}
                      onDeleteEvents={handleDeleteEvents}
                    />
                  );
                })}
              </>
            );
          }}
        />
      )}
    </div>
  );
}
