import clsx from 'clsx';
import { isEqual, set } from 'date-fns';
import { ComponentPropsWithoutRef, HTMLAttributes, ReactElement, useState } from 'react';

import { range } from '@/lib/utils';

import { Locale } from '../DateTimePicker';
import { DAY_PER_WEEK } from '../DateTimePicker/constants';
import { Icon } from '../Icon';
import { Typography } from '../Typography';
import { BaseScheduleEvent, EventClick } from './types';
import { get24hRange, getEventOnDate, getWeekDates } from './utils';

interface WeekViewProps<T extends BaseScheduleEvent> extends ComponentPropsWithoutRef<'div'> {
  events: T[];
  viewDate?: Date;
  locale: Locale;
  renderHeader: ({ date, locale }: { date: Date; locale: Locale }) => ReactElement;
  renderEventItems: (events: T[]) => ReactElement;
  onEventClick?: (eventClick: EventClick<T>) => void;
  activeCell?: boolean;
  disabled?: boolean;
  headerProps?: HTMLAttributes<HTMLTableRowElement>;
}

export default function WeekView<T extends BaseScheduleEvent>(props: WeekViewProps<T>) {
  const {
    events,
    locale,
    activeCell = false,
    viewDate = new Date(),
    renderHeader,
    onEventClick,
    renderEventItems,
    disabled,
    headerProps,
  } = props;

  const [date, setDate] = useState<Date>();
  const weekDates = getWeekDates(viewDate, locale);
  const range24h = get24hRange();

  const getEventOn = getEventOnDate(events);

  function renderColGroup() {
    return (
      <colgroup>
        {range(0, DAY_PER_WEEK).map((d) => (
          <col key={d} />
        ))}
      </colgroup>
    );
  }

  function handleEventClick(date: Date, time: string) {
    if (!onEventClick) return;
    setDate(set(date, { hours: parseInt(time.split(':').shift()!), minutes: 0 }));
    onEventClick({
      time: [
        set(date, { hours: parseInt(time.split(':').shift()!), minutes: 0 }),
        set(date, {
          hours: parseInt(time.split(':').shift()!) + 1,
          minutes: 0,
        }),
      ],
    });
  }

  function isActive(currentDate: Date): boolean {
    return !!activeCell && isEqual(currentDate, date!);
  }

  return (
    <table className='schedules__week'>
      <tbody>
        <tr
          {...(headerProps ?? {})}
          className={clsx('schedules__header-row', headerProps?.className)}
        >
          <td className='schedules__week-cell schedules__header-indent'>
            <div>
              <Typography variant='subheading' className='text-text-600'>
                Time
              </Typography>
            </div>
          </td>
          <td className='schedules__week-cell'>
            <div className='schedules__header-content-wrap'>
              <table>
                {renderColGroup()}
                <tbody>
                  <tr>
                    {weekDates.map((date) => {
                      return (
                        <td
                          key={date.toISOString()}
                          className={clsx(
                            'schedules__week-cell schedules__header-cell bg-surface-white',
                          )}
                        >
                          {renderHeader({ date, locale })}
                        </td>
                      );
                    })}
                  </tr>
                </tbody>
              </table>
            </div>
          </td>
        </tr>
        <tr>
          <td className='schedules__week-cell'>
            <div className='schedules__time-cells-wrap'>
              <table>
                <tbody>
                  {range24h.map((time) => (
                    <tr key={time}>
                      <td
                        className={clsx(
                          'schedules__week-cell schedules__time-cell',
                          time === '00:00' && 'hidden',
                        )}
                      >
                        <span className='text-text-500 body'>{time}</span>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </td>
          <td className='schedules__week-cell'>
            <div className='schedules__content-wrap'>
              <table>
                {renderColGroup()}
                <thead>
                  <tr>
                    {weekDates.map((date) => {
                      const eventOnDate = getEventOn(date);
                      const sortedEvent = eventOnDate.sort(
                        (curr, next) =>
                          curr.time[1].getHours() -
                          curr.time[0].getHours() -
                          (next.time[1].getHours() - next.time[0].getHours()),
                      );

                      return (
                        <td key={date.toISOString()} className='schedules__header-head-cell'>
                          {renderEventItems(sortedEvent)}
                        </td>
                      );
                    })}
                  </tr>
                </thead>
                <tbody>
                  {range24h.map((time) => {
                    return (
                      <tr key={time}>
                        {weekDates.map((d) => {
                          const date = set(d, {
                            hours: parseInt(time.split(':').shift()!),
                            minutes: 0,
                          });
                          return (
                            <td
                              role='presentation'
                              key={d.toISOString()}
                              className={clsx(
                                'schedules__week-cell schedules__content-cell',
                                isActive(date) && 'bg-palette-primary1000 text-white',
                                disabled && 'schedules_content-cell_disabled',
                              )}
                              onClick={disabled ? undefined : () => handleEventClick(d, time)}
                            >
                              <Icon
                                className={clsx(
                                  'schedules__content-icon',
                                  isActive(date)
                                    ? 'text-white flex items-center justify-center'
                                    : 'text-icon-primary',
                                )}
                                iconName='add'
                                variant='outline'
                                size={20}
                              />
                            </td>
                          );
                        })}
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </td>
        </tr>
      </tbody>
    </table>
  );
}
