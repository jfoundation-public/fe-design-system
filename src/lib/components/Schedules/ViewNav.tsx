import { ToggleBar, ToggleBarProps } from '../ToggleBar';
import { ViewOption } from './types';

interface ViewNavProps extends Partial<ToggleBarProps> {
  view: ViewOption;
}

const VIEW_OPTIONS: { value: ViewOption; label: string }[] = [
  { label: 'Day', value: 'day' },
  { label: 'Week', value: 'week' },
  { label: 'Month', value: 'month' },
  { label: 'Year', value: 'year' },
]; // @TO_DO: translation

export default function ViewNav(props: ViewNavProps) {
  const { view, className, options, ...restProps } = props;
  return (
    <ToggleBar
      {...restProps}
      className={className}
      options={options ?? VIEW_OPTIONS}
      value={view}
    />
  );
}
