function getFieldControlledClasses() {
  return 'field-controlled';
}

function getAdornmentWrapperClasses(type: 'start' | 'end') {
  if (type === 'start') {
    return 'start-adornment';
  }
  return 'end-adornment';
}

export { getAdornmentWrapperClasses, getFieldControlledClasses };
