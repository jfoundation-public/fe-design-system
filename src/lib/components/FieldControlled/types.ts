import {
  ChangeEventHandler,
  ComponentPropsWithoutRef,
  FocusEventHandler,
  PropsWithChildren,
  ReactElement,
  ReactNode,
} from 'react';

import { IconName } from '../Icon/types';

export interface WithControlledProps<T> {
  value?: T;
  onChange?: ChangeEventHandler<HTMLInputElement>;
  onBlur?: FocusEventHandler<HTMLInputElement>;
  onFocus?: FocusEventHandler<HTMLInputElement>;
  name?: string;
  readOnly?: boolean;
  disabled?: boolean;
}

export interface FieldControlledProps extends PropsWithChildren {
  Container?: ComponentPropsWithoutRef<'div'>;
  label?: ReactNode;
  required?: boolean;
  labelIcon?: IconName;
  helperText?: string;
  isError?: boolean;
  isSuccess?: boolean;
  isWarning?: boolean;
  startAdornment?: ReactElement;
  endAdornment?: ReactElement;
}
