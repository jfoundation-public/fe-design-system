import { ComponentType } from 'react';

import { useControlled } from './hooks';
import { WithControlledProps } from './types';

export default function withControlled<V, T extends WithControlledProps<V>>(
  Component: ComponentType<T>,
) {
  return function Comp(props: T) {
    const { name, onBlur, onChange, onFocus, disabled, readOnly } = props;
    const {
      onChange: handleChange,
      onFocus: handleFocus,
      onBlur: handleBlur,
    } = useControlled({ name, onBlur, onChange, onFocus, disabled, readOnly });
    return (
      <Component {...props} onBlur={handleBlur} onFocus={handleFocus} onChange={handleChange} />
    );
  };
}
