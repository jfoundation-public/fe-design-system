import { ChangeEvent, FocusEvent, useCallback, useEffect } from 'react';

import { useFieldControlledContext } from '../FieldInput/hooks';
import { WithControlledProps } from './types';

function useControlled<T>({
  name = '',
  onBlur,
  onChange,
  onFocus,
  disabled,
  readOnly,
}: Pick<
  WithControlledProps<T>,
  'name' | 'onBlur' | 'onChange' | 'onFocus' | 'disabled' | 'readOnly'
>) {
  const {
    setDisabled: syncDisabled,
    setFocused: syncFocused,
    setReadOnly: syncReadOnly,
  } = useFieldControlledContext();

  const handleFocus = useCallback(
    (e: FocusEvent<HTMLInputElement>) => {
      syncFocused(name, true);
      onFocus && onFocus(e);
    },
    [name, onFocus, syncFocused],
  );

  const handleBlur = useCallback(
    (e: FocusEvent<HTMLInputElement>) => {
      syncFocused(name, false);
      onBlur && onBlur(e);
    },
    [name, onBlur, syncFocused],
  );

  const handleChange = useCallback(
    (e: ChangeEvent<HTMLInputElement>) => {
      onChange && onChange(e);
    },
    [onChange],
  );

  useEffect(() => {
    syncDisabled(name, !!disabled);
    syncReadOnly(name, !!readOnly);
  }, [disabled, name, readOnly, syncDisabled, syncReadOnly]);

  return {
    onChange: handleChange,
    onFocus: handleFocus,
    onBlur: handleBlur,
  };
}

export { useControlled };
