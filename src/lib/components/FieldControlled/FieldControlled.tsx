import clsx from 'clsx';
import { cloneElement, isValidElement } from 'react';

import { FieldContainer } from '../FieldContainer';
import { getAdornmentWrapperClasses, getFieldControlledClasses } from './helper';
import { FieldControlledProps } from './types';

/**
 * Extends from FieldContainer. With start, end adornment
 */
export default function FieldControlled(props: FieldControlledProps) {
  const {
    label,
    required,
    labelIcon,
    helperText,
    startAdornment,
    endAdornment,
    Container,
    children,
    ...restProps
  } = props;
  const classes = getFieldControlledClasses();
  const startAdormentWrapperClasses = getAdornmentWrapperClasses('start');
  const endAdormentWrapperClasses = getAdornmentWrapperClasses('end');

  const renderStartAdornment = isValidElement(startAdornment)
    ? cloneElement(startAdornment as React.ReactElement<any>, {
        className: startAdormentWrapperClasses,
      })
    : null;

  const renderEndAdornment = isValidElement(endAdornment)
    ? cloneElement(endAdornment as React.ReactElement<any>, {
        className: endAdormentWrapperClasses,
      })
    : null;

  return (
    <FieldContainer
      label={label}
      required={required}
      labelIcon={labelIcon}
      helperText={helperText}
      className={clsx(classes, Container?.className)}
      {...restProps}
    >
      {renderStartAdornment}
      {children}
      {renderEndAdornment}
    </FieldContainer>
  );
}
