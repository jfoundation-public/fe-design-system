import clsx from 'clsx';

import { IconButton } from '../Button';
import { CountInputProps } from './types';

/**
 * Count input component (extends from HTMLInputElement)
 */
export default function CountInput(props: CountInputProps) {
  const { onIncrease, onDecrease, increaseDisabled, decreaseDisabled, className, ...restProps } =
    props;
  return (
    <div className={clsx('count-input', className)}>
      <div className={clsx('count-input__minus', decreaseDisabled && 'bg-[#f8f8fa]')}>
        <IconButton
          iconSize={16}
          iconName='minus'
          iconVariant='outline'
          isAdornment
          onClick={onDecrease}
          disabled={decreaseDisabled}
        />
      </div>
      <input className='count-input__input' type='number' {...restProps} />
      <div className={clsx('count-input__plus', increaseDisabled && 'bg-[#f8f8fa]')}>
        <IconButton
          iconSize={16}
          iconName='add'
          iconVariant='outline'
          isAdornment
          onClick={onIncrease}
          disabled={increaseDisabled}
        />
      </div>
    </div>
  );
}
