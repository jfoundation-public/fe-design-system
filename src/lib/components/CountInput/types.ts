import { ComponentPropsWithoutRef } from 'react';

export interface CountInputProps extends ComponentPropsWithoutRef<'input'> {
  onIncrease?: () => void;
  onDecrease?: () => void;
  increaseDisabled?: boolean;
  decreaseDisabled?: boolean;
}
