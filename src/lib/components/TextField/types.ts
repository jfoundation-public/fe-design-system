import { ChangeEventHandler, FocusEventHandler, ReactElement } from 'react';

import { SelectOption } from '@/lib/types';

import { FieldContainerProps } from '../FieldContainer/types';
import { HelperTextProps } from '../HelperText';

export interface TextFieldProps extends Omit<FieldContainerProps, 'state'> {
  value?: string;
  onChange?: ChangeEventHandler<HTMLInputElement>;
  onBlur?: FocusEventHandler<HTMLInputElement>;
  onFocus?: FocusEventHandler<HTMLInputElement>;
  selectValue?: string;
  onChangeSelect?: ChangeEventHandler<HTMLInputElement>;
  onBlurSelect?: FocusEventHandler<HTMLInputElement>;
  onFocusSelect?: FocusEventHandler<HTMLInputElement>;
  selectOptions?: SelectOption[];
  hasSearch?: boolean;
  searchFunc?: (option: SelectOption, search: string) => boolean;
  name: string;
  placeholder?: string;
  readOnly?: boolean;
  disabled?: boolean;
  startAdornment?: ReactElement;
  endAdornment?: ReactElement;
  type?: 'text' | 'password';
  format?: (val: string | undefined) => string | undefined;
  parse?: (val: string | undefined) => string | undefined;
  helperTextProps?: Omit<HelperTextProps, 'state'>;
}
