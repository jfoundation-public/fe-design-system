function getTextFieldClasses() {
  return 'text-field';
}

function getAdornmentWrapperClasses(type: 'start' | 'end') {
  if (type === 'start') {
    return 'start-adornment';
  }
  return 'end-adornment';
}

export { getAdornmentWrapperClasses, getTextFieldClasses };
