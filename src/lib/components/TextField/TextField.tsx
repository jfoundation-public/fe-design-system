import clsx from 'clsx';
import { cloneElement, isValidElement } from 'react';

import { ControlledInput } from '../ControlledInput';
import { FieldContainer } from '../FieldContainer';
import { ControlledSelect } from '../SelectBase';
import { getAdornmentWrapperClasses, getTextFieldClasses } from './helper';
import { TextFieldProps } from './types';

export default function TextField(props: TextFieldProps) {
  const {
    value,
    onChange,
    onBlur,
    onFocus,
    selectValue,
    onChangeSelect,
    onBlurSelect,
    onFocusSelect,
    selectOptions,
    hasSearch,
    searchFunc,
    name,
    readOnly,
    disabled,
    label,
    labelIcon,
    helperText,
    className,
    placeholder,
    startAdornment,
    endAdornment,
    onKeyDown,
    onKeyUp,
    type,
    format,
    parse,
    ...restProps
  } = props;
  const classes = getTextFieldClasses();
  const startAdormentWrapperClasses = getAdornmentWrapperClasses('start');
  const endAdormentWrapperClasses = getAdornmentWrapperClasses('end');

  const renderStartAdornment = isValidElement(startAdornment)
    ? cloneElement(startAdornment as React.ReactElement<any>, {
        className: startAdormentWrapperClasses,
      })
    : null;

  const renderEndAdornment = isValidElement(endAdornment)
    ? cloneElement(endAdornment as React.ReactElement<any>, {
        className: endAdormentWrapperClasses,
      })
    : null;

  return (
    <FieldContainer
      label={label}
      labelIcon={labelIcon}
      helperText={helperText}
      className={clsx(classes, className)}
      {...restProps}
    >
      {renderStartAdornment}
      <ControlledInput
        readOnly={readOnly}
        disabled={disabled}
        name={name}
        value={value}
        onChange={onChange}
        onBlur={onBlur}
        onFocus={onFocus}
        placeholder={placeholder}
        type={type}
        onKeyDown={onKeyDown}
        onKeyUp={onKeyUp}
        format={format}
        parse={parse}
      />
      {selectOptions && (
        <>
          <hr className='vertical-divider' />
          <ControlledSelect
            readOnly={readOnly}
            disabled={disabled}
            name={`${name}-select`}
            value={selectValue ?? ''}
            onChange={onChangeSelect}
            onFocus={onFocusSelect}
            onBlur={onBlurSelect}
            options={selectOptions}
            hasSearch={hasSearch}
            searchFunc={searchFunc}
          />
        </>
      )}

      {renderEndAdornment}
    </FieldContainer>
  );
}
