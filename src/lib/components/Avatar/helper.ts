export const mappingAvatarStyle = {
  32: {
    img: 'h-[32px]',
    name: 'text-xs',
    subName: 'text-xs',
    avatarClassName: 'flex-row items-center',
    detailClassName: 'text-start',
    textClassName: 'h-[32px] w-[32px] text-xs',
  },
  40: {
    img: 'h-[40px]',
    name: 'text-sm',
    subName: 'text-xs',
    avatarClassName: 'flex-row items-center',
    detailClassName: 'text-start',
    textClassName: 'h-[40px] w-[40px] text-lg',
  },
  56: {
    img: 'h-[56px]',
    name: 'text-sm',
    subName: 'text-sm',
    avatarClassName: 'flex-row items-center',
    detailClassName: 'text-start',
    textClassName: 'h-[56px] w-[56px] text-2xl',
  },
  72: {
    img: 'h-[72px]',
    name: 'text-lg',
    subName: 'text-sm',
    avatarClassName: 'flex-row items-center',
    detailClassName: 'text-start',
    textClassName: 'h-[72px] w-[72px] text-3xl',
  },
  96: {
    img: 'h-[96px]',
    name: 'text-2xl',
    subName: 'text-sm',
    avatarClassName: 'flex-col items-center text-center',
    detailClassName: 'text-center',
    textClassName: 'h-[96px] w-[96px] text-4xl',
  },
  120: {
    img: 'h-[120px]',
    name: 'text-3xl',
    subName: 'text-lg',
    avatarClassName: 'flex-col items-center text-center',
    detailClassName: 'text-center',
    textClassName: 'h-[120px] w-[120px] text-5xl',
  },
};

export const getFirstLetterFromName = (name: string) => {
  // eslint-disable-next-line no-useless-escape
  const specialChars = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
  const text = name.match(/\b(\w)/g);
  if (!name) return '';

  if (specialChars.test(name)) return name;

  return text;
};
