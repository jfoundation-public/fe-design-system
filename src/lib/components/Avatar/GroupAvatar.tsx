import clsx from 'clsx';
import { Fragment, useCallback, useState } from 'react';

import Avatar from './Avatar';
import { AvatarType, GroupAvatarProps } from './types';

const GroupAvatar = (props: GroupAvatarProps) => {
  const { avatars } = props;
  const [expand, setExpand] = useState<boolean>(false);
  const groupLength = avatars.length;

  const handleExpand = useCallback(() => {
    if (groupLength < 3) return;
    setExpand((prev: boolean) => !prev);
  }, [setExpand, groupLength]);

  return (
    <div className='avatar-group'>
      {avatars.map((item: AvatarType, index: number) => {
        const groupLengthAbove5AndSelectIndex5 = groupLength > 5 && index === 5;
        const groupLengthAbove3AndSelectIndex2 = groupLength > 3 && index === 2;
        const groupLengthAbove3 = groupLength > 3;
        const groupLengthAt = groupLength === 3 || groupLength === 2 || groupLength === 1;

        return (
          <Fragment key={item.id}>
            <Avatar
              onClick={handleExpand}
              className={clsx(
                '',
                !groupLengthAbove3AndSelectIndex2 &&
                  !groupLengthAbove5AndSelectIndex5 &&
                  'flex-wrap',

                groupLengthAt && 'avatar-group-item-collapse-below-3 absolute',
                groupLengthAbove3 && expand && 'avatar-group-item-expand',
                groupLengthAbove3 && !expand && 'avatar-group-item-collapse absolute',
              )}
              hideNames
              key={item.id}
              size={item.size}
              icon={item.icon}
              src={item.src}
              type={groupLengthAbove3AndSelectIndex2 ? 'text' : item.type}
              textColor={item.textColor}
              iconColor={item.iconColor}
              name={groupLengthAbove3AndSelectIndex2 && !expand ? `${groupLength - 2}+` : item.name}
            />
            {groupLengthAbove5AndSelectIndex5 && expand && (
              <div className='basis-full w-0 h-0 overflow-hidden'></div>
            )}
          </Fragment>
        );
      })}
    </div>
  );
};

export default GroupAvatar;
