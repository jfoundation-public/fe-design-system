import { HTMLAttributes, ReactNode } from 'react';

import { IconName } from '../Icon/types';
export interface AvatarProps {
  size?: 32 | 40 | 56 | 72 | 96 | 120;
  type: 'img' | 'text' | 'icon';
  src?: string;
  icon?: IconName;
  children?: React.ReactElement;
  name?: string;
  subName?: string;
  imgClassName?: string;
  className?: string;
  textColor?: string;
  badgeContent?: ReactNode;
  iconColor?: string;
  onClick?: () => void;
  hideNames?: boolean;
  imgProps?: HTMLAttributes<HTMLImageElement>;
}

export interface AvatarType {
  id: string;
  size: 32 | 40 | 56 | 72 | 96 | 120;
  type: 'img' | 'icon' | 'text';
  src?: string;
  icon?: IconName;
  name?: string;
  textColor?: string;
  iconColor?: string;
}
export interface GroupAvatarProps {
  avatars: AvatarType[];
}
