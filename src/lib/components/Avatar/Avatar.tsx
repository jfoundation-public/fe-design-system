/* eslint-disable no-useless-escape */
import clsx from 'clsx';
import { isValidElement } from 'react';

import { Icon } from '../Icon';
import { getFirstLetterFromName, mappingAvatarStyle } from './helper';
import { AvatarProps } from './types';

const Avatar = (props: AvatarProps) => {
  const {
    size = 32,
    src,
    icon,
    name = '',
    type,
    subName = '',
    className = '',
    imgClassName: _imgClassName = '',
    textColor = '',
    badgeContent,
    iconColor = '',
    hideNames,
    onClick,
    imgProps,
  } = props;

  const isImg = type === 'img';
  const isIcon = type === 'icon';
  const isText = type === 'text';

  const imgClassName = clsx('img-avatar', _imgClassName, mappingAvatarStyle[size].img);

  const nameClassName = clsx('avatar-name', mappingAvatarStyle[size].name);

  const subClassName = clsx('avatar-subName', mappingAvatarStyle[size].subName);

  const avatarClassName = clsx('avatar', className, mappingAvatarStyle[size].avatarClassName);

  const detailClassName = clsx('avatar-detail', mappingAvatarStyle[size].detailClassName);

  const textClassName = clsx(
    'text-avatar',
    textColor,
    mappingAvatarStyle[size].textClassName,
    imgClassName,
  );

  return (
    <span role={'presentation'} onClick={onClick} className={avatarClassName}>
      {isImg && (
        <img
          width={size}
          height={size}
          className={imgClassName}
          alt='avatarImg'
          src={src}
          {...(imgProps ?? {})}
        />
      )}
      {isIcon && icon && (
        <Icon className={clsx('icon-avatar', iconColor)} iconName={icon} size={size} />
      )}
      {isText && (
        <span className={textClassName}>
          <span className='text-avatar-content'>{getFirstLetterFromName(name)}</span>
        </span>
      )}
      {!hideNames && (
        <div className={detailClassName}>
          <span className={nameClassName}>{name}</span>
          <span className={subClassName}>{subName}</span>
        </div>
      )}
      {isValidElement(badgeContent) && badgeContent}
    </span>
  );
};

export default Avatar;
