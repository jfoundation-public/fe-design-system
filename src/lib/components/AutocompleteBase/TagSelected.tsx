import { clone } from '../../utils';
import { Chips } from '../Chips';
import { TagSelectedProps } from './types';

export default function TagSelected(props: TagSelectedProps) {
  const { tags, onRemoveTag, maxTagsDisplayLength } = props;
  if (!tags || !tags.length) return null;
  const remainTags = clone(tags);
  const showingTag = remainTags.splice(0, maxTagsDisplayLength ?? 2);
  return (
    <>
      {showingTag.map((tag) => (
        <Chips
          key={tag?.value}
          onRemove={() => onRemoveTag(tag?.value)}
          tagName={tag?.label ?? ''}
          className='whitespace-nowrap'
        />
      ))}
      {!!remainTags.length && <Chips tagName={`+${remainTags.length}`} />}
    </>
  );
}
