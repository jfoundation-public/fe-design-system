import { ComponentPropsWithRef, ReactElement } from 'react';

import { SelectOption } from '@/lib/types';

export interface AutocompleteBaseProps extends Omit<ComponentPropsWithRef<'input'>, 'value'> {
  name: string;
  options?: SelectOption[];
  isMultiple?: boolean;
  moreSelectItem?: (props: { onClose: () => void }) => ReactElement;
  value?: string[] | null;
}

export interface TagSelectedProps {
  tags?: SelectOption[] | null;
  onRemoveTag: (id: string) => void;
  maxTagsDisplayLength?: number;
}
