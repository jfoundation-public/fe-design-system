import {
  autoUpdate,
  flip,
  FloatingOverlay,
  FloatingPortal,
  offset,
  size,
  useDismiss,
  useFloating,
  useInteractions,
  useListNavigation,
  useRole,
} from '@floating-ui/react-dom-interactions';
import { useEffect, useLayoutEffect, useMemo, useRef, useState } from 'react';

import useFilterOptions from '@/lib/hooks/useFilterOptions';
import { SelectOption } from '@/lib/types';
import { createCustomInputEvent, isEqual, isNull, isUndefined, noop } from '@/lib/utils';

import { IconButton } from '../Button';
import { useFieldControlElement } from '../FieldInput/hooks';
import { InputBase } from '../InputBase';
import { SelectItem } from '../SelectItem';
import TagSelected from './TagSelected';
import { AutocompleteBaseProps } from './types';

function getOptionSelected(options?: SelectOption[], value?: string[] | null) {
  if (isUndefined(value) || isNull(value)) return [];
  if (!options) return [];
  return value.map((val) => options.find((opt) => opt.value === val));
}

const EMPTY_VALUE: string[] = [];

export default function AutocompleteBase(props: AutocompleteBaseProps) {
  const {
    value,
    name,
    onChange,
    onFocus,
    onBlur,
    placeholder,
    options,
    isMultiple,
    autoFocus,
    moreSelectItem: MoreSelectItem,
  } = props;
  const prevValue = useRef(value);
  const prevCurrentValue = useRef(value ?? EMPTY_VALUE);

  const containerElement = useFieldControlElement();
  const [currentValue, setCurrentValue] = useState(value ?? EMPTY_VALUE);
  const [open, setOpen] = useState(false);
  const [inputValue, setInputValue] = useState('');
  const [activeIndex, setActiveIndex] = useState<number | null>(null);
  const [focused, setFocused] = useState(autoFocus);

  const listRef = useRef<Array<HTMLElement | null>>([]);

  const { x, y, reference, floating, strategy, context } = useFloating({
    whileElementsMounted: autoUpdate,
    open,
    onOpenChange: setOpen,
    placement: 'bottom-start',
    middleware: [
      offset(5),
      flip({ padding: 8 }),
      size({
        apply({ rects, availableHeight, elements }) {
          Object.assign(elements.floating.style, {
            width: `${rects.reference.width}px`,
            maxHeight: `${availableHeight}px`,
          });
        },
        padding: 8,
      }),
    ],
  });

  const { getReferenceProps, getFloatingProps, getItemProps } = useInteractions([
    useRole(context, { role: 'listbox' }),
    useDismiss(context),
    useListNavigation(context, {
      listRef,
      activeIndex,
      onNavigate: setActiveIndex,
      virtual: true,
      loop: true,
    }),
  ]);

  function handleSelect(_item: SelectOption) {
    setCurrentValue((prev) => {
      if (!isMultiple) return [_item.value];
      return prev?.includes(_item.value)
        ? prev.filter((p) => p !== _item.value)
        : [...prev, _item.value];
    });
    setInputValue('');
    !isMultiple && setOpen(false);
  }

  function handleInputChange(event: React.ChangeEvent<HTMLInputElement>) {
    const value = event.target.value;
    setInputValue(value);

    if (value) {
      setOpen(true);
    } else {
      setOpen(false);
    }
  }

  function handleClear() {
    setInputValue('');
    setCurrentValue([]);
  }

  function handleFocus(e: React.FocusEvent<HTMLInputElement>) {
    setFocused(true);
    onFocus && onFocus(e);
  }

  function handleBlur(e: React.FocusEvent<HTMLInputElement>) {
    setTimeout(() => {
      setFocused(false);
      onBlur && onBlur(e);
    }, 150);
  }

  useLayoutEffect(() => {
    // IMPORTANT: When the floating element first opens, this runs when the
    // styles have **not yet** been applied to the element. This can cause an
    // infinite loop as `size` has not yet limited the maxHeight, so the whole
    // page tries to scroll. We must wrap it in rAF.
    requestAnimationFrame(() => {
      if (activeIndex != null) {
        listRef.current[activeIndex]?.scrollIntoView({ block: 'nearest' });
      }
    });
  }, [activeIndex]);

  useEffect(() => {
    if (containerElement) {
      reference({
        getBoundingClientRect: () => containerElement!.getBoundingClientRect(),
        contextElement: containerElement!,
      });
    }
  }, [containerElement, reference]);

  useEffect(() => {
    if (!isEqual(prevCurrentValue.current, currentValue)) {
      prevCurrentValue.current = currentValue;
      onChange && onChange(createCustomInputEvent({ name, value: currentValue }));
    }
  }, [currentValue, name, onChange]);

  (function syncValue() {
    if (!isEqual(prevValue.current, value)) {
      prevValue.current = value;
      setCurrentValue(value ?? []);
    }
  })();

  const deferredOptions = useFilterOptions({ options, searchParam: inputValue });
  const deferredChips = useMemo(
    () => getOptionSelected(options, currentValue).filter(Boolean) as SelectOption[],
    [currentValue, options],
  );
  const showClear = focused && Boolean(inputValue || currentValue.length);

  return (
    <>
      <TagSelected
        tags={deferredChips}
        onRemoveTag={(id) => {
          setCurrentValue((prev) => prev.filter((p) => p !== id));
        }}
      />

      <InputBase
        {...getReferenceProps({
          ref: reference,
          onChange: handleInputChange,
          value: inputValue,
          placeholder,
          'aria-autocomplete': 'list',
          onKeyDown(event) {
            if (event.key === 'Tab') {
              setOpen(false);
            }
          },
        })}
        onFocus={handleFocus}
        onBlur={handleBlur}
        endAdornment={
          showClear ? (
            <IconButton
              type='button'
              isAdornment
              onMouseDown={(e) => {
                e.preventDefault();
                e.stopPropagation();
              }}
              onMouseUp={(e) => {
                e.preventDefault();
                e.stopPropagation();
                handleClear();
              }}
              iconName='failed-remove'
            />
          ) : undefined
        }
      />

      <FloatingPortal id='portal-root'>
        {open && (
          <FloatingOverlay className='z-picker' lockScroll>
            <div
              {...getFloatingProps({
                ref: floating,
                style: {
                  position: strategy,
                  left: x ?? 0,
                  top: y ?? 0,
                  overflow: 'auto',
                },
              })}
            >
              <div className='paper'>
                {deferredOptions.length ? (
                  deferredOptions.map((item) => (
                    <SelectItem
                      key={item.value}
                      {...getItemProps()}
                      value={item.value}
                      onSelect={() => {
                        handleSelect(item);
                      }}
                      withCheck={isMultiple}
                      selected={currentValue.includes(item.value)}
                    >
                      {item.label}
                    </SelectItem>
                  ))
                ) : (
                  <SelectItem onSelect={noop} key='empty' value='' disabled>
                    No options
                  </SelectItem>
                )}
                {MoreSelectItem && (
                  <MoreSelectItem
                    onClose={() => {
                      setOpen(false);
                    }}
                  />
                )}
              </div>
            </div>
          </FloatingOverlay>
        )}
      </FloatingPortal>
    </>
  );
}
