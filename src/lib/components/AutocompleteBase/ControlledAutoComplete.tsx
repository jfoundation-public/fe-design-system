import { withControlled } from '../FieldControlled';
import AutocompleteBase from './AutocompleteBase';
import { AutocompleteBaseProps } from './types';

export default withControlled<string[] | null, AutocompleteBaseProps>(AutocompleteBase);
