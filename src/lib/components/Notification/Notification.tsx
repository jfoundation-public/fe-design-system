import { IconButton } from '../Button';
import { Icon } from '../Icon';
import { Typography } from '../Typography';
import { NotificationProps } from './types';

export default function Notification(props: NotificationProps) {
  const {
    iconName,
    iconClasses,
    title,
    description,
    onClose,
    children,
    iconVariant,
    ...restProps
  } = props;

  return (
    <div className='notification' {...restProps}>
      <div className='notification--header'>
        <Icon
          iconName={iconName}
          size={24}
          variant={iconVariant ?? 'solid'}
          className={iconClasses}
        />
        <Typography className='notification--header--title' variant='subtitle1'>
          {title}
        </Typography>
        <IconButton
          isAdornment
          iconName='close-small'
          iconSize={24}
          iconVariant='outline'
          className='notification--header--close-btn'
          onClick={onClose}
        />
      </div>
      <div className='notification--description'>{description}</div>
      <div className='notification--action'>{children}</div>
    </div>
  );
}
