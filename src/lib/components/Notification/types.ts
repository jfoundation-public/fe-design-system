import { ComponentPropsWithoutRef } from 'react';

import { IconName, IconVariant } from '../Icon/types';

export interface NotificationProps extends ComponentPropsWithoutRef<'div'> {
  iconName: IconName;
  iconVariant?: IconVariant;
  iconClasses?: string;
  title: string;
  onClose?: () => void;
  description: string;
}
