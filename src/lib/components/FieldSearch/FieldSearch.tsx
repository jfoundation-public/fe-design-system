import clsx from 'clsx';

import { ControlledInput } from '../ControlledInput';
import { FieldInput } from '../FieldInput';
import { Icon } from '../Icon';
import { FieldSearchProps } from './types';

export default function FieldSearch(props: FieldSearchProps) {
  const {
    name,
    noOutline,
    fullWidth,
    className,
    value,
    onChange,
    onBlur,
    autoFocus,
    onKeyDown,
    onKeyUp,
    onFocus,
  } = props;
  return (
    <FieldInput className={clsx(!fullWidth && 'w-[360px]', className)} isNoneOutline={noOutline}>
      <Icon color='default' iconName='search' variant='outline' />
      <ControlledInput
        name={name}
        placeholder='Search'
        type='search'
        value={value ?? ''}
        onChange={onChange}
        onBlur={onBlur}
        // eslint-disable-next-line jsx-a11y/no-autofocus
        autoFocus={autoFocus}
        onKeyDown={onKeyDown}
        onKeyUp={onKeyUp}
        onFocus={onFocus}
      />
    </FieldInput>
  );
}
