import { ChangeEventHandler, FocusEventHandler } from 'react';

import { FieldInputProps } from '../FieldInput/types';

export interface FieldSearchProps extends FieldInputProps {
  fullWidth?: boolean;
  noOutline?: boolean;
  value?: string;
  name: string;
  onChange?: ChangeEventHandler<HTMLInputElement>;
  onFocus?: FocusEventHandler<HTMLInputElement>;
  onBlur?: FocusEventHandler<HTMLInputElement>;
  autoFocus?: boolean;
}
