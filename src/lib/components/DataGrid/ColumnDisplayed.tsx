import { useEffect, useRef, useState } from 'react';
import { DndProvider, useDrag, useDrop } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';

import { isEmpty, isNil } from '@/lib';

import { Button } from '../Button';
import { Checkbox } from '../Checkbox';
import { Icon } from '../Icon';
import { Popover, usePopover } from '../Popover';
import { ColumnDisplayedItemProps, ColumnDisplayedProps, VisibilityState } from './types';

function ColumnDisplayedItem({
  id,
  label,
  checked,
  onChange,
  onOrdering,
}: ColumnDisplayedItemProps) {
  const ref = useRef<HTMLDivElement>(null);

  const [, dropRef] = useDrop<{ id: string }>({
    accept: 'columnItem',

    drop(item) {
      if (!ref.current) {
        return;
      }
      const dragId = item.id;
      const hoverId = id;

      // Don't replace items with themselves
      if (dragId === hoverId) {
        return;
      }

      onOrdering && onOrdering(dragId, hoverId);
    },
  });
  const [, dragRef] = useDrag({
    type: 'columnItem',
    item: { id },
  });
  dragRef(dropRef(ref));

  return (
    <div ref={ref} className='data-grid__column-displayed-item'>
      <Icon size={24} iconName='drag' variant='outline' />
      <Checkbox className='ml-3' checked={checked} onChange={onChange}>
        {label}
      </Checkbox>
    </div>
  );
}

/**
 * Controll columns display of DataGrid. Show/hide, change columns order
 */
export default function ColumnDisplayed<T>(props: ColumnDisplayedProps<T>) {
  const {
    columnNames,
    table,
    columnVisibility,
    onColumnVisibilityChange,
    columnOrder,
    onColumnOrderChange,
    startIcon = 'column',
    label = 'Columns displayed',
    className,
    hideColumnsId = [''],
  } = props;
  const { onOpen, ...popoverProps } = usePopover('bottom-start');
  const [columnList, setColumnList] = useState<string[]>([]);
  function getIsVisibility(
    colId: string,
    _visibilityRecord: VisibilityState | undefined = columnVisibility,
  ) {
    return (
      isNil(columnVisibility) ||
      isNil(columnVisibility?.[colId]) ||
      Boolean(columnVisibility[colId])
    );
  }

  function handleOrder(dragId: string, targetId: string) {
    const _columnOrder =
      isNil(columnOrder) || isEmpty(columnOrder)
        ? table?.getAllLeafColumns().map((col) => col.id)
        : columnOrder;
    const cloneColumnOrder = _columnOrder ? _columnOrder.slice() : [];
    const newColumnOrder = cloneColumnOrder.filter((_id) => _id !== dragId);
    newColumnOrder.splice(
      cloneColumnOrder.findIndex((_id) => _id === targetId),
      0,
      dragId,
    );
    onColumnOrderChange && onColumnOrderChange(newColumnOrder);
  }

  useEffect(() => {
    if (table && table.getAllColumns())
      setColumnList(table.getAllLeafColumns().map((col) => col.id));
  }, [columnOrder, table?.getAllLeafColumns()]);

  return (
    <div role='presentation' className={className}>
      <Button
        onClick={onOpen}
        variant='blank'
        leftIcon={<Icon iconName={startIcon} variant='solid' fill='none' />}
        className={popoverProps.open ? 'bg-surface-primary4' : ''}
      >
        {label}
      </Button>
      <Popover {...popoverProps}>
        <div className='paper data-grid__column-displayed-container'>
          <DndProvider backend={HTML5Backend}>
            {columnList &&
              columnList
                .filter((item) => !hideColumnsId.includes(item))
                .map((colId) => {
                  return (
                    <ColumnDisplayedItem
                      key={colId}
                      id={colId}
                      label={columnNames[colId] ?? ''}
                      checked={getIsVisibility(colId)} // columnVisibility is undefined which is currently showing
                      onChange={() => {
                        onColumnVisibilityChange &&
                          onColumnVisibilityChange((prev) => ({
                            ...prev,
                            [colId]: !getIsVisibility(colId, prev),
                          }));
                      }}
                      onOrdering={(dragId, targetId) => handleOrder(dragId, targetId)}
                    />
                  );
                })}
          </DndProvider>
        </div>
      </Popover>
    </div>
  );
}
