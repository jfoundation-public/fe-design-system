/* eslint-disable jsx-a11y/click-events-have-key-events */
import {
  flexRender,
  getCoreRowModel,
  getExpandedRowModel,
  getSortedRowModel,
  Row,
  SortDirection,
  useReactTable,
} from '@tanstack/react-table';
import clsx from 'clsx';
import { ComponentPropsWithRef, FC, useEffect } from 'react';
import { DndProvider, useDrag, useDrop } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';

import { isNil } from '@/lib/utils';

import { Icon } from '../Icon';
import { getAlignmentClass } from './helper';
import TablePagination from './TablePagination';
import { DataGridProps } from './types';

const Table = (props: ComponentPropsWithRef<'table'>) => <table {...props} />;
const TableBody = (props: ComponentPropsWithRef<'tbody'>) => <tbody {...props} />;
const TableHead = (props: ComponentPropsWithRef<'thead'>) => <thead {...props} />;
const TableHeader = (props: ComponentPropsWithRef<'th'>) => <th {...props} />;
const TableRow = (props: ComponentPropsWithRef<'tr'>) => <tr {...props} />;
const TableDataCell = (props: ComponentPropsWithRef<'td'>) => <td {...props} />;

/**
 * TanStack React table. Support Drag and Drop, Sub rows
 */
export default function DataGrid<T = Record<string, any>>(props: DataGridProps<T>) {
  const {
    data,
    onDataStateChange,
    columns,
    className,
    tableClassName,
    onTableStateChange,
    rowSelection,
    onRowSelectionChange,
    getSubRows,
    expanded,
    onExpandedChange,
    sorting,
    onSortingChange,
    columnVisibility,
    onColumnVisibilityChange,
    columnOrder,
    onColumnOrderChange,
    pagination,
    isDraggable = false,
    renderNoData = () => (
      <div className='flex justify-center items-center w-full py-32'>There&apos;s no any item</div>
    ),
  } = props;

  const reorderRow = (draggedRowIndex: number, targetRowIndex: number) => {
    data.splice(targetRowIndex, 0, data.splice(draggedRowIndex, 1)[0] as T);
    if (onDataStateChange) {
      onDataStateChange([...data]);
    }
  };

  const table = useReactTable({
    data,
    columns,
    state: { rowSelection, columnVisibility, columnOrder, expanded, sorting },
    getCoreRowModel: getCoreRowModel(),
    getExpandedRowModel: getExpandedRowModel(),
    onRowSelectionChange,
    onExpandedChange,
    getSubRows,
    onColumnVisibilityChange,
    onColumnOrderChange,
    onSortingChange,
    getSortedRowModel: getSortedRowModel(),
    enableSorting: sorting !== undefined,
    // debugTable: true,
    // debugHeaders: true,
    // debugColumns: true,
  });

  const DraggableRow: FC<{
    row: Row<T>;
    reorderRow: (draggedRowIndex: number, targetRowIndex: number) => void;
  }> = ({ row, reorderRow }) => {
    const [, dropRef] = useDrop({
      accept: 'row',
      drop: (draggedRow: Row<T>) => reorderRow(draggedRow.index, row.index),
    });

    const [{ isDragging }, dragRef, previewRef] = useDrag({
      collect: (monitor) => ({
        isDragging: monitor.isDragging(),
      }),
      item: () => row,
      type: 'row',
    });
    return (
      <tr // use tr instead of TableRow to avoid Warning: Function components cannot be given refs
        key={row.id}
        ref={previewRef} //previewRef could go here
        className={clsx('data-grid__row', isDragging ? 'opacity-50' : 'opacity-100')}
      >
        <>
          {isDraggable && (
            <td ref={dropRef} className='text-center'>
              {row.depth === 0 && (
                <button ref={dragRef} className='mt-1'>
                  <Icon iconName='drag' variant='outline' color='default' className='cursor-grab' />
                </button>
              )}
            </td>
          )}
          {row.getVisibleCells().map((cell) => {
            const { meta = { align: 'left', cellClassName: '' } } =
              cell.getContext().column.columnDef;
            const classes = getAlignmentClass(meta.align!);
            return (
              <TableDataCell
                key={cell.id}
                className={clsx('data-grid__cell', classes, meta.cellClassName)}
              >
                {flexRender(cell.column.columnDef.cell, cell.getContext())}
              </TableDataCell>
            );
          })}
        </>
      </tr>
    );
  };

  useEffect(() => {
    onTableStateChange && onTableStateChange(table);
  }, [onTableStateChange, table]);

  return (
    <>
      <DndProvider backend={HTML5Backend}>
        <div className={clsx('jf-scroll', className)}>
          <Table className={clsx('data-grid__table', tableClassName)}>
            <TableHead className='data-grid__table--head grid-cols-2'>
              {table.getHeaderGroups().map((headerGroup) => (
                <TableRow key={headerGroup.id}>
                  {isDraggable && <TableHeader className='data-grid__header' />}
                  {headerGroup.headers.map((header) => {
                    const { meta = { align: 'left', headerClassName: '' } } =
                      header.getContext().column.columnDef;
                    const classes = getAlignmentClass(meta.align!);
                    const toggleSorting = (desc: boolean) => {
                      header.column.toggleSorting(desc);
                    };
                    const isSorted: false | SortDirection = header.column.getIsSorted();
                    return (
                      <TableHeader
                        key={header.id}
                        className={clsx('data-grid__header', classes, meta.headerClassName)}
                      >
                        {header.isPlaceholder ? null : (
                          <div className='data-grid__header--content'>
                            {flexRender(header.column.columnDef.header, header.getContext())}
                            {header.column.getCanSort() && (
                              <div>
                                <Icon
                                  iconName='up'
                                  color={isSorted === 'asc' ? undefined : 'default'}
                                  size={20}
                                  viewBox='0 0 24 5'
                                  className='cursor-pointer'
                                  onClick={() => toggleSorting(false)}
                                />
                                <Icon
                                  iconName='down'
                                  color={isSorted === 'desc' ? undefined : 'default'}
                                  size={20}
                                  viewBox='0 6 24 24'
                                  className='cursor-pointer'
                                  onClick={() => toggleSorting(true)}
                                />
                              </div>
                            )}
                          </div>
                        )}
                      </TableHeader>
                    );
                  })}
                </TableRow>
              ))}
            </TableHead>
            <TableBody>
              {table.getRowModel().rows.map((row) => (
                <DraggableRow key={Math.random()} row={row} reorderRow={reorderRow} />
              ))}
            </TableBody>
          </Table>
        </div>
      </DndProvider>
      {!table.getRowModel()?.rows?.length && renderNoData()}
      {!isNil(pagination) && (
        <TablePagination
          page={pagination.page}
          pageSize={pagination.pageSize}
          total={pagination.total}
          onPageChange={pagination.onPageChange}
          onPageSizeChange={pagination.onPageSizeChange}
        />
      )}
    </>
  );
}
