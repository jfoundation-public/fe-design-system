import {
  ColumnOrderState,
  ExpandedState,
  RowSelectionState,
  SortingState,
  VisibilityState,
} from '@tanstack/react-table';
import { useState } from 'react';

import { GridTableType } from './types';

export function useDataGrid<T = any>() {
  const [expanded, setExpanded] = useState<ExpandedState>({});
  const [columnVisibility, setColumnVisibility] = useState<VisibilityState>({});
  const [columnOrder, setColumnOrder] = useState<ColumnOrderState>([]);
  const [rowSelection, setRowSelection] = useState<RowSelectionState>({});
  const [table, setTable] = useState<GridTableType<T>>();
  const [sorting, setSorting] = useState<SortingState>([]);

  return {
    expanded,
    setExpanded,
    columnVisibility,
    setColumnVisibility,
    columnOrder,
    setColumnOrder,
    rowSelection,
    setRowSelection,
    table,
    setTable,
    sorting,
    setSorting,
  };
}

export function usePagination() {
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const [total, setTotal] = useState(0);

  return { page, pageSize, onPageChange: setPage, onPageSizeChange: setPageSize, total, setTotal };
}
