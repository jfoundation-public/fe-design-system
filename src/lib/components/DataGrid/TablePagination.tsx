import {
  autoUpdate,
  flip,
  FloatingFocusManager,
  FloatingOverlay,
  offset,
  size,
  useDismiss,
  useFloating,
  useInteractions,
  useRole,
} from '@floating-ui/react-dom-interactions';
import clsx from 'clsx';
import { useEffect, useMemo, useState } from 'react';

import { Icon } from '@/lib';

import { getPageSizesDisplay } from './helper';
import { PageSizeSelectorProps, TablePaginationProps } from './types';

/**
 * Control DataGrid pagination. Change page and page size
 */
export default function TablePagination(props: TablePaginationProps) {
  const {
    page,
    pageSize,
    onPageChange,
    onPageSizeChange,
    total,
    pageSizes = [10, 20, 50, 100],
    rangeDisplay = 5,
    className,
    hidePageSizeSelector,
  } = props;
  const outOfPageSize = page > Math.ceil(total / pageSize!);
  const pages = useMemo(() => {
    if (outOfPageSize) {
      return getPageSizesDisplay(total, pageSize!, 1, rangeDisplay);
    }
    return getPageSizesDisplay(total, pageSize!, page, rangeDisplay);
  }, [outOfPageSize, total, pageSize, page, rangeDisplay]);

  function handleNextPage() {
    onPageChange(Math.min(page + 1, pages.at(-1)!));
  }

  function handlePrevPage() {
    onPageChange(Math.max(page - 1, 1));
  }

  function handleChangePage(p: number) {
    return () => {
      if (p === -1) {
        onPageChange(Math.max(page - rangeDisplay, 1));
        return;
      }
      if (p === -2) {
        onPageChange(Math.min(page + rangeDisplay, pages.at(-1)!));
        return;
      }
      onPageChange(p);
    };
  }

  useEffect(() => {
    if (outOfPageSize) {
      onPageChange(1);
    }
  }, [onPageChange, outOfPageSize]);

  return (
    <div className={clsx('data-grid__pagination', className)}>
      {!hidePageSizeSelector && (
        <PageSizeSelector
          pageSizes={pageSizes}
          selected={pageSize!}
          onSelected={onPageSizeChange!}
        />
      )}
      <ul className='flex flex-row'>
        <li role='presentation' onClick={handlePrevPage} className='data-grid__pagination-cell'>
          <Icon iconName='arrow-left' variant='outline' />
        </li>
        {pages.map((pageItem) => (
          <li
            key={pageItem}
            role='presentation'
            className={clsx(
              'data-grid__pagination-cell',
              page === pageItem && 'data-grid__pagination-cell--active',
            )}
            onClick={handleChangePage(pageItem)}
          >
            {pageItem === -1 || pageItem === -2 ? '...' : pageItem}
          </li>
        ))}
        <li role='presentation' onClick={handleNextPage} className='data-grid__pagination-cell'>
          <Icon iconName='arrow-right' variant='outline' />
        </li>
      </ul>
    </div>
  );
}

function PageSizeSelector(props: PageSizeSelectorProps) {
  const { pageSizes, selected, onSelected } = props;
  const [open, setOpen] = useState(false);
  const opts = useMemo(
    () => pageSizes.map((num) => ({ value: num, title: `${num} / Page` })),
    [pageSizes],
  );
  const displayOpts = opts.filter((opt) => opt.value !== selected);
  const selectedOpt = opts.find((opt) => opt.value === selected);

  const { x, y, reference, floating, strategy, context } = useFloating({
    open,
    placement: 'bottom-end',
    onOpenChange: setOpen,
    whileElementsMounted: autoUpdate,
    middleware: [
      offset(5),
      flip({ padding: 8 }),
      size({
        padding: 8,
      }),
    ],
  });

  const { getReferenceProps, getFloatingProps } = useInteractions([
    useRole(context, { role: 'listbox' }),
    useDismiss(context),
  ]);

  return (
    <>
      <button
        {...getReferenceProps({
          ref: reference,
          onClick: (event) => {
            event.stopPropagation();
            (event.currentTarget as HTMLButtonElement).focus();
            setOpen((o) => !o);
          },
          className: clsx(
            'data-grid__pagination-page-size-selector',
            open && 'data-grid__pagination-page-size-selector--focus',
          ),
        })}
      >
        {selectedOpt?.title}
        <Icon iconName='down' variant='outline' size={20} />
      </button>
      {open && (
        <FloatingOverlay lockScroll>
          <FloatingFocusManager modal={false} returnFocus={false} context={context}>
            <div
              {...getFloatingProps({
                ref: floating,
                style: {
                  position: strategy,
                  top: y ?? 0,
                  left: x ?? 0,
                  overflow: 'auto',
                },
              })}
            >
              <div className='paper'>
                {displayOpts.map((opt) => (
                  <li
                    role='presentation'
                    onClick={() => {
                      onSelected(opt.value);
                      setOpen(false);
                    }}
                    key={opt.value}
                    className='data-grid__pagination-page-size-selection-item'
                  >
                    {opt.title}
                  </li>
                ))}
              </div>
            </div>
          </FloatingFocusManager>
        </FloatingOverlay>
      )}
    </>
  );
}
