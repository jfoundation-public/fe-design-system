/* eslint-disable prettier/prettier */
import {
  ColumnDef,
  ColumnOrderState,
  ColumnPinningState,
  ExpandedState,
  OnChangeFn,
  RowData,
  RowSelectionState,
  SortingState,
  Table as GridTableType,
  VisibilityState,
} from '@tanstack/react-table';
import { ChangeEventHandler, ReactElement } from 'react';

import { IconName } from '../Icon/types';

export type {
  ColumnDef,
  ColumnOrderState,
  ExpandedState,
  GridTableType,
  OnChangeFn,
  RowSelectionState,
  SortingState,
  VisibilityState,
};

declare module '@tanstack/react-table' {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  interface ColumnMeta<TData extends RowData, TValue> {
    align?: 'left' | 'right' | 'center';
    headerClassName?: string;
    cellClassName?: string;
  }
}
export interface DataGridProps<T = Record<string, any>> {
  data: T[];
  onDataStateChange?: (updater: T[]) => void;
  columns: ColumnDef<T>[];
  className?: string;
  tableClassName?: string;
  onTableStateChange?: (updater: GridTableType<T>) => void;

  rowSelection?: RowSelectionState;
  onRowSelectionChange?: OnChangeFn<RowSelectionState>;

  expanded?: ExpandedState;
  onExpandedChange?: OnChangeFn<ExpandedState>;
  getSubRows?: (original: T) => T[] | undefined;

  sorting?: SortingState;
  onSortingChange?: OnChangeFn<SortingState>;

  columnVisibility?: VisibilityState;
  onColumnVisibilityChange?: OnChangeFn<VisibilityState>;

  columnOrder?: ColumnOrderState;
  onColumnOrderChange?: OnChangeFn<ColumnOrderState>;

  columnPinning?: ColumnPinningState;

  renderNoData?: () => ReactElement;
  isDraggable?: boolean;

  pagination?: {
    page: number;
    pageSize: number;
    total: number;
    onPageChange: (page: number) => void;
    onPageSizeChange: (pageSize: number) => void;
  };
}

export type TablePaginationProps =
  | {
      page: number;
      pageSize: number;
      total: number;
      onPageChange: (selected: number) => void;
      onPageSizeChange: (pageSizeSelected: number) => void;
      pageSizes?: number[];
      rangeDisplay?: number;
      className?: string;
      hidePageSizeSelector?: never;
    }
  | {
      page: number;
      pageSize: number;
      total: number;
      onPageChange: (selected: number) => void;
      onPageSizeChange?: never;
      pageSizes?: never;
      rangeDisplay?: number;
      className?: string;
      hidePageSizeSelector: boolean;
    };

export interface PageSizeSelectorProps {
  pageSizes: number[];
  selected: number;
  onSelected: (pageSize: number) => void;
}

export interface ColumnDisplayedProps<T> {
  columnNames: Record<string, string>;
  startIcon?: IconName;
  label?: string;
  table: GridTableType<T> | undefined;
  columnVisibility?: VisibilityState;
  onColumnVisibilityChange?: OnChangeFn<VisibilityState>;

  columnOrder?: ColumnOrderState;
  onColumnOrderChange?: OnChangeFn<ColumnOrderState>;
  className?: string;
  hideColumnsId?: string[];
}

export interface ColumnDisplayedItemProps {
  id: string;
  label: string;
  checked?: boolean;
  onChange?: ChangeEventHandler<HTMLInputElement>;
  onOrdering?: (dragId: string, targetId: string) => void;
}
