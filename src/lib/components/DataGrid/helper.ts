import { range } from '@/lib';

const ALIGNMENT_CLASSES = {
  left: 'data-grid__cell--left',
  right: 'data-grid__cell--right',
  center: 'data-grid__cell--center',
};

function getAlignmentClass(alignment: 'left' | 'right' | 'center') {
  return ALIGNMENT_CLASSES[alignment];
}

function getPageSizesDisplay(
  total: number,
  pageSize: number,
  current: number,
  rangeDisplay: number,
) {
  if (!total || !pageSize || total <= pageSize) return [1];
  const pages = range(1, Math.ceil(total / pageSize) + 1);
  const halfRangeDisplay = Math.floor(rangeDisplay / 2);
  const middleDisplay = range(
    Math.max(current - halfRangeDisplay, 1),
    Math.min(current + halfRangeDisplay, pages.at(-1)!) + 1,
  );
  if (middleDisplay[0] > 3) {
    middleDisplay.unshift(1, -1);
  } else {
    middleDisplay.unshift(...range(1, middleDisplay[0] + 1));
  }
  if (middleDisplay.at(-1)! < pages.at(-1)! - 2) {
    middleDisplay.push(-2, pages.at(-1)!);
  } else {
    middleDisplay.push(...range(middleDisplay.at(-1)!, pages.at(-1)! + 1));
  }
  return Array.from(new Set(middleDisplay));
}

export { getAlignmentClass, getPageSizesDisplay };
