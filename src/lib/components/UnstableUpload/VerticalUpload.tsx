import clsx from 'clsx';
import { useState } from 'react';

import { Icon } from '@/lib';

import { VerticalUploadProps } from './types';

const VerticalUpload = (props: VerticalUploadProps) => {
  const { previewFile, onEditFile, onRemoveFile } = props;
  const [hoverId, setHoverId] = useState<string>('');

  const _functionClassName = clsx(
    'flex upload-preview-vertical-function',
    hoverId !== previewFile.id && 'hidden',
    hoverId === previewFile.id && 'block',
  );

  return (
    <div
      onMouseEnter={() => setHoverId(previewFile.id)}
      onMouseLeave={() => setHoverId('')}
      className='upload-preview-vertical-wrapper'
    >
      <Icon
        className='w-7'
        color='default'
        iconName={previewFile.type === 'image' ? 'image-media' : 'file-document'}
        variant='solid'
      />
      <span className='upload-preview-vertical-text'>{previewFile.name}</span>
      <div className={_functionClassName}>
        <Icon
          color='default'
          width={16}
          height={16}
          onClick={() => onEditFile && onEditFile(previewFile.id)}
          iconName='edit'
          variant='outline'
        />
        <Icon
          color='default'
          width={16}
          height={16}
          onClick={() => onRemoveFile && onRemoveFile(previewFile.id)}
          iconName='delete'
          variant='outline'
        />
      </div>
    </div>
  );
};

export default VerticalUpload;
