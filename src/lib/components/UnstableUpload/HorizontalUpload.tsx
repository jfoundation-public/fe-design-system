import clsx from 'clsx';
import { useState } from 'react';

import { Icon, PreviewImage } from '@/lib';

import { HorizontalUploadProps } from './types';

const HorizontalUpload = (props: HorizontalUploadProps) => {
  const { previewFile, onEditFile, onPreviewFile, onRemoveFile } = props;
  const [hoverId, setHoverId] = useState<string>('');

  const _functionClassName = clsx(
    'upload-preview-horizontal-function',
    hoverId !== previewFile.id && 'hidden',
    hoverId === previewFile.id && 'block',
  );
  const _keepHover = clsx('upload-preview-keepHover', hoverId && 'brightness-50');

  const renderItem = () => {
    return (
      <>
        <div className={_functionClassName}>
          <div className='flex gap-1 brightness-100 cursor-pointer'>
            <Icon
              onClick={() => onPreviewFile && onPreviewFile(previewFile.url || '')}
              iconName='eye'
              variant='solid'
            />
            <Icon
              onClick={() => onEditFile && onEditFile(previewFile.id)}
              iconName='edit'
              variant='solid'
            />
            <Icon
              onClick={() => onRemoveFile && onRemoveFile(previewFile.id)}
              iconName='delete'
              variant='solid'
            />
          </div>
        </div>

        <>
          {previewFile.type === 'image' && previewFile.url ? (
            <PreviewImage
              className={hoverId ? 'brightness-50' : ''}
              src={previewFile.url}
              ratio='1:1'
            />
          ) : (
            <span className={_keepHover}>
              <Icon width={50} height={50} iconName='doc' variant='solid' viewBox='0 0 56 56' />
            </span>
          )}
        </>
      </>
    );
  };
  return (
    <span
      onMouseEnter={() => setHoverId(previewFile.id)}
      onMouseLeave={() => setHoverId('')}
      className='relative'
      key={previewFile.id}
    >
      {renderItem()}
    </span>
  );
};

export default HorizontalUpload;
