import clsx from 'clsx';
import { useRef } from 'react';

import { Icon } from '../Icon';
import { NonDragProps } from './types';

const NonDragUpload = (props: NonDragProps) => {
  const { className, layout, ...restProps } = props;

  const inputRef = useRef<HTMLInputElement>(null);

  const _classNames = clsx(
    layout === 'horizontal' &&
      'flex justify-center content-center flex-wrap flex-col items-center w-[103px] h-[100px] rounded border-lineBorder-primary btn-color-primary cursor-pointer hover:bg-surface-primary4 hover:opacity-100 relative z-10 bg-white',
    layout === 'vertical' &&
      'flex flex-row w-64 hover:bg-surface-primary4 content-start flex-wrap items-start gap-1 cursor-pointer btn-color-primary',
    className,
  );

  const _onClick = () => {
    inputRef.current && inputRef.current.click();
  };

  const onClickInput = (event: React.MouseEvent<HTMLInputElement, MouseEvent>) => {
    const target = event.target as HTMLInputElement;
    target.value = '';
  };

  return layout === 'horizontal' ? (
    <div className='dashed-border text-primary'>
      <div role='presentation' onClick={_onClick} className={_classNames}>
        <input onClick={onClickInput} type='file' hidden ref={inputRef} {...restProps} />
        <Icon iconName='add' variant='outline' />
        <span className='upload-text'>Upload</span>
      </div>
    </div>
  ) : (
    <div role='presentation' onClick={_onClick} className={_classNames}>
      <input onClick={onClickInput} type='file' hidden ref={inputRef} {...restProps} />
      <Icon iconName='add' variant='outline' />
      <span className='upload-text'>Upload</span>
    </div>
  );
};

export default NonDragUpload;
