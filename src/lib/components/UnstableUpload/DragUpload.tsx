import clsx from 'clsx';
import { useCallback, useRef } from 'react';
import Dropzone from 'react-dropzone';

import { Button, Icon } from '@/lib';

import { formatSize } from './helper';
import { DragProps } from './types';
const DragUpload = (props: DragProps) => {
  const {
    className,
    acceptFile,
    maxSizeFile = 0,
    onDragUpload,
    dragText = {
      btnText: 'Click or drag to upload',
      mainText: 'Drop files here or click to upload',
      subTextElement: undefined,
    },
    validator,
    onDropAccepted,
    onDropRejected,
    onError,
    onCancelUpload: _,
    ...restProps
  } = props;

  const fileSize = formatSize(maxSizeFile);

  const inputRef = useRef<HTMLInputElement>(null);

  const onDrop = useCallback(
    (files: any) => {
      onDragUpload && onDragUpload(files);
    },
    [onDragUpload],
  );

  return (
    <Dropzone
      validator={validator}
      onError={onError}
      onDropAccepted={onDropAccepted}
      onDropRejected={onDropRejected}
      onDrop={onDrop}
      multiple={props.multiple}
    >
      {({ getRootProps, getInputProps }) => (
        <div className={clsx(`dashed-border text-primary`, className)}>
          <div {...getRootProps({ className: 'dropzone' })} className='drag-upload'>
            <input {...getInputProps()} type='file' hidden ref={inputRef} {...restProps} />
            <Icon size={80} iconName='defaultDrag' variant='solid' viewBox='0 0 80 80' />
            <Button className={clsx('m-4', dragText.btnClassName)} variant='secondary' size='sm'>
              {dragText.btnText}
            </Button>
            <span className='drag-mainText'>{dragText.mainText}</span>
            {dragText.subTextElement ? (
              dragText.subTextElement()
            ) : (
              <>
                <span className='drag-subText'>{acceptFile}.</span>
                <span className='drag-subText'>Maximum: {fileSize} each file</span>
              </>
            )}
          </div>
        </div>
      )}
    </Dropzone>
  );
};

export default DragUpload;
