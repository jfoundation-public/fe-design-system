import clsx from 'clsx';
import { useState } from 'react';

import { Icon, UploadErrorProps } from '@/lib';

const UploadError = (props: UploadErrorProps) => {
  const { layout, id, file, errorMsg, onReUpload: ___, onRemoveErrorFile, onViewErrorFile } = props;

  console.log(file, 'file');

  const [hoverId, setHoverId] = useState<string>('');

  const _functionVerticalClassName = clsx(
    'upload-error-vertical-function',
    hoverId !== id && 'hidden',
    hoverId === id && 'block',
  );

  const _functionHorizontalClassName = clsx(
    'upload-error-horizontal-function',
    hoverId !== id && 'hidden',
    hoverId === id && 'block',
  );

  const _classNameHorizontal = clsx(
    'upload-error-horizontal-wrapper',
    hoverId === id && 'bg-slate-500',
  );

  return (
    <>
      {layout === 'horizontal' ? (
        <div
          onMouseEnter={() => setHoverId(id)}
          onMouseLeave={() => setHoverId('')}
          className={_classNameHorizontal}
        >
          <div className={_functionHorizontalClassName}>
            <div className='upload-error-horizontal'>
              <Icon
                onClick={() => onViewErrorFile && onViewErrorFile(errorMsg)}
                iconName='information'
                variant='solid'
              />
              <Icon
                onClick={() => onRemoveErrorFile && onRemoveErrorFile(id)}
                iconName='delete'
                variant='solid'
              />
            </div>
          </div>
          <Icon
            width={40}
            height={40}
            viewBox='0 0 40 40'
            iconName={file.type !== 'doc' ? 'image' : 'file'}
            color='error'
            variant='outline'
          />
        </div>
      ) : (
        <div
          onMouseEnter={() => setHoverId(id)}
          onMouseLeave={() => setHoverId('')}
          className='upload-error-vertical-wrapper'
        >
          <Icon
            className='w-7'
            color='error'
            iconName={file.type !== 'image' ? 'image-media' : 'file-document'}
            variant='solid'
          />
          <span className='upload-error-vertical-info'>{file.name}</span>
          <div className={_functionVerticalClassName}>
            <Icon
              color='error'
              width={16}
              height={16}
              onClick={() => onRemoveErrorFile && onRemoveErrorFile(id)}
              iconName='delete'
              variant='outline'
            />
          </div>
        </div>
      )}
    </>
  );
};

export default UploadError;
