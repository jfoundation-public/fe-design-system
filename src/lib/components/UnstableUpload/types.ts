import { ComponentPropsWithoutRef, ComponentPropsWithRef, ReactElement } from 'react';
import { DropzoneOptions } from 'react-dropzone';

export interface PreviewFile {
  id: string;
  name: string;
  status?: 'success' | 'error';
  url?: string;
  type?: 'image' | 'file';
}

export type UploadProgress = Record<string, number>;

export interface UploadErrorType {
  id: string;
  file: File;
  errorMsg: string;
  hasSuccessUpload?: boolean;
}

export type LayoutUploadType = 'horizontal' | 'vertical';

export interface UploadProps extends ComponentPropsWithRef<'input'> {
  layout?: LayoutUploadType;
  isDrag?: boolean;
  progress?: UploadProgress;
  errors?: UploadErrorType[];
  previews?: PreviewFile[];
  onCancelUpload?: (_id: string) => void;
  onReuploadErrorFile?: (file: File) => void;
  onRemoveErrorFile?: (id: string) => void;
  onViewErrorFile?: (errMsg: string) => void;
  //drag props
  onDragUpload?: (files: any) => void;
  acceptFile?: string;
  maxSizeFile?: number;
  dragText?: {
    btnText?: string;
    btnClassName?: string;
    mainText?: string;
    subTextElement?: () => ReactElement;
  };
  onEditFile?: (id: string) => void;
  onRemoveFile?: (id: string) => void;
  onPreviewFile?: (url: string) => void;
  wrapperClassName?: string;
}

export interface PreviewItemProps extends ComponentPropsWithoutRef<'div'> {
  previewFile: PreviewFile;
  onEdit?: (_id: string) => void;
  onRemove?: (_id: string) => void;
}

export type NonDragProps = Omit<
  UploadProps,
  'isDrag' | 'onDragUpload' | 'dragText' | 'validator' | 'onDropAccepted' | 'onDropRejected'
>;

export type DragProps = Omit<UploadProps, 'isDrag'> &
  Pick<DropzoneOptions, 'validator' | 'onDropAccepted' | 'onDropRejected' | 'onError'>;

export interface VerticalUploadProps {
  previewFile: PreviewFile;
  onEditFile: (id: string) => void;
  onRemoveFile: (id: string) => void;
}

export interface HorizontalUploadProps extends VerticalUploadProps {
  onPreviewFile: (url: string) => void;
}

export interface ProgressBarUploadProps {
  id: string;
  progress: number;
  onCancelUpload: (id: string) => void;
  layout: LayoutUploadType;
}

export interface UploadErrorProps {
  layout: LayoutUploadType;
  id: string;
  file: File;
  errorMsg: string;
  onReUpload: (file: File) => void;
  hasSuccessUpload?: boolean;
  onRemoveErrorFile: (id: string) => void;
  onViewErrorFile: (errMsg: string) => void;
}
