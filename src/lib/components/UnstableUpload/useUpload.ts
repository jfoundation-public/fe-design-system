import { ChangeEvent, useState } from 'react';

import { PreviewFile, UploadErrorType, UploadProgress } from './types';

const useUpload = (
  upload: (
    onSetProgress: React.Dispatch<React.SetStateAction<UploadProgress>>,
  ) => (file: File | undefined) => Promise<void>,
  signalRef: React.MutableRefObject<AbortController>,
) => {
  const [progress, setProgress] = useState<UploadProgress>({});
  const [errors, setErrors] = useState<UploadErrorType[]>([]);

  const [previews, setPreviews] = useState<PreviewFile[]>([]);
  const [previewFile, setPreviewFile] = useState<string>('');

  async function handleChange(event: ChangeEvent<HTMLInputElement>) {
    const files = event.target.files;
    if (files?.length) {
      const fileList = [...files];
      const initProgress: UploadProgress = {};
      fileList.forEach((f) => {
        initProgress[f.name] = 1;
      });
      setProgress(initProgress);
      for (let i = 0; i < files.length; i++) {
        await upload(setProgress)(files[i]);
      }
    }
  }

  async function handleDrag(data: FileList) {
    const files = data;
    if (files?.length) {
      const fileList = [...files];
      const initProgress: UploadProgress = {};
      fileList.forEach((f) => {
        initProgress[f.name] = 1;
      });
      setProgress(initProgress);
      for (let i = 0; i < files.length; i++) {
        await upload(setProgress)(files[i]);
      }
    }
  }

  function cancelRequest(id: string) {
    signalRef.current.abort();
    signalRef.current = new AbortController();
    setProgress((prv) => ({ ...prv, [id]: 0 }));
  }

  const handleRemoveFile = (id: string) => {
    const remain = previews.filter((item: PreviewFile) => item.id !== id);
    setPreviews(() => {
      return remain;
    });
  };

  const handleRemoveErrorFile = (id: string) => {
    const remain = errors.filter((item: UploadErrorType) => item.id !== id);
    setErrors(() => {
      return remain;
    });
  };

  const handlePreviewFile = (url: string) => {
    setPreviewFile(url);
  };

  const handleClosePreviewFile = () => {
    setPreviewFile('');
  };

  return {
    progress,
    errors,
    previews,
    previewFile,
    setErrors,
    setPreviewFile,
    setPreviews,
    setProgress,
    handleChange,
    handleDrag,
    cancelRequest,
    handleRemoveFile,
    handleRemoveErrorFile,
    handlePreviewFile,
    handleClosePreviewFile,
  } as const;
};

export default useUpload;
