import clsx from 'clsx';
import { isEmpty, isNil } from 'ramda';
import { useCallback } from 'react';
import { DropzoneOptions } from 'react-dropzone';

import DragUpload from './DragUpload';
import HorizontalUpload from './HorizontalUpload';
import NonDragUpload from './NonDragUpload';
import ProgressBarUpload from './ProgressBarUpload';
import { PreviewFile, UploadErrorType, UploadProps } from './types';
import UploadError from './UploadError';
import VerticalUpload from './VerticalUpload';

export default function Upload(
  props: UploadProps &
    Pick<DropzoneOptions, 'validator' | 'onDropAccepted' | 'onDropRejected' | 'onError'>,
) {
  const {
    progress = {},
    errors = [],
    previews = [],
    onCancelUpload,
    onRemoveFile,
    onReuploadErrorFile,
    onRemoveErrorFile,
    onEditFile,
    onPreviewFile,
    layout = 'vertical',
    isDrag,
    wrapperClassName,
    ...restProps
  } = props;

  function getShowProgressBar(_id: string) {
    if (isNil(progress) || isNil(progress?.[_id])) return false;
    const _progress = progress[_id];
    return _progress > 0 && _progress < 100;
  }

  const _fileListClassName = clsx(
    'upload-list-file',
    layout == 'horizontal' && 'flex flex-row flex-wrap gap-4',
    layout == 'vertical' && 'flex flex-col flex-wrap gap-4',
  );

  const _wrapperClassName = clsx(
    layout === 'horizontal' && 'flex flex-row gap-4',
    layout === 'vertical' && 'flex flex-col gap-4',
    isDrag && 'flex flex-col gap-4',
    wrapperClassName,
  );

  const handleOnCancelUpload = useCallback(
    (id: string) => {
      onCancelUpload && onCancelUpload(id);
    },
    [onCancelUpload],
  );

  const handleReUpload = useCallback(
    (file: File) => {
      onReuploadErrorFile && onReuploadErrorFile(file);
    },
    [onReuploadErrorFile],
  );

  const handleOnRemoveFile = useCallback(
    (id: string) => {
      onRemoveFile && onRemoveFile(id);
    },
    [onRemoveFile],
  );

  const handleOnRemoveErrorFile = useCallback(
    (id: string) => {
      onRemoveErrorFile && onRemoveErrorFile(id);
    },
    [onRemoveErrorFile],
  );

  const handleOnViewErrorFile = useCallback(
    (errorMsg: string) => {
      onRemoveErrorFile && onRemoveErrorFile(errorMsg);
    },
    [onRemoveErrorFile],
  );

  const handleOnPreviewFile = useCallback(
    (url?: string) => {
      onPreviewFile && onPreviewFile(url || '');
    },
    [onPreviewFile],
  );

  const handleOnEditFile = useCallback(
    (id: string) => {
      onEditFile && onEditFile(id);
    },
    [onEditFile],
  );

  return (
    <div className={_wrapperClassName}>
      {/* Upload */}
      {isDrag && <DragUpload {...restProps} />}
      {!isEmpty(previews) && (
        <div className={_fileListClassName}>
          {previews.map((file: PreviewFile) =>
            layout === 'vertical' ? (
              <VerticalUpload
                key={file.id}
                previewFile={file}
                onEditFile={handleOnEditFile}
                onRemoveFile={handleOnRemoveFile}
              />
            ) : (
              <HorizontalUpload
                key={file.id}
                previewFile={file}
                onEditFile={handleOnEditFile}
                onRemoveFile={handleOnRemoveFile}
                onPreviewFile={handleOnPreviewFile}
              />
            ),
          )}
          {!isNil(progress) && (
            <>
              {Object.entries(progress).map(([_id, _percent]) => {
                return (
                  getShowProgressBar(_id) && (
                    <ProgressBarUpload
                      key={_id}
                      layout={layout}
                      id={_id}
                      progress={_percent}
                      onCancelUpload={handleOnCancelUpload}
                    />
                  )
                );
              })}
            </>
          )}
          {!isEmpty(errors) && (
            <>
              {errors.map((item: UploadErrorType) => {
                return (
                  <UploadError
                    key={item.id}
                    id={item.id}
                    layout={layout}
                    file={item.file}
                    errorMsg={item.errorMsg}
                    hasSuccessUpload={item.hasSuccessUpload}
                    onReUpload={handleReUpload}
                    onRemoveErrorFile={handleOnRemoveErrorFile}
                    onViewErrorFile={handleOnViewErrorFile}
                  />
                );
              })}
            </>
          )}
        </div>
      )}
      {!isDrag && <NonDragUpload layout={layout} {...restProps} />}
    </div>
  );
}
