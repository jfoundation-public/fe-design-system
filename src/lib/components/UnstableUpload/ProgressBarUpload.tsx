import { Icon, IconButton, Progress } from '@/lib';

import { ProgressBarUploadProps } from './types';

const ProgressBarUpload = (props: ProgressBarUploadProps) => {
  const { progress, id, onCancelUpload, layout } = props;

  if (layout === 'vertical') {
    return (
      <div id={id} className='progress-bar-upload-vertical'>
        <Icon iconName='image-media' />
        <Progress percent={progress} />
        <IconButton
          onClick={() => {
            onCancelUpload(id);
          }}
          isAdornment
          iconName='close-small'
          iconVariant='outline'
        />
      </div>
    );
  }
  return (
    <div className='progress-bar-upload-horizontal'>
      <Icon
        className='cursor-pointer'
        onClick={() => onCancelUpload(id)}
        color='default'
        iconName='close-small'
        variant='outline'
      />
      <span className='text-xs'>Uploading</span>
      <Progress type='line' hideInfo percent={progress} />
    </div>
  );
};

export default ProgressBarUpload;
