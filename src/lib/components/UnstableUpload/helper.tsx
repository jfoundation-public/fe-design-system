export const isShowProgressBar = (_progress: number) => {
  return _progress > 0 && _progress < 100;
};

export const formatSize = (bytes: number) => {
  if (bytes === 0) {
    return '0 B';
  }
  const k = 1000;
  const dm = 3;
  const sizes = ['B', 'KB', 'MB', 'GB'];
  const i = Math.floor(Math.log(bytes) / Math.log(k));

  return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
};
