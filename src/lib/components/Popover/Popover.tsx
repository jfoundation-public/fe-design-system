import {
  autoUpdate,
  flip,
  FloatingFocusManager,
  FloatingOverlay,
  FloatingPortal,
  offset,
  shift,
  size,
  useDismiss,
  useFloating,
  useInteractions,
} from '@floating-ui/react-dom-interactions';
import { useEffect, useMemo } from 'react';

import { PopoverProps } from './types';

export default function Popover(props: PopoverProps) {
  const { open, anchorEl, onClose, placement = 'bottom-end', children, sizeMiddleware } = props;

  const middleware = useMemo(() => {
    const _middleware = [offset(5), flip(), shift()];
    if (sizeMiddleware) {
      _middleware.push(
        size({
          padding: 8,
        }),
      );
    }
    return _middleware;
  }, [sizeMiddleware]);

  const { x, y, reference, floating, strategy, context } = useFloating({
    open,
    onOpenChange: onClose,
    middleware,
    placement,
    whileElementsMounted: autoUpdate,
  });

  const { getFloatingProps } = useInteractions([useDismiss(context)]);

  useEffect(() => {
    if (anchorEl && open) {
      reference(anchorEl);
    }
  }, [anchorEl, reference, open]);

  return (
    <FloatingPortal id='portal-root'>
      {open && (
        <FloatingOverlay className='z-popover' lockScroll>
          <FloatingFocusManager
            context={context}
            modal={false}
            returnFocus={false}
            order={['reference', 'content']}
            initialFocus={0}
          >
            <div
              ref={floating}
              style={{
                position: strategy,
                top: y ?? 0,
                left: x ?? 0,
              }}
              {...getFloatingProps()}
            >
              {children}
            </div>
          </FloatingFocusManager>
        </FloatingOverlay>
      )}
    </FloatingPortal>
  );
}
