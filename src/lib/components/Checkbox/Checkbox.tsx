import clsx from 'clsx';
import { useId, useMemo } from 'react';

import { Icon } from '../Icon';
import CheckboxLabel from './CheckboxLabel';
import { getCheckboxClasses, getIconCheckboxProps } from './helper';
import { useCheckboxControlled } from './hooks';
import { CheckboxProps } from './types';

export default function Checkbox(props: CheckboxProps) {
  const {
    checked,
    onChange,
    readOnly,
    disabled,
    indeterminate,
    className,
    id,
    children,
    ...restProps
  } = props;

  const uuid = useId();
  const {
    onChange: _onChange,
    onMouseEnter,
    onMouseLeave,
    hovered,
  } = useCheckboxControlled({ onChange, readOnly, disabled });

  const checkboxClasses = useMemo(
    () => getCheckboxClasses({ checked, hovered, readOnly, disabled, indeterminate }),
    [checked, disabled, hovered, indeterminate, readOnly],
  );

  const { iconName, variant } = getIconCheckboxProps({ checked, indeterminate });

  return (
    <div className={clsx('checkbox', className)}>
      <label
        htmlFor={id ?? uuid}
        className={checkboxClasses}
        onMouseEnter={onMouseEnter}
        onMouseLeave={onMouseLeave}
      >
        <div className='checkbox-icon'>
          <input
            className='hidden'
            onChange={_onChange}
            id={id ?? uuid}
            type='checkbox'
            {...restProps}
          />
          <Icon size={24} iconName={iconName} variant={variant} />
        </div>
        <CheckboxLabel>{children}</CheckboxLabel>
      </label>
    </div>
  );
}
