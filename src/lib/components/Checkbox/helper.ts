import clsx from 'clsx';

function getCheckboxClasses({
  checked,
  hovered,
  readOnly,
  disabled,
  indeterminate,
}: {
  checked?: boolean;
  hovered?: boolean;
  readOnly?: boolean;
  disabled?: boolean;
  indeterminate?: boolean;
}) {
  return clsx('checkbox', [
    hovered && !checked && !readOnly && !disabled && 'checkbox-hovered',
    checked && 'checkbox-checked',
    indeterminate && 'checkbox-indeterminate',
    readOnly && 'checkbox-read-only',
    disabled && 'checkbox-disabled',
  ]);
}

function getIconCheckboxProps({
  checked,
  indeterminate,
}: {
  checked?: boolean;
  indeterminate?: boolean;
}) {
  if (indeterminate) {
    return { iconName: 'indeterminate', variant: 'solid' } as const;
  }
  if (checked) {
    return { iconName: 'checkbox', variant: 'solid' } as const;
  }
  return { iconName: 'uncheck', variant: 'outline' } as const;
}

export { getCheckboxClasses, getIconCheckboxProps };
