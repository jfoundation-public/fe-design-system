import { isString } from '@/lib/utils';

import { Typography } from '../Typography';
import { CheckboxLabelProps } from './types';

export default function CheckboxLabel({ children }: CheckboxLabelProps) {
  if (!children) return null;

  if (isString(children))
    return (
      <Typography className='checkbox-label' variant='body'>
        {children}
      </Typography>
    );
  return <div className='checkbox-label'>{children}</div>;
}
