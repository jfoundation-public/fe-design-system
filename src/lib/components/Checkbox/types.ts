import { ComponentPropsWithRef, ReactElement } from 'react';

export interface CheckboxProps extends Omit<ComponentPropsWithRef<'input'>, 'type'> {
  children?: string | ReactElement;
  indeterminate?: boolean;
}

export interface CheckboxLabelProps {
  children?: string | ReactElement;
}
