import Radio from './Radio';
import { RadioGroupProps } from './types';

export default function RadioGroup(props: RadioGroupProps) {
  const { name, value, onChange, options, ...restProps } = props;
  return (
    <div role='group' {...restProps}>
      {!!options &&
        options.length &&
        options.map((opt) => (
          <Radio
            key={opt.value}
            name={name}
            value={opt.value}
            checked={opt.value === value}
            onChange={onChange}
            disabled={opt.disabled}
            readOnly={opt.readonly}
            tooltipProps={opt.tooltipProps}
          >
            {opt.label}
          </Radio>
        ))}
    </div>
  );
}
