import clsx from 'clsx';

function getRadioClasses({
  checked,
  hovered,
  readOnly,
  disabled,
}: {
  checked?: boolean;
  hovered?: boolean;
  readOnly?: boolean;
  disabled?: boolean;
}) {
  return clsx('radio', [
    hovered && !checked && !readOnly && !disabled && 'radio-hovered',
    checked && 'radio-checked',
    readOnly && 'radio-read-only',
    disabled && 'radio-disabled',
  ]);
}

function getIconRadioProps({ checked }: { checked?: boolean }) {
  if (checked) {
    return { iconName: 'radio', variant: 'solid' } as const;
  }
  return { iconName: 'radio', variant: 'outline' } as const;
}

export { getIconRadioProps, getRadioClasses };
