import { useId, useMemo } from 'react';

import { Icon } from '../Icon';
import { Tooltip } from '../Tooltip';
import { getIconRadioProps, getRadioClasses } from './helper';
import { useRadioControlled } from './hooks';
import RadioLabel from './RadioLabel';
import { RadioProps } from './types';

export default function Radio(props: RadioProps) {
  const {
    checked,
    readOnly,
    disabled,
    onChange,
    id,
    className,
    tooltipProps,
    children,
    ...restProps
  } = props;
  const uuid = useId();

  const {
    onChange: _onChange,
    onMouseEnter,
    onMouseLeave,
    hovered,
  } = useRadioControlled({ onChange, readOnly, disabled });

  const radioClasses = useMemo(
    () => getRadioClasses({ checked, hovered, readOnly, disabled }),
    [checked, disabled, hovered, readOnly],
  );
  const { iconName, variant } = getIconRadioProps({ checked });
  return (
    <div className={className}>
      <label
        htmlFor={id ?? uuid}
        className={radioClasses}
        onMouseEnter={onMouseEnter}
        onMouseLeave={onMouseLeave}
      >
        {tooltipProps?.content ? (
          <Tooltip {...(tooltipProps ?? {})}>
            <div className='radio-icon'>
              <input
                id={id ?? uuid}
                type='radio'
                checked={checked}
                onChange={_onChange}
                {...restProps}
              />
              <Icon size={24} iconName={iconName} variant={variant} />
            </div>
          </Tooltip>
        ) : (
          <div className='radio-icon'>
            <input
              id={id ?? uuid}
              type='radio'
              checked={checked}
              onChange={_onChange}
              className='hidden'
              {...restProps}
            />
            <Icon size={24} iconName={iconName} variant={variant} />
          </div>
        )}
        <RadioLabel>{children}</RadioLabel>
      </label>
    </div>
  );
}
