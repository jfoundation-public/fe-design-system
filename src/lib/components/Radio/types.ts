import {
  ChangeEventHandler,
  ComponentPropsWithoutRef,
  ComponentPropsWithRef,
  ReactElement,
} from 'react';

import { SelectOption } from '@/lib/types';

import { TooltipProps } from '../Tooltip';

export type TooltipPropsWithoutChilren = Omit<TooltipProps, 'children'>;

export interface RadioProps extends Omit<ComponentPropsWithoutRef<'input'>, 'type'> {
  children?: string | ReactElement;
  tooltipProps?: TooltipPropsWithoutChilren;
}

export interface RadioLabelProps {
  children?: string | ReactElement;
}

export interface RadioGroupProps extends ComponentPropsWithRef<'div'> {
  name: string;
  options: Array<
    SelectOption & {
      disabled?: boolean;
      readonly?: boolean;
      tooltipProps?: TooltipPropsWithoutChilren;
    }
  >;
  onChange: ChangeEventHandler<HTMLInputElement>;
  value: string;
}
