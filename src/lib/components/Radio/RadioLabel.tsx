import { isString } from '@/lib/utils';

import { RadioLabelProps } from './types';

export default function RadioLabel({ children }: RadioLabelProps) {
  if (!children) return null;

  if (isString(children)) return <span className='radio-label body'>{children}</span>;
  return <div className='radio-label'>{children}</div>;
}
