import { FieldControlled } from '../FieldControlled';
import TreeSelectBase from './TreeSelectBase';
import { TreeSelectProps } from './types';

export default function TreeSelect<T extends { id: string } = { id: string }>(
  props: TreeSelectProps<T>,
) {
  const {
    label,
    labelIcon,
    helperText,
    startAdornment,
    endAdornment,
    isWarning,
    isSuccess,
    isError,
    Container,
    ...restProps
  } = props;

  return (
    <FieldControlled
      label={label}
      labelIcon={labelIcon}
      helperText={helperText}
      endAdornment={endAdornment}
      startAdornment={startAdornment}
      isWarning={isWarning}
      isSuccess={isSuccess}
      isError={isError}
      {...Container}
    >
      <TreeSelectBase {...restProps} />
    </FieldControlled>
  );
}
