import { FocusEventHandler, ReactElement, ReactEventHandler } from 'react';

import { FieldControlledProps } from '../FieldControlled';
import { TreeItem } from '../TreeView';

export interface TreeSelectBaseProps<T extends Record<string, unknown>> {
  options: TreeItem<T>[];
  moreSelectItem?: (props: { onClose: () => void }) => ReactElement;
  value: string;
  name: string;
  placeholder?: string;
  onChange?: ReactEventHandler<HTMLInputElement>;
  onBlur?: FocusEventHandler<HTMLInputElement>;
  onFocus?: FocusEventHandler<HTMLInputElement>;
}

export type TreeSelectProps<T extends Record<string, unknown>> = FieldControlledProps &
  TreeSelectBaseProps<TreeItem<T>> & {
    name: string;
    placeholder?: string;
    readOnly?: boolean;
    disabled?: boolean;
    startAdornment?: ReactElement;
    endAdornment?: ReactElement;
  };
