import {
  autoUpdate,
  flip,
  FloatingOverlay,
  FloatingPortal,
  offset,
  size,
  useDismiss,
  useFloating,
  useInteractions,
  useListNavigation,
  useRole,
} from '@floating-ui/react-dom-interactions';
import { useDeferredValue, useEffect, useLayoutEffect, useMemo, useRef, useState } from 'react';

import { createCustomInputEvent, isEqual } from '@/lib/utils';

import { IconButton } from '../Button';
import { withControlled } from '../FieldControlled';
import { useFieldControlElement } from '../FieldInput/hooks';
import { InputBase } from '../InputBase';
import { Radio } from '../Radio';
import { TreeItemContentComp, TreeItemCore, TreeView, TreeViewExpandedState } from '../TreeView';
import { flattenBy } from '../TreeView/utils';
import { TreeSelectBaseProps } from './types';

function TreeSelectBase<T extends Record<string, unknown>>(props: TreeSelectBaseProps<T>) {
  const {
    value,
    name,
    onChange,
    onFocus,
    onBlur,
    placeholder,
    options,
    moreSelectItem: MoreSelectItem,
  } = props;
  const prevValue = useRef(value);
  const prevCurrentValue = useRef(value ?? '');
  const [flatOptions] = useMemo(() => {
    return flattenBy(options, (node) => node.items ?? []);
  }, [options]);

  const containerElement = useFieldControlElement();
  const [currentValue, setCurrentValue] = useState(value);
  const [open, setOpen] = useState(false);
  const [activeIndex, setActiveIndex] = useState<number | null>(null);

  const [search, setSearch] = useState('');

  const searchCached = useDeferredValue(search);

  const [expanded, setExpanded] = useState<TreeViewExpandedState>({});

  const listRef = useRef<Array<HTMLElement | null>>([]);

  const { x, y, reference, floating, strategy, context } = useFloating({
    whileElementsMounted: autoUpdate,
    open,
    onOpenChange: setOpen,
    placement: 'bottom-start',
    middleware: [
      offset(5),
      flip({ padding: 8 }),
      size({
        apply({ rects, availableHeight, elements }) {
          Object.assign(elements.floating.style, {
            width: `${rects.reference.width}px`,
            maxHeight: `${availableHeight}px`,
          });
        },
        padding: 8,
      }),
    ],
  });

  const { getReferenceProps, getFloatingProps } = useInteractions([
    useRole(context, { role: 'listbox' }),
    useDismiss(context),
    useListNavigation(context, {
      listRef,
      activeIndex,
      onNavigate: setActiveIndex,
      virtual: true,
      loop: true,
    }),
  ]);

  function handleSelect(_id: string) {
    setCurrentValue(_id);
    setOpen(false);
  }

  function handleFocus(e: React.FocusEvent<HTMLInputElement>) {
    onFocus && onFocus(e);
    setOpen(true);
  }

  function handleBlur(e: React.FocusEvent<HTMLInputElement>) {
    onBlur && onBlur(e);
  }

  useLayoutEffect(() => {
    // IMPORTANT: When the floating element first opens, this runs when the
    // styles have **not yet** been applied to the element. This can cause an
    // infinite loop as `size` has not yet limited the maxHeight, so the whole
    // page tries to scroll. We must wrap it in rAF.
    requestAnimationFrame(() => {
      if (activeIndex != null) {
        listRef.current[activeIndex]?.scrollIntoView({ block: 'nearest' });
      }
    });
  }, [activeIndex]);

  useEffect(() => {
    if (containerElement) {
      reference({
        getBoundingClientRect: () => containerElement!.getBoundingClientRect(),
        contextElement: containerElement!,
      });
    }
  }, [containerElement, reference]);

  useEffect(() => {
    if (!isEqual(prevCurrentValue.current, currentValue)) {
      prevCurrentValue.current = currentValue;
      onChange && onChange(createCustomInputEvent({ name, value: currentValue }));
    }
  }, [currentValue, name, onChange]);

  const text = useDeferredValue(
    flatOptions.find((opt) => opt.id === currentValue)?.label ?? currentValue,
  );

  (function syncValue() {
    if (!isEqual(prevValue.current, value)) {
      prevValue.current = value;
      setCurrentValue(value ?? '');
    }
  })();

  const treeOptions = useMemo(
    () =>
      searchCached
        ? flatOptions.filter((item) =>
            item.label.toLocaleLowerCase().includes(searchCached.toLocaleLowerCase()),
          )
        : options,
    [flatOptions, options, searchCached],
  );

  const renderTreeItem: TreeItemContentComp<{ id: string }> = useMemo(
    () =>
      function RenderOption({ label, id, ...rest }: TreeItemCore<{ id: string }>) {
        return (
          <RenderTreeItem
            isSelected={currentValue === id}
            onSelected={handleSelect}
            label={label}
            id={id}
            {...rest}
          />
        );
      },
    [currentValue],
  );

  return (
    <>
      <InputBase
        {...getReferenceProps({
          ref: reference,
          readOnly: true,
          value: text,
          placeholder,
          'aria-autocomplete': 'list',
          onKeyDown(event) {
            if (event.key === 'Tab') {
              setOpen(false);
            }
          },
        })}
        onFocus={handleFocus}
        onBlur={handleBlur}
        endAdornment={
          <IconButton
            type='button'
            isAdornment
            onMouseDown={(e) => {
              e.preventDefault();
              e.stopPropagation();
            }}
            onMouseUp={(e) => {
              e.preventDefault();
              e.stopPropagation();
            }}
            iconName='down'
            iconVariant='outline'
          />
        }
      />

      <FloatingPortal id='portal-root'>
        {open && (
          <FloatingOverlay className='z-picker' lockScroll>
            <div
              {...getFloatingProps({
                ref: floating,
                style: {
                  position: strategy,
                  left: x ?? 0,
                  top: y ?? 0,
                  overflow: 'auto',
                },
              })}
            >
              <div className='paper'>
                <TreeView
                  expanded={expanded}
                  onExpand={setExpanded}
                  data={treeOptions}
                  renderTreeItem={renderTreeItem}
                  treeIdGenerator={(__id) => __id}
                  searchProps={{
                    noOutline: true,
                    name: 'tree-search',
                    value: search,
                    onChange: (e) => {
                      setSearch(e.target.value);
                    },
                  }}
                />
                {MoreSelectItem && (
                  <MoreSelectItem
                    onClose={() => {
                      setOpen(false);
                    }}
                  />
                )}
              </div>
            </div>
          </FloatingOverlay>
        )}
      </FloatingPortal>
    </>
  );
}

function RenderTreeItem({
  label,
  id,
  isSelected,
  onSelected,
}: TreeItemCore<{ label: string }> & { isSelected: boolean; onSelected: (_id: string) => void }) {
  return (
    <div className='tree-view__content'>
      <Radio
        checked={isSelected}
        onChange={() => {
          onSelected(id);
        }}
        className='flex items-center'
      >
        {label}
      </Radio>
    </div>
  );
}

export default withControlled<string | undefined, TreeSelectBaseProps<any>>(TreeSelectBase);
