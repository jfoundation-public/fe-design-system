import {
  autoUpdate,
  flip,
  FloatingFocusManager,
  FloatingOverlay,
  FloatingPortal,
  offset,
  size,
  useDismiss,
  useFloating,
  useInteractions,
} from '@floating-ui/react-dom-interactions';
import { format } from 'date-fns';
import { useEffect, useRef, useState } from 'react';

import { createCustomInputEvent, noop } from '@/lib/utils';

import { IconButton } from '../Button';
import withContainer from '../FieldContainer/withContainer';
import { InputBase } from '../InputBase';
import { getDatePickerClasses } from './helper';
import { useViewDate } from './hooks';
import PanelDate from './PanelDate';
import PanelMonth from './PanelMonth';
import PanelTime from './PanelTime';
import { DateTimePickerProps } from './types';
import { parseDateTimeString } from './utiils';

const today = new Date();

/**
 * Date time picker. Input extends from InputBase
 */
function DateTimePicker(props: DateTimePickerProps) {
  const {
    value: _value,
    onChange,
    name,
    placeholder,
    isError,
    isSuccess,
    isWarning,
    disabled,
    readOnly,
    isOnChangeFocusInput,
    pickerPlacement,
    disableFuture,
    disablePast,
  } = props;
  const prevValueProp = useRef(_value);
  const [dateValue, setDateValue] = useState<Date | null>(() => parseDateTimeString(_value).date);
  const [timeValue, setTimeValue] = useState<string | null | undefined>(
    () => parseDateTimeString(_value).time,
  );
  const [currentPanel, setCurrentPanel] = useState<'date' | 'month' | null>(null);

  const { x, y, reference, floating, strategy, context } = useFloating({
    whileElementsMounted: autoUpdate,
    open: !!currentPanel,
    onOpenChange: () => {
      if (currentPanel !== 'month') {
        setCurrentPanel(null);
      }
    },
    placement: pickerPlacement ?? 'bottom-start',
    middleware: [
      offset(5),
      flip({ padding: 8 }),
      size({
        padding: 8,
      }),
    ],
  });

  const { getFloatingProps } = useInteractions([useDismiss(context)]);

  const {
    viewDate,
    handleNextMonth,
    handleNextYear,
    handlePrevMonth,
    handlePrevYear,
    handleSetViewDate,
  } = useViewDate(dateValue ?? today);

  const handleSelected = (selected: Date) => {
    setDateValue(selected);
  };

  const handleMonthSelected = (date: Date) => {
    handleSetViewDate(date);
    setCurrentPanel('date');
  };

  useEffect(() => {
    if (dateValue && timeValue) {
      onChange &&
        onChange(
          createCustomInputEvent({
            name,
            value: `${format(dateValue, 'yyyy-MM-dd')}T${timeValue}`,
          }),
        );
    } else {
      onChange &&
        onChange(
          createCustomInputEvent({
            name,
            value: '',
          }),
        );
    }
  }, [name, onChange, timeValue, dateValue]);

  useEffect(() => {
    if (prevValueProp.current !== _value) {
      const { time, date } = parseDateTimeString(_value);
      setDateValue(date);
      setTimeValue(time);
      prevValueProp.current = _value;
    }
  }, [_value]);

  const datePickerClasses = getDatePickerClasses({
    isSuccess,
    isError,
    isWarning,
    disabled,
    readOnly,
  });

  const displayText = (function () {
    if (dateValue && timeValue) {
      return `${format(dateValue, 'yyyy-MM-dd')} / ${timeValue}`;
    }
    return '';
  })();

  return (
    <div className={datePickerClasses} ref={reference}>
      <InputBase
        placeholder={placeholder}
        value={displayText}
        onChange={noop}
        onFocus={
          isOnChangeFocusInput
            ? noop
            : () => {
                !readOnly && !disabled && setCurrentPanel('date');
              }
        }
        onClick={
          !isOnChangeFocusInput
            ? noop
            : () => {
                !readOnly && !disabled && setCurrentPanel('date');
              }
        }
        disabled={disabled}
        readOnly={readOnly}
        className='date-picker__input'
        endAdornment={
          !readOnly && !disabled ? (
            <>
              {dateValue && timeValue && (
                <IconButton
                  isAdornment
                  onClick={() => {
                    setDateValue(null);
                    setTimeValue(null);
                  }}
                  iconName='failed-remove'
                />
              )}
              <IconButton
                isAdornment
                onClick={() => {
                  currentPanel ? setCurrentPanel(null) : setCurrentPanel('date');
                }}
                iconName='calendar'
                iconVariant='outline'
                className='ml-2'
              />
            </>
          ) : undefined
        }
      />
      <FloatingPortal id='portal-root'>
        {!!currentPanel && (
          <FloatingFocusManager
            context={context}
            modal={false}
            returnFocus={!!isOnChangeFocusInput}
            order={['reference', 'content']}
          >
            <FloatingOverlay className='z-picker' lockScroll>
              <div
                {...getFloatingProps({
                  ref: floating,
                  style: {
                    position: strategy,
                    left: x ?? 0,
                    top: y ?? 0,
                    overflow: 'auto',
                  },
                })}
              >
                <div className='paper flex flex-row'>
                  {currentPanel === 'date' && (
                    <PanelDate
                      handleNextMonth={handleNextMonth}
                      handleNextYear={handleNextYear}
                      handlePrevMonth={handlePrevMonth}
                      handlePrevYear={handlePrevYear}
                      dateSelected={dateValue}
                      onSelected={handleSelected}
                      onTogglePanel={() => {
                        setCurrentPanel('month');
                      }}
                      viewDate={viewDate}
                      locale='vi'
                      disableFuture={disableFuture}
                      disablePast={disablePast}
                    />
                  )}
                  {currentPanel === 'month' && (
                    <PanelMonth
                      handleNextYear={handleNextYear}
                      handlePrevYear={handlePrevYear}
                      viewDate={viewDate}
                      onSelected={handleMonthSelected}
                      onTogglePanel={() => {
                        setCurrentPanel('date');
                      }}
                      monthSelected={viewDate}
                    />
                  )}
                  <div className='ml-6 border-l border-l-lineBorder-borderDefault'>
                    <PanelTime
                      value={timeValue}
                      onChange={(time) => {
                        setTimeValue(time);
                      }}
                    />
                  </div>
                </div>
              </div>
            </FloatingOverlay>
          </FloatingFocusManager>
        )}
      </FloatingPortal>
    </div>
  );
}

export default withContainer<DateTimePickerProps>(DateTimePicker);
