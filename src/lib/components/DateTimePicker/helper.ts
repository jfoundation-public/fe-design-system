import clsx from 'clsx';
import { isAfter, isBefore, isSameDay } from 'date-fns';

import { CellProps } from './types';

const CELL_TYPE_CLASSES = {
  day: 'date-picker-cell--day',
  start: 'date-picker-cell--start',
  center: 'date-picker-cell--center',
  end: 'date-picker-cell--end',
  month: 'date-picker-cell--month',
};

function getCellClasses({
  type = 'day',
  disabled,
  selected,
  actived,
  hovered,
  isDim,
}: Pick<CellProps, 'actived' | 'disabled' | 'hovered' | 'selected' | 'type' | 'isDim'>) {
  return clsx(
    'date-picker-cell',
    disabled && [CELL_TYPE_CLASSES[type], 'date-picker-cell--disabled'],
    !disabled &&
      (isDim
        ? 'date-picker-cell--dim'
        : [
            CELL_TYPE_CLASSES[type],
            selected && 'date-picker-cell--selected',
            actived && 'date-picker-cell--actived',
            hovered && 'date-picker-cell--hovered',
          ]),
  );
}

function getDatePickerClasses({
  isSuccess,
  isError,
  isWarning,
  disabled,
  readOnly,
}: {
  isSuccess?: boolean;
  isError?: boolean;
  isWarning?: boolean;
  disabled?: boolean;
  readOnly?: boolean;
}) {
  return clsx(
    'date-picker',
    isSuccess && 'date-picker--success',
    isError && 'date-picker--error',
    isWarning && 'date-picker--warning',
    disabled && 'date-picker--disabled',
    readOnly && 'date-picker--readonly',
  );
}

function getCellTypeForRange(
  fromDateSelected: Date | null,
  toDateSelected: Date | null,
): (
  _curr: Date,
  currentHover?: Date | null,
  selecting?: 'fromDate' | 'toDate',
) => 'day' | 'start' | 'center' | 'end' {
  return (currentDate: Date, currentHover?: Date | null, selecting?: 'fromDate' | 'toDate') => {
    if (
      (fromDateSelected && isSameDay(currentDate, fromDateSelected)) ||
      (selecting === 'fromDate' && currentHover && isSameDay(currentDate, currentHover))
    )
      return 'start';
    if (
      (toDateSelected && isSameDay(currentDate, toDateSelected)) ||
      (selecting === 'toDate' && currentHover && isSameDay(currentDate, currentHover))
    )
      return 'end';
    if (
      fromDateSelected &&
      toDateSelected &&
      isAfter(currentDate, fromDateSelected) &&
      isBefore(currentDate, toDateSelected)
    )
      return 'center';
    if (
      fromDateSelected &&
      selecting === 'toDate' &&
      currentHover &&
      isAfter(currentDate, fromDateSelected) &&
      isBefore(currentDate, currentHover)
    )
      return 'center';
    if (
      toDateSelected &&
      selecting === 'fromDate' &&
      currentHover &&
      isBefore(currentDate, toDateSelected) &&
      isAfter(currentDate, currentHover)
    )
      return 'center';
    return 'day';
  };
}

export { getCellClasses, getCellTypeForRange, getDatePickerClasses };
