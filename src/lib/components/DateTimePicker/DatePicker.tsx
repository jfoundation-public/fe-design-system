import {
  autoUpdate,
  flip,
  FloatingFocusManager,
  FloatingOverlay,
  FloatingPortal,
  offset,
  size,
  useDismiss,
  useFloating,
  useInteractions,
} from '@floating-ui/react-dom-interactions';
import { format } from 'date-fns';
import { useEffect, useRef, useState } from 'react';

import { createCustomInputEvent, noop, parseDateUsingDateFormat } from '@/lib/utils';

import { IconButton } from '../Button';
import withContainer from '../FieldContainer/withContainer';
import { InputBase } from '../InputBase';
import { getDatePickerClasses } from './helper';
import { useViewDate } from './hooks';
import PanelDate from './PanelDate';
import PanelMonth from './PanelMonth';
import { DatePickerProps } from './types';

// FIXME: fix bug Invalid Date when add dateformat

const today = new Date();

function DatePicker(props: DatePickerProps) {
  const {
    value: _value,
    onChange,
    name,
    placeholder,
    isError,
    isSuccess,
    isWarning,
    disabled,
    readOnly,
    dateformat = 'yyyy-MM-dd',
    pickerPlacement,
    disablePast,
    disableFuture,
    isOnChangeFocusInput,
  } = props;
  const prevValueProp = useRef(_value);
  const [value, setValue] = useState<string | null>(_value ?? null);
  const [currentPanel, setCurrentPanel] = useState<'date' | 'month' | null>(null);

  const { x, y, reference, floating, strategy, context } = useFloating({
    whileElementsMounted: autoUpdate,
    open: !!currentPanel,
    onOpenChange: () => {
      if (currentPanel !== 'month') {
        setCurrentPanel(null);
      }
    },
    placement: pickerPlacement ?? 'bottom-start',
    middleware: [
      offset(5),
      flip({ padding: 8 }),
      size({
        padding: 8,
      }),
    ],
  });

  const { getFloatingProps } = useInteractions([useDismiss(context)]);

  const {
    viewDate,
    handleNextMonth,
    handleNextYear,
    handlePrevMonth,
    handlePrevYear,
    handleSetViewDate,
  } = useViewDate(parseDateUsingDateFormat(value, dateformat) ?? today);

  const handleSelected = (selected: Date) => {
    setValue(format(selected, dateformat));
    setCurrentPanel(null);
  };

  const handleMonthSelected = (date: Date) => {
    handleSetViewDate(date);
    setCurrentPanel('date');
  };

  // const handleInputDate = (e: React.ChangeEvent<HTMLInputElement>) => {
  //   const { value } = e.target;
  //   const length = value.length;
  //   if (length > 10) return;

  //   const formattedDate = () => {
  //     switch (length) {
  //       // get keycode
  //       case 4:
  //         return `${value.slice(0, 4)}-`;
  //       case 7:
  //         return `${value.slice(0, 4)}-${value.slice(5, 7)}-`;
  //       default:
  //         return value;
  //     }
  //   };

  //   // Update the date state variable
  //   setValue(formattedDate());
  // };

  useEffect(() => {
    onChange && onChange(createCustomInputEvent({ name, value: value ?? '' }));
  }, [name, onChange, value, dateformat]);

  useEffect(() => {
    if (prevValueProp.current !== _value) {
      prevValueProp.current = _value;
      setValue(_value ?? '');
    }
  }, [_value]);

  const datePickerClasses = getDatePickerClasses({
    isSuccess,
    isError,
    isWarning,
    disabled,
    readOnly,
  });

  return (
    <div className={datePickerClasses} ref={reference}>
      <InputBase
        placeholder={
          placeholder ? placeholder + ' ' + dateformat.toUpperCase() : dateformat.toUpperCase()
        }
        value={value ?? ''}
        onChange={(e) => {
          setValue(e.target.value);
          // handleInputDate(e);
        }}
        onClick={
          !isOnChangeFocusInput
            ? noop
            : () => {
                !readOnly && !disabled && setCurrentPanel('date');
              }
        }
        onFocus={
          isOnChangeFocusInput
            ? noop
            : () => {
                !readOnly && !disabled && setCurrentPanel('date');
              }
        }
        disabled={disabled}
        readOnly={readOnly}
        className='date-picker__input'
        endAdornment={
          !readOnly && !disabled ? (
            <>
              {!!value && (
                <IconButton
                  type='button'
                  isAdornment
                  onMouseDown={(e) => {
                    e.preventDefault();
                    e.stopPropagation();
                  }}
                  onMouseUp={(e) => {
                    e.preventDefault();
                    e.stopPropagation();
                    setValue(null);
                  }}
                  iconName='failed-remove'
                />
              )}
              <IconButton
                type='button'
                isAdornment
                onClick={() => {
                  currentPanel ? setCurrentPanel(null) : setCurrentPanel('date');
                }}
                iconName='calendar'
                iconVariant='outline'
                className='ml-2'
              />
            </>
          ) : undefined
        }
      />
      <FloatingPortal id='portal-root'>
        {!!currentPanel && (
          <FloatingFocusManager
            context={context}
            modal={false}
            returnFocus={!!isOnChangeFocusInput}
            order={['reference', 'content']}
          >
            <FloatingOverlay className='z-picker' lockScroll>
              <div
                {...getFloatingProps({
                  ref: floating,
                  style: {
                    position: strategy,
                    left: x ?? 0,
                    top: y ?? 0,
                    overflow: 'auto',
                  },
                })}
              >
                <div className='paper'>
                  {currentPanel === 'date' && (
                    <PanelDate
                      handleNextMonth={handleNextMonth}
                      handleNextYear={handleNextYear}
                      handlePrevMonth={handlePrevMonth}
                      handlePrevYear={handlePrevYear}
                      dateSelected={parseDateUsingDateFormat(value, dateformat)}
                      onSelected={handleSelected}
                      onTogglePanel={() => {
                        setCurrentPanel('month');
                      }}
                      viewDate={viewDate}
                      locale='vi'
                      disablePast={disablePast}
                      disableFuture={disableFuture}
                    />
                  )}
                  {currentPanel === 'month' && (
                    <PanelMonth
                      handleNextYear={handleNextYear}
                      handlePrevYear={handlePrevYear}
                      viewDate={viewDate}
                      onSelected={handleMonthSelected}
                      onTogglePanel={() => {
                        setCurrentPanel('date');
                      }}
                      monthSelected={viewDate}
                    />
                  )}
                </div>
              </div>
            </FloatingOverlay>
          </FloatingFocusManager>
        )}
      </FloatingPortal>
    </div>
  );
}

export default withContainer<DatePickerProps>(DatePicker);
