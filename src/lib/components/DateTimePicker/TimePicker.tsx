import {
  autoUpdate,
  flip,
  FloatingFocusManager,
  FloatingOverlay,
  FloatingPortal,
  offset,
  size,
  useDismiss,
  useFloating,
  useInteractions,
} from '@floating-ui/react-dom-interactions';
import { useCallback, useState } from 'react';

import { createCustomInputEvent, noop } from '@/lib/utils';

import { IconButton } from '../Button';
import withContainer from '../FieldContainer/withContainer';
import { InputBase } from '../InputBase';
import { getDatePickerClasses } from './helper';
import PanelTime from './PanelTime';
import { TimePickerProps } from './types';

/**
 * Controlled time picker
 */
function TimePicker(props: TimePickerProps) {
  const {
    value,
    onChange,
    name,
    placeholder,
    isError,
    isSuccess,
    isWarning,
    disabled,
    readOnly,
    isOnChangeFocusInput,
    pickerPlacement,
  } = props;
  const [open, setOpen] = useState<boolean>(false);

  const { x, y, reference, floating, strategy, context } = useFloating({
    whileElementsMounted: autoUpdate,
    open,
    onOpenChange: setOpen,
    placement: pickerPlacement ?? 'bottom-start',
    middleware: [
      offset(5),
      flip({ padding: 8 }),
      size({
        padding: 8,
      }),
    ],
  });

  const { getFloatingProps } = useInteractions([useDismiss(context)]);

  const handleChange = useCallback(
    (_value?: string | null) => {
      onChange && onChange(createCustomInputEvent({ name, value: _value ?? '' }));
    },
    [name, onChange],
  );

  const datePickerClasses = getDatePickerClasses({
    isSuccess,
    isError,
    isWarning,
    disabled,
    readOnly,
  });

  return (
    <div className={datePickerClasses} ref={reference}>
      <InputBase
        placeholder={placeholder}
        value={value ?? ''}
        onChange={noop}
        onFocus={
          isOnChangeFocusInput
            ? noop
            : () => {
                !readOnly && !disabled && setOpen(true);
              }
        }
        onClick={
          !isOnChangeFocusInput
            ? noop
            : () => {
                !readOnly && !disabled && setOpen(true);
              }
        }
        disabled={disabled}
        readOnly={readOnly}
        className='date-picker__input'
        endAdornment={
          !readOnly && !disabled ? (
            <>
              {!!value && !readOnly && !disabled && (
                <IconButton
                  isAdornment
                  onMouseDown={(e) => {
                    e.preventDefault();
                    e.stopPropagation();
                  }}
                  onMouseUp={(e) => {
                    e.preventDefault();
                    e.stopPropagation();
                    onChange && onChange(createCustomInputEvent({ name, value: '' }));
                  }}
                  iconName='failed-remove'
                />
              )}
              <IconButton
                isAdornment
                onClick={() => setOpen((o) => !o)}
                iconName='time-clock'
                iconVariant='outline'
                className='ml-2'
              />
            </>
          ) : undefined
        }
      />

      <FloatingPortal id='portal-root'>
        {open && (
          <FloatingFocusManager
            context={context}
            modal={false}
            returnFocus={!!isOnChangeFocusInput}
            order={['reference', 'content']}
          >
            <FloatingOverlay className='z-picker' lockScroll>
              <div
                {...getFloatingProps({
                  ref: floating,
                  style: {
                    position: strategy,
                    left: x ?? 0,
                    top: y ?? 0,
                    overflow: 'auto',
                  },
                })}
              >
                <div className='paper'>
                  <PanelTime value={value} onChange={handleChange} />
                </div>
              </div>
            </FloatingOverlay>
          </FloatingFocusManager>
        )}
      </FloatingPortal>
    </div>
  );
}

export { TimePicker };

export default withContainer<TimePickerProps>(TimePicker);
