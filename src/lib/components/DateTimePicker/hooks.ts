import { addMonths, addYears, isSameDay } from 'date-fns';
import { useEffect, useRef, useState } from 'react';

const newDate = new Date();

function useViewDate(_viewDate = newDate) {
  const prevViewDate = useRef(_viewDate);

  const [viewDate, setViewDate] = useState(_viewDate);

  function handleNextMonth() {
    setViewDate(addMonths(viewDate, 1));
  }

  function handlePrevMonth() {
    setViewDate(addMonths(viewDate, -1));
  }

  function handleNextYear() {
    setViewDate(addYears(viewDate, 1));
  }

  function handlePrevYear() {
    setViewDate(addYears(viewDate, -1));
  }

  function handleSetViewDate(newViewDate: Date) {
    setViewDate(newViewDate);
  }

  // (function sync() {
  //   if (prevViewDate.current !== _viewDate) {
  //     prevViewDate.current = _viewDate;
  //     setViewDate(_viewDate);
  //   }
  // })();

  useEffect(() => {
    if (!isSameDay(prevViewDate.current, _viewDate)) {
      prevViewDate.current = _viewDate;
      setViewDate(_viewDate);
    }
  }, [_viewDate]);

  return {
    viewDate,
    handleNextMonth,
    handlePrevMonth,
    handleNextYear,
    handlePrevYear,
    handleSetViewDate,
  };
}

export { useViewDate };
