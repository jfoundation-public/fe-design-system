export const WEEK_START_DAY = {
  vi: 1 as const,
  enUS: 0 as const,
};

export const DAY_PER_WEEK = 7;

export const PANEL_DATE_ROW_NUMS = 6;

export const PANEL_DATE_COL_NUMS = 7;

export const HOURS = 24;

export const MINUTES = 60;
