import clsx from 'clsx';

import { getCellClasses } from './helper';
import { CellProps } from './types';

export default function Cell(props: CellProps) {
  const {
    type = 'day',
    disabled,
    selected,
    hovered,
    actived,
    isDim,
    className,
    children,
    ...restProps
  } = props;

  const classes = getCellClasses({ type, disabled, selected, hovered, actived, isDim });
  return (
    <button className={clsx(classes, className)} {...restProps}>
      {children}
    </button>
  );
}
