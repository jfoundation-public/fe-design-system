import {
  autoUpdate,
  flip,
  FloatingFocusManager,
  FloatingOverlay,
  FloatingPortal,
  offset,
  size,
  useDismiss,
  useFloating,
  useInteractions,
} from '@floating-ui/react-dom-interactions';
import { format } from 'date-fns';
import { useEffect, useRef, useState } from 'react';

import { IconButton, parseDateUsingDateFormat } from '@/lib';
import { createCustomInputEvent, isEqual, noop } from '@/lib/utils';

import withContainer from '../FieldContainer/withContainer';
import { Icon } from '../Icon';
import { InputBase } from '../InputBase';
import { getDatePickerClasses } from './helper';
import { useViewDate } from './hooks';
import PanelDateRange from './PanelDateRange';
import PanelMonth from './PanelMonth';
import { DateRangePickerProps } from './types';

const today = new Date();

function DateRangePicker(props: DateRangePickerProps) {
  const {
    value: _value,
    onChange,
    name,
    placeholder,
    isError,
    isSuccess,
    isWarning,
    disabled,
    readOnly,
    isOnChangeFocusInput,
    dateformat = 'yyyy-MM-dd',
  } = props;
  const prevValueProp = useRef(_value);
  const [fromValue, setFromValue] = useState<string | null>(_value?.[0] ?? null);
  const [toValue, setToValue] = useState<string | null>(_value?.[1] ?? null);
  const [currentPanel, setCurrentPanel] = useState<'date' | 'month' | null>(null);
  const [currentSelect, setCurrentSelect] = useState<'fromDate' | 'toDate'>('fromDate');

  const { x, y, reference, floating, strategy, context } = useFloating({
    whileElementsMounted: autoUpdate,
    open: !!currentPanel,
    onOpenChange: () => {
      if (currentPanel !== 'month') {
        setCurrentPanel(null);
      }
    },
    placement: 'bottom-start',
    middleware: [
      offset(5),
      flip({ padding: 8 }),
      size({
        padding: 8,
      }),
    ],
  });

  const { getFloatingProps } = useInteractions([useDismiss(context)]);

  const {
    viewDate,
    handleNextMonth,
    handleNextYear,
    handlePrevMonth,
    handlePrevYear,
    handleSetViewDate,
  } = useViewDate(parseDateUsingDateFormat(fromValue, dateformat) ?? today);

  const handleFromSelected = (selected: Date) => {
    setFromValue(format(selected, dateformat));
    setCurrentSelect('toDate');
    if (toValue) {
      setCurrentPanel(null);
    }
  };

  const handleToSelected = (selected: Date) => {
    setToValue(format(selected, dateformat));
    if (fromValue) {
      setCurrentPanel(null);
    }
  };

  const handleMonthSelected = (date: Date) => {
    handleSetViewDate(date);
    setCurrentPanel('date');
  };

  useEffect(() => {
    onChange &&
      onChange(
        createCustomInputEvent({
          name,
          value: [fromValue ?? '', toValue ?? ''],
        }),
      );
  }, [fromValue, name, onChange, toValue]);

  useEffect(() => {
    if (!isEqual(prevValueProp.current, _value)) {
      prevValueProp.current = _value;
      setFromValue(_value?.[0] ?? null);
      setToValue(_value?.[1] ?? null);
    }
  }, [_value]);

  const datePickerClasses = getDatePickerClasses({
    isSuccess,
    isError,
    isWarning,
    disabled,
    readOnly,
  });

  const [fromPlaceHolder, toPlaceHolder] = placeholder ?? ['', ''];

  return (
    <div className={datePickerClasses} ref={reference}>
      <div className='flex flex-row'>
        <InputBase
          placeholder={
            fromPlaceHolder
              ? fromPlaceHolder + ' ' + dateformat.toUpperCase()
              : dateformat.toUpperCase()
          }
          value={fromValue ?? ''}
          onChange={(e) => {
            setFromValue(e.target.value);
          }}
          onClick={
            !isOnChangeFocusInput
              ? noop
              : () => {
                  setCurrentSelect('fromDate');
                  !readOnly && !disabled && setCurrentPanel('date');
                }
          }
          onFocus={
            isOnChangeFocusInput
              ? noop
              : () => {
                  setCurrentSelect('fromDate');
                  !readOnly && !disabled && setCurrentPanel('date');
                }
          }
          disabled={disabled}
          readOnly={readOnly}
          className='date-picker__input date-picker__input--center'
          endAdornment={
            !readOnly && !disabled && fromValue ? (
              <IconButton
                type='button'
                isAdornment
                onMouseDown={(e) => {
                  e.preventDefault();
                  e.stopPropagation();
                }}
                onMouseUp={(e) => {
                  e.preventDefault();
                  e.stopPropagation();
                  setFromValue(null);
                }}
                iconName='failed-remove'
                iconVariant='solid'
                className='ml-2'
              />
            ) : undefined
          }
        />
        <Icon color='default' iconName='arrow_right' variant='outline' />
        <InputBase
          placeholder={
            toPlaceHolder
              ? toPlaceHolder + ' ' + dateformat.toUpperCase()
              : dateformat.toUpperCase()
          }
          value={toValue ?? ''}
          onChange={(e) => {
            setToValue(e.target.value);
          }}
          onClick={
            !isOnChangeFocusInput
              ? noop
              : () => {
                  setCurrentSelect('toDate');
                  !readOnly && !disabled && setCurrentPanel('date');
                }
          }
          onFocus={
            isOnChangeFocusInput
              ? noop
              : () => {
                  setCurrentSelect('toDate');
                  !readOnly && !disabled && setCurrentPanel('date');
                }
          }
          disabled={disabled}
          readOnly={readOnly}
          className='date-picker__input date-picker__input--center'
          endAdornment={
            !readOnly && !disabled && toValue ? (
              <IconButton
                type='button'
                isAdornment
                onMouseDown={(e) => {
                  e.preventDefault();
                  e.stopPropagation();
                }}
                onMouseUp={(e) => {
                  e.preventDefault();
                  e.stopPropagation();
                  setToValue(null);
                }}
                iconName='failed-remove'
                iconVariant='solid'
                className='ml-2'
              />
            ) : undefined
          }
        />
        {!readOnly && !disabled && (
          <IconButton
            isAdornment
            onClick={() => {
              currentPanel ? setCurrentPanel(null) : setCurrentPanel('date');
            }}
            iconName='calendar'
            iconVariant='outline'
            className='ml-2'
          />
        )}
      </div>

      <FloatingPortal id='portal-root'>
        {!!currentPanel && (
          <FloatingFocusManager
            context={context}
            modal={false}
            returnFocus={!!isOnChangeFocusInput}
            order={['reference', 'content']}
          >
            <FloatingOverlay className='z-picker' lockScroll>
              <div
                {...getFloatingProps({
                  ref: floating,
                  style: {
                    position: strategy,
                    left: x ?? 0,
                    top: y ?? 0,
                    overflow: 'auto',
                  },
                })}
              >
                <div className='paper'>
                  {currentPanel === 'date' && (
                    <PanelDateRange
                      handleNextMonth={handleNextMonth}
                      handleNextYear={handleNextYear}
                      handlePrevMonth={handlePrevMonth}
                      handlePrevYear={handlePrevYear}
                      fromDateSelected={parseDateUsingDateFormat(fromValue, dateformat)}
                      toDateSelected={parseDateUsingDateFormat(toValue, dateformat)}
                      onFromDateSelected={handleFromSelected}
                      onToDateSelected={handleToSelected}
                      onTogglePanel={() => {
                        setCurrentPanel('month');
                      }}
                      viewDate={viewDate}
                      selecting={currentSelect}
                      locale='vi'
                    />
                  )}
                  {currentPanel === 'month' && (
                    <PanelMonth
                      handleNextYear={handleNextYear}
                      handlePrevYear={handlePrevYear}
                      viewDate={viewDate}
                      onSelected={handleMonthSelected}
                      onTogglePanel={() => {
                        setCurrentPanel('date');
                      }}
                      monthSelected={viewDate}
                    />
                  )}
                </div>
              </div>
            </FloatingOverlay>
          </FloatingFocusManager>
        )}
      </FloatingPortal>
    </div>
  );
}

export default withContainer<DateRangePickerProps>(DateRangePicker);
