import { Placement } from '@floating-ui/react-dom-interactions';
import React, { ComponentPropsWithoutRef } from 'react';

import { IconName } from '../Icon/types';

export type Locale = 'vi' | 'enUS';

export interface DatePickerProps extends ComponentPropsWithoutRef<'div'> {
  name: string;
  value?: string;
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  onBlur?: (e: React.FocusEvent<HTMLInputElement>) => void;
  onFocus?: (e: React.FocusEvent<HTMLInputElement>) => void;
  placeholder?: string;
  label?: string;
  labelIcon?: IconName;
  helperText?: string;
  isError?: boolean;
  isSuccess?: boolean;
  isWarning?: boolean;
  disabled?: boolean;
  readOnly?: boolean;
  required?: boolean;
  dateformat?: string;
  pickerPlacement?: Placement;
  disableFuture?: boolean;
  disablePast?: boolean;
  isOnChangeFocusInput?: boolean;
}

export interface TimePickerProps extends ComponentPropsWithoutRef<'div'> {
  name: string;
  value?: string | null;
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  onBlur?: (e: React.FocusEvent<HTMLInputElement>) => void;
  onFocus?: (e: React.FocusEvent<HTMLInputElement>) => void;
  placeholder?: string;
  label?: string;
  labelIcon?: IconName;
  helperText?: string;
  isError?: boolean;
  isSuccess?: boolean;
  isWarning?: boolean;
  disabled?: boolean;
  readOnly?: boolean;
  required?: boolean;
  isOnChangeFocusInput?: boolean;
  pickerPlacement?: Placement;
}

export interface DateTimePickerProps extends ComponentPropsWithoutRef<'div'> {
  name: string;
  value?: string;
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  onBlur?: (e: React.FocusEvent<HTMLInputElement>) => void;
  onFocus?: (e: React.FocusEvent<HTMLInputElement>) => void;
  placeholder?: string;
  label?: string;
  labelIcon?: IconName;
  helperText?: string;
  isError?: boolean;
  isSuccess?: boolean;
  isWarning?: boolean;
  disabled?: boolean;
  readOnly?: boolean;
  required?: boolean;
  isOnChangeFocusInput?: boolean;
  pickerPlacement?: Placement;
  disableFuture?: boolean;
  disablePast?: boolean;
}

export interface DateRangePickerProps extends Omit<ComponentPropsWithoutRef<'div'>, 'placeholder'> {
  name: string;
  value?: [string, string];
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  onBlur?: (e: React.FocusEvent<HTMLInputElement>) => void;
  onFocus?: (e: React.FocusEvent<HTMLInputElement>) => void;
  placeholder?: [string, string];
  label?: string;
  labelIcon?: IconName;
  helperText?: string;
  isError?: boolean;
  isSuccess?: boolean;
  isWarning?: boolean;
  disabled?: boolean;
  readOnly?: boolean;
  required?: boolean;
  isOnChangeFocusInput?: boolean;
  dateformat?: string;
}

export interface TimeRangePickerProps extends Omit<ComponentPropsWithoutRef<'div'>, 'placeholder'> {
  name: string;
  value?: [string, string];
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  onBlur?: (e: React.FocusEvent<HTMLInputElement>) => void;
  onFocus?: (e: React.FocusEvent<HTMLInputElement>) => void;
  placeholder?: [string, string];
  label?: string;
  labelIcon?: IconName;
  helperText?: string;
  isError?: boolean;
  isSuccess?: boolean;
  isWarning?: boolean;
  disabled?: boolean;
  readOnly?: boolean;
  required?: boolean;
  isOnChangeFocusInput?: boolean;
}

export interface CellProps extends Omit<ComponentPropsWithoutRef<'button'>, 'type'> {
  disabled?: boolean;
  selected?: boolean;
  hovered?: boolean;
  actived?: boolean;
  isDim?: boolean;
  type?: 'day' | 'start' | 'center' | 'end' | 'month';
  className?: string;
  onMouseEnter?: () => void;
  onMouseLeave?: () => void;
}

export interface PanelDateProps {
  viewDate: Date;
  locale?: Locale;
  dateSelected?: Date | null;
  onSelected: (selected: Date) => void;
  handleNextMonth: () => void;
  handleNextYear: () => void;
  handlePrevMonth: () => void;
  handlePrevYear: () => void;
  onTogglePanel: () => void;
  disableFuture?: boolean;
  disablePast?: boolean;
}

export interface PanelDateRangeProps {
  viewDate: Date;
  locale?: Locale;
  selecting?: 'fromDate' | 'toDate';
  fromDateSelected?: Date | null;
  toDateSelected?: Date | null;
  onFromDateSelected: (selected: Date) => void;
  onToDateSelected: (selected: Date) => void;
  handleNextMonth: () => void;
  handleNextYear: () => void;
  handlePrevMonth: () => void;
  handlePrevYear: () => void;
  onTogglePanel: () => void;
}

export interface PanelMonthProps {
  viewDate: Date;
  locale?: Locale;
  monthSelected?: Date | null;
  onSelected: (selected: Date) => void;
  handleNextYear: () => void;
  handlePrevYear: () => void;
  onTogglePanel: () => void;
}

export interface PanelTimeProps {
  value?: string | null;
  onChange: (val?: string | null) => void;
}
