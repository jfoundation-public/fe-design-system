import clsx from 'clsx';
import { useEffect, useRef, useState } from 'react';

import { addLeadingZeros, isNil, noop, range } from '@/lib/utils';

import { HOURS, MINUTES } from './constants';
import { PanelTimeProps } from './types';
import { combineTimeValue, parseTimeFromValue } from './utiils';

const addLeadingZero = addLeadingZeros(2);

export default function PanelTime(props: PanelTimeProps) {
  const { value, onChange } = props;
  const prevValue = useRef(value);
  const [hourSelected, setHourSelected] = useState<number | null>(
    parseTimeFromValue(value).hourValue,
  );
  const [minuteSelected, setMinuteSelected] = useState<number | null>(
    parseTimeFromValue(value).minuteValue,
  );

  useEffect(() => {
    if (isNil(hourSelected) || isNil(minuteSelected)) return;
    onChange(combineTimeValue(hourSelected, minuteSelected));
  }, [hourSelected, minuteSelected, onChange]);

  (function syncValue() {
    if (prevValue.current !== value) {
      const { hourValue, minuteValue } = parseTimeFromValue(value);
      setHourSelected(hourValue);
      setMinuteSelected(minuteValue);
      prevValue.current = value;
    }
  })();

  return (
    <div className='date-picker__panel-time'>
      <div className='date-picker__panel-time-content'>
        <TimeColumn
          data={range(0, HOURS)}
          value={hourSelected}
          onSelected={setHourSelected}
          borderRight
        />
        <TimeColumn
          data={range(0, MINUTES)}
          value={minuteSelected}
          onSelected={setMinuteSelected}
        />
      </div>
    </div>
  );
}

function TimeColumn({
  data,
  value,
  onSelected,
  borderRight,
}: {
  data: number[];
  value: number | null;
  onSelected: (v: number) => void;
  borderRight?: boolean;
}) {
  const liRefs = useRef<Map<number, HTMLElement | null>>(new Map());

  return (
    <ul className={clsx(borderRight && 'time-column--border-right')}>
      {data.map((v) => (
        <li
          ref={(element) => {
            liRefs.current.set(v, element);
          }}
          key={v}
          role='menuitem'
          onKeyDown={noop}
          onClick={() => onSelected(v)}
        >
          <div className={clsx('time-unit', value === v && 'time-unit--selected')}>
            {addLeadingZero(v)}
          </div>
        </li>
      ))}
    </ul>
  );
}
