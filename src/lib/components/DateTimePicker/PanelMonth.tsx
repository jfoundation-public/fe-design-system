import { format, isSameMonth, isThisMonth, setDate, setMonth } from 'date-fns';
import { splitEvery } from 'ramda';
import { useMemo } from 'react';

import { IconButton } from '../Button';
import { Typography } from '../Typography';
import Cell from './Cell';
import { useViewDate } from './hooks';
import { PanelMonthProps } from './types';
import { getAbbrMonths, getLocaleFns } from './utiils';

export default function PanelMonth(props: PanelMonthProps) {
  const { viewDate: _viewDate, locale = 'vi', monthSelected, onSelected, onTogglePanel } = props;
  const { viewDate, handleNextYear, handlePrevYear } = useViewDate(_viewDate);

  const localeFns = useMemo(() => getLocaleFns(locale), [locale]);
  const abbrMonths = getAbbrMonths(locale);
  return (
    <div className='date-picker-panel'>
      <div className='date-picker-panel-header'>
        <div className='flex items-center'>
          <IconButton
            onClick={handlePrevYear}
            isAdornment
            iconVariant='outline'
            iconName='arrow-double-left'
          />
        </div>
        <Typography
          variant='subtitle1'
          className='text-text-700 cursor-pointer'
          onClick={onTogglePanel}
        >
          {format(viewDate, 'yyyy', { locale: localeFns })}
        </Typography>
        <div className='flex items-center'>
          <IconButton
            onClick={handleNextYear}
            isAdornment
            iconVariant='outline'
            iconName='arrow-double-right'
          />
        </div>
      </div>
      <table className='date-picker--month-table'>
        <tbody>
          {splitEvery(3, abbrMonths).map((abbrMonthQuater, quaterIndex) => {
            return (
              <tr key={abbrMonthQuater.join('')}>
                {abbrMonthQuater.map((abbrMonthStr, monthOfQuater) => {
                  const currentMonth = monthOfQuater + quaterIndex * 3;
                  const startMonthDate = setMonth(setDate(viewDate, 1), currentMonth);
                  const actived = isThisMonth(startMonthDate);
                  const selected = monthSelected
                    ? isSameMonth(monthSelected, startMonthDate)
                    : false;
                  return (
                    <td key={abbrMonthStr}>
                      <Cell
                        actived={actived}
                        selected={selected}
                        onClick={() => {
                          onSelected(startMonthDate);
                        }}
                        type='month'
                      >
                        {abbrMonthStr}
                      </Cell>
                    </td>
                  );
                })}
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}
