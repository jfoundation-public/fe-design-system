import { addDays, addMonths, format, getDate, isSameMonth, isToday } from 'date-fns';
import { useMemo, useState } from 'react';

import { range } from '@/lib/utils';

import { IconButton } from '../Button';
import { Typography } from '../Typography';
import Cell from './Cell';
import { DAY_PER_WEEK, PANEL_DATE_COL_NUMS, PANEL_DATE_ROW_NUMS } from './constants';
import { getCellTypeForRange } from './helper';
import { PanelDateRangeProps } from './types';
import { getCellState, getLocaleFns, getNarrowWeekDays, getStartDateOfPanel } from './utiils';

/**
 * Controlled date range panel
 */
export default function PanelDateRange(props: PanelDateRangeProps) {
  const {
    viewDate,
    handleNextMonth,
    handleNextYear,
    handlePrevMonth,
    handlePrevYear,
    fromDateSelected = null,
    toDateSelected = null,
    selecting = 'fromDate',
    onFromDateSelected,
    onToDateSelected,
    onTogglePanel,
    locale = 'vi',
  } = props;
  const localeFns = useMemo(() => getLocaleFns(locale), [locale]);
  const [currentHovered, setCurrentHovered] = useState<Date | null>(null);

  function handleSelectDate(date: Date) {
    if (selecting === 'fromDate') {
      onFromDateSelected(date);
    }

    if (selecting === 'toDate') {
      onToDateSelected(date);
    }
  }

  const firstDate = getStartDateOfPanel(viewDate, locale);
  const narrowWeekDays = getNarrowWeekDays(locale);

  const getCellType = getCellTypeForRange(fromDateSelected, toDateSelected);

  return (
    <div className='flex flex-row justify-between space-x-8'>
      <div className='date-picker-panel'>
        <div className='date-picker-panel-header'>
          <div className='flex items-center'>
            <IconButton
              onClick={handlePrevYear}
              isAdornment
              iconVariant='outline'
              iconName='arrow-double-left'
            />
            <IconButton
              onClick={handlePrevMonth}
              isAdornment
              iconVariant='outline'
              iconName='arrow-left'
            />
          </div>
          <Typography
            variant='subtitle1'
            className='text-text-700 cursor-pointer'
            onClick={onTogglePanel}
          >
            {format(viewDate, 'LLLL, yyyy', { locale: localeFns })}
          </Typography>
          <div className='flex items-center w-12'></div>
        </div>
        <table className='date-picker--date-table'>
          <thead>
            <tr>
              {narrowWeekDays.map((narrowDayStr) => (
                <th key={narrowDayStr}>
                  <span className='subtitle2 text-tertiary'>{narrowDayStr}</span>
                </th>
              ))}
            </tr>
          </thead>
          <tbody>
            {range(0, PANEL_DATE_ROW_NUMS).map((rowNum) => (
              <tr key={rowNum}>
                {range(0, PANEL_DATE_COL_NUMS).map((colNum) => {
                  const currDate = addDays(firstDate, rowNum * DAY_PER_WEEK + colNum);
                  const actived = isToday(currDate);
                  const isDim = !isSameMonth(currDate, viewDate);
                  const { selected, isDisabled, hovered } = getCellState({
                    currDate,
                    fromDateSelected,
                    toDateSelected,
                    selecting,
                    currentHovered,
                  });
                  return (
                    <td key={`${rowNum}-${colNum}`}>
                      <Cell
                        actived={actived}
                        isDim={isDim}
                        selected={selected}
                        hovered={hovered}
                        disabled={isDisabled}
                        onClick={() => {
                          handleSelectDate(currDate);
                        }}
                        type={getCellType(currDate, currentHovered, selecting)}
                        onMouseEnter={() => {
                          setCurrentHovered(currDate);
                        }}
                        onMouseLeave={() => {
                          setCurrentHovered(null);
                        }}
                      >
                        {getDate(currDate)}
                      </Cell>
                    </td>
                  );
                })}
              </tr>
            ))}
          </tbody>
        </table>
      </div>
      <div className='date-picker-panel'>
        <div className='date-picker-panel-header'>
          <div className='flex items-center w-12'></div>
          <Typography
            variant='subtitle1'
            className='text-text-700 cursor-pointer'
            onClick={onTogglePanel}
          >
            {format(addMonths(viewDate, 1), 'LLLL, yyyy', { locale: localeFns })}
          </Typography>
          <div className='flex items-center'>
            <IconButton
              onClick={handleNextMonth}
              isAdornment
              iconVariant='outline'
              iconName='arrow-right'
            />
            <IconButton
              onClick={handleNextYear}
              isAdornment
              iconVariant='outline'
              iconName='arrow-double-right'
            />
          </div>
        </div>
        <table className='date-picker--date-table'>
          <thead>
            <tr>
              {narrowWeekDays.map((narrowDayStr) => (
                <th key={narrowDayStr}>
                  <span className='subtitle2 text-tertiary'>{narrowDayStr}</span>
                </th>
              ))}
            </tr>
          </thead>
          <tbody>
            {range(0, PANEL_DATE_ROW_NUMS).map((rowNum) => (
              <tr key={rowNum}>
                {range(0, PANEL_DATE_COL_NUMS).map((colNum) => {
                  const currDate = addDays(addMonths(firstDate, 1), rowNum * DAY_PER_WEEK + colNum);
                  const actived = isToday(currDate);
                  const isDim = !isSameMonth(currDate, addMonths(viewDate, 1));
                  const { selected, isDisabled, hovered } = getCellState({
                    currDate,
                    fromDateSelected,
                    toDateSelected,
                    selecting,
                    currentHovered,
                  });
                  return (
                    <td key={`${rowNum}-${colNum}`}>
                      <Cell
                        actived={actived}
                        isDim={isDim}
                        disabled={isDisabled}
                        selected={selected}
                        hovered={hovered}
                        onClick={() => {
                          handleSelectDate(currDate);
                        }}
                        type={getCellType(currDate, currentHovered, selecting)}
                        onMouseEnter={() => {
                          setCurrentHovered(currDate);
                        }}
                        onMouseLeave={() => {
                          setCurrentHovered(null);
                        }}
                      >
                        {getDate(currDate)}
                      </Cell>
                    </td>
                  );
                })}
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}
