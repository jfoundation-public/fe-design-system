import {
  addDays,
  format,
  getDate,
  isAfter,
  isBefore,
  isSameDay,
  isSameMonth,
  isToday,
} from 'date-fns';
import { useCallback, useMemo } from 'react';

import { PanelDateProps } from '@/lib';
import { range } from '@/lib/utils';

import { IconButton } from '../Button';
import { Typography } from '../Typography';
import Cell from './Cell';
import { DAY_PER_WEEK, PANEL_DATE_COL_NUMS, PANEL_DATE_ROW_NUMS } from './constants';
import { getLocaleFns, getNarrowWeekDays, getStartDateOfPanel } from './utiils';

const today = new Date();

export default function PanelDate(props: PanelDateProps) {
  const {
    viewDate,
    handleNextMonth,
    handleNextYear,
    handlePrevMonth,
    handlePrevYear,
    dateSelected = null,
    onSelected,
    onTogglePanel,
    locale = 'vi',
    disableFuture,
    disablePast,
  } = props;
  const localeFns = useMemo(() => getLocaleFns(locale), [locale]);
  const firstDate = getStartDateOfPanel(viewDate, locale);
  const narrowWeekDays = getNarrowWeekDays(locale);

  const isSelectedDayValid = useCallback(
    (date: Date) => {
      return !((isBefore(date, today) && disablePast) || (isAfter(date, today) && disableFuture));
    },
    [today],
  );

  const handleSelectDay = (date: Date) => () => {
    if (isBefore(date, today) && disablePast) return;
    if (isAfter(date, today) && disableFuture) return;
    onSelected(date);
  };

  return (
    <div className='date-picker-panel'>
      <div className='date-picker-panel-header'>
        <div className='flex items-center'>
          <IconButton
            onClick={handlePrevYear}
            isAdornment
            iconVariant='outline'
            iconName='arrow-double-left'
          />
          <IconButton
            onClick={handlePrevMonth}
            isAdornment
            iconVariant='outline'
            iconName='arrow-left'
          />
        </div>
        <Typography
          variant='subtitle1'
          className='text-text-700 cursor-pointer'
          onClick={onTogglePanel}
        >
          {format(viewDate, 'LLLL, yyyy', { locale: localeFns })}
        </Typography>
        <div className='flex items-center'>
          <IconButton
            onClick={handleNextMonth}
            isAdornment
            iconVariant='outline'
            iconName='arrow-right'
          />
          <IconButton
            onClick={handleNextYear}
            isAdornment
            iconVariant='outline'
            iconName='arrow-double-right'
          />
        </div>
      </div>
      <table className='date-picker--date-table'>
        <thead>
          <tr>
            {narrowWeekDays.map((narrowDayStr) => (
              <th key={narrowDayStr}>
                <span className='subtitle2 text-tertiary'>{narrowDayStr}</span>
              </th>
            ))}
          </tr>
        </thead>
        <tbody>
          {range(0, PANEL_DATE_ROW_NUMS).map((rowNum) => (
            <tr key={rowNum}>
              {range(0, PANEL_DATE_COL_NUMS).map((colNum) => {
                const currDate = addDays(firstDate, rowNum * DAY_PER_WEEK + colNum);
                const active = isToday(currDate);
                const isDim = !isSameMonth(currDate, viewDate);
                const isDayValidToSelect = isSelectedDayValid(currDate);
                const selected = dateSelected ? isSameDay(currDate, dateSelected) : false;
                return (
                  <td key={`${rowNum}-${colNum}`}>
                    <Cell
                      actived={active}
                      isDim={isDim}
                      selected={selected}
                      onClick={handleSelectDay(currDate)}
                      type='day'
                      disabled={!isDayValidToSelect}
                    >
                      {getDate(currDate)}
                    </Cell>
                  </td>
                );
              })}
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}
