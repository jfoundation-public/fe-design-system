import {
  autoUpdate,
  flip,
  FloatingFocusManager,
  FloatingOverlay,
  FloatingPortal,
  offset,
  size,
  useDismiss,
  useFloating,
  useInteractions,
} from '@floating-ui/react-dom-interactions';
import { useEffect, useRef, useState } from 'react';

import { IconButton } from '@/lib';
import { createCustomInputEvent, isEqual, noop } from '@/lib/utils';

import withContainer from '../FieldContainer/withContainer';
import { Icon } from '../Icon';
import { InputBase } from '../InputBase';
import { getDatePickerClasses } from './helper';
import PanelTime from './PanelTime';
import { TimeRangePickerProps } from './types';

function TimeRangePicker(props: TimeRangePickerProps) {
  const {
    value: _value,
    onChange,
    name,
    placeholder,
    isError,
    isSuccess,
    isWarning,
    disabled,
    readOnly,
    isOnChangeFocusInput,
  } = props;
  const prevValueProp = useRef(_value);
  const [fromValue, setFromValue] = useState<string>(_value?.[0] ?? '');
  const [toValue, setToValue] = useState<string>(_value?.[1] ?? '');
  const [currentPanel, setCurrentPanel] = useState<'fromTime' | 'toTime' | null>(null);

  const { x, y, reference, floating, strategy, context } = useFloating({
    whileElementsMounted: autoUpdate,
    open: !!currentPanel,
    onOpenChange: () => {
      setCurrentPanel(null);
    },
    placement: 'bottom-start',
    middleware: [
      offset(5),
      flip({ padding: 8 }),
      size({
        padding: 8,
      }),
    ],
  });

  const { getFloatingProps } = useInteractions([useDismiss(context)]);

  useEffect(() => {
    onChange &&
      onChange(
        createCustomInputEvent({
          name,
          value: [fromValue, toValue],
        }),
      );
  }, [fromValue, name, onChange, toValue]);

  useEffect(() => {
    if (!isEqual(prevValueProp.current, _value)) {
      prevValueProp.current = _value;
      setFromValue(_value?.[0] ?? '');
      setToValue(_value?.[1] ?? '');
    }
  }, [_value]);

  const datePickerClasses = getDatePickerClasses({
    isSuccess,
    isError,
    isWarning,
    disabled,
    readOnly,
  });

  const [fromPlaceHolder, toPlaceHolder] = placeholder ?? ['', ''];

  return (
    <div className={datePickerClasses} ref={reference}>
      <div className='flex flex-row'>
        <InputBase
          placeholder={fromPlaceHolder}
          value={fromValue}
          onChange={noop}
          onFocus={
            isOnChangeFocusInput
              ? noop
              : () => {
                  !readOnly && !disabled && setCurrentPanel('fromTime');
                }
          }
          onClick={
            !isOnChangeFocusInput
              ? noop
              : () => {
                  !readOnly && !disabled && setCurrentPanel('fromTime');
                }
          }
          disabled={disabled}
          readOnly={readOnly}
          className='date-picker__input date-picker__input--center'
          endAdornment={
            !readOnly && !disabled && fromValue ? (
              <IconButton
                type='button'
                isAdornment
                onMouseDown={(e) => {
                  e.preventDefault();
                  e.stopPropagation();
                }}
                onMouseUp={(e) => {
                  e.preventDefault();
                  e.stopPropagation();
                  setFromValue('');
                }}
                iconName='failed-remove'
                iconVariant='solid'
                className='ml-2'
              />
            ) : undefined
          }
        />
        <Icon color='default' iconName='arrow_right' variant='outline' />
        <InputBase
          placeholder={toPlaceHolder}
          value={toValue}
          onChange={noop}
          onFocus={
            isOnChangeFocusInput
              ? noop
              : () => {
                  !readOnly && !disabled && setCurrentPanel('toTime');
                }
          }
          onClick={
            !isOnChangeFocusInput
              ? noop
              : () => {
                  !readOnly && !disabled && setCurrentPanel('toTime');
                }
          }
          disabled={disabled}
          readOnly={readOnly}
          className='date-picker__input date-picker__input--center'
          endAdornment={
            !readOnly && !disabled && toValue ? (
              <IconButton
                type='button'
                isAdornment
                onMouseDown={(e) => {
                  e.preventDefault();
                  e.stopPropagation();
                }}
                onMouseUp={(e) => {
                  e.preventDefault();
                  e.stopPropagation();
                  setToValue('');
                }}
                iconName='failed-remove'
                iconVariant='solid'
                className='ml-2'
              />
            ) : undefined
          }
        />
        {!readOnly && !disabled && (
          <IconButton
            isAdornment
            onClick={() => {
              currentPanel ? setCurrentPanel(null) : setCurrentPanel('fromTime');
            }}
            iconName='time-clock'
            iconVariant='outline'
            className='ml-2'
          />
        )}
      </div>

      <FloatingPortal id='portal-root'>
        {!!currentPanel && (
          <FloatingFocusManager
            context={context}
            modal={false}
            returnFocus={!!isOnChangeFocusInput}
            order={['reference', 'content']}
          >
            <FloatingOverlay className='z-picker' lockScroll>
              <div
                {...getFloatingProps({
                  ref: floating,
                  style: {
                    position: strategy,
                    left: x ?? 0,
                    top: y ?? 0,
                    overflow: 'auto',
                  },
                  onKeyDown: noop,
                })}
              >
                <div className='paper'>
                  {currentPanel === 'fromTime' && (
                    <PanelTime value={fromValue} onChange={(v) => setFromValue(v ?? '')} />
                  )}
                  {currentPanel === 'toTime' && (
                    <PanelTime value={toValue} onChange={(v) => setToValue(v ?? '')} />
                  )}
                </div>
              </div>
            </FloatingOverlay>
          </FloatingFocusManager>
        )}
      </FloatingPortal>
    </div>
  );
}

export default withContainer<TimeRangePickerProps>(TimeRangePicker);
