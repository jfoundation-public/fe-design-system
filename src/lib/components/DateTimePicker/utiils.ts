import {
  add,
  addDays,
  getDate,
  getDay,
  getMonth,
  isAfter,
  isBefore,
  isSameDay,
  set,
  setDate,
  sub,
} from 'date-fns';
import * as LocaleFns from 'date-fns/locale';

import { range } from '@/lib/utils';
import { addLeadingZeros } from '@/lib/utils';

import { DAY_PER_WEEK, WEEK_START_DAY } from './constants';
import { Locale } from './types';

function getWeekStartDay(locale: Locale) {
  return WEEK_START_DAY[locale] ?? WEEK_START_DAY['vi'];
}
function getStartDateOfPanel(viewDate: Date, locale: Locale) {
  const weekStartDay = getWeekStartDay(locale);
  const firstDateOfMonth = setDate(viewDate, 1);
  const dayOfFirstDate = getDay(firstDateOfMonth);
  const firstDateAlignment = addDays(firstDateOfMonth, weekStartDay - dayOfFirstDate);

  if (getMonth(firstDateAlignment) === getMonth(viewDate) && getDate(firstDateAlignment) > 1) {
    return addDays(firstDateAlignment, -7);
  }
  return firstDateAlignment;
}

function getLocaleFns(locale: Locale) {
  return LocaleFns[locale];
}

function getNarrowWeekDays(locale: Locale) {
  const weekStartDay = getWeekStartDay(locale);
  const fnsLocal = LocaleFns[locale];
  return range(weekStartDay, weekStartDay + DAY_PER_WEEK).map((day) =>
    fnsLocal.localize?.day(day % DAY_PER_WEEK, { width: 'narrow' }),
  );
}

function getAbbrMonths(locale: Locale) {
  const fnsLocal = LocaleFns[locale];
  return range(0, 12).map((month) => fnsLocal.localize?.month(month, { width: 'abbreviated' }));
}

function toISOString(date: Date | null) {
  return date ? date.toISOString() : '';
}

function parseTimeFromValue(value?: null | string) {
  if (!value) return { hourValue: null, minuteValue: null };
  const [hourString, minuteString] = value.split(':');
  return { hourValue: Number(hourString), minuteValue: Number(minuteString) };
}

function combineTimeValue(hourValue: number, minuteValue: number) {
  return `${addLeadingZeros(2)(hourValue)}:${addLeadingZeros(2)(minuteValue)}`;
}

function parseDateTimeString(val?: string | null) {
  if (!val) {
    return { time: null, date: null };
  }
  const [date = '', time = ''] = val.split('T');
  return { time: time || null, date: date ? new Date(date) : null };
}

function setTimeZero(date: Date | null) {
  if (!date) return null;
  return set(date, { hours: 0, minutes: 0, seconds: 0, milliseconds: 0 });
}

function getCellState({
  currDate,
  selecting,
  fromDateSelected,
  toDateSelected,
  currentHovered,
}: {
  currDate: Date;
  selecting: 'toDate' | 'fromDate';
  fromDateSelected: Date | null;
  toDateSelected: Date | null;
  currentHovered: Date | null;
}) {
  const selected = Boolean(
    (toDateSelected && isSameDay(currDate, toDateSelected)) ||
      (fromDateSelected && isSameDay(currDate, fromDateSelected)) ||
      (fromDateSelected &&
        toDateSelected &&
        isBefore(setTimeZero(currDate)!, setTimeZero(toDateSelected)!) &&
        isAfter(setTimeZero(currDate)!, setTimeZero(fromDateSelected)!)),
  );
  const isDisabled = Boolean(
    (selecting === 'toDate' &&
      fromDateSelected &&
      isBefore(setTimeZero(currDate)!, setTimeZero(fromDateSelected)!)) ||
      (selecting === 'fromDate' &&
        toDateSelected &&
        isAfter(setTimeZero(currDate)!, setTimeZero(toDateSelected)!)),
  );

  const hovered = Boolean(
    (fromDateSelected &&
      selecting === 'toDate' &&
      currentHovered &&
      isBefore(setTimeZero(sub(currDate, { days: 1 }))!, setTimeZero(currentHovered)!) &&
      isAfter(setTimeZero(currDate)!, setTimeZero(fromDateSelected)!)) ||
      (toDateSelected &&
        selecting === 'fromDate' &&
        currentHovered &&
        isAfter(setTimeZero(add(currDate, { days: 1 }))!, setTimeZero(currentHovered)!) &&
        isBefore(setTimeZero(currDate)!, setTimeZero(toDateSelected)!)),
  );
  return { selected, isDisabled, hovered };
}

export {
  combineTimeValue,
  getAbbrMonths,
  getCellState,
  getLocaleFns,
  getNarrowWeekDays,
  getStartDateOfPanel,
  parseDateTimeString,
  parseTimeFromValue,
  setTimeZero,
  toISOString,
};
