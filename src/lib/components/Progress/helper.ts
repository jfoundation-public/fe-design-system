import clsx from 'clsx';

function getValidPercent(percent: number) {
  if (!percent || percent < 0) {
    return 0;
  }
  if (percent > 100) return 100;
  return percent;
}

function getProgressStatusClass({
  isError,
  isSuccess,
}: {
  isError?: boolean;
  isSuccess?: boolean;
}) {
  return clsx(
    isError && 'progress--error',
    isSuccess && 'progress--success',
    !isError && !isSuccess && 'progress--primary',
  );
}

export { getProgressStatusClass, getValidPercent };
