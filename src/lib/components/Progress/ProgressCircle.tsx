import clsx from 'clsx';

import { Icon } from '../Icon';
import { getProgressStatusClass } from './helper';
import { useProgress } from './hooks';
import { ProgressProps } from './types';

const STROKE_WIDTH = 1.8;

export default function ProgressCircle(props: Omit<ProgressProps, 'type'>) {
  const { percent, className, hideInfo, isError, ...restProps } = props;

  const { showInfo, isSuccess, _percent } = useProgress({ percent, isError, hideInfo });

  return (
    <div className={clsx('progress-circle', className)} {...restProps}>
      <svg
        className='progress-circle__canvas'
        xmlns='http://www.w3.org/2000/svg'
        viewBox='-1 -1 34 34'
      >
        <circle
          cx='16'
          cy='16'
          r='15.9155'
          strokeWidth={STROKE_WIDTH}
          fill='none'
          className='progress-circle__background'
        />
        <circle
          cx='16'
          cy='16'
          r='15.9155'
          strokeLinecap='round'
          strokeWidth={STROKE_WIDTH}
          strokeDasharray={100}
          strokeDashoffset={100 - _percent}
          fill='none'
          className={clsx(
            'progress-circle__progress',
            getProgressStatusClass({ isError, isSuccess }),
          )}
        />
      </svg>
      {showInfo && <span className='progress-circle__info'>{percent}%</span>}
      {isSuccess && (
        <Icon
          size={24}
          variant='outline'
          iconName='tick'
          className='text-icon-success  progress-circle__info'
        />
      )}
      {isError && (
        <Icon
          size={24}
          variant='outline'
          iconName='close-small'
          className='text-icon-error progress-circle__info'
        />
      )}
    </div>
  );
}
