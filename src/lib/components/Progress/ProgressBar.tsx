import clsx from 'clsx';

import { Icon } from '../Icon';
import { getProgressStatusClass } from './helper';
import { useProgress } from './hooks';
import { ProgressProps } from './types';

export default function ProgressBar(props: Omit<ProgressProps, 'type'>) {
  const { percent, className, hideInfo, isError, ...restProps } = props;

  const { showInfo, isSuccess, _percent } = useProgress({ percent, isError, hideInfo });

  return (
    <div className={clsx('progress-bar', className)}>
      <div className={clsx('progress-bar__wrapper', className)} {...restProps}>
        <div className='progress-bar__backline'>
          <div
            className={clsx(
              'progress-bar__progress',
              getProgressStatusClass({ isError, isSuccess }),
            )}
            style={{ width: `${_percent}%` }}
          />
        </div>
      </div>
      {showInfo && <span className='progress-bar__info'>{percent}%</span>}
      {isSuccess && <Icon size={16} iconName='success' className='text-icon-success ml-2' />}
      {isError && <Icon size={16} iconName='failed-remove' className='text-icon-error ml-2' />}
    </div>
  );
}
