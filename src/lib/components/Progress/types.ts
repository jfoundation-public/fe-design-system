import { ComponentPropsWithoutRef } from 'react';

export interface ProgressProps extends ComponentPropsWithoutRef<'div'> {
  percent: number;
  type?: 'line' | 'circle';
  hideInfo?: boolean;
  isError?: boolean;
}
