import ProgressBar from './ProgressBar';
import ProgressCircle from './ProgressCircle';
import { ProgressProps } from './types';

const COMPONENTS = {
  line: ProgressBar,
  circle: ProgressCircle,
};

export default function Progress(props: ProgressProps) {
  const { type = 'line', ...restProps } = props;
  const Component = COMPONENTS[type];
  if (!Component) return null;
  return <Component {...restProps} />;
}
