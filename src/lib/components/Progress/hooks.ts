import { getValidPercent } from './helper';
import { ProgressProps } from './types';

function useProgress({
  percent,
  isError,
  hideInfo,
}: Pick<ProgressProps, 'percent' | 'isError' | 'hideInfo'>) {
  const _percent = getValidPercent(percent);

  const isSuccess = _percent === 100;

  const showInfo = !hideInfo && !isSuccess && !isError;

  return { _percent, isSuccess, showInfo };
}

export { useProgress };
