import { ComponentPropsWithoutRef, ReactNode } from 'react';

import { IconName, IconVariant } from '../Icon/types';

export interface AlertProps extends ComponentPropsWithoutRef<'div'> {
  title: string;
  action?: ReactNode;
  onClose?: () => void;
  iconName: IconName;
  iconVariant?: IconVariant;
  iconClasses?: string;
  titleClassName?: string;
}
