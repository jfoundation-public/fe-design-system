import clsx from 'clsx';
import { isValidElement } from 'react';

import { IconButton } from '../Button';
import { Icon } from '../Icon';
import { Typography } from '../Typography';
import { AlertProps } from './types';

export default function Alert(props: AlertProps) {
  const {
    iconName,
    iconClasses,
    action,
    title,
    onClose,
    titleClassName,
    children,
    iconVariant,
    ...restProps
  } = props;

  return (
    <div className='alert' {...restProps}>
      <div className='alert--header'>
        <Icon
          iconName={iconName}
          size={24}
          variant={iconVariant ?? 'solid'}
          className={iconClasses}
        />
        <Typography className={clsx('alert--header--title', titleClassName)} variant='body'>
          {title}
        </Typography>
        {!!action && isValidElement(action) && <div>{action}</div>}
        <IconButton
          isAdornment
          iconName='close-small'
          iconSize={24}
          iconVariant='outline'
          className='alert--header--close-btn'
          onClick={onClose}
        />
      </div>
      {children && <div className='alert--body'>{children}</div>}
    </div>
  );
}
