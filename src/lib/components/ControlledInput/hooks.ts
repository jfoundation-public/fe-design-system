import { ChangeEvent, FocusEvent, useCallback, useEffect, useState } from 'react';

import { useToggle } from '@/lib/hooks';

import { useFieldControlledContext } from '../FieldInput/hooks';
import { isShowRemoveIcon } from './helper';
import { ControlledInputProps } from './types';

function useControlledInput({
  type = 'text',
  name,
  autoFocus,
  value: _value = '',
  onBlur,
  onChange,
  onFocus,
  disabled,
  readOnly,
}: Pick<
  ControlledInputProps,
  | 'type'
  | 'value'
  | 'onChange'
  | 'onBlur'
  | 'onFocus'
  | 'autoFocus'
  | 'name'
  | 'disabled'
  | 'readOnly'
>) {
  const [value, setValue] = useState<string>(_value);
  const [focused, setFocused] = useState(Boolean(autoFocus));
  const [isPasswordShowed, togglePassowordShowed] = useToggle();
  const {
    setDisabled: syncDisabled,
    setFocused: syncFocused,
    setHasValue: syncHasValue,
    setReadOnly: syncReadOnly,
  } = useFieldControlledContext();

  const handleFocus = useCallback(
    (e: FocusEvent<HTMLInputElement>) => {
      setFocused(true);
      syncFocused(name, true);
      onFocus && onFocus(e);
    },
    [name, onFocus, syncFocused],
  );

  const handleBlur = useCallback(
    (e: FocusEvent<HTMLInputElement>) => {
      setFocused(false);
      syncFocused(name, false);
      onBlur && onBlur(e);
    },
    [name, onBlur, syncFocused],
  );

  const handleChange = useCallback(
    (e: ChangeEvent<HTMLInputElement>) => {
      setValue(e.target.value);
      syncHasValue(name, Boolean(e.target.value));
      onChange && onChange(e);
    },
    [name, onChange, syncHasValue],
  );

  const handleClearValue = useCallback(() => {
    setValue('');
    const newEvent = new Event('input');
    const newEventTarget = { name: name ?? '', value: '' };
    onChange && onChange({ ...newEvent, target: { ...newEventTarget } as never } as never);
    // @TO_DO: remove never type
    syncHasValue(name, false);
  }, [name, onChange, syncHasValue]);

  const showRemoveIcon = isShowRemoveIcon(type, value, focused, !!readOnly, !!disabled);

  useEffect(() => {
    setValue(_value);
    syncHasValue(name, !!_value);
  }, [name, _value, syncHasValue]);

  useEffect(() => {
    syncDisabled(name, !!disabled);
    syncReadOnly(name, !!readOnly);
  }, [disabled, name, readOnly, syncDisabled, syncReadOnly]);

  return {
    value,
    onChange: handleChange,
    onFocus: handleFocus,
    onBlur: handleBlur,
    showRemoveIcon,
    isPasswordShowed,
    togglePassowordShowed,
    showEndAdornment: type === 'password' || showRemoveIcon,
    onClearValue: handleClearValue,
  };
}

export { useControlledInput };
