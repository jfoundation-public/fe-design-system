import { forwardRef, useMemo } from 'react';

import { IconButton } from '../Button';
import { IconName } from '../Icon/types';
import { InputBase } from '../InputBase';
import { useControlledInput } from './hooks';
import { ControlledInputProps } from './types';

function EndAdornment({
  showEndAdornment,
  showRemoveIcon,
  onToggleShowPassword,
  isPasswordShowed,
  onClear,
}: {
  showEndAdornment: boolean;
  showRemoveIcon: boolean;
  onToggleShowPassword: () => void;
  isPasswordShowed: boolean;
  onClear: () => void;
}) {
  const handleClick = useMemo(
    () => (showRemoveIcon ? onClear : onToggleShowPassword),
    [onClear, onToggleShowPassword, showRemoveIcon],
  );

  if (!showEndAdornment) return null;
  const iconName: IconName =
    (showRemoveIcon && 'failed-remove') || (isPasswordShowed && 'eye') || 'eye-slash';
  return (
    <IconButton
      type='button'
      isAdornment
      onMouseDown={(e) => {
        e.stopPropagation();
        e.preventDefault();
      }}
      onMouseUp={(e) => {
        e.stopPropagation();
        e.preventDefault();
        handleClick();
      }}
      iconName={iconName}
    />
  );
}

/**
 * Extends from InputBase
 */
function ControlledInput(props: ControlledInputProps, ref?: React.Ref<HTMLInputElement>) {
  const {
    type = 'text',
    value: _value,
    onChange,
    onBlur,
    onFocus,
    autoFocus,
    name,
    disabled,
    readOnly,
    ...restProps
  } = props;

  const {
    value,
    onChange: handleChange,
    onFocus: handleFocus,
    onBlur: handleBlur,
    showRemoveIcon,
    isPasswordShowed,
    togglePassowordShowed,
    showEndAdornment,
    onClearValue,
  } = useControlledInput({
    type,
    name,
    autoFocus,
    value: _value,
    onBlur,
    onChange,
    onFocus,
    disabled,
    readOnly,
  });

  return (
    <InputBase
      name={name}
      value={value}
      onChange={handleChange}
      onFocus={handleFocus}
      onBlur={handleBlur}
      readOnly={readOnly}
      disabled={disabled}
      // eslint-disable-next-line jsx-a11y/no-autofocus
      autoFocus={autoFocus}
      type={type === 'password' && !isPasswordShowed ? 'password' : 'text'}
      ref={ref}
      {...restProps}
      endAdornment={
        <EndAdornment
          showEndAdornment={showEndAdornment}
          showRemoveIcon={showRemoveIcon}
          onToggleShowPassword={togglePassowordShowed}
          isPasswordShowed={isPasswordShowed}
          onClear={onClearValue}
        />
      }
    />
  );
}

export default forwardRef(ControlledInput);
