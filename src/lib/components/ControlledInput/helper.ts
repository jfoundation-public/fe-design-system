import { ControlledInputType } from './types';

function isShowRemoveIcon(
  type: ControlledInputType,
  value: string,
  focused: boolean,
  readOnly: boolean,
  disabled: boolean,
) {
  if (!value || type === 'password') return false;
  return !disabled && !readOnly && (type === 'search' || focused);
}

export { isShowRemoveIcon };
