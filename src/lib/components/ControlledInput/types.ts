import { ComponentPropsWithRef } from 'react';

import { FieldState } from '@/lib/types';

import { InputBase } from '../InputBase';

export type ControlledInputType = 'password' | 'search' | 'text';

export interface ControlledInputProps
  extends Omit<ComponentPropsWithRef<typeof InputBase>, 'type' | 'value'> {
  type?: ControlledInputType;
  value?: string;
  name: string;
  format?: (val: string | undefined) => string | undefined;
  parse?: (val: string | undefined) => string | undefined;
}

export interface ControlledInputState {
  value: string;
  fieldState: FieldState;
}

export type Actions =
  | { type: 'SET_FIELD_STATE'; payload: FieldState }
  | { type: 'SET_VALUE'; payload: string }
  | { type: 'MOUSE_ENTER' }
  | { type: 'FOCUS' }
  | { type: 'BLUR' };
