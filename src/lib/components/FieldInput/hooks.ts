import { produce } from 'immer';
import { useCallback, useContext, useReducer } from 'react';

import { FieldState } from '@/lib/types';

import { createInitStatusIfNotExists, getUncontrolledState } from './helper';
import { FieldInputContext, FieldInputRefContext } from './Provider';
import { FieldInputControlledActions, FieldInputControlledValues } from './types';

const fieldInputControlledReducer = (
  state: FieldInputControlledValues,
  action: FieldInputControlledActions,
) => {
  const { type, payload } = action;
  switch (type) {
    case 'SET_FOCUSED':
      return produce(state, (draft) => {
        createInitStatusIfNotExists(draft, payload.name);
        draft[payload.name].focused = payload.value;
      });
    case 'SET_DISABLED':
      return produce(state, (draft) => {
        createInitStatusIfNotExists(draft, payload.name);
        draft[payload.name].disabled = payload.value;
      });
    case 'SET_HAS_VALUE':
      return produce(state, (draft) => {
        createInitStatusIfNotExists(draft, payload.name);
        draft[payload.name].hasValue = payload.value;
      });
    case 'SET_HOVERED':
      return produce(state, (draft) => {
        createInitStatusIfNotExists(draft, payload.name);
        draft[payload.name].hovered = payload.value;
      });
    case 'SET_ALL_HOVERED':
      return produce(state, (draft) => {
        Object.keys(draft).forEach((name) => {
          draft[name].hovered = payload.value;
        });
      });
    case 'SET_READ_ONLY':
      return produce(state, (draft) => {
        createInitStatusIfNotExists(draft, payload.name);
        draft[payload.name].readOnly = payload.value;
      });
    default:
      throw new Error('This action is not allowed!');
  }
};

function useCreateContextValue() {
  const [state, dispatch] = useReducer(fieldInputControlledReducer, {});
  const setFocused = useCallback((name: string, value: boolean) => {
    dispatch({ type: 'SET_FOCUSED', payload: { name, value } });
  }, []);
  const setDisabled = useCallback((name: string, value: boolean) => {
    dispatch({ type: 'SET_DISABLED', payload: { name, value } });
  }, []);
  const setHasValue = useCallback((name: string, value: boolean) => {
    dispatch({ type: 'SET_HAS_VALUE', payload: { name, value } });
  }, []);
  const setHovered = useCallback((name: string, value: boolean) => {
    dispatch({ type: 'SET_HOVERED', payload: { name, value } });
  }, []);
  const setAllHovered = useCallback((value: boolean) => {
    dispatch({ type: 'SET_ALL_HOVERED', payload: { value } });
  }, []);
  const setReadOnly = useCallback((name: string, value: boolean) => {
    dispatch({ type: 'SET_READ_ONLY', payload: { name, value } });
  }, []);

  return {
    state,
    setFocused,
    setDisabled,
    setHasValue,
    setHovered,
    setReadOnly,
    setAllHovered,
  };
}

function useFieldState(state?: FieldState) {
  const { state: fieldsState } = useFieldControlledContext();
  return state ?? getUncontrolledState(fieldsState);
}

function useFieldControlledContext() {
  const contextValue = useContext(FieldInputContext);
  if (!contextValue)
    throw new Error('This component must be wrapped by FieldInputControlledProvider');
  return contextValue;
}

function useFieldControlElement() {
  return useContext(FieldInputRefContext);
}

function useSyncHover() {
  const { setAllHovered } = useFieldControlledContext();
  const onMouseEnter = useCallback(() => {
    setAllHovered(true);
  }, [setAllHovered]);
  const onMouseLeave = useCallback(() => {
    setAllHovered(false);
  }, [setAllHovered]);
  return { onMouseEnter, onMouseLeave };
}

export {
  useCreateContextValue,
  useFieldControlElement,
  useFieldControlledContext,
  useFieldState,
  useSyncHover,
};
