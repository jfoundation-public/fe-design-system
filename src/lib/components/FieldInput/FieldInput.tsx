import clsx from 'clsx';
import { useId, useRef } from 'react';

import { getFieldInputClasses } from './helper';
import { useFieldState, useSyncHover } from './hooks';
import FieldInputControlledProvider, { FieldInputRefContext } from './Provider';
import { FieldInputProps } from './types';

function FieldInputComp(props: FieldInputProps) {
  const {
    size = 'md',
    isError,
    isSuccess,
    isWarning,
    isNoneOutline = false,
    className,
    children,
    ...restProps
  } = props;

  const state = useFieldState();
  const containerRef = useRef<HTMLDivElement>(null);
  const classes = getFieldInputClasses({
    state,
    size,
    isNoneOutline,
    isError,
    isWarning,
    isSuccess,
  });
  const { onMouseEnter, onMouseLeave } = useSyncHover();
  const key = useId();
  return (
    <div
      onMouseEnter={onMouseEnter}
      onMouseLeave={onMouseLeave}
      className={clsx(classes, className)}
      ref={containerRef}
      key={key}
      {...restProps}
    >
      <FieldInputRefContext.Provider value={containerRef.current}>
        {children}
      </FieldInputRefContext.Provider>
    </div>
  );
}

export default function FieldInput(props: FieldInputProps) {
  return (
    <FieldInputControlledProvider>
      <FieldInputComp {...props} />
    </FieldInputControlledProvider>
  );
}
