import { ComponentPropsWithRef } from 'react';

import { Size } from '@/lib/types';

export interface FieldInputProps
  extends Omit<
    ComponentPropsWithRef<'input'>,
    'onChange' | 'onBlur' | 'onFocus' | 'size' | 'type'
  > {
  size?: Size;
  isNoneOutline?: boolean;
  isError?: boolean;
  isWarning?: boolean;
  isSuccess?: boolean;
}

export type FieldInputControlledValues = Record<
  string,
  {
    focused: boolean;
    disabled: boolean;
    hasValue: boolean;
    hovered: boolean;
    readOnly: boolean;
  }
>;

export type FieldInputControlledActions =
  | { type: 'SET_FOCUSED'; payload: { name: string; value: boolean } }
  | { type: 'SET_DISABLED'; payload: { name: string; value: boolean } }
  | { type: 'SET_HAS_VALUE'; payload: { name: string; value: boolean } }
  | { type: 'SET_HOVERED'; payload: { name: string; value: boolean } }
  | { type: 'SET_ALL_HOVERED'; payload: { value: boolean } }
  | { type: 'SET_READ_ONLY'; payload: { name: string; value: boolean } };
