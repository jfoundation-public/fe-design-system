import { createContext, ReactElement } from 'react';

import { useCreateContextValue } from './hooks';
import { FieldInputControlledValues } from './types';

export const FieldInputContext = createContext<
  | {
      state: FieldInputControlledValues;
      setFocused: (name: string, value: boolean) => void;
      setDisabled: (name: string, value: boolean) => void;
      setHasValue: (name: string, value: boolean) => void;
      setHovered: (name: string, value: boolean) => void;
      setAllHovered: (value: boolean) => void;
      setReadOnly: (name: string, value: boolean) => void;
    }
  | undefined
>(undefined);

export const FieldInputRefContext = createContext<HTMLDivElement | null>(null);

export default function FieldInputControlledProvider({
  children,
}: {
  children: ReactElement | ReactElement[];
}) {
  const contextValue = useCreateContextValue();
  return <FieldInputContext.Provider value={contextValue}>{children}</FieldInputContext.Provider>;
}
