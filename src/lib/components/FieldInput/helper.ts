import clsx from 'clsx';

import { FieldState, Size } from '@/lib/types';

import { FieldInputControlledValues } from './types';

const FIELD_INPUT_STATE_CLASSES = {
  default: 'field-input-state-default',
  hover: 'field-input-state--hover',
  focused: 'field-input-state--focused',
  error: 'field-input-state-error',
  active: 'field-input-state-active',
  'read-only': 'field-input-state-read-only',
  'read-only-focused': 'field-input-state-read-only--focused',
  disabled: 'field-input-state-disabled',
  success: 'field-input-state-success',
  warning: 'field-input-state-warning',
};

const FIELD_INPUT_SIZE_CLASSES = {
  lg: 'field-input-size-lg',
  md: 'field-input-size-md',
  sm: 'field-input-size-sm',
};

function getFieldInputStateClasses({
  state,
  isError,
  isWarning,
  isSuccess,
}: {
  state: FieldState;
  isError?: boolean;
  isWarning?: boolean;
  isSuccess?: boolean;
}) {
  if (state === 'focused') {
    return (
      (isError && 'field-input-state-error--focused') ||
      (isWarning && 'field-input-state-warning--focused') ||
      (isSuccess && 'field-input-state-success--focused') ||
      FIELD_INPUT_STATE_CLASSES['focused']
    );
  }
  return (
    (isError && 'field-input-state-error') ||
    (isWarning && 'field-input-state-warning') ||
    (isSuccess && 'field-input-state-success') ||
    FIELD_INPUT_STATE_CLASSES[state]
  );
}

function getFieldInputClasses({
  state,
  size,
  isNoneOutline,
  isError,
  isWarning,
  isSuccess,
}: {
  state: FieldState;
  size: Size;
  isNoneOutline: boolean;
  isError?: boolean;
  isWarning?: boolean;
  isSuccess?: boolean;
}) {
  return clsx(
    'field-input',
    isNoneOutline
      ? 'field-input-outline-none'
      : getFieldInputStateClasses({ state, isError, isWarning, isSuccess }),
    FIELD_INPUT_SIZE_CLASSES[size],
  );
}

function getStatusState(
  state: FieldInputControlledValues,
  type: 'focused' | 'disabled' | 'hasValue' | 'hovered' | 'readOnly',
) {
  return Object.values(state).some((val) => val[type]);
}

function getStateFromStatus({
  focused,
  disabled,
  hovered,
  // hasValue,
  readOnly,
}: {
  focused: boolean;
  disabled: boolean;
  hovered: boolean;
  hasValue: boolean;
  readOnly: boolean;
}): FieldState {
  if (disabled) return 'disabled';
  if (readOnly && focused) return 'read-only-focused';
  if (readOnly) return 'read-only';
  if (focused) return 'focused';
  if (hovered) return 'hover';
  return 'default';
}

function getUncontrolledState(state: FieldInputControlledValues) {
  const focused = getStatusState(state, 'focused');
  const disabled = getStatusState(state, 'disabled');
  const hovered = getStatusState(state, 'hovered');
  const hasValue = getStatusState(state, 'hasValue');
  const readOnly = getStatusState(state, 'readOnly');
  return getStateFromStatus({
    focused,
    disabled,
    hovered,
    hasValue,
    readOnly,
  });
}

function createInitStatusIfNotExists(state: FieldInputControlledValues, name: string) {
  if (!state[name]) {
    state[name] = {
      focused: false,
      disabled: false,
      hovered: false,
      hasValue: false,
      readOnly: false,
    };
    return true;
  }
  return false;
}

export { createInitStatusIfNotExists, getFieldInputClasses, getUncontrolledState };
