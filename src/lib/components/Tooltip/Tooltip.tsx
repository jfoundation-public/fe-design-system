import {
  arrow,
  autoUpdate,
  flip,
  FloatingPortal,
  offset,
  shift,
  useDismiss,
  useFloating,
  useFocus,
  useHover,
  useInteractions,
  useRole,
} from '@floating-ui/react-dom-interactions';
import clsx from 'clsx';
import { cloneElement, ComponentElement, useRef, useState } from 'react';

import { isNil, mergeRefs } from '@/lib';

import { TooltipProps } from './types';

export default function Tooltip(props: TooltipProps) {
  const { className, content, placement: _placement = 'top-start', children } = props;
  const [open, setOpen] = useState(false);
  const arrowRef = useRef<HTMLDivElement>(null);

  const { x, y, placement, middlewareData, reference, floating, strategy, context } = useFloating({
    placement: _placement,
    open,
    onOpenChange: setOpen,
    middleware: [offset(8), flip(), shift({ padding: 8 }), arrow({ element: arrowRef })],
    whileElementsMounted: autoUpdate,
  });

  const { getReferenceProps, getFloatingProps } = useInteractions([
    useHover(context),
    useFocus(context),
    useRole(context, { role: 'tooltip' }),
    useDismiss(context),
  ]);

  const { arrow: arrowData } = middlewareData;

  const staticSide = {
    top: 'bottom',
    right: 'left',
    bottom: 'top',
    left: 'right',
  }[placement.split('-')[0]] as 'top' | 'right' | 'bottom' | 'left';

  return (
    <>
      {cloneElement(
        children,
        getReferenceProps({
          ref: mergeRefs(reference, (children as ComponentElement<any, any>).ref),
          ...children.props,
        }),
      )}
      <FloatingPortal id='portal-root'>
        {open && (
          <div
            ref={floating}
            style={{
              position: strategy,
              top: y ?? 0,
              left: x ?? 0,
            }}
            {...getFloatingProps({ className: clsx('tooltip', className) })}
          >
            {content}
            <span
              ref={arrowRef}
              style={{
                left: !isNil(arrowData?.x) ? `${arrowData?.x}px` : '',
                top: !isNil(arrowData?.y) ? `${arrowData?.y}px` : '',
                right: '',
                bottom: '',
                [staticSide]: '-4px',
              }}
              className='tooltip--arrow'
            />
          </div>
        )}
      </FloatingPortal>
    </>
  );
}
