import { Placement } from '@floating-ui/react-dom-interactions';
import { ReactElement, ReactNode } from 'react';

export interface TooltipProps {
  placement?: Placement;
  content: ReactNode;
  children: ReactElement;
  className?: string;
}
