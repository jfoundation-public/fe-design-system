import { ComponentPropsWithRef } from 'react';

import { VARIANTS } from './helper';
export type TypographyVariant = keyof typeof VARIANTS;

export interface TypographyProps extends ComponentPropsWithRef<'p'> {
  variant: TypographyVariant;
}
