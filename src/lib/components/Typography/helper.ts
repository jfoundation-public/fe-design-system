import { TypographyVariant } from './types';

export const VARIANTS = {
  h1: 'h1',
  h2: 'h2',
  h3: 'h3',
  h4: 'h4',
  h5: 'h5',
  subheading: 'subheading',
  subtitle1: 'subtitle1',
  subtitle2: 'subtitle2',
  body: 'body',
  caption1: 'caption1',
  caption2: 'caption2',
};

function getTypographyClass(variant: TypographyVariant) {
  return VARIANTS[variant];
}

export { getTypographyClass };
