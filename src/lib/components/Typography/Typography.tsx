import clsx from 'clsx';

import { getTypographyClass } from './helper';
import { TypographyProps } from './types';

export default function Typography(props: TypographyProps) {
  const { variant, className, ...restProps } = props;
  const classes = getTypographyClass(variant);
  return <p className={clsx(classes, className)} {...restProps} />;
}
