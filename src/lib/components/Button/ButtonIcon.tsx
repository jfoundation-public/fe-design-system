import clsx from 'clsx';
import { useMemo } from 'react';

import { buttonIconClassGenerator } from './helper';
import { ButtonIconProps } from './types';

export default function ButtonIcon(props: ButtonIconProps) {
  const { size = 'md', className, children, ...restProps } = props;

  const classes = useMemo(() => buttonIconClassGenerator(size), [size]);

  return (
    <span className={clsx(classes, className)} {...restProps}>
      {children}
    </span>
  );
}
