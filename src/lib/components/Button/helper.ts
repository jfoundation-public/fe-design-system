import clsx from 'clsx';

import { Size, Variants } from '@/lib/types';

import { ButtonColor } from './types';

const BUTTON_VARIANT_CLASSES = {
  primary: 'btn-primary',
  positive: 'btn-positive',
  negative: 'btn-negative',
  secondary: 'btn-secondary',
  blank: 'btn-blank',
  link: 'btn-link',
};

const BUTTON_SIZE_CLASSES = {
  lg: 'btn-lg',
  md: 'btn-md',
  sm: 'btn-sm',
};

const BUTTON_ICON_SIZE_CLASSES = {
  lg: 'btn-icon-lg',
  md: 'btn-icon-md',
  sm: 'btn-icon-sm',
};

const BUTTON_ICON_SPACING = {
  lg: { leftIcon: 'btn-left-icon-lg-spacing', rightIcon: 'btn--right-icon-lg-spacing' },
  md: { leftIcon: 'btn-left-icon-md-spacing', rightIcon: 'btn--right-icon-md-spacing' },
  sm: { leftIcon: 'btn-left-icon-sm-spacing', rightIcon: 'btn--right-icon-sm-spacing' },
};

const BUTTON_ICON_COLOR = {
  primary: 'btn-color-primary',
  error: 'btn-color-error',
  warning: 'btn-color-warning',
  success: 'btn-color-success',
};

function buttonClassGenerator(
  variant: Variants,
  size: Size,
  disabled: boolean,
  fullWidth: boolean,
  minWidth: boolean,
  color?: ButtonColor,
  hasDottedBorder?: boolean,
) {
  return clsx(
    'btn relative',
    BUTTON_VARIANT_CLASSES[variant],
    minWidth && BUTTON_SIZE_CLASSES[size],
    fullWidth && 'btn-w-full',
    minWidth && 'btn-w-min',
    disabled && 'btn-disabled',
    color && BUTTON_ICON_COLOR[color],
    hasDottedBorder &&
      'relative z-10 h-full bg-white text-current rounded hover:bg-surface-primary4',
  );
}

function buttonIconClassGenerator(size: Size) {
  return clsx('btn-icon', BUTTON_ICON_SIZE_CLASSES[size]);
}

function buttonIconSpacingClassGenerator(size: Size) {
  return BUTTON_ICON_SPACING[size];
}

function iconButtonClassGenerator(isAdornment?: boolean) {
  if (isAdornment) return 'btn-icon-button-adornment';
  return 'btn-icon-button';
}

export {
  buttonClassGenerator,
  buttonIconClassGenerator,
  buttonIconSpacingClassGenerator,
  iconButtonClassGenerator,
};
