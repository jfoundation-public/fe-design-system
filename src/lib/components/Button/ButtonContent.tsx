import { useMemo } from 'react';

import ButtonIcon from './ButtonIcon';
import { buttonIconSpacingClassGenerator } from './helper';
import { ButtonContentProps } from './types';

export default function ButtonContent(props: ButtonContentProps) {
  const { size = 'md', leftIcon, rightIcon, children } = props;

  const classes = useMemo(() => buttonIconSpacingClassGenerator(size), [size]);

  return (
    <>
      {leftIcon && <ButtonIcon className={classes.leftIcon}>{leftIcon}</ButtonIcon>}
      {children}
      {rightIcon && <ButtonIcon className={classes.rightIcon}>{rightIcon}</ButtonIcon>}
    </>
  );
}
