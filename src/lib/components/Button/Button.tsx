import clsx from 'clsx';
import { forwardRef, Ref, useMemo } from 'react';

import ButtonContent from './ButtonContent';
import { buttonClassGenerator } from './helper';
import { ButtonProps } from './types';

function Button(props: ButtonProps, ref: Ref<HTMLButtonElement>) {
  const {
    variant = 'primary',
    size = 'md',
    leftIcon,
    rightIcon,
    disabled = false,
    className,
    children,
    fullWidth,
    color,
    hasDottedBorder,
    wrapperClassName,
    ...restProps
  } = props;
  // TODO: rework this!!!
  const isNotIconBtn = (children as any)?.type !== 'function';
  const classes = useMemo(
    () =>
      buttonClassGenerator(
        variant,
        size,
        disabled,
        !!fullWidth,
        !!isNotIconBtn,
        color,
        !!hasDottedBorder,
      ),
    [disabled, fullWidth, isNotIconBtn, size, variant, color, hasDottedBorder],
  );

  if (hasDottedBorder) {
    return (
      <div
        className={clsx('dashed-border h-[49px] text-primary hover:text-primary', wrapperClassName)}
      >
        <button
          ref={ref}
          disabled={disabled}
          className={clsx('w-full', classes, className)}
          {...restProps}
        >
          <ButtonContent leftIcon={leftIcon} rightIcon={rightIcon} size={size}>
            {children}
          </ButtonContent>
        </button>
      </div>
    );
  }

  return (
    <button ref={ref} disabled={disabled} className={clsx(classes, className)} {...restProps}>
      <ButtonContent leftIcon={leftIcon} rightIcon={rightIcon} size={size}>
        {children}
      </ButtonContent>
    </button>
  );
}

export default forwardRef(Button);
