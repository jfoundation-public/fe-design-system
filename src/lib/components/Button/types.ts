import { ComponentPropsWithoutRef, ComponentPropsWithRef, ReactElement, ReactNode } from 'react';

import { Size, Variants } from '@/lib/types';

import { IconName, IconVariant } from '../Icon/types';

export type ButtonColor = 'error' | 'success' | 'warning' | 'primary';

export interface ButtonProps extends ComponentPropsWithRef<'button'> {
  variant?: Variants;
  size?: Size;
  disabled?: boolean;
  leftIcon?: ReactElement;
  rightIcon?: ReactElement;
  fullWidth?: boolean;
  color?: ButtonColor;
  hasDottedBorder?: boolean;
  wrapperClassName?: string;
}

export interface ButtonIconProps extends ComponentPropsWithoutRef<'span'> {
  size?: Size;
}

export interface ButtonContentProps {
  leftIcon?: ReactElement;
  rightIcon?: ReactElement;
  children: ReactNode;
  size?: Size;
}

export interface IconButtonProps extends ButtonProps {
  iconName: IconName;
  iconVariant?: IconVariant;
  isAdornment?: boolean;
  iconSize?: number;
  viewBox?: string;
}
