import clsx from 'clsx';
import { forwardRef, Ref } from 'react';

import { Icon } from '../Icon';
import Button from './Button';
import { iconButtonClassGenerator } from './helper';
import { IconButtonProps } from './types';

function IconButton(props: IconButtonProps, ref: Ref<HTMLButtonElement>) {
  const {
    iconName,
    iconVariant = 'solid',
    iconSize = 24,
    className,
    isAdornment,
    viewBox,
    ...restProps
  } = props;
  const classes = iconButtonClassGenerator(isAdornment);

  if (isAdornment) {
    return (
      <button tabIndex={-1} ref={ref} className={clsx(classes, className)} {...restProps}>
        <Icon size={iconSize} iconName={iconName} variant={iconVariant} />
      </button>
    );
  }

  return (
    <Button ref={ref} className={clsx(classes, className)} type='button' {...restProps}>
      <Icon size={iconSize} iconName={iconName} variant={iconVariant} viewBox={viewBox} />
    </Button>
  );
}

export default forwardRef(IconButton);
