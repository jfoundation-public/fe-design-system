import { ReactNode } from 'react';

import { IconName, IconVariant } from '../Icon/types';

export type StepDirection = 'horizontal' | 'vertical';

export type StepStatus = Record<string, 'error' | 'finish' | 'default' | 'disable'>;

export type StepType = 'icon' | 'dot' | 'number';
export interface StepItem {
  id: string;
  title: string | ReactNode;
  description?: string | ReactNode;
}
export interface StepProps {
  className?: string;
  type?: StepType;
  config?:
    | {
        icon: IconName;
        variant?: IconVariant;
        size?: number;
      }
    | {
        icon: IconName;
        variant?: IconVariant;
        size?: number;
      }[];
  direction: StepDirection;
  steps: StepItem[];
  current: number;
  status: StepStatus;
  onSelect?: (current: number) => void;
  lineClassName?: string;
  headerConfig?: {
    icon: IconName;
    variant?: IconVariant;
    size?: number;
    type: StepType;
    isCurrent: boolean;
    status: 'error' | 'finish' | 'default' | 'disable';
  }[];
  alternativeLabel?: boolean;
  detailClassName?: string;
}

export type StepVerticalProps = Omit<StepProps, 'className' | 'direction'>;

export type StepHorizontalProps = StepVerticalProps;

export interface StepDetailsProps {
  title: string | ReactNode;
  description?: string | ReactNode;
  type?: StepType;
  isCurrent: boolean;
  className: string;
}

export interface StepHeaderProps {
  type: StepType;
  config: {
    icon: IconName;
    variant?: IconVariant;
    size?: number;
  };
  status: 'error' | 'finish' | 'default' | 'disable';
  isCurrent: boolean;
  number: number;
  alternativeLabel?: boolean;
}
