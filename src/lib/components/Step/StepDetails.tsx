import clsx from 'clsx';

import { StepDetailsProps } from './types';

const StepDetails = (props: StepDetailsProps) => {
  const { title, description = '', type, isCurrent, className } = props;

  return (
    <div
      className={clsx(
        className,
        type === 'dot' && 'mt-[-6px]',
        isCurrent && type === 'dot' && 'mt-[-2px]',
      )}
    >
      <div className='vertical-item-top-info'>{title}</div>
      <div className='vertical-item-bottom-info'>{description}</div>
    </div>
  );
};

export default StepDetails;
