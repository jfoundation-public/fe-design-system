import clsx from 'clsx';

import StepHorizontal from './StepHorizontal';
import StepVertical from './StepVertical';
import { StepProps } from './types';

const Step = (props: StepProps) => {
  const { direction, className, ...restProps } = props;
  const isVertical = direction === 'vertical';

  return (
    <div className={clsx('timeline-wrapper', className)}>
      {isVertical ? <StepVertical {...restProps} /> : <StepHorizontal {...restProps} />}
    </div>
  );
};

export default Step;
