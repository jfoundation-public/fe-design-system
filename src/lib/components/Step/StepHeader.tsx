import clsx from 'clsx';

import { Icon } from '@/lib';

import { StepHeaderProps } from './types';

const StepHeader = (props: StepHeaderProps) => {
  const { config, type, status, isCurrent, number, alternativeLabel } = props;

  const dynamicHeader = () => {
    switch (type) {
      case 'dot': {
        return (
          <div
            className={clsx(
              'step-header-dot w-[8px] h-[8px] rounded-full',
              status === 'default' && 'bg-surface-default',
              status === 'finish' && 'bg-surface-primary',
              status === 'error' && 'bg-red-500',
              status === 'disable' && 'bg-surface-disable',
              isCurrent &&
                status !== 'error' &&
                status !== 'disable' &&
                '!w-[12px] !h-[12px] bg-surface-primary',
            )}
          />
        );
      }

      case 'icon': {
        return (
          <Icon
            className={clsx(
              '',
              status === 'finish' && 'text-primary',
              status === 'disable' && 'text-text-disable',
              isCurrent && status === 'default' && 'text-primary',
            )}
            color={status === 'finish' || status === 'disable' ? undefined : status}
            iconName={config.icon}
            size={config.size}
            variant={config.variant}
          />
        );
      }

      case 'number': {
        return (
          <div
            className={clsx(
              'rounded-full border w-6 h-6 justify-center items-center text-center',
              status === 'default' && 'border-lineBorder-primary text-lineBorder-whiteLine',
              isCurrent && status === 'default' && 'bg-lineBorder-primary',
            )}
          >
            <>
              {status === 'default' && (
                <span className={clsx('text-xs', isCurrent ? 'text-white' : 'text-primary')}>
                  {number}
                </span>
              )}
              {status === 'finish' && (
                <Icon className='text-primary' iconName='circleTick' variant='solid' />
              )}
              {status === 'error' && <Icon color={status} iconName='circleCross' variant='solid' />}
              {status === 'disable' && (
                <>
                  {alternativeLabel ? (
                    <Icon className='text-text-disable' iconName='circleWarning' variant='solid' />
                  ) : (
                    <span className={clsx('text-xs', isCurrent ? 'text-white' : 'text-primary')}>
                      {number}
                    </span>
                  )}
                </>
              )}
            </>
          </div>
        );
      }
    }
  };
  return <div className='step-header'>{dynamicHeader()}</div>;
};

export default StepHeader;
