import clsx from 'clsx';

import StepDetails from './StepDetails';
import StepHeader from './StepHeader';
import { StepItem, StepVerticalProps } from './types';

const StepVertical = (props: StepVerticalProps) => {
  const {
    steps,
    current,
    status,
    config = { icon: 'time-clock', size: 24, variant: 'solid' },
    type = 'dot',
    onSelect,
    headerConfig,
    alternativeLabel,
    detailClassName,
  } = props;

  return (
    <div className='vertical-step-container'>
      {steps.map((item: StepItem, index: number) => {
        return (
          <div
            role={'presentation'}
            onClick={(e) => {
              if (e.defaultPrevented) return;
              if (status[item.id] === 'disable') return;
              onSelect && onSelect(index);
            }}
            key={item.id}
            className='vertical-step-section'
          >
            <div className='vertical-step-icon-wrapper'>
              <div className={clsx('vertical-step-icon-control ', onSelect && 'cursor-pointer')}>
                <StepHeader
                  number={index + 1}
                  type={headerConfig?.[index].type || type}
                  config={
                    headerConfig
                      ? {
                          icon: headerConfig?.[index].icon || 'time-clock',
                          size: headerConfig?.[index].size || 24,
                          variant: headerConfig?.[index].variant || 'solid',
                        }
                      : Array.isArray(config)
                      ? {
                          icon: config[index].icon || 'time-clock',
                          size: config[index].size || 24,
                          variant: config[index].variant || 'solid',
                        }
                      : config
                  }
                  status={headerConfig?.[index].status || status?.[item.id] || 'default'}
                  isCurrent={headerConfig?.[index].isCurrent ?? current === index}
                  alternativeLabel={alternativeLabel}
                />
              </div>
              {steps.length !== index + 1 && (
                <div
                  role='presentation'
                  className='w-8 flex justify-center'
                  onClick={(e) => {
                    e.preventDefault();
                  }}
                >
                  <div
                    className={clsx(
                      'vertical-step-line',
                      status[item.id] === 'finish' && 'border-lineBorder-primary',
                    )}
                  />
                </div>
              )}
            </div>
            <StepDetails
              className={clsx('flex flex-col cursor-pointer', detailClassName)}
              isCurrent={current === index}
              type={type}
              title={item.title}
              description={item.description}
            />
          </div>
        );
      })}
    </div>
  );
};

export default StepVertical;
