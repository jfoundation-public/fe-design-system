import clsx from 'clsx';

import StepDetails from './StepDetails';
import StepHeader from './StepHeader';
import { StepHorizontalProps, StepItem } from './types';
const StepHorizontal = (props: StepHorizontalProps) => {
  const {
    steps,
    current,
    status,
    config = { icon: 'time-clock', size: 24, variant: 'solid' },
    type = 'dot',
    onSelect,
    lineClassName,
    alternativeLabel,
    detailClassName,
  } = props;

  return (
    <div className='horizontal-step-container flex items-center'>
      {steps.map((item: StepItem, index: number) => (
        <div
          role='presentation'
          onClick={(e) => {
            if (e.defaultPrevented) return;
            if (status[item.id] === 'disable') return;
            onSelect && onSelect(index);
          }}
          key={item.id}
          className={clsx(
            'flex items-center relative ',
            index === steps.length - 1 ? 'w-auto' : 'w-full',
            onSelect && 'cursor-pointer',
          )}
        >
          <div className='relative'>
            <StepHeader
              number={index + 1}
              type={type}
              config={
                Array.isArray(config)
                  ? {
                      icon: config[index].icon || 'time-clock',
                      size: config[index].size || 24,
                      variant: config[index].variant || 'solid',
                    }
                  : config
              }
              status={status?.[item.id] || 'default'}
              isCurrent={current === index}
              alternativeLabel={alternativeLabel}
            />
            <div
              className={clsx(
                'centered-absolute-x text-center  w-32 text-xs',
                type === 'dot' ? 'mt-2' : 'mt-1',
                detailClassName,
                // type === 'dot' && '-ml-14',
                // type !== 'dot' && '-ml-12 mt-8',
              )}
            >
              <StepDetails
                className='flex flex-col'
                isCurrent={current === index}
                type={type}
                title={item.title}
                description={item.description}
              />
            </div>
          </div>

          {steps.length !== index + 1 && (
            <div
              role='presentation'
              className='h-8 w-full flex items-center'
              onClick={(e) => {
                e.preventDefault();
              }}
            >
              <div
                className={clsx(
                  'ml-1 mr-1 w-full border-t-[1px]',
                  status[item.id] === 'finish' && 'border-lineBorder-primary',
                  lineClassName,
                )}
              />
            </div>
          )}
        </div>
      ))}
    </div>
  );
};

export default StepHorizontal;
