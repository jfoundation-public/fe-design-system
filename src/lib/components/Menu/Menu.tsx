import clsx from 'clsx';
import { useState } from 'react';

import { Icon, isArray } from '@/lib';

import MenuItem from './MenuItem';
import { MenuItemType, MenuProps } from './types';

const Menu = (props: MenuProps) => {
  const { data, selectedMenu, onClickMenu } = props;
  const { title, listSubMenu = [] } = data;

  const [collapse, setCollapse] = useState(true);

  const _selectedMenu = listSubMenu.find((item) => item.id === selectedMenu.id);

  const _classNameMenu = clsx('menu');

  return (
    <div className={_classNameMenu}>
      <div
        role='presentation'
        onClick={() => setCollapse((prev) => !prev)}
        className={clsx('menu-header', _selectedMenu && 'w-full')}
      >
        <span className='menu-header-title'>{title}</span>
        <span className='menu-header-arrow'>
          <Icon
            iconName={`${collapse ? 'arrowDown' : 'arrowUp'}`}
            variant='solid'
            // color='default'
          />
        </span>
      </div>
      {!collapse && (
        <div className='menu-list-item'>
          {isArray(listSubMenu) &&
            listSubMenu.map((item: MenuItemType) => (
              <MenuItem
                selectedMenu={selectedMenu}
                key={item.name}
                onClickMenu={onClickMenu}
                item={item}
                className={item.className}
              />
            ))}
        </div>
      )}
    </div>
  );
};

export default Menu;
