import { ReactElement } from 'react';

import { SelectSubMenuType } from '../Sidebar/type';
export interface MenuProps {
  data: MenuData;
  selectedMenu: SelectSubMenuType;
  onClickMenu: (item: SelectSubMenuType) => void;
}

export interface MenuData {
  id: string;
  title: string;
  listSubMenu: MenuItemType[];
}
export interface MenuItemType {
  id: string;
  name: string;
  url: string;
  startAdornment?: ReactElement;
  endAdornment?: ReactElement;
  className?: string;
}

export interface MenuItemProps {
  item: MenuItemType;
  selectedMenu: SelectSubMenuType;
  isFlat?: boolean;
  onClickMenu: (item: SelectSubMenuType) => void;
  className?: string;
}
