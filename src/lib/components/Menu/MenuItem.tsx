import clsx from 'clsx';
import { useCallback } from 'react';

import { MenuItemProps } from './types';

const MenuItem = (props: MenuItemProps) => {
  const { item, onClickMenu, selectedMenu, isFlat = false, className } = props;

  const handleOnClick = useCallback(() => {
    onClickMenu && onClickMenu(item);
  }, [onClickMenu, item]);

  return (
    <div
      role='presentation'
      onClick={handleOnClick}
      className={clsx(
        'menu-item',
        isFlat && 'capitalize',
        selectedMenu.id === item.id && 'active',
        className,
      )}
    >
      {item.startAdornment}
      <p className='truncate'>{item.name}</p>
      {item.endAdornment}
    </div>
  );
};

export default MenuItem;
