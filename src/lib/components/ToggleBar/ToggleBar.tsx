import clsx from 'clsx';

import { noop } from '@/lib/utils';

import { ToggleBarProps } from './types';

export default function ToggleBar(props: ToggleBarProps) {
  const { value, options, onChange, className, ...restProps } = props;
  return (
    <ul className={clsx('toggle-bar', className)} {...restProps}>
      {options.map((opt) => {
        const isActive = value === opt.value;
        return (
          <li
            key={opt.value}
            role='menuitem'
            className={clsx(
              'toggle-bar__item',
              isActive ? 'toggle-bar__item--active' : 'toggle-bar__item--default',
            )}
            onClick={() => {
              onChange && onChange(opt.value);
            }}
            onKeyDown={noop}
            tabIndex={0}
          >
            {opt.label}
          </li>
        );
      })}
    </ul>
  );
}
