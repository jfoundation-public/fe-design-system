import { ComponentPropsWithoutRef } from 'react';

export interface ToggleBarProps extends Omit<ComponentPropsWithoutRef<'ul'>, 'onChange'> {
  value: string;
  options: { value: any; label: string }[];
  onChange?: (val: any) => void;
}
