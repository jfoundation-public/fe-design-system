import { ChangeEvent } from 'react';

import { Checkbox, FieldSearch, TablePagination } from '@/lib';
import { isEmpty, noop } from '@/lib/utils';

import TransferCheckBoxMapping from './TransferCheckBoxMapping';
import { DataTransferCheckBox, Position, TransferCheckBoxProps } from './types';

const TransferCheckBox = (props: TransferCheckBoxProps) => {
  const { checkBox, onSearch, className, title, position, onCheckBoxAll } = props;

  const selected = checkBox.data.filter((item: DataTransferCheckBox) => item.checked === true);
  const allChecked =
    !isEmpty(checkBox.data) &&
    checkBox.data.every((item: DataTransferCheckBox) => item.checked === true);
  const indeterminate = checkBox.data.some((item: DataTransferCheckBox) => item.checked === true);

  const handleSearch = (position: Position, value: string) => {
    onSearch && onSearch(position, value);
  };

  return (
    <div className={className}>
      <div className='transfer-checkBox-item-header'>
        <Checkbox
          indeterminate={!allChecked && indeterminate}
          checked={allChecked}
          onChange={() => onCheckBoxAll(position, allChecked)}
        >
          <span className='transfer-checkBox-item-title'>{title}</span>
        </Checkbox>
        <span className='transfer-checkBox-item-title-selectedCount'>
          {selected.length} selected
        </span>
      </div>
      <div className='transfer-checkBox-item-search'>
        <FieldSearch
          noOutline
          className='w-auto'
          name='transfer-checkBox-item-search'
          onChange={(e: ChangeEvent<HTMLInputElement>) => handleSearch(position, e.target.value)}
          onBlur={noop}
          onFocus={noop}
        />
      </div>
      <div className='mt-6'>
        <TransferCheckBoxMapping
          data={checkBox.searchValue ? checkBox.filterData : checkBox.data}
          onCheckBox={checkBox.onCheckBox}
          isShowAvatar={checkBox.isShowAvatar}
        />
      </div>
      <div className='transfer-checkBox-item-pagination'>
        <TablePagination
          page={checkBox.pagination.page}
          pageSize={checkBox.pagination.pageSize}
          total={checkBox.pagination.total}
          onPageChange={checkBox.pagination.onPageChange}
          onPageSizeChange={checkBox.pagination.onPageSizeChange}
        />
      </div>
    </div>
  );
};

export default TransferCheckBox;
