import { useCallback, useDeferredValue, useEffect, useState } from 'react';

import { noop } from '@/lib';

import { DataTransferCheckBox, TransferCheckBoxPaginationType } from './types';

const useTransferCheckBox = ({
  dataL,
  dataR,
  dataCheckBoxTitleL,
  dataCheckBoxTitleR,
  classNameL,
  classNameR,
  paginationR = {
    page: 1,
    pageSize: 10,
    total: 10,
    onPageChange: noop,
    onPageSizeChange: noop,
  },
  paginationL = {
    page: 1,
    pageSize: 10,
    total: 10,
    onPageChange: noop,
    onPageSizeChange: noop,
  },
  isShowAvatar = false,
}: {
  dataL: DataTransferCheckBox[];
  dataR: DataTransferCheckBox[];
  dataCheckBoxTitleL: string;
  dataCheckBoxTitleR: string;
  classNameL?: string;
  classNameR?: string;
  paginationL?: TransferCheckBoxPaginationType;
  paginationR?: TransferCheckBoxPaginationType;
  isShowAvatar?: boolean;
}) => {
  const [dataCheckBoxL, setDataCheckBoxL] = useState<DataTransferCheckBox[]>(dataL);
  const [dataCheckBoxR, setDataCheckBoxR] = useState<DataTransferCheckBox[]>(dataR);

  const [dataFilterL, setDataFilterL] = useState<DataTransferCheckBox[]>(dataL);
  const [dataFilterR, setDataFilterR] = useState<DataTransferCheckBox[]>(dataR);

  const [searchValueR, setSearchValueR] = useState<string>('');
  const [searchValueL, setSearchValueL] = useState<string>('');

  const handleCheckAll = useCallback(
    (position: string, isCheckAll: boolean) => {
      if (position === 'left')
        return setDataCheckBoxL(
          dataCheckBoxL.map((item: DataTransferCheckBox) => ({ ...item, checked: !isCheckAll })),
        );
      return setDataCheckBoxR(
        dataCheckBoxR.map((item: DataTransferCheckBox) => ({ ...item, checked: !isCheckAll })),
      );
    },
    [dataCheckBoxL, dataCheckBoxR],
  );
  const handleChangeSearch = useCallback((position: string, value: string) => {
    if (position === 'left') return setSearchValueL(value);
    return setSearchValueR(value);
  }, []);

  const searchCheckBoxCachedR = useDeferredValue(searchValueR);

  const searchCheckBoxCachedL = useDeferredValue(searchValueL);

  useEffect(() => {
    setDataFilterR(
      dataCheckBoxR.filter((t) =>
        t.text.toLocaleLowerCase().includes(searchCheckBoxCachedR.toLocaleLowerCase()),
      ),
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [searchCheckBoxCachedR]);

  useEffect(() => {
    setDataFilterL(
      dataCheckBoxL.filter((t) =>
        t.text.toLocaleLowerCase().includes(searchCheckBoxCachedL.toLocaleLowerCase()),
      ),
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [searchCheckBoxCachedL]);

  const onCheckBox = useCallback(
    (id: string, isCheck: boolean) => {
      dataCheckBoxL.map((res1: DataTransferCheckBox) => {
        if (res1.id === id) {
          res1.checked = !isCheck;
          return res1.checked;
        }
      });

      dataCheckBoxR.map((res2: DataTransferCheckBox) => {
        if (res2.id === id) {
          res2.checked = !isCheck;
          return res2.checked;
        }
      });

      setDataCheckBoxL(() => [...dataCheckBoxL]);
      setDataCheckBoxR(() => [...dataCheckBoxR]);
    },
    [dataCheckBoxL, dataCheckBoxR],
  );

  const handleTransfer = useCallback(
    (direction: string) => {
      if (direction === 'right') {
        dataCheckBoxL.map((result: DataTransferCheckBox) => {
          if (result.checked) {
            dataCheckBoxR.push(result);
            setDataCheckBoxL(dataCheckBoxL.filter((i: DataTransferCheckBox) => !i.checked));
            setDataCheckBoxR([...dataCheckBoxR]);
          }
          return null;
        });
      }
      if (direction === 'left') {
        dataCheckBoxR.map((result: DataTransferCheckBox) => {
          if (result.checked) {
            dataCheckBoxL.push(result);
            setDataCheckBoxL([...dataCheckBoxL]);
            setDataCheckBoxR(dataCheckBoxR.filter((i: DataTransferCheckBox) => !i.checked));
          }
          return null;
        });
      }
    },
    [dataCheckBoxL, dataCheckBoxR],
  );

  const checkBox = {
    searchValueL,
    searchValueR,
    dataCheckBoxTitleL,
    dataCheckBoxTitleR,
    classNameL,
    classNameR,
    dataFilterL,
    dataFilterR,
    dataCheckBoxL,
    dataCheckBoxR,
    paginationL,
    paginationR,
    onCheckBox,
    isShowAvatar,
  };

  return [checkBox, handleChangeSearch, handleCheckAll, handleTransfer] as const;
};

export default useTransferCheckBox;
