import { FocusEventHandler } from 'react';

import { TreeItem, TreeViewExpandedState, TreeViewSelectedState } from '../TreeView/types';

export type Position = 'left' | 'right';

export interface DataTransferCheckBox {
  id: string;
  avatar?: string;
  text: string;
  subText?: string;
  checked: boolean;
  customClass?: string;
}

export interface TransferCheckBoxPaginationType {
  page: number;
  pageSize: number;
  total: number;
  onPageChange: () => void;
  onPageSizeChange: () => void;
}

export interface TransferCheckBoxObjectType {
  isShowAvatar?: boolean;
  classNameR?: string;
  classNameL?: string;
  dataFilterR: DataTransferCheckBox[];
  dataFilterL: DataTransferCheckBox[];
  dataCheckBoxL: DataTransferCheckBox[];
  dataCheckBoxR: DataTransferCheckBox[];
  dataCheckBoxTitleL: string;
  dataCheckBoxTitleR: string;
  searchValueL: string;
  searchValueR: string;
  paginationL: TransferCheckBoxPaginationType;
  paginationR: TransferCheckBoxPaginationType;
  onCheckBox: (id: string, isChecked: boolean) => void;
}

export interface TransferTreeViewObjectType {
  classNameR?: string;
  classNameL?: string;
  dataTreeViewL: TreeItem<any>[];
  dataTreeViewR: TreeItem<any>[];
  dataTreeViewTitleL: string;
  dataTreeViewTitleR: string;
  searchValueL: string;
  searchValueR: string;
  dataTreeSelectedR: TreeViewSelectedState;
  dataTreeExpandedR: TreeViewExpandedState;
  dataTreeSelectedL: TreeViewSelectedState;
  dataTreeExpandedL: TreeViewExpandedState;
  onSelect: (position: Position, updater: TreeViewSelectedState) => void;
  onExpand: (position: Position, updater: TreeViewExpandedState) => void;
}

export interface TransferProps {
  onCheckAll: (position: Position, isCheckAll: boolean) => void;
  onTransfer: (position: Position) => void;
  onChangeSearch?: (position: Position, value: string) => void;
  onSearch?: (position: Position, value: string) => void;
  type: 'checkbox' | 'tree-view';
  className?: string;
  treeView?: TransferTreeViewObjectType;
  checkBox?: TransferCheckBoxObjectType;
}

export interface TransferCheckBoxProps {
  checkBox: {
    isShowAvatar: boolean;
    onCheckBox: (id: string, isChecked: boolean) => void;
    searchValue: string;
    data: DataTransferCheckBox[];
    filterData: DataTransferCheckBox[];
    pagination: TransferCheckBoxPaginationType;
  };
  className?: string;
  title: string;
  position: Position;
  onSearch: (position: Position, value: string) => void;
  onBlurSearch?: FocusEventHandler<HTMLInputElement>;
  onFocusSearch?: FocusEventHandler<HTMLInputElement>;
  onCheckBoxAll: (position: Position, isCheckAll: boolean) => void;
}

export interface TransferCheckBoxMappingProps {
  isShowAvatar: boolean;
  data: DataTransferCheckBox[];
  onCheckBox: (id: string, isChecked: boolean) => void;
}

export interface TransferTreeViewProps {
  treeView: {
    onSelect: (position: Position, updater: TreeViewSelectedState) => void;
    onExpand: (position: Position, updater: TreeViewExpandedState) => void;
    searchValue: string;
    data: TreeItem<any>[];
    dataTreeSelected: TreeViewSelectedState;
    dataTreeExpanded: TreeViewExpandedState;
  };
  className?: string;
  title: string;
  position: Position;
  onSearch: (position: Position, value: string) => void;
  onBlurSearch?: FocusEventHandler<HTMLInputElement>;
  onFocusSearch?: FocusEventHandler<HTMLInputElement>;
  onCheckBoxAll: (position: Position, isCheckAll: boolean) => void;
}
