import { useDeferredValue, useEffect, useState } from 'react';

import { TreeItem, TreeViewExpandedState, TreeViewSelectedState } from '../TreeView';
import { flattenBy } from '../TreeView/utils';
import { Position } from './types';

const useTransferTreeView = ({
  dataL,
  dataR,
  dataCheckBoxTitleL,
  dataCheckBoxTitleR,
  classNameL,
  classNameR,
}: {
  dataL: TreeItem<any>[];
  dataR: TreeItem<any>[];
  dataCheckBoxTitleL: string;
  dataCheckBoxTitleR: string;
  classNameL?: string;
  classNameR?: string;
}) => {
  const [treeDataR, setTreeDataR] = useState(dataR);
  const [treeDataL, setTreeDataL] = useState(dataL);

  const [selectedR, setSelectedR] = useState<TreeViewSelectedState>({});
  const [selectedL, setSelectedL] = useState<TreeViewSelectedState>({});

  const [expandedR, setExpandedR] = useState<TreeViewExpandedState>({});
  const [expandedL, setExpandedL] = useState<TreeViewExpandedState>({});

  const [searchValueR, setSearchValueR] = useState('');
  const [searchValueL, setSearchValueL] = useState('');

  const handleSearchTreeView = (position: Position, value: string) => {
    if (position === 'left') return setSearchValueL(value);
    return setSearchValueR(value);
  };

  const handleExpandedTreeView = (position: Position, value: TreeViewExpandedState) => {
    if (position === 'left') return setExpandedL(value);
    return setExpandedR(value);
  };

  const handleSelectedTreeView = (position: Position, value: TreeViewSelectedState) => {
    if (position === 'left') return setSelectedL(value);
    return setSelectedR(value);
  };

  const handleCheckAllTreeView = (position: Position, isCheckedAll: boolean) => {
    const newSelected: TreeViewSelectedState = {};
    if (position === 'left') {
      const flattenItem = flattenBy(treeDataL, (node) => node.items ?? []);
      flattenItem[0].forEach((elem: any) => {
        newSelected[elem.id] = !isCheckedAll;
      });
      return setSelectedL(newSelected);
    }
    const flattenItem = flattenBy(treeDataR, (node) => node.items ?? []);
    flattenItem[0].forEach((elem: any) => {
      newSelected[elem.id] = !isCheckedAll;
    });
    return setSelectedR(newSelected);
  };

  const handleTransferTreeView = (position: Position) => {
    if (position === 'left') {
      console.log(selectedL, 'selectedL');
    }
  };

  const searchCachedR = useDeferredValue(searchValueR);

  const searchCachedL = useDeferredValue(searchValueL);

  useEffect(() => {
    setTreeDataR(
      dataR.filter((t) => t.label.toLocaleLowerCase().includes(searchCachedR.toLocaleLowerCase())),
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [searchCachedR]);

  useEffect(() => {
    setTreeDataL(
      dataL.filter((t) => t.label.toLocaleLowerCase().includes(searchCachedL.toLocaleLowerCase())),
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [searchCachedL]);

  const treeView = {
    dataTreeViewL: treeDataL,
    dataTreeViewR: treeDataR,
    dataTreeViewTitleL: dataCheckBoxTitleL,
    dataTreeViewTitleR: dataCheckBoxTitleR,
    onExpand: handleExpandedTreeView,
    dataTreeSelectedR: selectedR,
    dataTreeExpandedR: expandedR,
    dataTreeSelectedL: selectedL,
    dataTreeExpandedL: expandedL,
    onSelect: handleSelectedTreeView,
    searchValueL: searchValueL,
    searchValueR: searchValueR,
    classNameL: classNameL,
    classNameR: classNameR,
  };

  return [treeView, handleSearchTreeView, handleCheckAllTreeView, handleTransferTreeView] as const;
};

export default useTransferTreeView;
