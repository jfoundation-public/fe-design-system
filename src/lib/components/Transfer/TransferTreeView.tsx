import { Checkbox, isEmpty, TreeView } from '@/lib';

import { flattenBy } from '../TreeView/utils';
import { TransferTreeViewProps } from './types';

const TransferTreeView = (props: TransferTreeViewProps) => {
  const { treeView, className, title, position, onCheckBoxAll, onSearch } = props;
  const flattenData = flattenBy(treeView.data, (node) => node.items ?? []);
  const selected = Object.values(treeView.dataTreeSelected).filter((item) => item === true);

  const allChecked =
    !isEmpty(treeView.data) &&
    Object.values(treeView.dataTreeSelected).every((item) => item === true) &&
    flattenData[0].length ===
      Object.values(treeView.dataTreeSelected).filter((item) => item === true).length;

  const indeterminate = Object.values(treeView.dataTreeSelected).some((item) => item === true);

  return (
    <div className={className}>
      <div className='transfer-treeView-item-header'>
        <Checkbox
          disabled={isEmpty(treeView.data)}
          indeterminate={!allChecked && indeterminate}
          checked={allChecked}
          onChange={() => onCheckBoxAll(position, allChecked)}
        >
          <span className='transfer-treeView-item-title'>{title}</span>
        </Checkbox>
        <span className='transfer-treeView-item-title-selectedCount'>
          {selected.length} selected
        </span>
      </div>
      <div className='mt-6'>
        <TreeView
          treeIdGenerator={(__id) => __id}
          className='transfer-treeView-item'
          data={treeView.data}
          selected={treeView.dataTreeSelected}
          onSelected={(value: any) => treeView.onSelect(position, value)}
          expanded={treeView.dataTreeExpanded}
          onExpand={(value: any) => treeView.onExpand(position, value)}
          searchProps={{
            noOutline: true,
            name: 'tree-search',
            value: treeView.searchValue,
            onChange: (e) => {
              onSearch(position, e.target.value);
            },
          }}
        />
      </div>
    </div>
  );
};

export default TransferTreeView;
