import clsx from 'clsx';

import { Button, Icon, isEmpty } from '@/lib';
import { noop } from '@/lib/utils';

import { _initCheckBox, _initTreeView } from './helper';
import TransferCheckBox from './TransferCheckBox';
import TransferTreeView from './TransferTreeView';
import { TransferProps } from './types';

const Transfer = (props: TransferProps) => {
  const {
    className,
    checkBox = _initCheckBox,
    treeView = _initTreeView,
    type = 'checkbox',
    onCheckAll,
    onSearch = noop,
    onTransfer,
  } = props;

  const _className = clsx(className, 'transfer-list');

  const _checkBoxClassNameR = clsx(checkBox.classNameR, 'transfer-right-side');
  const _checkBoxClassNameL = clsx(checkBox.classNameL, 'transfer-left-side');

  const _treeViewClassNameR = clsx(treeView.classNameR, 'transfer-right-side');
  const _treeViewClassNameL = clsx(treeView.classNameL, 'transfer-left-side');

  const disableBtnTransferR =
    type === 'checkbox' ? isEmpty(checkBox.dataCheckBoxR) : isEmpty(treeView.dataTreeViewR);

  const disableBtnTransferL =
    type === 'checkbox' ? isEmpty(checkBox.dataCheckBoxL) : isEmpty(treeView.dataTreeViewL);

  return (
    <div className={_className}>
      <div>
        {type === 'checkbox' ? (
          <TransferCheckBox
            checkBox={{
              isShowAvatar: checkBox.isShowAvatar || false,
              onCheckBox: checkBox.onCheckBox,
              searchValue: checkBox.searchValueL,
              data: checkBox.dataCheckBoxL,
              filterData: checkBox.dataFilterL,
              pagination: checkBox.paginationL,
            }}
            position='left'
            title={checkBox.dataCheckBoxTitleL}
            className={_checkBoxClassNameL}
            onCheckBoxAll={onCheckAll}
            onSearch={onSearch}
          />
        ) : (
          <TransferTreeView
            position='left'
            treeView={{
              onExpand: treeView.onExpand,
              onSelect: treeView.onSelect,
              searchValue: treeView.searchValueL,
              data: treeView.dataTreeViewL,
              dataTreeExpanded: treeView.dataTreeExpandedL,
              dataTreeSelected: treeView.dataTreeSelectedL,
            }}
            title={treeView.dataTreeViewTitleL}
            className={_treeViewClassNameL}
            onCheckBoxAll={onCheckAll}
            onSearch={onSearch}
          />
        )}
      </div>
      <div className='transfer-buttons'>
        <div>
          <Button
            disabled={disableBtnTransferR}
            className='transfer-btn-left'
            onClick={() => onTransfer('left')}
          >
            <Icon size={16} style={{ color: '#C4C8D6' }} iconName='left' variant='solid' />
          </Button>
        </div>
        <div>
          <Button
            disabled={disableBtnTransferL}
            className='transfer-btn-right'
            onClick={() => onTransfer('right')}
          >
            <Icon size={16} style={{ color: '#C4C8D6' }} iconName='right' variant='solid' />
          </Button>
        </div>
      </div>
      <div>
        {type === 'checkbox' ? (
          <TransferCheckBox
            position='right'
            checkBox={{
              isShowAvatar: checkBox.isShowAvatar || false,
              onCheckBox: checkBox.onCheckBox,
              searchValue: checkBox.searchValueR,
              data: checkBox.dataCheckBoxR,
              filterData: checkBox.dataFilterR,
              pagination: checkBox.paginationR,
            }}
            title={checkBox.dataCheckBoxTitleR}
            className={_checkBoxClassNameR}
            onCheckBoxAll={onCheckAll}
            onSearch={onSearch}
          />
        ) : (
          <TransferTreeView
            position='right'
            treeView={{
              onExpand: treeView.onExpand,
              onSelect: treeView.onSelect,
              searchValue: treeView.searchValueR,
              data: treeView.dataTreeViewR,
              dataTreeExpanded: treeView.dataTreeExpandedR,
              dataTreeSelected: treeView.dataTreeSelectedR,
            }}
            title={treeView.dataTreeViewTitleR}
            className={_treeViewClassNameR}
            onCheckBoxAll={onCheckAll}
            onSearch={onSearch}
          />
        )}
      </div>
    </div>
  );
};
export default Transfer;
