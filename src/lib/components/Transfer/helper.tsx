import { noop } from '@/lib';

import { TransferCheckBoxObjectType, TransferTreeViewObjectType } from './types';

export const _initCheckBox: TransferCheckBoxObjectType = {
  classNameL: '',
  classNameR: '',
  isShowAvatar: false,
  searchValueL: '',
  searchValueR: '',
  dataCheckBoxTitleL: '',
  dataCheckBoxTitleR: '',
  dataCheckBoxL: [],
  dataCheckBoxR: [],
  dataFilterL: [],
  dataFilterR: [],
  paginationL: {
    page: 1,
    pageSize: 10,
    total: 10,
    onPageChange: noop,
    onPageSizeChange: noop,
  },
  paginationR: {
    page: 1,
    pageSize: 10,
    total: 10,
    onPageChange: noop,
    onPageSizeChange: noop,
  },
  onCheckBox: noop,
};

export const _initTreeView: TransferTreeViewObjectType = {
  classNameL: '',
  classNameR: '',
  dataTreeViewL: [],
  dataTreeViewR: [],
  dataTreeViewTitleL: '',
  dataTreeViewTitleR: '',
  searchValueL: '',
  searchValueR: '',
  onSelect: noop,
  onExpand: noop,
  dataTreeSelectedR: {},
  dataTreeExpandedR: {},
  dataTreeSelectedL: {},
  dataTreeExpandedL: {},
};
