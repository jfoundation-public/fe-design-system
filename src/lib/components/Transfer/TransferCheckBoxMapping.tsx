import { Avatar, Checkbox, isEmpty } from '@/lib';

import { DataTransferCheckBox, TransferCheckBoxMappingProps } from './types';

const TransferCheckBoxMapping = (props: TransferCheckBoxMappingProps) => {
  const { data = [], onCheckBox, isShowAvatar } = props;

  return (
    <div className='transfer-item-checkbox-wrapper'>
      {!isEmpty(data) &&
        data.map((item: DataTransferCheckBox) => (
          <div key={item.id} className='transfer-item-checkbox'>
            <Checkbox
              value={item.id}
              checked={item.checked}
              onChange={() => onCheckBox(item.id, item.checked)}
            >
              {isShowAvatar ? (
                <Avatar
                  imgClassName={item.customClass}
                  type={item.avatar ? 'img' : 'text'}
                  name={item.text}
                  subName={item.subText}
                  src={item.avatar}
                />
              ) : (
                item.text
              )}
            </Checkbox>
          </div>
        ))}
    </div>
  );
};

export default TransferCheckBoxMapping;
