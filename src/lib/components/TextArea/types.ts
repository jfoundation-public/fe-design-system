import { ComponentPropsWithRef, Dispatch, HTMLAttributes, SetStateAction } from 'react';

import { IconName, IconVariant } from '../Icon/types';

export interface TextAreaProps extends ComponentPropsWithRef<'textarea'> {
  onChange?: (
    event: React.ChangeEvent<HTMLTextAreaElement>,
  ) => void | Dispatch<SetStateAction<string>> | undefined;
  label?: string;
  labelIcon?: IconName;
  labelIconVariant?: IconVariant;
  helperText?: string;
  value?: string;
  isError?: boolean;
  isWarning?: boolean;
  isSuccess?: boolean;
  inputProps?: HTMLAttributes<HTMLTextAreaElement>;
}
