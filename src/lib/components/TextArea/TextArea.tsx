import clsx from 'clsx';
import { useMemo } from 'react';

import { getFieldState } from '../FieldContainer/helper';
import { FieldLabel } from '../FieldLabel';
import { HelperText } from '../HelperText';
import { TextAreaProps } from './types';

export default function TextArea(props: TextAreaProps) {
  const {
    label,
    labelIcon,
    labelIconVariant,
    value,
    onChange,
    maxLength,
    helperText,
    isSuccess,
    isError,
    isWarning,
    disabled,
    readOnly,
    className,
    rows = 4,
    inputProps,
    ...restProps
  } = props;
  // const [value, setValue] = useState(valueProp ?? '');

  // function handleChange(e: ChangeEvent<HTMLTextAreaElement>) {
  //   const _value = e.target.value;
  //   setValue(_value);
  //   onChange && onChange(e);
  // }

  const fieldState = useMemo(
    () => getFieldState({ isError, isSuccess, isWarning }),
    [isError, isSuccess, isWarning],
  );

  return (
    <div
      className={clsx(
        'field-container',
        'text-area',
        isError && 'text-area--error',
        isWarning && 'text-area--warning',
        isSuccess && 'text-area--success',
        readOnly && 'text-area--readonly',
        disabled && 'text-area--disabled',
        className,
      )}
    >
      {(!!label || !!labelIcon) && (
        <FieldLabel
          required={restProps.required}
          iconName={labelIcon}
          iconVariant={labelIconVariant}
        >
          {label}
        </FieldLabel>
      )}
      <textarea
        value={value}
        onChange={onChange}
        maxLength={maxLength}
        rows={rows}
        readOnly={readOnly}
        disabled={disabled}
        {...restProps}
        {...(inputProps ?? [])}
      />
      {(maxLength || helperText) && (
        <div className='flex items-center'>
          {helperText && <HelperText state={fieldState ?? 'default'}>{helperText}</HelperText>}
          {Boolean(maxLength) && (
            <HelperText
              state={fieldState ?? 'default'}
              className='ml-auto'
            >{`${value?.length}/${maxLength}`}</HelperText>
          )}
        </div>
      )}
    </div>
  );
}
