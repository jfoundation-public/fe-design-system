import { ChangeEventHandler, FocusEventHandler, ReactElement } from 'react';

import { SelectOption } from '@/lib/types';

import { AutocompleteBaseProps } from '../AutocompleteBase/types';
import { FieldControlledProps } from '../FieldControlled/types';

export interface AutocompleteProps extends FieldControlledProps, AutocompleteBaseProps {
  value?: string[];

  options: SelectOption[];
  name: string;
  placeholder?: string;
  readOnly?: boolean;
  disabled?: boolean;
  startAdornment?: ReactElement;
  endAdornment?: ReactElement;
  onChange?: ChangeEventHandler<HTMLInputElement>;
  onBlur?: FocusEventHandler<HTMLInputElement>;
  onFocus?: FocusEventHandler<HTMLInputElement>;
}
