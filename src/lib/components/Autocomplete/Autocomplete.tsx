import { ControlledAutoComplete } from '../AutocompleteBase';
import { FieldControlled } from '../FieldControlled';
import { AutocompleteProps } from './types';

export default function Autocomplete(props: AutocompleteProps) {
  const {
    label,
    labelIcon,
    helperText,
    startAdornment,
    endAdornment,
    isWarning,
    isSuccess,
    isError,
    Container,
    ...restProps
  } = props;

  return (
    <FieldControlled
      label={label}
      labelIcon={labelIcon}
      helperText={helperText}
      endAdornment={endAdornment}
      startAdornment={startAdornment}
      isWarning={isWarning}
      isSuccess={isSuccess}
      isError={isError}
      {...Container}
    >
      <ControlledAutoComplete {...restProps} />
    </FieldControlled>
  );
}
