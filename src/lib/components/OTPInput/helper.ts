import { isNil, range } from '@/lib';

function parseOtpValue(length: number, val?: string) {
  const initValue = range(0, length).map(() => '');
  if (isNil(val)) return initValue;
  const splitVal = val.toString().split('');
  return initValue.map((v, i) => splitVal[i] ?? v);
}

function isInputValueValid(value?: string) {
  if (isNil(value)) return false;
  return !isNaN(parseInt(value, 10)) && value.trim().length === 1;
}

export { isInputValueValid, parseOtpValue };
