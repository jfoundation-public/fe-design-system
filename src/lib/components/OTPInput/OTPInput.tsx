import clsx from 'clsx';
import produce from 'immer';
import { ChangeEvent, ClipboardEvent, KeyboardEvent, useEffect, useMemo, useState } from 'react';

import { createCustomInputEvent, range } from '@/lib';

import { getFieldState } from '../FieldContainer/helper';
import { HelperText } from '../HelperText';
import DigitInput from './DigitInput';
import { isInputValueValid, parseOtpValue } from './helper';
import { OTPInputProps } from './types';

export default function OTPInput(props: OTPInputProps) {
  const { length = 6, value, onChange, name, className, isError, helperText, ...restProps } = props;

  const [focusIndex, setFocusIndex] = useState(0);
  const [digitValues, setDigitValues] = useState<string[]>(parseOtpValue(length, value));

  const fieldState = useMemo(() => getFieldState({ isError }), [isError]);

  function setDigitValueAtIndex(_index: number, _value: string) {
    setDigitValues((prev) =>
      produce(prev, (draft) => {
        draft[_index] = _value;
      }),
    );
  }

  function handleDigitInputFocus(_index: number) {
    return () => {
      setFocusIndex(Math.max(Math.min(_index, length - 1), 0));
    };
  }

  function handleDigitInputChange(_index: number) {
    return (e: ChangeEvent<HTMLInputElement>) => {
      if (isInputValueValid(e.target.value)) {
        setDigitValueAtIndex(_index, e.target.value);
      }
    };
  }

  function handleKeyDown(e: KeyboardEvent<HTMLInputElement>) {
    switch (e.key) {
      case 'Backspace':
        e.preventDefault();
        setDigitValueAtIndex(focusIndex, '');
        handleDigitInputFocus(focusIndex - 1)();
        break;
      case 'Delete':
        e.preventDefault();
        setDigitValueAtIndex(focusIndex, '');
        break;
      case 'ArrowLeft':
        e.preventDefault();
        handleDigitInputFocus(focusIndex - 1)();
        break;
      case 'ArrowRight':
        e.preventDefault();
        handleDigitInputFocus(focusIndex + 1)();
        break;
      case ' ':
      case 'Spacebar':
      case 'Space':
        e.preventDefault();
        break;
      default:
        break;
    }
  }

  function handleInput(e: ChangeEvent<HTMLInputElement>) {
    if (isInputValueValid(e.target.value)) {
      handleDigitInputFocus(focusIndex + 1)();
    }
  }

  function handlePaste(e: ClipboardEvent<HTMLInputElement>) {
    e.preventDefault();
    const pastedData = e.clipboardData
      .getData('text/plain')
      .slice(0, length - focusIndex)
      .split('');
    const digitsValueCopied = [...digitValues];
    let currentPointer = focusIndex;
    for (let i = focusIndex; i < length; ++i) {
      const pastedDigitValue = pastedData.shift();
      if (isInputValueValid(pastedDigitValue)) {
        digitsValueCopied[i] = pastedDigitValue!;
        currentPointer++;
      }
    }
    handleDigitInputFocus(currentPointer)();
    setDigitValues(digitsValueCopied);
  }

  useEffect(() => {
    function syncOnChange(_digitValues: string[]) {
      onChange && onChange(createCustomInputEvent({ name, value: _digitValues.join('') }));
    }
    syncOnChange(digitValues);
  }, [digitValues, name, onChange]);

  return (
    <>
      <div className={clsx('otp-input', className)} {...restProps}>
        {range(0, length).map((digitIndex) => (
          <DigitInput
            key={digitIndex}
            value={digitValues[digitIndex]}
            onFocus={handleDigitInputFocus(digitIndex)}
            onChange={handleDigitInputChange(digitIndex)}
            onInput={handleInput}
            onKeyDown={handleKeyDown}
            onPaste={handlePaste}
            focused={digitIndex === focusIndex}
            isError={isError}
            placeholder='-'
          />
        ))}
      </div>
      {!!helperText && (
        <HelperText state={fieldState ?? 'default'} className='mt-1'>
          {helperText}
        </HelperText>
      )}
    </>
  );
}
