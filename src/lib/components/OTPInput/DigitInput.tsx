import clsx from 'clsx';
import { useEffect, useRef } from 'react';

import { DigitInputProps } from './types';

export default function DigitInput(props: DigitInputProps) {
  const { focused = false, isError = false, ...restProps } = props;
  const inputRef = useRef<HTMLInputElement>(null);

  useEffect(() => {
    if (focused) {
      inputRef.current?.focus();
      inputRef.current?.select();
    }
  }, [focused]);

  return (
    <input
      className={clsx('otp-input__digit', isError && 'border-text-error')}
      type='tel'
      maxLength={1}
      ref={inputRef}
      {...restProps}
    />
  );
}
