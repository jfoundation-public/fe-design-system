import { ChangeEventHandler, ComponentPropsWithoutRef } from 'react';

import { FieldContainerProps } from '../FieldContainer';

export interface OTPInputProps
  extends ComponentPropsWithoutRef<'div'>,
    Pick<FieldContainerProps, 'isError' | 'helperText'> {
  length?: number;
  name?: string;
  onChange?: ChangeEventHandler<HTMLInputElement>;
  value?: string;
}

export interface DigitInputProps extends ComponentPropsWithoutRef<'input'> {
  focused?: boolean;
  isError?: boolean;
}
