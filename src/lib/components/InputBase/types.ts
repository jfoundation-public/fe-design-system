import { ComponentPropsWithRef, ReactElement } from 'react';

export interface InputBaseProps extends ComponentPropsWithRef<'input'> {
  endAdornment?: ReactElement;
  format?: (val: string | undefined) => string | undefined;
  parse?: (val: string | undefined) => string | undefined;
}
