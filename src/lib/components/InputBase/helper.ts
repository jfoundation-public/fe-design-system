function getInputBaseWrapperClasses() {
  return 'input-base-container';
}

function getInputBaseClasses() {
  return 'input-base';
}

function getAdornmentContainerClasses() {
  return 'adornment-container';
}

function setCaretPosition(el: HTMLInputElement, caretPos: number) {
  // eslint-disable-next-line no-self-assign
  el.value = el.value;
  // ^ this is used to not only get 'focus', but
  // to make sure we don't have it everything -selected-
  // (it causes an issue in chrome, and having it doesn't hurt any other browser)
  if (el !== null) {
    if ((el as any).createTextRange) {
      const range = (el as any).createTextRange();
      range.move('character', caretPos);
      range.select();
      return true;
    }
    // (el.selectionStart === 0 added for Firefox bug)
    if (el.selectionStart || el.selectionStart === 0) {
      el.focus();
      el.setSelectionRange(caretPos, caretPos);
      return true;
    }

    // fail city, fortunately this never happens (as far as I've tested) :)
    el.focus();
    return false;
  }
}

export {
  getAdornmentContainerClasses,
  getInputBaseClasses,
  getInputBaseWrapperClasses,
  setCaretPosition,
};
