/* eslint-disable jsx-a11y/no-redundant-roles */
/* eslint-disable jsx-a11y/no-autofocus */
import clsx from 'clsx';
import { cloneElement, forwardRef, isValidElement, useMemo } from 'react';

import {
  getAdornmentContainerClasses,
  getInputBaseClasses,
  getInputBaseWrapperClasses,
} from './helper';
import { InputBaseProps } from './types';

/**
 * Extends from HTML input with adornment
 */
function InputBase(props: InputBaseProps, ref?: React.LegacyRef<HTMLInputElement>) {
  const { endAdornment, className, onChange, format, parse, ...restProps } = props;
  const containerClasses = useMemo(() => getInputBaseWrapperClasses(), []);
  const classes = useMemo(() => getInputBaseClasses(), []);
  const endAdormentWrapperClasses = useMemo(() => getAdornmentContainerClasses(), []);
  const renderEndAdornment = isValidElement(endAdornment)
    ? cloneElement(endAdornment as React.ReactElement<any>, {
        className: endAdormentWrapperClasses,
      })
    : null;

  if (!format) {
    return (
      <div className={clsx(containerClasses, className)}>
        <input
          {...restProps}
          ref={ref}
          onChange={onChange}
          className={classes}
          // add below addtributes to prevent auto-complete on keyboard IOS
          role='textbox'
          autoFocus={false}
          autoCapitalize='off'
          autoComplete='off'
          autoCorrect='off'
          spellCheck='false'
          tabIndex={0}
        />
        {renderEndAdornment}
      </div>
    );
  }
  return (
    <div className={clsx(containerClasses, className)}>
      <input
        {...restProps}
        value={format(restProps.value as string)}
        onChange={(e) => {
          onChange &&
            onChange({
              ...e,
              target: {
                ...e.target,
                name: restProps.name ?? '',
                value: parse ? (parse(e.target.value) as string) : e.target.value,
              },
            });
        }}
        className={classes}
        // add below addtributes to prevent auto-complete on keyboard IOS
        role='textbox'
        autoFocus={false}
        autoCapitalize='off'
        autoComplete='off'
        autoCorrect='off'
        spellCheck='false'
        tabIndex={0}
      />
      {renderEndAdornment}
    </div>
  );
}

export default forwardRef(InputBase);
