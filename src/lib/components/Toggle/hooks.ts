import { ChangeEvent, useCallback, useMemo, useState } from 'react';

import { ToggleProps } from './types';

function useToggleControlled({
  readOnly,
  disabled,
  onChange,
}: Pick<ToggleProps, 'readOnly' | 'disabled' | 'onChange'>) {
  const [hovered, setHovered] = useState(false);

  const handleChange = useCallback(
    (event: ChangeEvent<HTMLInputElement>) => {
      if (disabled || readOnly) {
        return;
      }
      onChange && onChange(event);
    },
    [disabled, onChange, readOnly],
  );

  const handleMouseEnter = useCallback(() => {
    setHovered(true);
  }, []);

  const handleMouseLeave = useCallback(() => {
    setHovered(false);
  }, []);

  return useMemo(
    () => ({
      onChange: handleChange,
      onMouseEnter: handleMouseEnter,
      onMouseLeave: handleMouseLeave,
      hovered,
    }),
    [handleChange, handleMouseEnter, handleMouseLeave, hovered],
  );
}

export { useToggleControlled };
