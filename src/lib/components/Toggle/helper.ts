import clsx from 'clsx';

function getToggleClasses({
  checked,
  hovered,
  readOnly,
  disabled,
}: {
  checked?: boolean;
  hovered?: boolean;
  readOnly?: boolean;
  disabled?: boolean;
}) {
  return clsx('toggle', [
    hovered && !checked && !readOnly && !disabled && 'toggle-hovered',
    checked && 'toggle-checked',
    readOnly && 'toggle-read-only',
    disabled && 'toggle-disabled',
  ]);
}

export { getToggleClasses };
