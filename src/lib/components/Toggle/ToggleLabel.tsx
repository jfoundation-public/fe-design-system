import { isString } from '@/lib/utils';

import { Typography } from '../Typography';
import { ToggleLabelProps } from './types';

export default function ToggleLabel({ children }: ToggleLabelProps) {
  if (!children) return null;

  if (isString(children))
    return (
      <Typography className='toggle-label' variant='body'>
        {children}
      </Typography>
    );
  return <div className='toggle-label'>{children}</div>;
}
