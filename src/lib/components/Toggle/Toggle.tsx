import { useId, useMemo } from 'react';

import { getToggleClasses } from './helper';
import { useToggleControlled } from './hooks';
import ToggleLabel from './ToggleLabel';
import { ToggleProps } from './types';

export default function Toggle(props: ToggleProps) {
  const { checked, onChange, readOnly, disabled, className, id, children, ...restProps } = props;

  const uuid = useId();
  const {
    onChange: _onChange,
    onMouseEnter,
    onMouseLeave,
    hovered,
  } = useToggleControlled({ onChange, readOnly, disabled });

  const toggleClasses = useMemo(
    () => getToggleClasses({ checked, hovered, readOnly, disabled }),
    [checked, disabled, hovered, readOnly],
  );

  return (
    <div className={className}>
      <label
        htmlFor={id ?? uuid}
        className={toggleClasses}
        onMouseEnter={onMouseEnter}
        onMouseLeave={onMouseLeave}
      >
        <div className='toggle-slider'>
          <input
            onChange={_onChange}
            id={id ?? uuid}
            type='checkbox'
            role='switch'
            className='hidden'
            {...restProps}
          />
          <div className='toggle-slider-wrapper'></div>
          <div className='toggle-slider-dot'></div>
        </div>
        <ToggleLabel>{children}</ToggleLabel>
      </label>
    </div>
  );
}
