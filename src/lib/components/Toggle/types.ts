import { ComponentPropsWithRef, ReactElement } from 'react';

export interface ToggleProps extends Omit<ComponentPropsWithRef<'input'>, 'type'> {
  children?: string | ReactElement;
}

export interface ToggleLabelProps {
  children?: string | ReactElement;
}
