import clsx from 'clsx';
import { lazy, useCallback, useMemo } from 'react';

import { AccordionItemsProps } from './types';

const Icon = lazy(() => import('../Icon/Icon'));

const AccordionItem = (props: AccordionItemsProps) => {
  const {
    onExpand,
    accordionId,
    expand = false,
    className,
    contentClassName,
    hasBorderHeader,
    children,
    title = '',
    expandIcon,
    collapseIcon,
  } = props;
  const handleExpand = useCallback(() => {
    onExpand && onExpand(accordionId);
  }, [onExpand, accordionId]);

  const customIcon = useMemo(() => {
    return expandIcon && collapseIcon ? (
      <>
        {expand ? (
          <div onClick={handleExpand} aria-hidden='true'>
            {collapseIcon}
          </div>
        ) : (
          <div onClick={handleExpand} aria-hidden='true'>
            {expandIcon}
          </div>
        )}
      </>
    ) : null;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [expand]);

  return (
    <div className='accordion-item'>
      <div className={clsx('accordion-header', className)}>
        {customIcon ? (
          <>{customIcon}</>
        ) : (
          <Icon
            onClick={handleExpand}
            viewBox='0 0 24 24'
            size={18}
            iconName={expand ? 'downWithCircle' : 'upWithCircle'}
            variant='outline'
            color='default'
          />
        )}
        <div className='accordion-header-text'>{title}</div>
        {hasBorderHeader && <div className='accordion-border' />}
      </div>
      {expand && <div className={clsx('accordion-body', contentClassName)}>{children}</div>}
    </div>
  );
};

export default AccordionItem;
