import { ComponentPropsWithRef } from 'react';

export type AccordionProps = ComponentPropsWithRef<'div'>;

export interface AccordionItemsProps extends ComponentPropsWithRef<'div'> {
  hasBorderHeader?: boolean;
  onExpand?: (accordionId: string) => void;
  expand: boolean;
  accordionId: string;
  title?: string;
  contentClassName?: string;
  expandIcon?: React.ReactNode;
  collapseIcon?: React.ReactNode;
}
