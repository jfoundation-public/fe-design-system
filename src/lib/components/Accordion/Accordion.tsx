import clsx from 'clsx';

import AccordionItem from './AccordionItem';
import { AccordionProps } from './types';
const Accordion = (props: AccordionProps) => {
  const { className, ...restProps } = props;
  return <div className={clsx('accordion-wrapper', className)} {...restProps} />;
};

Accordion.Item = AccordionItem;

export default Accordion;
