import { ChangeEvent, useCallback, useState } from 'react';

function useSearch() {
  const [query, setQuery] = useState('');

  const handleChange = useCallback((e: ChangeEvent<HTMLInputElement>) => {
    setQuery(e.target.value);
  }, []);

  return { query, onSearchChange: handleChange };
}

export { useSearch };
