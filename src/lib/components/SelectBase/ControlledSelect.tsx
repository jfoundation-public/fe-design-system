import { withControlled } from '../FieldControlled';
import SelectBase from './SelectBase';
import { SelectBaseProps } from './types';

/**
 * Controlled SelectBase
 */
export default withControlled<string | undefined | string[], SelectBaseProps<any>>(SelectBase);
