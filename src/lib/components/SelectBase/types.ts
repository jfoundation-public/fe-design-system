import { ComponentPropsWithRef, HTMLAttributes, ReactElement } from 'react';

import { SelectOption } from '@/lib/types';

import { TagSelectedProps } from '../AutocompleteBase';

export interface SelectBaseProps<T extends SelectOption> extends ComponentPropsWithRef<'input'> {
  value: string | string[];
  options: T[];
  hasSearch?: boolean;
  searchFunc?: (option: T, search: string) => boolean;
  fullWidth?: boolean;
  moreSelectItem?: (props: { onClose: () => void }) => ReactElement;
  renderOption?: (props: {
    option: T;
    onSelected: (value: string) => void;
    isSelected: boolean;
  }) => ReactElement;
  renderMultipleOptions?: (props: {
    options: SelectOption[];
    onChangeSelected?: (value: string) => void;
  }) => ReactElement;
  isMultiple?: boolean;
  floatingContentProps?: HTMLAttributes<HTMLDivElement>;
  optionsListProps?: HTMLAttributes<HTMLUListElement>;
  selectedTagsProps?: Partial<TagSelectedProps>;
  inputProps?: HTMLAttributes<HTMLInputElement>;
}
