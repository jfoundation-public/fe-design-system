import {
  autoUpdate,
  flip,
  FloatingFocusManager,
  FloatingOverlay,
  FloatingPortal,
  offset,
  size,
  useDismiss,
  useFloating,
  useInteractions,
  useListNavigation,
  useRole,
  useTypeahead,
} from '@floating-ui/react-dom-interactions';
import clsx from 'clsx';
import { useDeferredValue, useEffect, useState } from 'react';

import useFilterOptions from '@/lib/hooks/useFilterOptions';
import { SelectOption } from '@/lib/types';
import { createCustomInputEvent } from '@/lib/utils';

import { useFieldControlElement } from '../FieldInput/hooks';
import { FieldSearch } from '../FieldSearch';
import { Icon } from '../Icon';
import { SelectItem } from '../SelectItem';
import { useSearch } from './hooks';
import SelectMutiple from './SelectMutiple';
import { SelectBaseProps } from './types';

export default function SelectBase<T extends SelectOption = SelectOption>(
  props: SelectBaseProps<T>,
) {
  const {
    value,
    placeholder,
    onChange,
    onFocus,
    onBlur,
    options,
    name,
    hasSearch = false,
    searchFunc,
    fullWidth,
    readOnly,
    disabled,
    moreSelectItem: MoreSelectItem,
    renderOption,
    className,
    isMultiple,
    floatingContentProps,
    optionsListProps,
    inputProps,
  } = props;

  const containerElement = useFieldControlElement();

  const text = useDeferredValue(options.find((opt) => opt.value === value)?.label ?? value);
  const [openPopover, setOpenPopover] = useState(false);
  const { query, onSearchChange } = useSearch();

  const deferredOptions = useFilterOptions({ options, searchParam: query, searchFunc });

  const { x, y, reference, floating, strategy, context } = useFloating({
    open: openPopover,
    onOpenChange: setOpenPopover,
    whileElementsMounted: autoUpdate,
    placement: 'bottom-start',
    middleware: [
      offset(5),
      flip({ padding: 8 }),
      size({
        apply({ rects, availableHeight, elements }) {
          Object.assign(elements.floating.style, {
            width: `${rects.reference.width}px`,
            maxHeight: `${availableHeight}px`,
          });
        },
        padding: 8,
      }),
    ],
  });

  const { getReferenceProps, getFloatingProps } = useInteractions([
    useRole(context, { role: 'listbox' }),
    useDismiss(context),
    useListNavigation(context),
    useTypeahead(context),
  ]);

  function handleSelect(id: string) {
    if (!isMultiple) {
      onChange && onChange(createCustomInputEvent({ name, value: id }));
      setOpenPopover(false);
    } else {
      // console.log(createCustomInputEvent({ name, value: id }));
      onChange &&
        onChange(
          createCustomInputEvent({
            name,
            value: [...(value as string[]), id],
          }),
        );
    }
  }

  useEffect(() => {
    if (containerElement) {
      reference({
        getBoundingClientRect: () => containerElement!.getBoundingClientRect(),
        contextElement: containerElement!,
      });
    }
  }, [containerElement, reference]);

  return isMultiple ? (
    <SelectMutiple {...props} />
  ) : (
    <>
      <div
        role='presentation'
        className={clsx('select-base-container', fullWidth && 'select-base-container-w-full')}
        onClick={(e) => {
          if (e.defaultPrevented) return;
          !disabled && !readOnly && setOpenPopover((prev) => !prev);
        }}
      >
        <input
          readOnly
          value={text}
          placeholder={placeholder}
          onFocus={(e) => {
            onFocus && onFocus(e);
          }}
          onBlur={onBlur}
          ref={reference}
          className='select-base-input'
          {...(inputProps ?? {})}
          {...getReferenceProps({
            className: clsx(
              'select-base-input',
              disabled && 'select-base-input-disabled',
              inputProps?.className,
            ),
          })}
        />

        <Icon size={24} className='text-icon-default' iconName='down' variant='outline' />
      </div>
      <FloatingPortal id='portal-root'>
        {openPopover && (
          <FloatingOverlay className='z-picker' lockScroll>
            <FloatingFocusManager context={context} preventTabbing>
              <div
                {...getFloatingProps({
                  ref: floating,
                  style: {
                    position: strategy,
                    top: y ?? 0,
                    left: x ?? 0,
                  },
                })}
              >
                <div
                  {...floatingContentProps}
                  className={clsx('jf-scroll max-h-[536px] paper', floatingContentProps?.className)}
                >
                  {hasSearch && (
                    <FieldSearch
                      value={query}
                      onChange={onSearchChange}
                      className='p-2'
                      name={`${name}-search`}
                      noOutline
                      fullWidth
                    />
                  )}
                  <ul
                    {...optionsListProps}
                    className={clsx('select-base-options', optionsListProps?.className)}
                  >
                    {deferredOptions.map((item) =>
                      renderOption ? (
                        renderOption({
                          option: item,
                          onSelected: handleSelect,
                          isSelected: item.value === value,
                        })
                      ) : (
                        <SelectItem
                          key={item.value as string}
                          value={item.value as string}
                          selected={item.value === value}
                          onSelect={handleSelect}
                          disabled={item.disabled}
                          className={className}
                        >
                          {item.label}
                        </SelectItem>
                      ),
                    )}
                    {MoreSelectItem && (
                      <MoreSelectItem
                        onClose={() => {
                          setOpenPopover(false);
                        }}
                      />
                    )}
                  </ul>
                </div>
              </div>
            </FloatingFocusManager>
          </FloatingOverlay>
        )}
      </FloatingPortal>
    </>
  );
}
