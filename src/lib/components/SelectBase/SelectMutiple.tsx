/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import {
  autoUpdate,
  flip,
  FloatingFocusManager,
  FloatingOverlay,
  FloatingPortal,
  offset,
  size,
  useDismiss,
  useFloating,
  useInteractions,
  useListNavigation,
  useRole,
  useTypeahead,
} from '@floating-ui/react-dom-interactions';
import clsx from 'clsx';
import { useEffect, useMemo, useState } from 'react';

import useFilterOptions from '@/lib/hooks/useFilterOptions';
import { SelectOption } from '@/lib/types';
import { createCustomInputEvent, isNull, isUndefined } from '@/lib/utils';

import TagSelected from '../AutocompleteBase/TagSelected';
import { useFieldControlElement } from '../FieldInput/hooks';
import { FieldSearch } from '../FieldSearch';
import { Icon } from '../Icon';
import { SelectItem } from '../SelectItem';
import { Typography } from '../Typography';
import { useSearch } from './hooks';
import { SelectBaseProps } from './types';

function getOptionSelected(options?: SelectOption[], value?: string[] | null) {
  if (!value) return;
  if (isUndefined(value) || isNull(value)) return [];
  if (!options) return [];
  return value.map((val) => options.find((opt) => opt.value === val));
}

export default function SelectMutiple<T extends SelectOption = SelectOption>(
  props: SelectBaseProps<T>,
) {
  const {
    value,
    onChange,
    options,
    name,
    hasSearch = false,
    searchFunc,
    readOnly,
    disabled,
    moreSelectItem: MoreSelectItem,
    renderOption,
    className,
    fullWidth,
    placeholder,
    renderMultipleOptions,
    selectedTagsProps,
  } = props;

  const containerElement = useFieldControlElement();

  const [openPopover, setOpenPopover] = useState(false);
  const { query, onSearchChange } = useSearch();

  const deferredOptions = useFilterOptions({ options, searchParam: query, searchFunc });

  const { x, y, reference, floating, strategy, context } = useFloating({
    open: openPopover,
    onOpenChange: setOpenPopover,
    whileElementsMounted: autoUpdate,
    placement: 'bottom-start',
    middleware: [
      offset(5),
      flip({ padding: 8 }),
      size({
        apply({ rects, availableHeight, elements }) {
          Object.assign(elements.floating.style, {
            width: `${rects.reference.width}px`,
            maxHeight: `${availableHeight}px`,
          });
        },
        padding: 8,
      }),
    ],
  });

  const { getFloatingProps } = useInteractions([
    useRole(context, { role: 'listbox' }),
    useDismiss(context),
    useListNavigation(context),
    useTypeahead(context),
  ]);

  function handleSelect(itemValue: string) {
    onChange &&
      onChange(
        createCustomInputEvent({
          name,
          value: value?.includes(itemValue)
            ? (value as string[]).filter((val) => val !== itemValue)
            : [...(value ?? []), itemValue],
        }),
      );
  }

  useEffect(() => {
    if (containerElement) {
      reference({
        getBoundingClientRect: () => containerElement!.getBoundingClientRect(),
        contextElement: containerElement!,
      });
    }
  }, [containerElement, reference]);

  const deferredChips = useMemo(() => {
    if (!options || !value) {
      return [];
    } else {
      return getOptionSelected(options, value as string[])?.filter(Boolean) as SelectOption[];
    }
  }, [options, value]);

  return (
    <>
      <div
        className={clsx(
          'select-base-container',
          fullWidth && 'select-base-container-w-full',
          (disabled || readOnly) && 'pointer-events-none',
        )}
        onClick={(e) => {
          if (e.defaultPrevented) return;
          !disabled && !readOnly && setOpenPopover((prev) => !prev);
        }}
      >
        {value.length <= 0 ? (
          <Typography variant='body' className='text-text-400'>
            {placeholder}
          </Typography>
        ) : renderMultipleOptions ? (
          <>
            {renderMultipleOptions({
              options: deferredChips,
              onChangeSelected: handleSelect,
            })}
          </>
        ) : (
          <TagSelected
            tags={deferredChips}
            onRemoveTag={(id) => {
              onChange &&
                onChange(
                  createCustomInputEvent({
                    name,
                    value: (value as string[]).filter((p) => p !== id),
                  }),
                );
            }}
            {...(selectedTagsProps ?? {})}
          />
        )}

        <Icon size={24} className='text-icon-default !ml-auto' iconName='down' variant='outline' />
      </div>
      <FloatingPortal id='portal-root'>
        {openPopover && (
          <FloatingOverlay className='z-picker' lockScroll>
            <FloatingFocusManager context={context} preventTabbing>
              <div
                {...getFloatingProps({
                  ref: floating,
                  style: {
                    position: strategy,
                    top: y ?? 0,
                    left: x ?? 0,
                  },
                })}
              >
                <div className='jf-scroll max-h-[536px] paper'>
                  {hasSearch && (
                    <FieldSearch
                      value={query}
                      onChange={onSearchChange}
                      className='p-2'
                      name={`${name}-search`}
                      noOutline
                      fullWidth
                    />
                  )}
                  <ul className='select-base-options'>
                    {deferredOptions.map((item) =>
                      renderOption ? (
                        renderOption({
                          option: item,
                          onSelected: handleSelect,
                          isSelected: item.value === value,
                        })
                      ) : (
                        <SelectItem
                          key={item.value as string}
                          value={item.value as string}
                          selected={value.includes(item.value)}
                          onSelect={() => {
                            handleSelect(item.value);
                          }}
                          disabled={item.disabled}
                          className={className}
                          withCheck={true}
                        >
                          {item.label}
                        </SelectItem>
                      ),
                    )}
                    {MoreSelectItem && (
                      <MoreSelectItem
                        onClose={() => {
                          setOpenPopover(false);
                        }}
                      />
                    )}
                  </ul>
                </div>
              </div>
            </FloatingFocusManager>
          </FloatingOverlay>
        )}
      </FloatingPortal>
    </>
  );
}
