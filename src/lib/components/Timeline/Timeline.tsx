import clsx from 'clsx';

import { TimeLineProps } from './types';
import VerticalTimeLine from './VerticalTimeLine';

const Timeline = (props: TimeLineProps) => {
  const { direction = 'vertical', className, ...restProps } = props;
  const isVertical = direction === 'vertical';

  //TODO HORIZONTAL
  return (
    <div className={clsx('timeline-wrapper', className)}>
      {isVertical ? <VerticalTimeLine {...restProps} /> : 'TODO'}
    </div>
  );
};

export default Timeline;
