import clsx from 'clsx';

import { Icon } from '@/lib';

import { TimeLineItems, VerticalTimeLineProps } from './types';

const VerticalTimeLine = (props: VerticalTimeLineProps) => {
  const { items = [], renderBottom, renderTop, hasLastLine = false } = props;
  return (
    <div className='vertical-timeline-container'>
      {items.map((item: TimeLineItems, index: number) => (
        <div key={item.id} className='vertical-timeline-section'>
          <div className='vertical-timeline-icon-wrapper'>
            <div className='vertical-timeline-icon-control'>
              <div className='vertical-timeline-icon'>
                <Icon
                  color={item.iconColor || 'default'}
                  iconName={item.icon}
                  variant={item.iconVariant || 'solid'}
                  size={item.iconSize || 20}
                  className={clsx('icon-color-default', item.iconClassName)}
                />
              </div>
            </div>
            {index === items.length - 1 && !hasLastLine ? null : <div className='vertical-line' />}
          </div>
          <div className='vertical-item-information'>
            <div className='vertical-item-top-info'>{renderTop(item.topInfo, item.id)}</div>
            <div className='vertical-item-bottom-info'>
              {renderBottom(item.bottomInfo, item.id)}
            </div>
          </div>
        </div>
      ))}
    </div>
  );
};

export default VerticalTimeLine;
