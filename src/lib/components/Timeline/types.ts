import { ReactNode } from 'react';

import { IconName, IconVariant } from '@/lib';
import { Colors } from '@/lib/types';

export interface TimeLineTopInfo {
  time: {
    value: string;
    style?: string;
  };
  detail: {
    value: string;
    style?: string;
  };
}
export interface TimeLineBottomInfo {
  info: {
    value: string;
    style?: string;
  };
  detail: {
    value: string;
    style?: string;
  };
}
export interface TimeLineItems {
  id: string;
  icon: IconName;
  iconVariant?: IconVariant;
  iconColor?: Colors;
  iconClassName?: string;
  iconSize?: number;
  topInfo: TimeLineTopInfo;
  bottomInfo: TimeLineBottomInfo;
}

export interface TimeLineProps {
  className?: string;
  direction: 'vertical' | 'horizontal';
  items: TimeLineItems[];
  renderTop: (data: TimeLineTopInfo, id: string) => ReactNode;
  renderBottom: (data: TimeLineBottomInfo, id: string) => ReactNode;
  hasLastLine?: boolean;
}

export type VerticalTimeLineProps = Omit<TimeLineProps, 'direction' | 'className'>;
