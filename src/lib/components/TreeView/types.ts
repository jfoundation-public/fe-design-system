import { FC, ReactNode } from 'react';

import { FieldSearchProps } from '../FieldSearch/types';

export type TreeViewExpandedState = Record<string, boolean | undefined>;
export type TreeViewSelectedState = Record<string, boolean | undefined>;
export type TreeViewCheckedState = Record<string, boolean | undefined>;
export type Updater<T> = T | ((prev: T) => T);
export type TreeIdGenerator = (currentId: string, parentId: string) => string;

export type TreeItemBase<T extends Record<string, unknown>> = T & {
  id: string;
  label: string;
  disabled?: boolean;
};

export type TreeItemCore<T extends Record<string, unknown>> = TreeItemBase<T> & {
  getCanExpand: () => boolean;
  getIsExpanded: () => boolean;
  getCanSelect: () => boolean;
  getIsSelected: () => boolean;
  getCanCheck: () => boolean;
  getIsChecked: () => boolean;
  getIsSomeChildSelected: () => boolean;
  getIsAllChildSelected: () => boolean;
  getToggleSelectedHandler: () => () => void;
  getToggleExpandHandler: () => () => void;
  getToggleCheckedHandler: () => () => void;
  depth: number;
  treeId: string;
  childrenIds: null | string[];
  items?: TreeItemCore<T>[];
};

export type TreeItemContentComp<T extends Record<string, unknown>> = FC<TreeItemCore<T>>;
export interface TreeItemProps<T extends Record<string, unknown>> {
  treeItem: TreeItemCore<T>;
  treeItemContent: TreeItemContentComp<Record<string, unknown>>;
  children?: ReactNode;
  readOnly?: boolean;
}
export interface TreeViewProps<T extends Record<string, unknown>> {
  searchProps?: FieldSearchProps;
  selected?: TreeViewSelectedState;
  expanded?: TreeViewExpandedState;
  onSelected?: (updater: Updater<TreeViewSelectedState>) => void;
  onExpand?: (updater: Updater<TreeViewExpandedState>) => void;
  treeIdGenerator?: TreeIdGenerator;
  renderTreeItem?: TreeItemContentComp<Record<string, unknown>>;
  data: TreeItem<T>[];
  readOnly?: boolean;
  className?: string;
  checked?: TreeViewCheckedState;
  onChecked?: (updater: Updater<TreeViewSelectedState>) => void;
}

export type TreeItemFlat<T extends Record<string, unknown>> = TreeItemBase<T> & {
  depth: number;
  treeId: string;
  parentId: string;
  childrenIds: null | string[];
};

export type TreeItem<T extends Record<string, unknown>> = TreeItemBase<T> & {
  items?: TreeItem<T>[];
};
