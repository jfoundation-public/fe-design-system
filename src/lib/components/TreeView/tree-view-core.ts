import {
  TreeIdGenerator,
  TreeItem,
  TreeItemCore,
  TreeItemFlat,
  TreeViewCheckedState,
  TreeViewExpandedState,
  TreeViewSelectedState,
  Updater,
} from './types';
import { memo } from './utils';

function createTreeView<T extends Record<string, unknown>>(
  tree: TreeItem<T>[],
  options: {
    expanded?: TreeViewExpandedState;
    selected?: TreeViewSelectedState;
    onExpanded?: (updater: Updater<TreeViewExpandedState>) => void;
    onSelected?: (updater: Updater<TreeViewSelectedState>) => void;
    treeIdGenerator: TreeIdGenerator;
    checked?: TreeViewCheckedState;
    onChecked?: (updater: Updater<TreeViewCheckedState>) => void;
  },
) {
  const EMPTY_EXPANDED: TreeViewExpandedState = {};
  const EMPTY_SELECTED: TreeViewSelectedState = {};
  let inst = {
    _treeData: tree,
    expanded: options.expanded ?? EMPTY_EXPANDED,
    selected: options.selected ?? EMPTY_SELECTED,
    checked: options.checked ?? null,
    _setExpand: options.onExpanded,
    _setSelect: options.onSelected,
    _setChecked: options.onChecked,
  };

  function updateTreeData(_tree: TreeItem<T>[]) {
    if (_tree !== inst._treeData) {
      inst = { ...inst, _treeData: _tree };
    }
  }

  function updateOptions(opts: {
    expanded?: TreeViewExpandedState;
    selected?: TreeViewSelectedState;
    checked?: TreeViewCheckedState;
  }) {
    if (opts.expanded && opts.expanded !== inst.expanded) {
      inst = { ...inst, expanded: opts.expanded };
    }

    if (opts.selected && opts.selected !== inst.selected) {
      inst = { ...inst, selected: opts.selected };
    }

    if (opts.checked && opts.checked !== inst.checked) {
      inst = { ...inst, checked: opts.checked };
    }
  }

  function getTreeItems(parentId = '') {
    const _expanded: TreeViewExpandedState = memo(() => inst?.expanded, [inst?.expanded])();
    let _selected: TreeViewSelectedState = memo(() => inst?.selected, [inst.selected])();
    const _checked: TreeViewCheckedState | null = memo(() => inst?.checked, [inst.checked])();
    const setExpanded = options.onExpanded;
    const setSelected = options.onSelected;
    const setChecked = options.onChecked;
    const [flatById, flat] = memo(
      () => getFlattenTree(inst._treeData, options.treeIdGenerator),
      [inst._treeData],
    )();
    const flatItem = !parentId
      ? flat.filter((f) => f.depth === 0)
      : flatById[parentId].childrenIds?.map((id) => ({ ...flatById[id] }));
    if (!flatItem) return [] as TreeItemCore<T>[];
    return flatItem.map((f) => {
      const getCanSelect = () => !!options.selected;
      const getIsSelected = () => !!_selected[f.treeId];
      const getIsSomeChildSelected = () => getChildSelected(f, _selected, flatById) === 'some';
      const getIsAllChildSelected = () => getChildSelected(f, _selected, flatById) === 'all';
      const getCanCheck = () => (_checked ? true : false);
      return {
        ...f,
        getCanExpand: () => !!f.childrenIds?.length,
        getIsExpanded: () => !!_expanded[f.treeId],
        getIsChecked: () => (_checked ? !!_checked[f.treeId] : false),
        getCanSelect,
        getCanCheck,
        getIsSelected,
        getIsSomeChildSelected,
        getIsAllChildSelected,
        getToggleSelectedHandler: () => () => {
          const isSelected = getIsSelected();
          const isAllChildSelected = getIsAllChildSelected();
          const prevSelected = { ..._selected, [f.treeId]: !(isSelected || isAllChildSelected) };
          syncChildrenSelected(f, prevSelected, flatById);
          syncParentSelected(f, prevSelected, flatById);
          _selected = prevSelected;
          setSelected && setSelected(prevSelected);
        },
        getToggleExpandHandler: () => () => {
          const prevExpanded = { ..._expanded, [f.treeId]: !_expanded[f.treeId] };
          setExpanded && setExpanded(prevExpanded);
        },
        getToggleCheckedHandler: () => () => {
          const prevChecked = { [f.treeId]: true };
          setChecked!(prevChecked);
        },
      };
    }) as TreeItemCore<T>[];
  }

  return { getTreeItems, updateTreeData, updateOptions };
}

function getFlattenTree<T extends Record<string, unknown>>(
  tree: TreeItem<T>[],
  treeIdGenerator: TreeIdGenerator,
) {
  const flatById: Record<string, TreeItemFlat<T>> = {};
  const flat: Array<TreeItemFlat<T>> = [];
  function treeFlatten<T extends Record<string, unknown>>(
    tree: TreeItem<T>[],
    _flatById: Record<string, TreeItemFlat<T>> = {},
    _flat: Array<TreeItemFlat<T>> = [],
    depth = 0,
    parentId = '',
  ) {
    tree.forEach((treeItem) => {
      const { items, ...restItem } = treeItem;
      const treeId = treeIdGenerator(restItem.id as string, parentId);
      const newItem = {
        ...restItem,
        treeId,
        parentId,
        depth,
        childrenIds: items?.length
          ? items.map((child) => treeIdGenerator(child.id as string, treeId))
          : null,
      } as TreeItemFlat<T>;
      _flatById[treeId] = newItem;
      _flat.push(newItem);
      if (items?.length) {
        treeFlatten(items, _flatById, _flat, depth + 1, treeId);
      }
    });
  }
  treeFlatten(tree, flatById, flat);
  return [flatById, flat] as const;
}

function getChildSelected<T extends Record<string, unknown>>(
  treeItem: TreeItemFlat<T>,
  selected: TreeViewSelectedState,
  flatById: Record<string, TreeItemFlat<T>>,
): boolean | 'some' | 'all' {
  let isSomeChildSelected = false;
  let isAllChildSelected = true;
  if (!treeItem?.childrenIds?.length) return !!selected[treeItem.treeId];
  treeItem.childrenIds.forEach((treeItemChild) => {
    if (isSomeChildSelected && !isAllChildSelected) {
      return;
    }
    const currentSelected = Boolean(selected[flatById[treeItemChild]?.treeId]);
    isAllChildSelected &&= currentSelected;
    isSomeChildSelected ||= currentSelected;
  });
  return isAllChildSelected ? 'all' : isSomeChildSelected ? 'some' : false;
}

function syncChildrenSelected<T extends Record<string, unknown>>(
  treeItem: TreeItemFlat<T>,
  selected: TreeViewSelectedState,
  flatById: Record<string, TreeItemFlat<T>>,
) {
  const currentSelected = !!selected[treeItem.treeId];
  if (treeItem.childrenIds?.length) {
    treeItem.childrenIds.forEach((treeId) => {
      selected[treeId] = currentSelected;
      if (flatById[treeId]?.childrenIds?.length) {
        syncChildrenSelected(flatById[treeId], selected, flatById);
      }
    });
  }
}

function syncParentSelected<T extends Record<string, unknown>>(
  treeItem: TreeItemFlat<T>,
  selected: TreeViewSelectedState,
  flatById: Record<string, TreeItemFlat<T>>,
) {
  const parentId = treeItem.parentId;
  // console.log("👌 ~ parentId", parentId)
  if (parentId) {
    const parentTreeItem = flatById[parentId];
    const isAllChildSelected = parentTreeItem.childrenIds?.every((id) => !!selected[id]);
    selected[parentId] = isAllChildSelected;
    if (parentTreeItem.parentId) {
      syncParentSelected(parentTreeItem, selected, flatById);
    }
  }
}

export { createTreeView };
