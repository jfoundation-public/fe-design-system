export type NoInfer<T> = [T][T extends any ? 0 : never];

export function flattenBy<TNode extends { id: string }>(
  arr: TNode[],
  getChildren: (item: TNode) => TNode[],
) {
  const flatList: Array<TNode & { parentId: TNode['id'] }> = [];
  const flatListObj: Record<string, TNode & { parentId: TNode['id'] }> = {};

  const recurse = (subArr: TNode[], _parent: TNode['id']) => {
    subArr.forEach((item) => {
      const pushedItem = { ...item, parentId: _parent };
      flatList.push(pushedItem);
      flatListObj[pushedItem['id']] = pushedItem;
      const children = getChildren(item);
      if (children?.length) {
        recurse(children, item.id);
      }
    });
  };

  recurse(arr, '');

  return [flatList, flatListObj] as const;
}

export function treeIdGenerator(currentId: string, parentId?: string) {
  if (!parentId) return currentId;
  return [parentId, currentId].join('.');
}

export function memo<TDeps extends readonly any[], TResult>(
  fn: () => TResult,
  dependencies: [...TDeps],
): () => TResult {
  let deps: any[] = [];
  let result: TResult | undefined;

  return () => {
    const newDeps = dependencies;

    const depsChanged =
      newDeps.length !== deps.length ||
      newDeps.some((dep: any, index: number) => deps[index] !== dep);

    if (!depsChanged) {
      return result!;
    }

    deps = newDeps;
    result = fn();
    return result!;
  };
}
