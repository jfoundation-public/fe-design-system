import clsx from 'clsx';

import { IconButton } from '../Button';
import { Checkbox } from '../Checkbox';
import { Radio } from '../Radio';
import { TreeItemCore, TreeItemProps } from './types';

export function TreeItemContentDefault(props: TreeItemCore<{ label: string }>) {
  const { label } = props;
  return <div className='tree-view__content'>{label}</div>;
}

export default function TreeItem<T extends Record<string, unknown>>(props: TreeItemProps<T>) {
  const { readOnly, treeItem, treeItemContent: TreeItemContent, children } = props;

  return (
    <li>
      <summary>
        {treeItem.getCanExpand() && (
          <IconButton
            iconName={treeItem.getIsExpanded() ? 'minus-square' : 'add-square'}
            iconVariant='outline'
            onClick={treeItem.getToggleExpandHandler()}
            isAdornment
            className='mr-2'
            type='button'
          />
        )}
        {!treeItem.getCanCheck() && treeItem.getCanSelect() && (
          <Checkbox
            checked={treeItem.getIsSelected() || treeItem.getIsAllChildSelected()}
            indeterminate={treeItem.getIsSomeChildSelected()}
            onChange={treeItem.getToggleSelectedHandler()}
            readOnly={readOnly}
            className={clsx('mr-2', readOnly && 'pointer-events-none')}
            disabled={readOnly}
          />
        )}
        {treeItem.getCanCheck() && (
          <Radio
            className='h-6 mr-2'
            checked={treeItem.getIsChecked()}
            onChange={treeItem.getToggleCheckedHandler()}
          />
        )}
        <TreeItemContent {...treeItem} />
      </summary>
      {treeItem.getIsExpanded() && children}
    </li>
  );
}
