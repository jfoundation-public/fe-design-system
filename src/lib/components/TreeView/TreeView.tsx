import { useState } from 'react';

import { isNil } from '@/lib';

import { FieldSearch } from '../FieldSearch';
import { createTreeView } from './tree-view-core';
import TreeItemComp, { TreeItemContentDefault } from './TreeItem';
import { TreeItemCore, TreeViewProps } from './types';
import { treeIdGenerator as _treeIdGenerator } from './utils';

export default function TreeView<T extends Record<string, unknown>>(props: TreeViewProps<T>) {
  const {
    data,
    expanded,
    onExpand,
    selected,
    onSelected,
    readOnly,
    searchProps,
    treeIdGenerator = _treeIdGenerator,
    renderTreeItem = TreeItemContentDefault,
    className,
    checked,
    onChecked,
  } = props;

  const [treeCore] = useState(() =>
    createTreeView(data, {
      expanded,
      onExpanded: onExpand,
      selected,
      onSelected,
      treeIdGenerator,
      checked,
      onChecked,
    }),
  );

  treeCore.updateTreeData(data);
  treeCore.updateOptions({ expanded, selected, checked });

  function renderTree<T extends Record<string, unknown>>(treeItems: TreeItemCore<T>[]) {
    return (
      <ul>
        {treeItems.map((treeItem) => (
          <TreeItemComp
            treeItemContent={renderTreeItem}
            key={treeItem.treeId}
            treeItem={treeItem}
            readOnly={readOnly}
          >
            {!!treeItem.childrenIds?.length && renderTree(treeCore.getTreeItems(treeItem.treeId))}
          </TreeItemComp>
        ))}
      </ul>
    );
  }
  return (
    <div className={className}>
      {!isNil(searchProps) && <FieldSearch {...searchProps} />}
      <div className='tree-view'>{renderTree(treeCore.getTreeItems(''))}</div>
    </div>
  );
}
