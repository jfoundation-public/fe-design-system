import clsx from 'clsx';
import { ComponentPropsWithRef, useEffect, useRef } from 'react';

import { Tooltip } from '../Tooltip';
import { getLeftPosition } from './helper';

interface HandleProps extends ComponentPropsWithRef<'div'> {
  focused?: boolean;
  position: number;
  max: number;
  min: number;
  onFocusChange: (focusIdx: number) => void;
}

export default function Handle(props: HandleProps) {
  const { focused, position, max, min, className, onFocusChange, ...restProps } = props;
  const ref = useRef<HTMLDivElement>(null);

  const left = getLeftPosition(position, min, max);

  useEffect(() => {
    if (focused) {
      ref.current?.focus();
    }
  }, [focused]);

  return (
    <Tooltip placement='top' content={position}>
      <div
        className={clsx('slider__handle', focused && 'slider__handle--focused', className)}
        style={{ left }}
        role='slider'
        aria-valuenow={position}
        {...restProps}
        tabIndex={0}
        onBlur={() => {
          onFocusChange(-1);
        }}
        ref={ref}
      ></div>
    </Tooltip>
  );
}
