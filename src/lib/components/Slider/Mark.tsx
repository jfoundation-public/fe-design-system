import clsx from 'clsx';
import { ComponentPropsWithoutRef, ReactElement } from 'react';

import { getLeftPosition } from './helper';
import { RenderMarkProps } from './types';

interface MarkProps extends ComponentPropsWithoutRef<'div'> {
  position: number;
  min: number;
  max: number;
  isActive: boolean;
  onChangeValue: (position: number) => void;
  render: <T>(props: T & RenderMarkProps) => ReactElement;
}

export default function Mark(props: MarkProps) {
  const { position, min, max, isActive, onChangeValue, render: RenderComp, className } = props;
  const left = getLeftPosition(position, min, max);
  return (
    <div className={clsx('slider__mark', className)} style={{ left }}>
      <RenderComp value={position} isActive={isActive} onClick={() => onChangeValue(position)} />
    </div>
  );
}
