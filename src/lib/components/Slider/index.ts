export { default as Slider } from './Slider';
export type { RenderMarkProps, SliderProps } from './types';
