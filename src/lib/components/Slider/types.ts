import { ReactElement } from 'react';

export interface RenderMarkProps {
  value: number;
  isActive: boolean;
  onClick: () => void;
}

export type Marks = { [point: number]: (props: RenderMarkProps) => ReactElement };

export type VALUE<T extends boolean | undefined> = T extends true ? [number, number] : number;

export interface SliderProps<T extends boolean | undefined> {
  range?: T;
  value: VALUE<T>;
  disabled?: boolean;
  onChange: (value: VALUE<T>) => void;
  max: number;
  min?: number;
  step?: number;
  marks?: Marks;
  className?: string;
}
