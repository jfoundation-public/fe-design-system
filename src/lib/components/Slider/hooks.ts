import { RefObject } from 'react';

// https://javascript.info/mouse-drag-and-drop
function useDrag({
  containerRef,
  max,
  min,
}: {
  containerRef: RefObject<HTMLDivElement>;
  max: number;
  min: number;
}) {
  function onStartMove(
    e: React.MouseEvent<HTMLDivElement>,
    setOffSet: (widthDiff: number) => void,
  ) {
    e.preventDefault();
    e.stopPropagation();
    const { pageX: startX } = e;
    function onMouseMove(event: MouseEvent) {
      event.preventDefault();
      const { pageX: moveX } = event;
      const offsetX = moveX - startX;

      const { width } = containerRef.current!.getBoundingClientRect();
      const offSetPercent = offsetX / width;
      setOffSet((max - min) * offSetPercent);
    }

    document.addEventListener('mousemove', onMouseMove);
    document.addEventListener('mouseup', onMouseUp);
    function onMouseUp() {
      document.removeEventListener('mouseup', onMouseUp);
      document.removeEventListener('mousemove', onMouseMove);
    }
  }

  return { onStartMove };
}

export { useDrag };
