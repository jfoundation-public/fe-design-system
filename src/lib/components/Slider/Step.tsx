import clsx from 'clsx';
import { ComponentPropsWithoutRef } from 'react';

import { getLeftPosition } from './helper';

interface StepProps extends ComponentPropsWithoutRef<'div'> {
  position: number;
  min: number;
  max: number;
  isActive?: boolean;
  onChangeValue: (position: number) => void;
}

export default function Step(props: StepProps) {
  const { isActive, onChangeValue, position, min, max, className, ...restProps } = props;

  const left = getLeftPosition(position, min, max);
  return (
    <div
      className={clsx('slider__step', isActive && 'slider__step--active', className)}
      {...restProps}
      style={{ left }}
      onClick={() => {
        onChangeValue(position);
      }}
      role='presentation'
    ></div>
  );
}
