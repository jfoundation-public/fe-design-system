import clsx from 'clsx';
import { ComponentPropsWithRef } from 'react';

interface TrackProps extends ComponentPropsWithRef<'div'> {
  position: [number, number];
  max: number;
  min: number;
  isActive?: boolean;
}

export default function Track(props: TrackProps) {
  const { position, max, min, className, ...restProps } = props;
  const diff = max - min;
  const [from, to] = position;
  const left = ((from - min) / diff) * 100 + '%';
  const width = ((to - from) / diff) * 100 + '%';
  return (
    <div className={clsx('slider__track', className)} {...restProps} style={{ left, width }}></div>
  );
}
