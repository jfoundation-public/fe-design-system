import { isNumber } from '@/lib/utils';

function getPostion(
  isRange: boolean,
  value: number | [number, number],
  min: number,
): { handle: number[]; track: [number, number] } {
  if (!isRange) {
    return { handle: [value as number], track: [min, value as number] };
  }
  return { handle: value as [number, number], track: value as [number, number] };
}

function getFractionDigits(num: number) {
  return (num.toString().split('.')[1] ?? '').length;
}

function roundNumber(num: number, fractionDigits: number) {
  return Number(num.toFixed(fractionDigits));
}

function getValidValue(min: number, max: number, step: number) {
  return (value: number) => {
    const fractionDigits = Math.max(
      getFractionDigits(min),
      getFractionDigits(max),
      getFractionDigits(step),
    );
    const valueClosest = min + Math.round((value - min) / step) * step;

    return Math.min(Math.max(roundNumber(valueClosest, fractionDigits), min), max);
  };
}

function getLeftPosition(position: number, min: number, max: number) {
  const diff = max - min;
  return ((position - min) / diff) * 100 + '%';
}

function getIsActiveMark(position: number, min: number, value: number | [number, number]) {
  if (isNumber(value)) {
    return min <= position && position <= value;
  }
  return value[0] <= position && position <= value[1];
}

export { getIsActiveMark, getLeftPosition, getPostion, getValidValue };
