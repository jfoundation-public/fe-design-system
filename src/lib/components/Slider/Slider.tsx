import clsx from 'clsx';
import { Fragment, useDeferredValue, useEffect, useRef, useState } from 'react';

import { isEmpty, isNil, isNumber } from '@/lib';

import Handle from './Handle';
import { getIsActiveMark, getPostion, getValidValue } from './helper';
import { useDrag } from './hooks';
import Mark from './Mark';
import Rail from './Rail';
import Step from './Step';
import Track from './Track';
import { SliderProps, VALUE } from './types';

export default function Slider<T extends boolean | undefined = undefined>(props: SliderProps<T>) {
  const {
    range = false,
    min = 0,
    max,
    step = 1,
    value: valueProp,
    onChange,
    marks,
    className,
  } = props;

  const containerRef = useRef<HTMLDivElement>(null);

  const [value, setValue] = useState<number | [number, number]>(valueProp);
  const [focusedIndex, setFocusedIndex] = useState(0);

  const deferedValue = useDeferredValue(value);

  const { onStartMove } = useDrag({ containerRef, max, min });

  const { track, handle } = getPostion(range, value, min);

  function handleChangeValue(pos: number) {
    // @TO_DO: refactor this function
    if (!range || isNumber(value)) {
      setValue(pos);
      return;
    }
    if (pos >= value[1]) {
      setValue([value[0], pos]);
      return;
    }
    if (pos <= value[0]) {
      setValue([pos, value[1]]);
      return;
    }
    if (Math.abs(value[1] - pos) > Math.abs(pos - value[0])) {
      setValue([pos, value[1]]);
      return;
    }
    setValue([value[0], pos]);
  }

  function handleDiffWidth(idx: number) {
    return (diff: number) => {
      const _getNextValue = getValidValue(min, max, step);
      if (!range || isNumber(value)) {
        const prevValue = value as number;
        setValue(_getNextValue(prevValue + diff));
        return;
      }
      const prevValue = [value[0], value[1]];
      const nextValue = _getNextValue(prevValue[idx] + diff);
      prevValue[idx] = nextValue;
      const nextValues = prevValue.sort((a, b) => a - b) as [number, number];
      const nextFocus = nextValues.findIndex((v: number) => v === nextValue);
      setValue(nextValues);
      setFocusedIndex(nextFocus);
    };
  }

  useEffect(() => {
    if (onChange) {
      onChange(deferedValue as VALUE<T>);
    }
  }, [deferedValue, onChange]);

  return (
    <div ref={containerRef} className={clsx('slider', className)}>
      <Rail />
      <Track max={max} min={min} position={track} />
      {!isNil(marks) &&
        !isEmpty(marks) &&
        Object.entries(marks).map(([pos, MarkComp], markIndex) => {
          const isActive = getIsActiveMark(+pos, min, value);
          return (
            <Fragment key={markIndex}>
              <Step
                key={`step-${markIndex}`}
                isActive={isActive}
                position={+pos}
                min={min}
                max={max}
                onChangeValue={handleChangeValue}
              />
              <Mark
                key={`mark-${markIndex}`}
                isActive={isActive}
                position={+pos}
                min={min}
                max={max}
                onChangeValue={handleChangeValue}
                render={MarkComp}
              />
            </Fragment>
          );
        })}
      {handle.map((v, index) => (
        <Handle
          key={`handle-${index}`}
          max={max}
          min={min}
          position={v}
          focused={focusedIndex === index}
          onFocusChange={(idx: number) => {
            setFocusedIndex(idx);
          }}
          onMouseDown={(e) => {
            setFocusedIndex(index);
            onStartMove(e, handleDiffWidth(index));
          }}
        />
      ))}
    </div>
  );
}
