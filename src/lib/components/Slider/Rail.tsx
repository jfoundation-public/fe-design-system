import clsx from 'clsx';
import { ComponentPropsWithRef } from 'react';

type RailProps = ComponentPropsWithRef<'div'>;

export default function Rail(props: RailProps) {
  const { className, ...restProps } = props;
  return <div className={clsx('slider__rail', className)} {...restProps}></div>;
}
