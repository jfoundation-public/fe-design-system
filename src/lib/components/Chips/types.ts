import { ComponentPropsWithRef } from 'react';

export type ChipsSize = 'lg' | 'md' | 'sm';

export type ChipsVariant = 'contained' | 'outlined';

export interface ChipsProps extends ComponentPropsWithRef<'span'> {
  tagName: string;
  onRemove?: () => void;
  selected?: boolean;
  disabled?: boolean;
  size?: ChipsSize;
  variant?: ChipsVariant;
  hideSelectedIndicator?: boolean;
}
