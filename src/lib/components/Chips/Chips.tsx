import clsx from 'clsx';

import { isFunction } from '@/lib/utils';

import { IconButton } from '../Button';
import { Icon } from '../Icon';
import { getChipsClasses, getIconSize } from './helper';
import { ChipsProps } from './types';

export default function Chips(props: ChipsProps) {
  const {
    tagName,
    size = 'sm',
    onRemove,
    selected,
    disabled,
    className,
    variant = 'contained',
    hideSelectedIndicator,
    ...restProps
  } = props;
  const classes = getChipsClasses({ selected, disabled, size, variant });
  const iconSize = getIconSize(size);
  return (
    <span className={clsx(classes, className)} {...restProps}>
      {selected && !hideSelectedIndicator && (
        <Icon size={iconSize} iconName='tick' variant='outline' />
      )}
      <span>{tagName}</span>
      {isFunction(onRemove) && (
        <IconButton
          iconName='close-small'
          iconVariant='outline'
          isAdornment
          onClick={(e) => {
            e.preventDefault();
            onRemove();
          }}
          iconSize={iconSize}
        />
      )}
    </span>
  );
}
