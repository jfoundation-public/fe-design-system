import clsx from 'clsx';

import { ChipsSize, ChipsVariant } from './types';

const CHIPS_SIZE_CLASSES = {
  lg: 'chips-lg',
  md: 'chips-md',
  sm: 'chips-sm',
};

const ICONS_SIZE = {
  lg: 20,
  md: 16,
  sm: 14,
};

function getChipsClasses({
  selected,
  disabled,
  size,
  variant,
}: {
  selected?: boolean;
  disabled?: boolean;
  size: ChipsSize;
  variant?: ChipsVariant;
}) {
  return clsx(
    'chips',
    (selected && (variant === 'contained' ? 'chips-selected' : 'chips-selected-outlined')) ||
      (disabled && 'chips-disabled') ||
      'chips-default',
    CHIPS_SIZE_CLASSES[size],
  );
}

function getIconSize(size: ChipsSize) {
  return ICONS_SIZE[size];
}

export { getChipsClasses, getIconSize };
