import { FieldState } from '@/lib/types';

function getFieldContainerClasses() {
  return 'field-container';
}

function getFieldState({
  isError,
  isSuccess,
  isWarning,
}: {
  isError?: boolean;
  isSuccess?: boolean;
  isWarning?: boolean;
}): FieldState | undefined {
  return (isError && 'error') || (isSuccess && 'success') || (isWarning && 'warning') || undefined;
}

export { getFieldContainerClasses, getFieldState };
