import { Placement } from '@floating-ui/react-dom-interactions';
import { ComponentPropsWithoutRef, ReactNode } from 'react';

import { HelperTextProps } from '../HelperText';
import { IconName, IconVariant } from '../Icon/types';

export interface FieldContainerProps extends ComponentPropsWithoutRef<'div'> {
  label?: ReactNode;
  labelIcon?: IconName;
  labelIconVariant?: IconVariant;
  helperText?: string;
  isError?: boolean;
  isSuccess?: boolean;
  isWarning?: boolean;
  required?: boolean;
  helperTextProps?: Omit<HelperTextProps, 'state'>;
}

export interface WithContainerProps {
  label?: string;
  labelIcon?: IconName;
  helperText?: string;
  isError?: boolean;
  isSuccess?: boolean;
  isWarning?: boolean;
  className?: string;
  required?: boolean;
  pickerPlacement?: Placement;
  disableFuture?: boolean;
  disablePast?: boolean;
}
