import clsx from 'clsx';
import { useMemo } from 'react';

import { FieldInput } from '../FieldInput';
import { FieldLabel } from '../FieldLabel';
import { HelperText } from '../HelperText';
import { getFieldContainerClasses, getFieldState } from './helper';
import { FieldContainerProps } from './types';

export default function FieldContainer(props: FieldContainerProps) {
  const {
    label,
    labelIcon,
    helperText,
    className,
    isError,
    isSuccess,
    isWarning,
    children,
    helperTextProps,
    labelIconVariant,
    ...restProps
  } = props;
  const classes = getFieldContainerClasses();
  const fieldState = useMemo(
    () => getFieldState({ isError, isSuccess, isWarning }),
    [isError, isSuccess, isWarning],
  );
  return (
    <div className={clsx(classes, className)} {...restProps}>
      {!!label && (
        <FieldLabel
          required={restProps.required}
          iconVariant={labelIconVariant}
          iconName={labelIcon}
        >
          {label}
        </FieldLabel>
      )}
      <FieldInput isError={isError} isWarning={isWarning} isSuccess={isSuccess}>
        {children}
      </FieldInput>
      {!!helperText && (
        <HelperText {...helperTextProps} state={fieldState ?? 'default'}>
          {helperText}
        </HelperText>
      )}
    </div>
  );
}
