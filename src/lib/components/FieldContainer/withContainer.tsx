/* eslint-disable @typescript-eslint/no-unused-vars */
import clsx from 'clsx';
import { ComponentType, useMemo } from 'react';

import { FieldLabel } from '../FieldLabel';
import { HelperText } from '../HelperText';
import { getFieldContainerClasses, getFieldState } from './helper';
import { WithContainerProps } from './types';

export default function withContainer<T>(
  Component: ComponentType<Omit<T, keyof WithContainerProps>>,
) {
  return function Comp(props: T & WithContainerProps) {
    const {
      label,
      labelIcon,
      helperText,
      className,
      isError,
      isSuccess,
      isWarning,
      required,
      disableFuture,
      disablePast,
      pickerPlacement,
      ...restProps
    } = props;
    const classes = getFieldContainerClasses();
    const fieldState = useMemo(
      () => getFieldState({ isError, isSuccess, isWarning }),
      [isError, isSuccess, isWarning],
    );
    return (
      <div className={clsx(classes, className)} {...restProps}>
        {!!label && (
          <FieldLabel required={required} iconName={labelIcon}>
            {label}
          </FieldLabel>
        )}
        <Component
          disableFuture={disableFuture}
          disablePast={disablePast}
          pickerPlacement={pickerPlacement}
          isError={isError}
          isSuccess={isSuccess}
          isWarning={isWarning}
          {...restProps}
        />
        {!!helperText && <HelperText state={fieldState ?? 'default'}>{helperText}</HelperText>}
      </div>
    );
  };
}
