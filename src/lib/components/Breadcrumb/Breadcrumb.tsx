import clsx from 'clsx';
import { useCallback, useState } from 'react';

import BreadcrumbItem from './BreadcrumbItem';
import { BreadCrumbItem, BreadcrumbProps } from './types';

const Breadcrumb = (props: BreadcrumbProps) => {
  const { breadcrumbs, onSelected, className, defaultBreadcrumb, ...restProps } = props;
  const breadcrumbClassName = clsx('breadcrumb', className);
  const [selected, setSelected] = useState<BreadCrumbItem>(defaultBreadcrumb || breadcrumbs[0]);

  const handleOnSelected = useCallback(
    (data: BreadCrumbItem) => {
      onSelected(data);
      setSelected(data);
    },
    [onSelected],
  );

  return (
    <ol className={breadcrumbClassName}>
      {breadcrumbs?.map((item: BreadCrumbItem, index: number) => (
        <BreadcrumbItem
          key={item.id}
          className={item.className}
          data={item}
          onSelected={handleOnSelected}
          selected={selected}
          isLastItem={index + 1 === breadcrumbs.length}
          {...restProps}
        />
      ))}
    </ol>
  );
};

export default Breadcrumb;
