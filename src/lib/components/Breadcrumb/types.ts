export interface BreadCrumbItem {
  id: string;
  url: string;
  name: string;
  className?: string;
}
export interface BreadcrumbProps {
  breadcrumbs: BreadCrumbItem[];
  onSelected: (selected: BreadCrumbItem) => void;
  defaultBreadcrumb?: BreadCrumbItem;
  className?: string;
}

export interface BreadcrumbItemsProps {
  data: BreadCrumbItem;
  onSelected: (selected: BreadCrumbItem) => void;
  selected: BreadCrumbItem;
  className?: string;
  isLastItem: boolean;
}
