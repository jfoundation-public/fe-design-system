import clsx from 'clsx';

import { Icon } from '@/lib';

import { BreadcrumbItemsProps } from './types';
const BreadcrumbItem = ({
  data,
  className,
  onSelected,
  // selected,
  isLastItem,
  ...restProps
}: BreadcrumbItemsProps) => {
  // const isSelected = selected.url === data.url;
  const _className = clsx(
    'breadcrumb-item',
    {
      active: isLastItem,
    },
    className,
  );

  return (
    <li {...restProps} role='presentation' onClick={() => onSelected(data)} className={_className}>
      <span className='breadcrumb-item-name'>{data.name}</span>
      {!isLastItem && (
        <Icon className='icon-color-default' variant='outline' iconName='right' size={12} />
      )}
    </li>
  );
};

export default BreadcrumbItem;
