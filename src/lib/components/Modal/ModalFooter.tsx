import clsx from 'clsx';

import { getModalFooterClasses } from './helper';
import { ModalFooterProps } from './types';

const classes = getModalFooterClasses();

export default function ModalFooter(props: ModalFooterProps) {
  const { className, ...restProps } = props;

  return <div className={clsx(classes, className)} {...restProps} />;
}
