import clsx from 'clsx';

import { ConfirmVariant, ModalSize } from './types';

const SIZE_CLASSES = {
  sm: 'modal-paper-sm',
  md: 'modal-paper-md',
  lg: 'modal-paper-lg',
  xl: 'modal-paper-xl',
};

function getModalOverlayClasses() {
  return 'modal-overlay';
}

function getModalPaperClasses({ size }: { size: ModalSize }) {
  return clsx('modal-paper', SIZE_CLASSES[size]);
}

function getModalHeaderClasses() {
  return 'modal-header';
}

function getLeftHeaderClasses() {
  return 'flex items-center gap-x-2';
}

const ICONS_BY_VARIANT = {
  information: {
    iconName: 'information',
    color: 'information',
  },
  success: {
    iconName: 'success',
    color: 'success',
  },
  warning: {
    iconName: 'warning',
    color: 'warning',
  },
  error: {
    iconName: 'failed-remove',
    color: 'error',
  },
  delete: {
    iconName: 'delete',
    color: 'error',
  },
} as const;

const TITLE_HEADER_CLASSES_BY_VARIANT = {
  information: 'text-text-information',
  success: 'text-text-success',
  warning: 'text-text-warning',
  error: 'text-text-error',
  delete: 'text-text-error',
} as const;

function getHeaderIconByVariant(variant: ConfirmVariant) {
  return ICONS_BY_VARIANT[variant];
}

function getTitleHeaderClasses(variant: ConfirmVariant) {
  return TITLE_HEADER_CLASSES_BY_VARIANT[variant];
}

function getModalBodyClasses() {
  return 'modal-body';
}

function getModalFooterClasses() {
  return 'modal-footer';
}

export {
  getHeaderIconByVariant,
  getLeftHeaderClasses,
  getModalBodyClasses,
  getModalFooterClasses,
  getModalHeaderClasses,
  getModalOverlayClasses,
  getModalPaperClasses,
  getTitleHeaderClasses,
};
