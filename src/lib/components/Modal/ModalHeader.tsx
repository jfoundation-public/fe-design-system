import { IconButton } from '../Button';
import { getLeftHeaderClasses, getModalHeaderClasses } from './helper';
import { ModalHeaderProps } from './types';

export default function ModalHeader(props: ModalHeaderProps) {
  const { icon, title, description, status, onClose, ...restProps } = props;
  const classes = getModalHeaderClasses();
  const leftHeaderClasses = getLeftHeaderClasses();

  return (
    <div className={classes} {...restProps}>
      <div className={leftHeaderClasses}>
        {icon}
        {title}
        <span className='text-text-400'>{description}</span>
        {status}
      </div>
      <IconButton
        type='button'
        onClick={onClose}
        isAdornment
        iconName='close-small'
        iconVariant='outline'
      />
    </div>
  );
}
