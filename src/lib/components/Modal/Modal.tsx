import {
  FloatingFocusManager,
  FloatingOverlay,
  FloatingPortal,
  useClick,
  useDismiss,
  useFloating,
  useInteractions,
  useRole,
} from '@floating-ui/react-dom-interactions';
import clsx from 'clsx';

import { getModalOverlayClasses, getModalPaperClasses } from './helper';
import { ModalProps } from './types';

export default function Modal(props: ModalProps) {
  const { open, onClose, size = 'md', children, className, ...restProps } = props;

  const { floating, context } = useFloating({
    open,
    onOpenChange: onClose,
  });

  const { getFloatingProps } = useInteractions([
    useClick(context),
    useRole(context),
    useDismiss(context),
  ]);

  const overlayClasses = getModalOverlayClasses();
  const paperClasses = getModalPaperClasses({ size });

  return (
    <FloatingPortal id='portal-root'>
      {open && (
        <FloatingOverlay lockScroll className={overlayClasses} style={{ overflow: 'hidden' }}>
          <FloatingFocusManager context={context}>
            <div ref={floating} {...getFloatingProps()}>
              <div className={clsx(paperClasses, className)} {...restProps}>
                {children}
              </div>
            </div>
          </FloatingFocusManager>
        </FloatingOverlay>
      )}
    </FloatingPortal>
  );
}
