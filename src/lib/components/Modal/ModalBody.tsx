import clsx from 'clsx';

import { getModalBodyClasses } from './helper';
import { ModalBodyProps } from './types';

const classes = getModalBodyClasses();

export default function ModalBody(props: ModalBodyProps) {
  const { className, ...restProps } = props;

  return <div className={clsx(classes, className)} {...restProps} />;
}
