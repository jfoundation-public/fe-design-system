import { useMemo } from 'react';

import { isString } from '@/lib/utils';

import { Icon } from '../Icon';
import { getHeaderIconByVariant, getTitleHeaderClasses } from './helper';
import ModalHeader from './ModalHeader';
import { ModalConfirmHeaderProps } from './types';

export default function ModalConfirmHeader(props: ModalConfirmHeaderProps) {
  const { icon, variant, title, ...restProps } = props;

  const iconProps = useMemo(() => getHeaderIconByVariant(variant), [variant]);
  const titleClasses = useMemo(() => getTitleHeaderClasses(variant), [variant]);

  return (
    <ModalHeader
      icon={icon ?? <Icon size={20} className={titleClasses} {...iconProps} />}
      title={isString(title) ? <h5 className={titleClasses}>{title}</h5> : title}
      {...restProps}
    />
  );
}
