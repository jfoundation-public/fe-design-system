import { ReactElement } from 'react';
export interface SidebarLogoProps {
  src?: string;
  logo?: () => ReactElement;
  className?: string;
  onClick: () => void;
}
const SidebarLogo = (props: SidebarLogoProps) => {
  const { logo, onClick: handleLogoClick } = props;
  return (
    <div role='presentation' onClick={handleLogoClick} className='side-bar-item side-bar-logo'>
      {logo ? logo() : <img src={'/favicon.svg'} alt='app-logo' />}
    </div>
  );
};

export default SidebarLogo;
