import clsx from 'clsx';
import { useMemo } from 'react';

import { Icon } from '@/lib';

import { SidebarItemProps } from './type';

const SidebarItem = (props: SidebarItemProps) => {
  const { data, onClickSideBar, selectedSideBar } = props;
  const { icon, iconClassName, iconSize = 24 } = data;

  const isSelected = useMemo(() => selectedSideBar?.icon === icon, [icon, selectedSideBar]);
  const iconColor = isSelected ? 'text-primary' : 'text-lineBorder-borderDefault';

  const _classNames = clsx('side-bar-item', {
    active: isSelected,
    iconClassName,
  });

  return (
    <div role='presentation' onClick={() => onClickSideBar(data)} className={_classNames}>
      <Icon iconName={icon} variant='solid' className={iconColor} size={iconSize} />
    </div>
  );
};

export default SidebarItem;
