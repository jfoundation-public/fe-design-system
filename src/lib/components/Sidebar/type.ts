import { ReactElement } from 'react';

import { IconName } from '../Icon/types';

export type SelectSubMenuType<T = unknown> = {
  id: string;
  name: string;
  url: string;
  startAdornment?: ReactElement;
  endAdornment?: ReactElement;
  className?: string;
} & T;

export interface TabSubMenuProps {
  id: string;
  title: string;
  listSubMenu: SelectSubMenuType[];
}
export interface TabMenuProps {
  id: string;
  tabTitle: string;
  listItem?: SelectSubMenuType[];
  tabSubMenu?: TabSubMenuProps[];
}

export interface SidebarList {
  icon: IconName;
  tabMenu?: TabMenuProps[];
  iconClassName?: string;
  iconSize?: number;
  title: string;
  id: string;
  listItems?: SelectSubMenuType[];
}

export interface SidebarProps {
  sidebars: SidebarList[];
  logo?: () => ReactElement;
  defaultIndexTab?: number;
  selectedSideBar: SidebarList | undefined;
  expanded: boolean;
  onExpand: () => void;
  onLogoClick?: () => void;
  onClickMenu: (data: SelectSubMenuType) => void;
  onClickSideBar: (sidebar: SidebarList) => void;
  className?: string;
  defaultSidebarMenuItem?: SelectSubMenuType;
}

export interface SidebarItemProps {
  data: SidebarList;
  onClickSideBar: (sidebar: SidebarList) => void;
  selectedSideBar?: SidebarList;
}
