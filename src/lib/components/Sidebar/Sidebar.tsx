import clsx from 'clsx';
import { useCallback, useEffect, useMemo, useState } from 'react';

import { Icon, isArray, isEmpty, isUndefined, noop, Tabs } from '@/lib';

import { Menu } from '../Menu';
import MenuItem from '../Menu/MenuItem';
import SidebarItem from './SidebarItem';
import SidebarLogo from './SidebarLogo';
import {
  SelectSubMenuType,
  SidebarList,
  SidebarProps,
  TabMenuProps,
  TabSubMenuProps,
} from './type';
const DEFAULT_SELECT_MENU = {
  id: '',
  name: '',
  url: '',
};
const Sidebar = (props: SidebarProps) => {
  const {
    sidebars,
    className,
    onClickSideBar,
    onClickMenu,
    selectedSideBar,
    logo,
    onExpand,
    expanded,
    defaultIndexTab = 0,
    onLogoClick = noop,
    defaultSidebarMenuItem,
  } = props;

  const modifiedTab = (selectedSideBar?.tabMenu || []).map((item: TabMenuProps) => ({
    title: item.tabTitle,
    tabId: item.id,
    listItem: item.listItem,
    tabSubMenu: item.tabSubMenu,
  }));

  const listItems = selectedSideBar?.listItems || [];

  const defaultTab = useMemo(() => {
    return modifiedTab[defaultIndexTab]?.tabId || '';
  }, [modifiedTab, defaultIndexTab]);

  const defaultSelectMenu = useMemo(() => {
    return (
      modifiedTab[0]?.listItem?.[0] ||
      modifiedTab[0]?.tabSubMenu?.[0]?.listSubMenu?.[0] ||
      defaultSidebarMenuItem
    );
  }, [modifiedTab]);

  const [selectedTab, setSelectedTab] = useState<Record<string, any>>({});
  const [isHover] = useState<boolean>(true);
  const [selectedMenu, setSelectedMenu] = useState<SelectSubMenuType>(
    defaultSidebarMenuItem ?? DEFAULT_SELECT_MENU,
  );
  const selectedSidebarId = selectedSideBar?.id || '';

  const selectedSubMenuByTab = modifiedTab.find(
    (item: any) => item.tabId === selectedTab[selectedSidebarId],
  );

  const handleClickMenu = useCallback(
    (item: SelectSubMenuType) => {
      setSelectedTab({ [selectedSidebarId]: selectedTab[selectedSidebarId] });
      setSelectedMenu(item);
      onClickMenu(item);
    },
    [setSelectedMenu, onClickMenu, setSelectedTab, selectedTab, selectedSidebarId],
  );

  const handleClickIcon = useCallback(
    (data: SidebarList) => {
      onClickSideBar && onClickSideBar(data);
    },
    [onClickSideBar],
  );

  useEffect(() => {
    if (selectedSideBar && isUndefined(selectedTab[selectedSidebarId])) {
      setSelectedTab((prev) => ({ ...prev, [selectedSidebarId]: defaultTab }));
    }
  }, [selectedSideBar, defaultSelectMenu, defaultTab, selectedTab, selectedSidebarId]);

  return (
    <div
      // onMouseEnter={() => setIsHover(true)}
      // onMouseLeave={() => setIsHover(false)}
      className={clsx('side-bar-wrapper')}
    >
      <div
        className={clsx(
          'side-bar gap-2',
          expanded ? 'sidebar-with-submenu' : 'sidebar-without-submenu',
          className,
        )}
      >
        <SidebarLogo onClick={onLogoClick} logo={logo} />

        {sidebars.map((sidebar: SidebarList) => (
          <SidebarItem
            key={sidebar.icon}
            data={sidebar}
            onClickSideBar={handleClickIcon}
            selectedSideBar={selectedSideBar}
          />
        ))}
      </div>

      <div className={expanded ? '' : 'hidden'}>
        {expanded && <div className='sidebar-submenu-title'>{selectedSideBar?.title}</div>}

        {!isEmpty(modifiedTab) && expanded && (
          <div className='side-bar-tab'>
            <Tabs
              className='gap-0 capitalize pl-0 pr-0'
              tabClassName='border-b-2 justify-center w-full min-w-[125px]'
              tabs={modifiedTab}
              onSelect={(id: string) =>
                setSelectedTab((prev) => ({ ...prev, [selectedSidebarId]: id }))
              }
              selected={selectedTab[selectedSidebarId]}
            />
          </div>
        )}

        <div
          id='side-bar-list'
          className='max-h-[calc(100vh-70px)] overflow-y-auto scroll-bar min-w-[250px]'
        >
          {expanded &&
          !isUndefined(selectedSubMenuByTab) &&
          isArray(selectedSubMenuByTab.tabSubMenu)
            ? selectedSubMenuByTab.tabSubMenu.map((subMenu: TabSubMenuProps) => (
                <Menu
                  key={subMenu.id}
                  data={subMenu}
                  selectedMenu={selectedMenu}
                  onClickMenu={handleClickMenu}
                />
              ))
            : expanded &&
              isArray(selectedSubMenuByTab?.listItem) &&
              selectedSubMenuByTab?.listItem.map((item: SelectSubMenuType) => (
                <MenuItem
                  key={item.id}
                  selectedMenu={selectedMenu}
                  onClickMenu={handleClickMenu}
                  item={item}
                  isFlat={true}
                  className={item.className}
                />
              ))}
          {expanded &&
            !isEmpty(listItems) &&
            listItems?.map((item: SelectSubMenuType) => (
              <MenuItem
                key={item.id}
                selectedMenu={selectedMenu}
                onClickMenu={handleClickMenu}
                item={item}
                isFlat={true}
                className={item.className}
              />
            ))}
        </div>
      </div>

      <div
        role='presentation'
        className={clsx(
          !expanded && 'sidebar-submenu-button-collapse',
          expanded && 'sidebar-submenu-button-expand',
          isUndefined(selectedSideBar) && 'pointer-events-none',
          isHover && 'visible',
          !isHover && 'invisible',
        )}
      >
        <Icon
          onClick={onExpand}
          className={clsx(
            'sidebar-submenu-button-icon',
            expanded ? 'right-[-12px]' : 'right-[-28px]',
          )}
          iconName={expanded ? 'leftWithCircle' : 'rightWithCircle'}
          // iconName='rightWithCircle'
          variant='outline'
        />
      </div>
    </div>
  );
};

export default Sidebar;
