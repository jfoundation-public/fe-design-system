import { FieldControlled } from '../FieldControlled';
import ComboboxBase from './ComboboxBase';
import { ComboboxProps } from './types';

export default function Combobox<T extends { id: string } = { id: string }>(
  props: ComboboxProps<T>,
) {
  const {
    label,
    required,
    labelIcon,
    helperText,
    startAdornment,
    endAdornment,
    isWarning,
    isSuccess,
    isError,
    Container,
    ...restProps
  } = props;

  return (
    <FieldControlled
      label={label}
      required={required}
      labelIcon={labelIcon}
      helperText={helperText}
      endAdornment={endAdornment}
      startAdornment={startAdornment}
      isWarning={isWarning}
      isSuccess={isSuccess}
      isError={isError}
      {...Container}
    >
      <ComboboxBase {...restProps} />
    </FieldControlled>
  );
}
