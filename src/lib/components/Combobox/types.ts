import {
  Dispatch,
  FocusEventHandler,
  ReactElement,
  ReactEventHandler,
  SetStateAction,
} from 'react';

import { FieldControlledProps } from '../FieldControlled';

type ID = string;

export interface BaseOption {
  id: ID;
}

export interface ComboboxBaseProps<OPTION extends BaseOption> {
  renderOption: (props: {
    option: OPTION;
    onSelected: (id: ID) => void;
    isSelected: boolean;
  }) => ReactElement;
  renderInput?: (props: { onOpenChange: Dispatch<SetStateAction<boolean>> }) => ReactElement;
  noOptionsRender?: ReactElement;
  inputValue?: string;
  onInputChange?: ReactEventHandler<HTMLInputElement>;
  options: OPTION[];
  moreSelectItem?: (props: { onClose: () => void }) => ReactElement;
  value: string;
  name: string;
  placeholder?: string;
  onChange?: ReactEventHandler<HTMLInputElement>;
  onBlur?: FocusEventHandler<HTMLInputElement>;
  onFocus?: FocusEventHandler<HTMLInputElement>;
  disabled?: boolean;
  readOnly?: boolean;
}

export type ComboboxProps<OPTION extends BaseOption> = FieldControlledProps &
  ComboboxBaseProps<OPTION> & {
    name: string;
    placeholder?: string;
    readOnly?: boolean;
    disabled?: boolean;
    required?: boolean;
    startAdornment?: ReactElement;
    endAdornment?: ReactElement;
  };
