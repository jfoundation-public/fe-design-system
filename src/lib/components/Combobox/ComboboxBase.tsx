import {
  autoUpdate,
  flip,
  FloatingOverlay,
  FloatingPortal,
  offset,
  size,
  useDismiss,
  useFloating,
  useInteractions,
  useListNavigation,
  useRole,
} from '@floating-ui/react-dom-interactions';
import { useEffect, useLayoutEffect, useRef, useState } from 'react';

import { createCustomInputEvent, isEqual, isNil } from '@/lib/utils';

import { IconButton } from '../Button';
import { withControlled } from '../FieldControlled';
import { useFieldControlElement } from '../FieldInput/hooks';
import { InputBase } from '../InputBase';
import { ComboboxBaseProps } from './types';

function ComboboxBase<T extends { id: string } = { id: string }>(props: ComboboxBaseProps<T>) {
  const {
    value,
    name,
    onChange,
    onFocus,
    onBlur,
    placeholder,
    options,
    renderOption,
    renderInput,
    noOptionsRender,
    moreSelectItem: MoreSelectItem,
    inputValue: inputValueProp,
    onInputChange: onInputChangeProp,
    disabled,
    readOnly,
  } = props;
  const prevValue = useRef(value);
  const prevCurrentValue = useRef(value ?? '');

  const containerElement = useFieldControlElement();
  const [currentValue, setCurrentValue] = useState(value);
  const [open, setOpen] = useState(false);
  const [inputValue, setInputValue] = useState(inputValueProp ?? '');
  const [activeIndex, setActiveIndex] = useState<number | null>(null);

  const listRef = useRef<Array<HTMLElement | null>>([]);

  const { x, y, reference, floating, strategy, context } = useFloating({
    whileElementsMounted: autoUpdate,
    open,
    onOpenChange: setOpen,
    placement: 'bottom-start',
    middleware: [
      offset(5),
      flip({ padding: 8 }),
      size({
        apply({ rects, availableHeight, elements }) {
          Object.assign(elements.floating.style, {
            width: `${rects.reference.width}px`,
            maxHeight: `${availableHeight}px`,
          });
        },
        padding: 8,
      }),
    ],
  });

  const { getReferenceProps, getFloatingProps } = useInteractions([
    useRole(context, { role: 'listbox' }),
    useDismiss(context),
    useListNavigation(context, {
      listRef,
      activeIndex,
      onNavigate: setActiveIndex,
      virtual: true,
      loop: true,
    }),
  ]);

  function handleSelect(_id: string) {
    setCurrentValue(_id);
    setOpen(false);
  }

  function handleInputChange(event: React.ChangeEvent<HTMLInputElement>) {
    const value = event.target.value;
    setInputValue(value);
  }

  function handleClear() {
    setInputValue('');
    setCurrentValue('');
  }

  function handleFocus(e: React.FocusEvent<HTMLInputElement>) {
    onFocus && onFocus(e);
    !disabled && !readOnly && setOpen(true);
  }

  function handleBlur(e: React.FocusEvent<HTMLInputElement>) {
    onBlur && onBlur(e);
  }

  useLayoutEffect(() => {
    // IMPORTANT: When the floating element first opens, this runs when the
    // styles have **not yet** been applied to the element. This can cause an
    // infinite loop as `size` has not yet limited the maxHeight, so the whole
    // page tries to scroll. We must wrap it in rAF.
    requestAnimationFrame(() => {
      if (activeIndex != null) {
        listRef.current[activeIndex]?.scrollIntoView({ block: 'nearest' });
      }
    });
  }, [activeIndex]);

  useEffect(() => {
    if (containerElement) {
      reference({
        getBoundingClientRect: () => containerElement!.getBoundingClientRect(),
        contextElement: containerElement!,
      });
    }
  }, [containerElement, reference]);

  useEffect(() => {
    if (!isEqual(prevCurrentValue.current, currentValue)) {
      prevCurrentValue.current = currentValue;
      onChange && onChange(createCustomInputEvent({ name, value: currentValue }));
    }
  }, [currentValue, name, onChange]);

  useEffect(() => {
    if (!isNil(inputValueProp)) {
      setInputValue(inputValueProp);
    }
  }, [inputValueProp]);

  useEffect(() => {
    if (onInputChangeProp) {
      onInputChangeProp(createCustomInputEvent({ name: `${name}-input`, value: inputValue }));
    }
  }, [inputValue, name, onInputChangeProp]);

  (function syncValue() {
    if (!isEqual(prevValue.current, value)) {
      prevValue.current = value;
      setCurrentValue(value ?? '');
    }
  })();

  const showClear = Boolean(!disabled && !readOnly && (inputValue || currentValue?.length));

  return (
    <>
      {renderInput ? (
        renderInput({
          onOpenChange: setOpen,
        })
      ) : (
        <InputBase
          {...getReferenceProps({
            ref: reference,
            onChange: handleInputChange,
            value: inputValue,
            placeholder,
            'aria-autocomplete': 'list',
            onKeyDown(event) {
              if (event.key === 'Tab') {
                setOpen(false);
              }
            },
          })}
          onFocus={handleFocus}
          onBlur={handleBlur}
          disabled={disabled}
          readOnly={readOnly}
          endAdornment={
            showClear ? (
              <IconButton
                type='button'
                isAdornment
                onMouseDown={(e) => {
                  e.preventDefault();
                  e.stopPropagation();
                }}
                onMouseUp={(e) => {
                  e.preventDefault();
                  e.stopPropagation();
                  handleClear();
                }}
                iconName='failed-remove'
              />
            ) : undefined
          }
        />
      )}

      <FloatingPortal id='portal-root'>
        {open && (
          <FloatingOverlay className='z-picker' lockScroll>
            <div
              {...getFloatingProps({
                ref: floating,
                style: {
                  position: strategy,
                  left: x ?? 0,
                  top: y ?? 0,
                  overflow: 'auto',
                },
              })}
            >
              <div className='paper'>
                {options && options.length
                  ? options.map((opt) =>
                      renderOption({
                        option: opt,
                        onSelected: handleSelect,
                        isSelected: currentValue === opt.id,
                      }),
                    )
                  : noOptionsRender || null}
                {MoreSelectItem && (
                  <MoreSelectItem
                    onClose={() => {
                      setOpen(false);
                    }}
                  />
                )}
              </div>
            </div>
          </FloatingOverlay>
        )}
      </FloatingPortal>
    </>
  );
}

export default withControlled<string | undefined, ComboboxBaseProps<any>>(ComboboxBase);
