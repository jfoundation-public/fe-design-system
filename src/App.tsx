import AccordionDemo from './demos/Accordion';
import AlertDemo from './demos/Alert';
import AutocompleteDemo from './demos/Autocomplete';
import AvatarDemo from './demos/Avatar';
import BadgeDemos from './demos/Badge';
import BreadcrumbDemo from './demos/BreadCrumb';
import ButtonDemo from './demos/Button';
import CheckboxDemo from './demos/Checkbox';
import ChipsDemo from './demos/Chips';
import ComboboxDemo from './demos/Combobox';
import CountInputDemo from './demos/CountInput';
import DataGridDemo from './demos/DataGrid';
import DatePickerDemo from './demos/DatePicker';
import InputRangeDemo from './demos/InputRange';
import ModalDemo from './demos/Modal';
import NotificationDemo from './demos/Notification';
import OTPInputDemo from './demos/OTPInput';
import PopoverDemo from './demos/Popover';
import PreviewImageDemo from './demos/PreviewImage';
import ProgressDemo from './demos/Progress';
import RadioDemo from './demos/Radio';
import SchedulesDemo from './demos/Schedules';
import SearchDemo from './demos/Search';
import SelectDemo from './demos/Select';
import SideBarDemo from './demos/Sidebar';
import SliderDemo from './demos/Slider';
import StatusDemo from './demos/Status';
import StepDemo from './demos/Step';
import TabDemo from './demos/Tab';
import TextAreaDemo from './demos/TextArea';
import TextFieldDemo from './demos/TextField';
import TimelineDemo from './demos/Timeline';
import ToggleDemo from './demos/Toggle';
import TooltipDemo from './demos/Tooltip';
import TransferDemo from './demos/Transfer';
import TreeSelectDemo from './demos/TreeSelect';
import TreeViewDemo from './demos/TreeView';
import TypographyDemo from './demos/Typography';
import UnstableUploadDemo from './demos/UnstableUpload';

function App() {
  return (
    <>
      <div className='flex fixed w-screen'>
        <SideBarDemo />
        <div className='p-6 max-h-screen min-h-screen w-screen overflow-auto scroll-bar'>
          <TimelineDemo />
          <UnstableUploadDemo />
          <TransferDemo />
          <AccordionDemo />
          <TreeSelectDemo />
          <SchedulesDemo />
          <InputRangeDemo />
          <CountInputDemo />
          <SliderDemo />
          <ComboboxDemo />
          <ProgressDemo />
          <PreviewImageDemo />
          <OTPInputDemo />
          <BreadcrumbDemo />
          <StepDemo />
          <TooltipDemo />
          <NotificationDemo />
          <AlertDemo />
          <TreeViewDemo />
          <AvatarDemo />
          <BadgeDemos />
          <TabDemo />
          <TextAreaDemo />
          <DataGridDemo />
          <DatePickerDemo />
          <AutocompleteDemo />
          <SelectDemo />
          <RadioDemo />
          <CheckboxDemo />
          <ToggleDemo />
          <ModalDemo />
          <StatusDemo />
          <PopoverDemo />
          <ChipsDemo />
          <SearchDemo />
          <TextFieldDemo />
          <TypographyDemo />
          <ButtonDemo />
        </div>
      </div>
    </>
  );
}

export default App;
