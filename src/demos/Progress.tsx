import { Progress } from '@/lib';

export default function ProgressDemo() {
  return (
    <div className='p-8'>
      <h3>Demo Progress</h3>
      <div className='mt-4 grid grid-cols-4 gap-4 justify-items-center'>
        <Progress percent={20} />
        <Progress percent={100} />
        <Progress percent={50} isError />
        <Progress percent={80} hideInfo />
      </div>
      <div className='mt-4 grid grid-cols-4 gap-4 justify-items-center'>
        <Progress type='circle' percent={20} />
        <Progress type='circle' percent={100} />
        <Progress type='circle' percent={50} isError />
        <Progress type='circle' percent={80} hideInfo />
      </div>
    </div>
  );
}
