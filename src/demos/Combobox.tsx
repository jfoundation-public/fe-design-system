import clsx from 'clsx';
import { useFormik } from 'formik';

import { Combobox, PreviewImage } from '@/lib';

import provinces from './province-options';
import SelectItemCreate from './shared/SelectItemCreate';

const options = provinces.map((p) => ({
  ...p,
  id: p.value,
  plus: Math.floor(Math.random() * 1000),
}));

export default function ComboboxDemo() {
  const { values, handleChange, setFieldValue } = useFormik({
    initialValues: { province: '', 'province-input': '' },
    onSubmit: console.log,
  });

  const optionsFilter = options.filter((opt) =>
    opt.label.toLocaleLowerCase().includes(values['province-input'].toLocaleLowerCase()),
  );

  const provinceSelected = options.find((opt) => opt.value === values.province);
  return (
    <div className='m-8'>
      <h3>Demo Combobox</h3>
      <Combobox
        label='label demo'
        required
        name='province'
        options={options}
        value={values.province}
        onChange={handleChange}
        renderInput={({ onOpenChange }) => (
          <button
            type='button'
            onClick={() => {
              onOpenChange((p) => !p);
            }}
          >
            {provinceSelected ? (
              <div className='flex gap-3'>
                <PreviewImage
                  width={24}
                  src='https://res.cloudinary.com/crunchbase-production/image/upload/c_lpad,h_170,w_170,f_auto,b_white,q_auto:eco,dpr_1/v1463602429/hseqthtzwhrd9hatu9be.png'
                />
                {provinceSelected?.label} - {provinceSelected?.plus}
              </div>
            ) : (
              'Select option'
            )}
          </button>
        )}
        renderOption={({ option, onSelected, isSelected }) => (
          <div
            key={option.id}
            className={clsx(isSelected && 'text-text-error')}
            onClick={() => {
              onSelected(option.id);
              setFieldValue('province-input', option.label);
            }}
            role='presentation'
          >
            <h5>{option.label}</h5>
            <p>{option.plus}</p>
          </div>
        )}
        moreSelectItem={({ onClose }) => (
          <SelectItemCreate onClose={onClose} onSelect={() => console.log('>>>>>')} />
        )}
      />
      <Combobox
        label='label demo'
        name='province'
        options={optionsFilter}
        value={values.province}
        onChange={handleChange}
        inputValue={values['province-input']}
        onInputChange={handleChange}
        renderOption={({ option, onSelected, isSelected }) => (
          <div
            key={option.id}
            className={clsx(isSelected && 'text-text-error')}
            onClick={() => {
              onSelected(option.id);
              setFieldValue('province-input', option.label);
            }}
            role='presentation'
          >
            <h5>{option.label}</h5>
            <p>{option.plus}</p>
          </div>
        )}
        moreSelectItem={({ onClose }) => (
          <SelectItemCreate onClose={onClose} onSelect={() => console.log('>>>>>')} />
        )}
        disabled
      />

      <Combobox
        Container={{ className: 'mt-4' }}
        label='label demo'
        name='province'
        options={optionsFilter}
        value={values.province}
        onChange={handleChange}
        inputValue={values['province-input']}
        onInputChange={handleChange}
        renderOption={({ option, onSelected, isSelected }) => (
          <div
            key={option.id}
            className={clsx(isSelected && 'text-text-error')}
            onClick={() => {
              onSelected(option.id);
              setFieldValue('province-input', option.label);
            }}
            role='presentation'
          >
            <h5>{option.label}</h5>
            <p>{option.plus}</p>
          </div>
        )}
        isError
        helperText='Error'
        readOnly
      />
    </div>
  );
}
