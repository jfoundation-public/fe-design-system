import { Alert, Button } from '@/lib';

export default function AlertDemo() {
  return (
    <div className='p-8'>
      <h3 className='mb-4'>Demo Alert</h3>
      <Alert title='Demo alert' iconName='information' iconClasses='text-text-information' />
      <Alert
        title='Demo alert'
        action={
          <Button size='sm' variant='blank'>
            Undo
          </Button>
        }
        iconName='failed-remove'
        iconClasses='text-text-error'
      />
      <Alert
        title='This is content for some inforation !'
        iconName='warning'
        iconClasses='text-text-warning'
      />
      <Alert
        title='This is content for some inforation !'
        iconName='delete'
        iconClasses='text-text-error'
      >
        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Unde eaque voluptate mollitia ab
        aspernatur eos totam laudantium natus, pariatur nostrum doloremque excepturi odio ea
        expedita similique quod soluta error a.
      </Alert>
    </div>
  );
}
