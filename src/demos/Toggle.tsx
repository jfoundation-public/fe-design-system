import { useToggle } from '@/lib';
import { Toggle } from '@/lib/components/Toggle';

export default function ToggleDemo() {
  const [checked, onToggle] = useToggle();
  return (
    <div className='p-8'>
      <h3>Demo Toggle</h3>
      <Toggle className='block mt-4' checked={checked} onChange={onToggle}>
        Controlled
      </Toggle>
    </div>
  );
}
