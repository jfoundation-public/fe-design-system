import { useState } from 'react';

import { Accordion } from '@/lib';

const AccordionDemo = () => {
  const [expand, setExpand] = useState<Record<string, boolean>>({});

  const handleExpand = (id: string) => {
    setExpand((prev) => ({
      [id]: !prev[id],
    }));
  };

  const handleExpandMulti = (id: string) => {
    setExpand((prev) => ({ ...prev, [id]: !prev[id] }));
  };

  return (
    <div className='p-8'>
      <h3 className='mb-4'>Demo Accordion</h3>
      <h4 className='m-4'>Expand Single Item</h4>
      <Accordion>
        <Accordion.Item
          title='Today - Sep 09, 2022'
          onExpand={handleExpand}
          accordionId='1'
          expand={expand['1']}
          hasBorderHeader
        >
          Lorem ipsum, dolor sit amet consectetur adipisicing elit. Unde praesentium provident minus
          optio distinctio asperiores quisquam modi voluptas quis tenetur iste ad, facilis numquam
          sequi pariatur dicta inventore odit placeat.
        </Accordion.Item>
        <Accordion.Item
          title='Today - Sep 10, 2022'
          onExpand={handleExpand}
          accordionId='2'
          expand={expand['2']}
          hasBorderHeader
        >
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto adipisci repellendus est
          impedit doloribus aspernatur rem obcaecati? Aliquid ab sequi modi, quod doloribus adipisci
          quisquam, atque aliquam, minima quaerat veritatis.
        </Accordion.Item>
        <Accordion.Item
          title='Today - Sep 11, 2022'
          onExpand={handleExpand}
          accordionId='3'
          expand={expand['3']}
          hasBorderHeader
        >
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Exercitationem magni delectus
          laudantium? Repellat at repudiandae architecto! Suscipit, earum? Nostrum alias officiis
          facilis temporibus beatae asperiores magnam eos sunt nulla illo?
        </Accordion.Item>
        <Accordion.Item
          title='Today - Sep 12, 2022'
          onExpand={handleExpand}
          accordionId='4'
          expand={expand['4']}
          hasBorderHeader
        >
          Lorem ipsum dolor sit amet consectetur, adipisicing elit. At est, sed cupiditate omnis
          atque libero eligendi architecto possimus earum natus vero fugiat unde hic saepe corrupti,
          dolorem enim molestiae blanditiis.
        </Accordion.Item>
      </Accordion>
      <h4 className='m-4'>Expand Multi Item</h4>
      <Accordion>
        <Accordion.Item
          title='Today - Sep 13, 2022'
          onExpand={handleExpandMulti}
          accordionId='example1'
          expand={expand['example1']}
          hasBorderHeader
        >
          Lorem ipsum, dolor sit amet consectetur adipisicing elit. Unde praesentium provident minus
          optio distinctio asperiores quisquam modi voluptas quis tenetur iste ad, facilis numquam
          sequi pariatur dicta inventore odit placeat.
        </Accordion.Item>
        <Accordion.Item
          title='Today - Sep 14, 2022'
          onExpand={handleExpandMulti}
          accordionId='example2'
          expand={expand['example2']}
          hasBorderHeader
        >
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto adipisci repellendus est
          impedit doloribus aspernatur rem obcaecati? Aliquid ab sequi modi, quod doloribus adipisci
          quisquam, atque aliquam, minima quaerat veritatis.
        </Accordion.Item>
        <Accordion.Item
          title='Today - Sep 15, 2022'
          onExpand={handleExpandMulti}
          accordionId='example3'
          expand={expand['example3']}
          hasBorderHeader
        >
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Exercitationem magni delectus
          laudantium? Repellat at repudiandae architecto! Suscipit, earum? Nostrum alias officiis
          facilis temporibus beatae asperiores magnam eos sunt nulla illo?
        </Accordion.Item>
        <Accordion.Item
          title='Today - Sep 16, 2022'
          onExpand={handleExpandMulti}
          accordionId='example4'
          expand={expand['example4']}
          hasBorderHeader
        >
          Lorem ipsum dolor sit amet consectetur, adipisicing elit. At est, sed cupiditate omnis
          atque libero eligendi architecto possimus earum natus vero fugiat unde hic saepe corrupti,
          dolorem enim molestiae blanditiis.
        </Accordion.Item>
      </Accordion>
      <h4 className='m-4'>Expand With Custom Icon</h4>
      <Accordion>
        <Accordion.Item
          title='Today - Sep 13, 2022'
          onExpand={handleExpandMulti}
          accordionId='example1'
          expand={expand['example1']}
          hasBorderHeader
          collapseIcon={
            <svg
              width='24'
              height='24'
              viewBox='0 0 24 24'
              fill='none'
              xmlns='http://www.w3.org/2000/svg'
            >
              <path
                d='M17.5 14.75L12.0707 9.32071C12.0317 9.28166 11.9683 9.28166 11.9293 9.32071L6.5 14.75'
                stroke='#9A9DB2'
                strokeWidth='1.5'
                strokeLinecap='round'
              />
            </svg>
          }
          expandIcon={
            <svg
              width='24'
              height='24'
              viewBox='0 0 24 24'
              fill='none'
              xmlns='http://www.w3.org/2000/svg'
              className='rotate-180'
            >
              <path
                d='M17.5 14.75L12.0707 9.32071C12.0317 9.28166 11.9683 9.28166 11.9293 9.32071L6.5 14.75'
                stroke='#9A9DB2'
                strokeWidth='1.5'
                strokeLinecap='round'
              />
            </svg>
          }
        >
          Lorem ipsum, dolor sit amet consectetur adipisicing elit. Unde praesentium provident minus
          optio distinctio asperiores quisquam modi voluptas quis tenetur iste ad, facilis numquam
          sequi pariatur dicta inventore odit placeat.
        </Accordion.Item>
        <Accordion.Item
          title='Today - Sep 14, 2022'
          onExpand={handleExpandMulti}
          accordionId='example2'
          expand={expand['example2']}
          hasBorderHeader
          collapseIcon={
            <svg
              width='24'
              height='24'
              viewBox='0 0 24 24'
              fill='none'
              xmlns='http://www.w3.org/2000/svg'
            >
              <path
                d='M17.5 14.75L12.0707 9.32071C12.0317 9.28166 11.9683 9.28166 11.9293 9.32071L6.5 14.75'
                stroke='#9A9DB2'
                strokeWidth='1.5'
                strokeLinecap='round'
              />
            </svg>
          }
          expandIcon={
            <svg
              width='24'
              height='24'
              viewBox='0 0 24 24'
              fill='none'
              xmlns='http://www.w3.org/2000/svg'
              className='rotate-180'
            >
              <path
                d='M17.5 14.75L12.0707 9.32071C12.0317 9.28166 11.9683 9.28166 11.9293 9.32071L6.5 14.75'
                stroke='#9A9DB2'
                strokeWidth='1.5'
                strokeLinecap='round'
              />
            </svg>
          }
        >
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto adipisci repellendus est
          impedit doloribus aspernatur rem obcaecati? Aliquid ab sequi modi, quod doloribus adipisci
          quisquam, atque aliquam, minima quaerat veritatis.
        </Accordion.Item>
        <Accordion.Item
          title='Today - Sep 15, 2022'
          onExpand={handleExpandMulti}
          accordionId='example3'
          expand={expand['example3']}
          hasBorderHeader
          collapseIcon={
            <svg
              width='24'
              height='24'
              viewBox='0 0 24 24'
              fill='none'
              xmlns='http://www.w3.org/2000/svg'
            >
              <path
                d='M17.5 14.75L12.0707 9.32071C12.0317 9.28166 11.9683 9.28166 11.9293 9.32071L6.5 14.75'
                stroke='#9A9DB2'
                strokeWidth='1.5'
                strokeLinecap='round'
              />
            </svg>
          }
          expandIcon={
            <svg
              width='24'
              height='24'
              viewBox='0 0 24 24'
              fill='none'
              xmlns='http://www.w3.org/2000/svg'
              className='rotate-180'
            >
              <path
                d='M17.5 14.75L12.0707 9.32071C12.0317 9.28166 11.9683 9.28166 11.9293 9.32071L6.5 14.75'
                stroke='#9A9DB2'
                strokeWidth='1.5'
                strokeLinecap='round'
              />
            </svg>
          }
        >
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Exercitationem magni delectus
          laudantium? Repellat at repudiandae architecto! Suscipit, earum? Nostrum alias officiis
          facilis temporibus beatae asperiores magnam eos sunt nulla illo?
        </Accordion.Item>
        <Accordion.Item
          title='Today - Sep 16, 2022'
          onExpand={handleExpandMulti}
          accordionId='example4'
          expand={expand['example4']}
          hasBorderHeader
          collapseIcon={
            <svg
              width='24'
              height='24'
              viewBox='0 0 24 24'
              fill='none'
              xmlns='http://www.w3.org/2000/svg'
            >
              <path
                d='M17.5 14.75L12.0707 9.32071C12.0317 9.28166 11.9683 9.28166 11.9293 9.32071L6.5 14.75'
                stroke='#9A9DB2'
                strokeWidth='1.5'
                strokeLinecap='round'
              />
            </svg>
          }
          expandIcon={
            <svg
              width='24'
              height='24'
              viewBox='0 0 24 24'
              fill='none'
              xmlns='http://www.w3.org/2000/svg'
              className='rotate-180'
            >
              <path
                d='M17.5 14.75L12.0707 9.32071C12.0317 9.28166 11.9683 9.28166 11.9293 9.32071L6.5 14.75'
                stroke='#9A9DB2'
                strokeWidth='1.5'
                strokeLinecap='round'
              />
            </svg>
          }
        >
          Lorem ipsum dolor sit amet consectetur, adipisicing elit. At est, sed cupiditate omnis
          atque libero eligendi architecto possimus earum natus vero fugiat unde hic saepe corrupti,
          dolorem enim molestiae blanditiis.
        </Accordion.Item>
      </Accordion>
    </div>
  );
};

export default AccordionDemo;
