import { useFormik } from 'formik';

import { noop, OTPInput } from '@/lib';

export default function OTPInputDemo() {
  const { values, handleChange } = useFormik({ initialValues: { otp: '2222' }, onSubmit: noop });
  return (
    <div className='p-8'>
      <h3>Demo OTP Input</h3>
      <div className='w-[354px]'>
        <OTPInput
          name='otp'
          value={values.otp}
          onChange={handleChange}
          isError={true}
          helperText='OTP không chính xác'
        />
      </div>
    </div>
  );
}
