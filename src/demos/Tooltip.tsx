import { Placement } from '@floating-ui/react-dom-interactions';
import { useState } from 'react';

import { RadioGroup, Tooltip } from '@/lib';

const POSITIONS: Placement[] = [
  'top-start',
  'top',
  'top-end',
  'left',
  'right',
  'bottom-start',
  'bottom',
  'bottom-end',
];
const RADIO_OPTIONS = POSITIONS.map((pos) => ({ value: pos, label: pos }));

export default function TooltipDemo() {
  const [placement, setPlacement] = useState<Placement>('top-start');
  return (
    <div className='p-8'>
      <h3 className='mb-4'>Demo tooltip</h3>
      <Tooltip placement={placement} content='This is a tooltip content'>
        <span>Hover me!</span>
      </Tooltip>

      <Tooltip placement={placement} content='This is a tooltip content'>
        <span className='ml-4'>Hover me!</span>
      </Tooltip>

      <Tooltip placement={placement} content='This is a tooltip content'>
        <span className='ml-4'>Hover me!</span>
      </Tooltip>

      <h5 className='mt-4'>Select a Placement</h5>
      <RadioGroup
        className='flex space-x-2'
        name='position'
        value={placement}
        onChange={(e) => setPlacement(e.target.value as Placement)}
        options={RADIO_OPTIONS}
      />
    </div>
  );
}
