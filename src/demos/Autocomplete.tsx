import { useFormik } from 'formik';

import { Autocomplete } from '@/lib';

import provinces from './province-options';
export default function AutocompleteDemo() {
  const { values, handleChange } = useFormik({
    initialValues: { provinces: [], province: [] },
    onSubmit: console.log,
  });
  return (
    <div className='p-8'>
      <h3>Autocomplete</h3>
      <div className='grid grid-cols-3 space-x-4'>
        <Autocomplete
          label='Provinces'
          name='provinces'
          isMultiple
          options={provinces}
          value={values.provinces}
          onChange={handleChange}
        />
        <Autocomplete
          label='Province'
          name='province'
          options={provinces}
          value={values.province}
          onChange={handleChange}
        />
      </div>
    </div>
  );
}
