import { useState } from 'react';

import { Tabs } from '@/lib';
import { TabDataProps } from '@/lib/components/Tab/types';

const TabDemo = () => {
  const [selectTab, setSelectTab] = useState('');
  const handleOnSelect = (tabId: string) => {
    setSelectTab(tabId);
  };

  const tabs: TabDataProps[] = [
    {
      tabId: '0',
      disabled: false,
      title: 'Tab 0',
      leftIcon: {
        iconName: 'profile-user',
        variant: 'solid',
      },
      rightIcon: {
        iconName: 'profile-user',
        variant: 'solid',
      },
      status: {
        content: '100+',
        type: 'text',
      },
    },
    {
      tabId: '1',
      title: 'Tab 1',
      disabled: true,
    },
    {
      tabId: '2',
      title: 'Tab 2',
      leftIcon: {
        iconName: 'calendar',
        variant: 'solid',
      },
    },
    {
      tabId: '3',
      title: 'Tab 3',
      rightIcon: {
        iconName: 'calendar',
        variant: 'solid',
      },
    },
    {
      tabId: '4',
      title: 'Tab 4',
      disabled: true,
      status: {
        content: '99+',
        type: 'text',
      },
    },
  ];

  return (
    <div className='p-8'>
      <h3>Tabs</h3>
      <Tabs onSelect={handleOnSelect} tabs={tabs} selected={selectTab} />
    </div>
  );
};

export default TabDemo;
