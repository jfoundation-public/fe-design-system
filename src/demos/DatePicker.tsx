import { useFormik } from 'formik';

import { DatePicker, DateRangePicker, TimeRangePicker } from '@/lib';
import DateTimePicker from '@/lib/components/DateTimePicker/DateTimePicker';
import TimePicker from '@/lib/components/DateTimePicker/TimePicker';

export default function DatePickerDemo() {
  const { values, handleChange } = useFormik({
    initialValues: {
      date: '',
      time: '',
      dateTime: '',
      dateRange: ['', ''] as [string, string],
      dateRange1: ['2022-01-19', '2022-08-19'] as [string, string],
      timeRange: ['12:13', '12:30'] as [string, string],
    },
    onSubmit: () => undefined,
  });
  return (
    <div className='p-8'>
      <h3>Demo Date Time Picker</h3>
      <DatePicker
        name='date'
        placeholder='Pick a date'
        value={values.date}
        onChange={handleChange}
        label='Date of birth'
        className='mt-4'
        // dateformat='dd/MM/yyyy'
        pickerPlacement='right'
        // disableFuture
        disablePast
      />

      <DatePicker
        name='date'
        placeholder='Pick a date'
        value={values.date}
        onChange={handleChange}
        label='Date of birth'
        isError
        className='mt-4'
      />
      <TimePicker
        readOnly
        label='Time'
        name='time'
        value={'12:00'}
        onChange={handleChange}
        className='mt-4'
      />

      <TimePicker
        label='Time'
        name='time'
        value={values.time}
        onChange={handleChange}
        className='mt-4'
        isWarning
      />

      <DateTimePicker
        label='Date time'
        name='dateTime'
        value={values.dateTime}
        onChange={handleChange}
        className='mt-4'
      />
      <DateTimePicker
        label='Date time'
        name='dateTime'
        value={values.dateTime}
        onChange={handleChange}
        className='mt-4'
        isSuccess
      />

      <DateRangePicker
        label='Date range'
        name='dateRange'
        placeholder={['Input from Date', 'input to date']}
        value={values.dateRange}
        onChange={handleChange}
        className='mt-4'
      />

      <DateRangePicker
        label='Date range'
        name='dateRange1'
        value={values.dateRange1}
        onChange={handleChange}
        className='mt-4'
      />

      <TimeRangePicker
        label='Time range'
        name='timeRange'
        placeholder={['select from', 'select to']}
        value={values.timeRange}
        onChange={handleChange}
        className='mt-4'
      />
    </div>
  );
}
