import { useState } from 'react';

import { Typography } from '@/lib';

import Step from '../lib/components/Step/Step';
import { StepItem, StepStatus } from '../lib/components/Step/types';

const StepDemo = () => {
  const [selected, setSelected] = useState(0);
  const data: StepItem[] = [
    { id: 'step1', title: <Typography variant='subtitle1'>Hello</Typography> },
    {
      id: 'step2',
      title: 'Header Content',
      description: 'Something',
    },
    { id: 'step3', title: 'Example 3' },
    { id: 'step4', title: 'Example 4' },
    {
      id: 'step5',
      title: 'Example 5',
    },
  ];

  const mockStatus: StepStatus = {
    step1: 'default',
    step2: 'default',
    step3: 'finish',
    step4: 'error',
    step5: 'disable',
  };

  const data2: StepItem[] = [
    { id: 'step1', title: 'Verify' },
    {
      id: 'step2',
      title: 'OTP',
    },
    { id: 'step3', title: 'New password' },
  ];

  return (
    <>
      <div className='p-8'>
        <h3 className='pb-8'>Demo Step Horizontal</h3>
        <Step
          className='p-8'
          onSelect={(id) => setSelected(id)}
          direction='horizontal'
          steps={data}
          type='dot'
          current={selected}
          status={mockStatus}
        />
        <Step
          className='p-8'
          direction='horizontal'
          onSelect={(id) => setSelected(id)}
          steps={data}
          type='number'
          current={selected}
          status={mockStatus}
          // alternativeLabel
        />
        <Step
          className='p-8'
          direction='horizontal'
          onSelect={(id) => setSelected(id)}
          steps={data}
          type='icon'
          current={selected}
          status={mockStatus}
          config={[
            { icon: 'dashboard' },
            { icon: 'time-clock' },
            { icon: '3dot', variant: 'outline' },
            { icon: 'profile-user' },
            { icon: 'profile-user' },
          ]}
        />
      </div>
      <div className='p-8'>
        <h3 className='pb-8'>Demo Step Vertical</h3>
        <div className='flex flex-row gap-10'>
          <Step
            onSelect={(id) => setSelected(id)}
            direction='vertical'
            steps={data}
            type='dot'
            current={selected}
            status={mockStatus}
            headerConfig={[
              { type: 'dot', icon: 'dashboard', isCurrent: true, status: 'default' },
              { type: 'dot', icon: 'time-clock', isCurrent: true, status: 'default' },
              {
                type: 'dot',
                icon: '3dot',
                variant: 'outline',
                isCurrent: false,
                status: 'default',
              },
              { type: 'dot', icon: 'profile-user', isCurrent: true, status: 'default' },
              { type: 'dot', icon: 'profile-user', isCurrent: true, status: 'default' },
            ]}
          />
          <Step
            direction='vertical'
            onSelect={(id) => setSelected(id)}
            steps={data}
            type='number'
            current={selected}
            status={mockStatus}
          />
          <Step
            onSelect={(id) => setSelected(id)}
            direction='vertical'
            steps={data}
            type='icon'
            current={selected}
            status={mockStatus}
            headerConfig={[
              {
                icon: 'circleTick',
                variant: 'solid',
                isCurrent: true,
                status: 'finish',
                type: 'icon',
              },
              {
                icon: 'circleTick',
                variant: 'solid',
                isCurrent: true,
                status: 'finish',
                type: 'icon',
              },
              {
                icon: 'circleTick',
                isCurrent: false,
                status: 'default',
                type: 'number',
              },
              {
                icon: 'circleTick',
                isCurrent: false,
                status: 'default',
                type: 'number',
              },
              {
                icon: 'circleTick',
                isCurrent: false,
                status: 'default',
                type: 'number',
              },
            ]}
          />
        </div>
      </div>

      <Step
        className='w-[366px]'
        direction='horizontal'
        onSelect={(id) => setSelected(id)}
        steps={data2}
        type='number'
        current={selected}
        status={mockStatus}
      />
    </>
  );
};

export default StepDemo;
