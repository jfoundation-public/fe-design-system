import { Avatar, GroupAvatar, Icon } from '@/lib';
import { AvatarType } from '@/lib/components/Avatar/types';

const AvatarDemo = () => {
  const avatars: AvatarType[] = [
    {
      id: '1',
      type: 'img',
      size: 40,
      name: 'Yuna',
      src: 'https://i.ytimg.com/vi/G7F1eEKhvrs/maxresdefault.jpg',
    },
    {
      id: '2',
      type: 'img',
      size: 40,
      name: 'Yuna',
      src: 'https://i.ytimg.com/vi/G7F1eEKhvrs/maxresdefault.jpg',
    },
    {
      id: '3',
      type: 'img',
      size: 40,
      name: 'Yuna',
      src: 'https://i.ytimg.com/vi/G7F1eEKhvrs/maxresdefault.jpg',
    },
    { id: '4', type: 'text', size: 40, name: 'Terra Branford' },
    { id: '5', type: 'text', size: 40, name: 'Rikku' },
    { id: '6', type: 'text', size: 40, name: 'Celes Chere' },
    { id: '7', type: 'text', size: 40, name: 'Aerith Gainsborough' },
    { id: '8', type: 'text', size: 40, name: 'Aerith Gainsborough' },
    { id: '9', type: 'text', size: 40, name: 'Aerith Gainsborough' },
    {
      id: '10',
      type: 'img',
      size: 40,
      name: 'Yuna',
      src: 'https://i.ytimg.com/vi/G7F1eEKhvrs/maxresdefault.jpg',
    },
    {
      id: '11',
      type: 'img',
      size: 40,
      name: 'Yuna',
      src: 'https://i.ytimg.com/vi/G7F1eEKhvrs/maxresdefault.jpg',
    },
    {
      id: '12',
      type: 'img',
      size: 40,
      name: 'Yuna',
      src: 'https://i.ytimg.com/vi/G7F1eEKhvrs/maxresdefault.jpg',
    },
  ];
  return (
    <div className='p-8'>
      <h3>Avatar Demo</h3>
      <Avatar
        size={32}
        type={'icon'}
        icon={'calendar'}
        name='Tien Bui'
        subName='Software Engineer'
      />
      <Avatar
        size={32}
        type={'img'}
        src='https://i.ytimg.com/vi/G7F1eEKhvrs/maxresdefault.jpg'
        name='Tien Bui'
        subName='Software Engineer'
        className='relative w-fit'
        badgeContent={
          <Icon
            iconName='payment'
            color='error'
            size={10}
            className='w-5 h-5 rounded-full absolute bottom-0 left-0 border-2 border-red-500 flex items-center justify-center bg-white'
            onClick={() => alert('hello')}
          />
        }
      />
      <Avatar
        size={32}
        type={'img'}
        src='https://i.ytimg.com/vi/G7F1eEKhvrs/maxresdefault.jpg'
        name='Tien Bui'
        subName='Software Engineer'
      />
      <Avatar size={72} type={'img'} src='https://i.ytimg.com/vi/G7F1eEKhvrs/maxresdefault.jpg' />
      <Avatar
        size={72}
        type={'img'}
        src='https://i.ytimg.com/vi/G7F1eEKhvrs/maxresdefault.jpg'
        name='Tien Bui'
        subName='Software Engineer'
      />
      <Avatar
        size={96}
        type={'img'}
        src='https://i.ytimg.com/vi/G7F1eEKhvrs/maxresdefault.jpg'
        name='Tien Bui'
        subName='Software Engineer'
      />
      <Avatar
        size={120}
        type={'img'}
        src='https://staticg.sportskeeda.com/editor/2022/06/3c537-16556951297834-1920.jpg'
        name='Jacob Johns'
        subName='Apple Owner'
      />
      <Avatar size={72} type={'icon'} icon='cms' name='Tien Bui' subName='Software Engineer' />
      <Avatar size={96} type={'icon'} icon='cms' name='Tien Bui' subName='Software Engineer' />
      <Avatar size={120} type={'icon'} icon='cms' name='Jacob Johns' subName='Apple Owner' />

      <Avatar
        textColor='bg-slate-400'
        size={32}
        type={'text'}
        name='Eddard Stark'
        subName='Apple Owner'
      />
      <Avatar size={40} type={'text'} name='Jon Snow' subName='Apple Owner' />
      <Avatar size={56} type={'text'} name='Viserys Targaryen' subName='Apple Owner' />
      <Avatar size={72} type={'text'} name='Cersei Lannister' subName='Apple Owner' />
      <Avatar size={96} type={'text'} name='Jaime Lannister' subName='Apple Owner' />
      <Avatar size={120} type={'text'} name='Stannis Baratheon' subName='Apple Owner' />

      <div className='m-6 p-6'>
        <GroupAvatar avatars={avatars} />
      </div>

      <div className='m-6 p-6'>
        <GroupAvatar avatars={avatars.slice(0, 6)} />
      </div>

      <div className='m-6 p-6'>
        <GroupAvatar avatars={avatars.slice(0, 4)} />
      </div>

      <div className='m-6 p-6'>
        <GroupAvatar avatars={avatars.slice(0, 3)} />
      </div>
      <div className='m-6 p-6'>
        <GroupAvatar avatars={avatars.slice(0, 2)} />
      </div>
      <div className='m-6 p-6'>
        <GroupAvatar avatars={avatars.slice(0, 1)} />
      </div>
    </div>
  );
};

export default AvatarDemo;
