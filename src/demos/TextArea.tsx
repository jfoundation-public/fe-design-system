import { useFormik } from 'formik';

import { TextArea } from '@/lib';

export default function TextAreaDemo() {
  const { values, handleChange, handleBlur } = useFormik({
    initialValues: { textarea: '' },
    onSubmit: (_values) => {
      console.log(_values);
    },
  });

  return (
    <div className='p-8'>
      <h3>Text Area Demo</h3>
      <div className='grid grid-cols-3 gap-4'>
        <TextArea
          label='Text Area'
          placeholder='Placeholder'
          name='textarea'
          value={values.textarea}
          onChange={handleChange}
          onBlur={handleBlur}
          maxLength={500}
          required
        />

        <TextArea
          label='Text Area'
          labelIcon='information'
          placeholder='Placeholder'
          name='textarea'
          value={values.textarea}
          onChange={handleChange}
          onBlur={handleBlur}
          maxLength={500}
          readOnly
        />
        <TextArea
          label='Text Area'
          labelIcon='information'
          placeholder='Placeholder'
          name='textarea'
          value={values.textarea}
          onChange={handleChange}
          onBlur={handleBlur}
          maxLength={500}
          disabled
        />
      </div>
      <div className='grid grid-cols-3 gap-4 mt-4'>
        <TextArea
          label='Text Area'
          labelIcon='information'
          placeholder='Placeholder'
          name='textarea'
          value={values.textarea}
          onChange={handleChange}
          onBlur={handleBlur}
          maxLength={500}
          helperText='Error'
          isError
        />

        <TextArea
          label='Text Area'
          labelIcon='information'
          placeholder='Placeholder'
          name='textarea'
          value={values.textarea}
          onChange={handleChange}
          onBlur={handleBlur}
          maxLength={500}
          isSuccess
          helperText='Success'
        />
        <TextArea
          label='Text Area'
          labelIcon='information'
          placeholder='Placeholder'
          name='textarea'
          value={values.textarea}
          onChange={handleChange}
          onBlur={handleBlur}
          maxLength={500}
          isWarning
          helperText='Warning'
        />
      </div>
    </div>
  );
}
