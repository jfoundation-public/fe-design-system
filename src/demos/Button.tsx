import { Button, Icon, IconButton } from '@/lib';

export default function ButtonDemo() {
  return (
    <div className='p-8'>
      <IconButton iconName='3dot' className='mb-2' />
      <Button
        hasDottedBorder
        rightIcon={<Icon iconName='profile-user' color='default' />}
        className='justify-between'
        wrapperClassName='my-2'
      >
        Button
      </Button>
      <div className='w-[400px]'>
        <Button fullWidth>Button</Button>
      </div>
      <Button leftIcon={<Icon iconName='profile-user' />} size='lg'>
        button
      </Button>
      <Button variant='positive' size='lg'>
        button
      </Button>
      <Button variant='negative' size='lg'>
        button
      </Button>
      <Button variant='secondary' size='lg'>
        button
      </Button>
      <Button variant='link' size='lg'>
        button
      </Button>
      <Button variant='blank' size='lg'>
        button
      </Button>
      <Button disabled size='lg'>
        button
      </Button>
      <Button variant='secondary' disabled size='lg'>
        button
      </Button>
      <Button variant='blank' disabled size='lg'>
        button
      </Button>
      <Button variant='link' disabled size='lg'>
        button
      </Button>

      <Button>button</Button>
      <Button variant='positive'>button</Button>
      <Button variant='negative'>button</Button>
      <Button variant='secondary'>button</Button>
      <Button variant='link'>button</Button>
      <Button variant='blank'>button</Button>
      <Button disabled>button</Button>

      <Button size='sm'>button</Button>
      <Button variant='positive' size='sm'>
        button
      </Button>
      <Button variant='negative' size='sm'>
        button
      </Button>
      <Button variant='secondary' size='sm'>
        button
      </Button>
      <Button variant='link' size='sm'>
        button
      </Button>
      <Button variant='blank' size='sm'>
        button
      </Button>
      <Button disabled size='sm'>
        button
      </Button>
      <IconButton iconName='profile-user' />
      <IconButton variant='secondary' iconName='delete' color='error' />
      <IconButton variant='secondary' iconName='delete' color='warning' />
      <IconButton variant='secondary' iconName='delete' color='success' />
      <IconButton variant='secondary' iconName='delete' color='primary' />
    </div>
  );
}
