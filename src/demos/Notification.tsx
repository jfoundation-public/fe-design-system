import { Button, Notification } from '@/lib';

export default function NotificationDemo() {
  return (
    <div id='step1' className='p-8'>
      <h3 className='mb-4'>Demo Notification</h3>
      <Notification
        iconName='information'
        iconClasses='text-text-information'
        title='This is content for some inforation !'
        description='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce eget pharetra eros, sagittis dignissim '
      >
        <Button variant='secondary'>Canel</Button>
        <Button>Confirm</Button>
      </Notification>
    </div>
  );
}
