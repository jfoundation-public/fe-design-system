import { addDays, addWeeks, endOfDay, format, getWeekOfMonth, set, startOfDay } from 'date-fns';
import { useState } from 'react';

import {
  BaseScheduleEvent,
  Button,
  EventClick,
  EventItem,
  Icon,
  IconButton,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  TextField,
  TimeRangePicker,
  Toggle,
  useToggle,
  ViewNav,
  WeekHeader,
  WeekView,
} from '@/lib';

const events: BaseScheduleEvent[] = [
  {
    id: '01',
    title: 'Small sale Mid-Autumn Festival 11-50',
    startEventDate: new Date(),
    time: [set(new Date(), { hours: 1, minutes: 0 }), set(new Date(), { hours: 2, minutes: 0 })],
    priority: 1,
  },
  {
    id: '02',
    title: 'Small sale Mid-Autumn Festival 11-51',
    startEventDate: new Date(),
    time: [set(new Date(), { hours: 1, minutes: 0 }), set(new Date(), { hours: 2, minutes: 0 })],
    priority: 2,
  },
  {
    id: '06',
    title: 'Small sale Mid-Autumn Festival 11-52',
    startEventDate: new Date(),
    time: [set(new Date(), { hours: 1, minutes: 0 }), set(new Date(), { hours: 2, minutes: 0 })],
    priority: 2,
  },
  {
    id: '12',
    title: 'Small sale Mid-Autumn Festival 11-53',
    startEventDate: new Date(),
    time: [
      startOfDay(set(new Date(), { hours: 1, minutes: 20 })),
      endOfDay(set(new Date(), { hours: 3, minutes: 30 })),
    ],
    priority: 4,
  },
  {
    id: '08',
    title: 'Small sale Mid-Autumn Festival 11-54',
    startEventDate: new Date(),
    time: [set(new Date(), { hours: 5, minutes: 0 }), set(new Date(), { hours: 7, minutes: 0 })],
    priority: 2,
  },
  {
    id: '188',
    title: 'Small sale Mid-Autumn Festival 11-56',
    startEventDate: new Date(),
    time: [set(new Date(), { hours: 6, minutes: 0 }), set(new Date(), { hours: 7, minutes: 0 })],
    priority: 1,
  },
  {
    id: '04',
    title: 'Small sale Mid-Autumn Festival 11-57',
    startEventDate: new Date(),
    time: [set(new Date(), { hours: 5, minutes: 0 }), set(new Date(), { hours: 6, minutes: 0 })],
    recurring: { frequency: 'weekly', interval: 1, count: 4 },
    priority: 1,
  },
  {
    id: '02',
    title: 'Small sale Mid-Autumn Festival 22-58',
    startEventDate: addDays(new Date(), 1),
    time: [set(new Date(), { hours: 3, minutes: 30 }), set(new Date(), { hours: 5, minutes: 0 })],
    priority: 1,
  },
];

export default function SchedulesDemo() {
  const [viewDate, setViewDate] = useState(new Date());
  const [openModal, toggleModal] = useToggle();
  const [activeCell, setActiveCell] = useState<boolean>(true);

  function onNextWeek() {
    setViewDate((prevDate) => addWeeks(prevDate, 1));
  }

  function onPrevWeek() {
    setViewDate((prevDate) => addWeeks(prevDate, -1));
  }

  function onToday() {
    setViewDate(new Date());
  }

  function handleEventClick(eventClick: EventClick<BaseScheduleEvent>) {
    // eslint-disable-next-line no-console
    console.log('EVENT CLICK >>>>', eventClick);

    toggleModal();
    setActiveCell(true);
  }

  function handleCreateEvent() {
    events.push({
      id: '14',
      title: 'Small sale Mid-Autumn Festival 11-55',
      startEventDate: new Date(),
      time: [
        set(new Date(), { hours: 14, minutes: 30 }),
        set(new Date(), { hours: 15, minutes: 30 }),
      ],
      priority: 3,
    });
    toggleModal();
    setActiveCell(false);
  }

  function handleEditEvent(event: BaseScheduleEvent) {
    // eslint-disable-next-line no-console
    console.log(event);
  }

  function handleDeleteEvent(event: BaseScheduleEvent) {
    // eslint-disable-next-line no-console
    console.log(event);
  }

  function handleDeleteEvents(events: BaseScheduleEvent[]) {
    // eslint-disable-next-line no-console
    console.log(events);
  }

  function handleCloseModal() {
    setActiveCell(false);
    toggleModal();
  }

  return (
    <div className='p-8'>
      <h3>SchedulesDemo</h3>
      <div className='mt-4'>
        {/* Schedules Component */}
        <div className='schedules h-[700px] scroll-bar'>
          <div className='flex justify-between mb-6 gap-4'>
            <ViewNav className='flex-1' view='week' />
            <div className='flex items-center gap-2'>
              <IconButton
                iconName='arrow-left'
                iconVariant='outline'
                variant='secondary'
                size='sm'
                className='border-lineBorder-borderDefault text-lineBorder-borderHover w-8 h-8'
                onClick={onPrevWeek}
              />
              <p>
                Week {getWeekOfMonth(viewDate)} - {format(viewDate, 'MMMM y')}
              </p>
              <IconButton
                iconName='arrow-right'
                iconVariant='outline'
                variant='secondary'
                size='sm'
                className='border-lineBorder-borderDefault text-lineBorder-borderHover w-8 h-8'
                onClick={onNextWeek}
              />
            </div>
            <div className='flex items-center'>
              <Button onClick={onToday} size='sm' variant='secondary' className='min-w-[40px]'>
                Today
              </Button>
            </div>
          </div>
          <WeekView
            activeCell={activeCell}
            events={events}
            locale='enUS'
            onEventClick={handleEventClick}
            viewDate={viewDate}
            renderHeader={({ date, locale }) => (
              <WeekHeader key={date.toDateString()} date={date} locale={locale} />
            )}
            renderEventItems={(evts) => {
              return (
                <>
                  {evts.map((evt) => {
                    const nestedEvent = evts.filter(
                      ({ time }) =>
                        time[0] >= evt.time[0] &&
                        time[0] <= evt.time[1] &&
                        time[1] >= evt.time[0] &&
                        time[1] <= evt.time[1],
                    );

                    const overlayEvents = evts.filter(
                      ({ time }) =>
                        time[0] <= evt.time[0] && time[1] <= evt.time[1] && time[1] > evt.time[0],
                    );

                    return (
                      <EventItem
                        key={evt.id}
                        event={evt}
                        nestedEvents={nestedEvent}
                        badge={nestedEvent.length}
                        isTilted={overlayEvents.length - 1 > 0}
                        onEditEvent={handleEditEvent}
                        onEventClick={handleEventClick}
                        onDeleteEvent={handleDeleteEvent}
                        onDeleteEvents={handleDeleteEvents}
                      />
                    );
                  })}
                </>
              );
            }}
          />
        </div>

        <Modal open={openModal} onClose={handleCloseModal} size='sm'>
          <ModalHeader title='Create Schedule' onClose={handleCloseModal} />
          <ModalBody className='space-y-4'>
            <TextField label='Title' name='title' />
            <TextField label='Priority' name='priority' />
            <TextField label='Number of repeated unit' name='recurring.interval' />
            <TextField label='Repeated unit' name='recurring.count' />
            <Toggle name='active' />
            <TimeRangePicker label='Time' name='time' />
            <Button
              variant='blank'
              leftIcon={<Icon iconName='add' variant='outline' className='icon-primary' />}
            >
              Add time
            </Button>
          </ModalBody>
          <ModalFooter className='flex items-center'>
            <Button className='h-12 w-full' variant='secondary' onClick={handleCloseModal}>
              Cancel
            </Button>
            <Button className='h-12 w-full' onClick={handleCreateEvent}>
              Create
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    </div>
  );
}
