import { Typography } from '@/lib';

export default function TypographyDemo() {
  return (
    <div className='p-8'>
      <h1>H1</h1>
      <h2>H2</h2>
      <h3>H3</h3>
      <h4>H4</h4>
      <h5>H5</h5>
      <Typography variant='subheading'>Subheading</Typography>
      <Typography variant='subtitle1'>Subtitle 1</Typography>
      <Typography variant='subtitle2'>subtitle 2</Typography>
      <Typography variant='body'>Body</Typography>
      <Typography variant='caption1'>Caption 1</Typography>
      <Typography variant='caption2'>Caption 2</Typography>
    </div>
  );
}
