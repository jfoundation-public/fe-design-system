import clsx from 'clsx';
import { useFormik } from 'formik';
import { useState } from 'react';

import { Select, Typography } from '@/lib';

import SelectItemCreate from './shared/SelectItemCreate';

export default function SelectDemo() {
  const { values, handleChange } = useFormik({
    initialValues: { gender: '' },
    onSubmit: (_values) => {
      // console.log(_values);
    },
  });
  const [multipleValue, setMultipleValue] = useState<string[]>([]);

  const handleMultipleChange = (e: any) => {
    setMultipleValue(e.target.value);
  };

  return (
    <div className='p-8'>
      <h3>Demo Select</h3>
      <div className='grid grid-cols-3'>
        <Select
          label='Gender'
          name='gender'
          placeholder='search'
          options={[
            { label: 'Male', value: 'male' },
            { label: 'Female', value: 'female' },
            { label: 'Other', value: 'other' },
            { label: 'Male2', value: 'male2' },
            { label: 'Female2', value: 'female2' },
            { label: 'Other2', value: 'other2' },
          ]}
          value={multipleValue}
          onChange={handleMultipleChange}
          className='py-4 px-3'
          isMultiple
        />

        <Select
          label='Gender'
          name='gender'
          placeholder='search'
          options={[
            { label: 'Male', value: 'male' },
            { label: 'Female', value: 'female' },
            { label: 'Other', value: 'other' },
            { label: 'Male2', value: 'male2' },
            { label: 'Female2', value: 'female2' },
            { label: 'Other2', value: 'other2' },
          ]}
          value={multipleValue}
          onChange={handleMultipleChange}
          className='py-4 px-3'
          isMultiple
          renderMultipleOptions={({ options }: { options: { label: string; value: string }[] }) => (
            <Typography variant='caption2'>
              {options.map((item) => item.label).join(', ')}
            </Typography>
          )}
        />
        <Select
          label='Custom Option'
          required
          startAdornment={
            <img src='https://seeklogo.com/images/G/grab-logo-7020E74857-seeklogo.com.png' alt='' />
          }
          name='gender'
          placeholder='search'
          options={[
            { label: 'Male', value: 'male', description: 'Gender' },
            { label: 'Female', value: 'female', description: 'Gender' },
            { label: 'Other', value: 'other', disabled: true, description: 'Gender' },
          ]}
          renderOption={({ option, onSelected, isSelected }) => (
            <li
              role='presentation'
              onClick={() => onSelected(option.value)}
              className={clsx(
                'flex flex-col m-2 p-2 hover:bg-surface-primary4 rounded-md',
                isSelected && 'text-text-error',
              )}
            >
              <div className='subtitle1'>{option.label}</div>
              <div>{option.description}</div>
            </li>
          )}
          value={values.gender}
          onChange={handleChange}
        />
      </div>
      <div className='grid grid-cols-3 space-x-4 mt-4'>
        <Select
          label='Gender'
          name='gender'
          placeholder='search'
          options={[
            { label: 'Male', value: 'male' },
            { label: 'Female', value: 'female' },
            { label: 'Other', value: 'other', disabled: true },
          ]}
          value={values.gender}
          onChange={handleChange}
          className='py-4 px-3'
        />
        <Select
          label='Gender'
          name='gender'
          placeholder='search'
          options={[
            { label: 'Male', value: 'male' },
            { label: 'Female', value: 'female' },
            { label: 'Other', value: 'other', disabled: true },
          ]}
          value={values.gender}
          onChange={handleChange}
          readOnly
        />
        <Select
          label='Gender'
          name='gender'
          placeholder='search'
          options={[
            { label: 'Male', value: 'male' },
            { label: 'Female', value: 'female' },
            { label: 'Other', value: 'other', disabled: true },
          ]}
          value={values.gender}
          onChange={handleChange}
          disabled
        />
      </div>
      <div className='grid grid-cols-3 space-x-4 mt-4'>
        <Select
          name='gender'
          placeholder='search'
          options={[
            { label: 'Male', value: 'male' },
            { label: 'Female', value: 'female' },
            { label: 'Other', value: 'other', disabled: true },
          ]}
          value={values.gender}
          onChange={handleChange}
          label='Gender'
          isError
          helperText='Please input!'
        />
        <Select
          name='gender'
          placeholder='search'
          options={[
            { label: 'Male', value: 'male' },
            { label: 'Female', value: 'female' },
            { label: 'Other', value: 'other', disabled: true },
          ]}
          value={values.gender}
          onChange={handleChange}
          label='Gender'
          isSuccess
          helperText='Please input!'
          moreSelectItem={({ onClose }) => (
            <SelectItemCreate onClose={onClose} onSelect={() => undefined} />
          )}
        />
      </div>
      <div className='grid grid-cols-3 mt-4'>
        <Select
          name='gender'
          placeholder='search'
          options={[
            { label: 'Male', value: 'male' },
            { label: 'Female', value: 'female' },
            { label: 'Other', value: 'other', disabled: true },
          ]}
          value={values.gender}
          onChange={handleChange}
          label='Gender'
          isWarning
          helperText='Please input!'
        />
      </div>
    </div>
  );
}
