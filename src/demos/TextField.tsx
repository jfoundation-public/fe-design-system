import { useFormik } from 'formik';
import { useState } from 'react';

import { Icon, noop } from '@/lib';
import { TextField } from '@/lib/components/TextField';

export default function TextFieldDemo() {
  const { values, handleChange } = useFormik({
    initialValues: { text1: '4353', text11: '' },
    onSubmit: noop,
  });
  const [selectValue, setSelectValue] = useState('');
  return (
    <div className='p-8'>
      <div className='grid grid-cols-3 space-x-6'>
        <TextField
          readOnly
          label='Input'
          name='text1'
          value={values.text1}
          onChange={handleChange}
          labelIcon='information'
          helperText='Helper text'
          placeholder='Place holder'
          selectOptions={[
            { label: 'Male', value: 'male' },
            { label: 'Female', value: 'female' },
            { label: 'Other', value: 'other', disabled: true },
          ]}
          selectValue={selectValue}
          onChangeSelect={(e) => {
            setSelectValue(e.target.value);
          }}
          format={(v) => {
            if (!v) return '';
            const parts = v.toString().split('.');
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
            return parts.join('.');
          }}
          parse={(v) => v?.toString().replaceAll(',', '')}
        />
        <TextField
          label='Password'
          type='password'
          name='text11'
          value={values.text11}
          onChange={handleChange}
          labelIcon='information'
          helperText='Helper text'
          placeholder='Password'
        />
        <TextField
          readOnly
          value='Read Only'
          label='Input'
          name='text2'
          labelIcon='information'
          helperText='Helper text'
        />
      </div>
      <div className='grid grid-cols-3 space-x-6 mt-6'>
        <TextField
          disabled
          value='Disabled'
          label='Input'
          name='text3'
          labelIcon='information'
          helperText='Helper text'
        />
        <TextField
          isError
          label='Input'
          name='text4'
          labelIcon='information'
          helperText='Helper text'
        />
        <TextField
          isSuccess
          label='Input'
          name='text5'
          labelIcon='information'
          helperText='Helper text'
        />
      </div>
      <div className='grid grid-cols-3 space-x-6 mt-6'>
        <TextField
          isWarning
          label='Input'
          name='text6'
          labelIcon='information'
          helperText='Helper text'
        />
        <TextField
          isError
          label='Input'
          name='text4'
          labelIcon='information'
          helperText='Helper text'
          startAdornment={<Icon iconName='information' type='solid' color='default' />}
          endAdornment={<Icon iconName='profile-user' type='solid' color='default' />}
        />
      </div>
    </div>
  );
}
