import { useFormik } from 'formik';

import { useToggle } from '@/lib';
import { Radio } from '@/lib/components/Radio';
import RadioGroup from '@/lib/components/Radio/RadioGroup';

export default function ToggleDemo() {
  const [checked, onToggle] = useToggle();
  const { values, handleChange } = useFormik({
    initialValues: { gender: '' },
    onSubmit: (_values) => {
      console.log(_values);
    },
  });
  return (
    <div className='p-8'>
      <h3>Demo Radio</h3>
      <Radio className='mt-4' checked={checked} disabled onChange={onToggle}>
        Disable
      </Radio>
      <Radio className='mt-4' checked={checked} readOnly onChange={onToggle}>
        Read only
      </Radio>
      <Radio className='mt-4' checked={true} readOnly onChange={onToggle}>
        Checked Read only
      </Radio>
      <Radio className='mt-4' checked={checked} onChange={onToggle}>
        Controlled
      </Radio>
      <Radio className='mt-4' checked={true} onChange={onToggle}>
        Checked
      </Radio>
      <Radio className='mt-4' disabled checked={true} onChange={onToggle}>
        Checked and Disabled
      </Radio>
      <h5>Gender: {values.gender}</h5>
      <RadioGroup
        options={[
          { label: 'Male', value: 'male', disabled: true },
          {
            label: 'Female',
            value: 'female',
            tooltipProps: {
              content: 'Tooltip description',
            },
          },
        ]}
        name='gender'
        onChange={handleChange}
        value={values.gender}
      />
    </div>
  );
}
