import { useDeferredValue, useEffect, useState } from 'react';

import {
  IconButton,
  TreeItemCore,
  TreeView,
  TreeViewExpandedState,
  TreeViewSelectedState,
} from '@/lib';

const tree = [
  {
    id: '1',
    label: 'Furniture',
    items: [
      {
        id: '1',
        label: 'Tables & Chairs',
      },
      {
        id: '2',
        label: 'Sofas',
      },
      {
        id: '3',
        label: 'Occasional Furniture',
      },
    ],
  },
  {
    id: '2',
    label: 'Decor',
    items: [
      {
        id: '1',
        label: 'Bed Linen',
        items: [
          {
            id: '1',
            label: 'B',
            items: [
              {
                id: '1',
                label: 'Tables & Chairs',
              },
              {
                id: '2',
                label: 'Sofas',
              },
              {
                id: '3',
                label: 'Occasional Furniture',
              },
            ],
          },
        ],
      },
      {
        id: '2',
        label: 'Curtains & Blinds',
      },
      {
        id: '3',
        label: 'Carpets',
      },
    ],
  },
];

export default function TreeViewDemo() {
  const [treeData, setTreeData] = useState(tree);
  const [selected, setSelected] = useState<TreeViewSelectedState>({});
  const [expanded, setExpanded] = useState<TreeViewExpandedState>({});
  const [checked, setChecked] = useState({});

  const [search, setSearch] = useState('');

  const searchCached = useDeferredValue(search);

  useEffect(() => {
    setTreeData(
      tree.filter((t) => t.label.toLocaleLowerCase().includes(searchCached.toLocaleLowerCase())),
    );
  }, [searchCached]);

  return (
    <div className='p-8'>
      <h3>Demo TreeView</h3>
      <TreeView data={treeData} expanded={expanded} onExpand={setExpanded} />
      <h5 className='mt-4'>With Select</h5>
      <TreeView
        data={treeData}
        selected={selected}
        onSelected={setSelected}
        expanded={expanded}
        onExpand={setExpanded}
      />

      <h5 className='mt-4'>With Custom TreeItem</h5>
      <TreeView
        data={treeData}
        selected={selected}
        onSelected={setSelected}
        expanded={expanded}
        onExpand={setExpanded}
        renderTreeItem={RenderTreeItem}
        readOnly
        searchProps={{
          noOutline: true,
          name: 'tree-search',
          value: search,
          onChange: (e) => {
            setSearch(e.target.value);
          },
        }}
      />

      <h5 className='mt-4'>With Radio</h5>
      <TreeView
        data={treeData}
        selected={selected}
        onSelected={setSelected}
        expanded={expanded}
        onExpand={setExpanded}
        renderTreeItem={RenderTreeItem}
        checked={checked}
        onChecked={setChecked}
        searchProps={{
          noOutline: true,
          name: 'tree-search',
          value: search,
          onChange: (e) => {
            setSearch(e.target.value);
          },
        }}
      />
    </div>
  );
}

function RenderTreeItem({ label, treeId }: TreeItemCore<{ other?: string }>) {
  return (
    <div className='text-primary flex items-center'>
      {label}
      <IconButton
        className='ml-2'
        isAdornment
        iconName='edit'
        iconVariant='outline'
        onClick={() => {
          console.log('>>>>treeId', treeId);
        }}
      />
    </div>
  );
}
