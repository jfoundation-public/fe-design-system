import { noop } from '@/lib';
import { Transfer, useTransferCheckBox, useTransferTreeView } from '@/lib/components/Transfer';

import { DataTransferCheckBox } from '../lib/components/Transfer/types';

const TransferDemo = () => {
  const leftSourceData: DataTransferCheckBox[] = [
    { checked: false, id: '112', text: 'example8' },
    { checked: false, id: '113', text: 'example9' },
    { checked: false, id: '114', text: 'example10' },
    { checked: false, id: '115', text: 'example11' },
    { checked: false, id: '116', text: 'example12' },
    { checked: false, id: '117', text: 'example13' },
    { checked: false, id: '118', text: 'example14' },
  ];
  const rightSourceData: DataTransferCheckBox[] = [
    { checked: false, id: '100', text: 'example_1' },
    { checked: false, id: '101', text: 'example_2' },
    { checked: false, id: '102', text: 'example_3' },
    { checked: false, id: '103', text: 'example_4' },
    { checked: false, id: '104', text: 'example_5' },
    { checked: false, id: '105', text: 'example_6' },
    { checked: false, id: '106', text: 'example-7' },
    { checked: false, id: '107', text: 'example-8' },
    { checked: false, id: '108', text: 'example-9' },
    { checked: false, id: '109', text: 'example-10' },
    { checked: false, id: '110', text: 'example-11' },
    { checked: false, id: '111', text: 'example-12' },
  ];

  const leftDataImg = [
    {
      checked: false,
      id: '112',
      text: 'Tien Bui',
      subText: 'Tien Bui',
      customClass: 'bg-amber-600',
    },
    {
      checked: false,
      id: '113',
      text: 'Tien Bui',
      subText: 'Tien Bui',
      avatar: 'https://techcrunch.com/wp-content/uploads/2019/03/lp-logo-3.jpg',
    },
    {
      checked: false,
      id: '114',
      text: 'Tien Bui',
      subText: 'Tien Bui',
      avatar: 'https://techcrunch.com/wp-content/uploads/2019/03/lp-logo-3.jpg',
    },
    {
      checked: false,
      id: '115',
      text: 'Tien Bui',
      subText: 'Tien Bui',
      avatar: 'https://techcrunch.com/wp-content/uploads/2019/03/lp-logo-3.jpg',
    },
    {
      checked: false,
      id: '116',
      text: 'Tien Bui',
      subText: 'Tien Bui',
      avatar: 'https://techcrunch.com/wp-content/uploads/2019/03/lp-logo-3.jpg',
    },
    {
      checked: false,
      id: '117',
      text: 'Tien Bui',
      subText: 'Tien Bui',
      avatar: 'https://techcrunch.com/wp-content/uploads/2019/03/lp-logo-3.jpg',
    },
    {
      checked: false,
      id: '118',
      text: 'Tien Bui',
      subText: 'Tien Bui',
      avatar: 'https://techcrunch.com/wp-content/uploads/2019/03/lp-logo-3.jpg',
    },
  ];

  const rightDataImg = [
    {
      checked: false,
      id: '100',
      text: 'Tien Bui',
      subText: 'Tien Bui',
      avatar: 'https://techcrunch.com/wp-content/uploads/2019/03/lp-logo-3.jpg',
    },
    {
      checked: false,
      id: '101',
      text: 'Tien Bui',
      subText: 'Tien Bui',
      avatar: 'https://techcrunch.com/wp-content/uploads/2019/03/lp-logo-3.jpg',
    },
    {
      checked: false,
      id: '102',
      text: 'Tien Bui',
      subText: 'Tien Bui',
      avatar: 'https://techcrunch.com/wp-content/uploads/2019/03/lp-logo-3.jpg',
    },
    {
      checked: false,
      id: '103',
      text: 'Tien Bui',
      subText: 'Tien Bui',
      avatar: 'https://techcrunch.com/wp-content/uploads/2019/03/lp-logo-3.jpg',
    },
    {
      checked: false,
      id: '104',
      text: 'Tien Bui',
      subText: 'Tien Bui',
      avatar: 'https://techcrunch.com/wp-content/uploads/2019/03/lp-logo-3.jpg',
    },
    {
      checked: false,
      id: '105',
      text: 'Tien Bui',
      subText: 'Tien Bui',
      avatar: 'https://techcrunch.com/wp-content/uploads/2019/03/lp-logo-3.jpg',
    },
    {
      checked: false,
      id: '106',
      text: 'Tien Bui',
      subText: 'Tien Bui',
      avatar: 'https://techcrunch.com/wp-content/uploads/2019/03/lp-logo-3.jpg',
    },
    {
      checked: false,
      id: '107',
      text: 'Tien Bui',
      subText: 'Tien Bui',
      avatar: 'https://techcrunch.com/wp-content/uploads/2019/03/lp-logo-3.jpg',
    },
    {
      checked: false,
      id: '108',
      text: 'Tien Bui',
      subText: 'Tien Bui',
      avatar: 'https://techcrunch.com/wp-content/uploads/2019/03/lp-logo-3.jpg',
    },
    {
      checked: false,
      id: '109',
      text: 'Tien Bui',
      subText: 'Tien Bui',
      avatar: 'https://techcrunch.com/wp-content/uploads/2019/03/lp-logo-3.jpg',
    },
    {
      checked: false,
      id: '110',
      text: 'Tien Bui',
      subText: 'Tien Bui',
      avatar: 'https://techcrunch.com/wp-content/uploads/2019/03/lp-logo-3.jpg',
    },
    {
      checked: false,
      id: '111',
      text: 'Tien Bui',
      subText: 'Tien Bui',
      avatar: 'https://techcrunch.com/wp-content/uploads/2019/03/lp-logo-3.jpg',
    },
  ];

  //transfer tree-view
  const tree = [
    {
      id: 'item-1',
      label: 'Furniture',
      items: [
        {
          id: 'item-2',
          label: 'Tables & Chairs',
        },
        {
          id: 'item-3',
          label: 'Sofas',
        },
        {
          id: 'item-4',
          label: 'Occasional Furniture',
        },
      ],
    },
    {
      id: 'item-5',
      label: 'Decor',
      items: [
        {
          id: 'item-6',
          label: 'Bed Linen',
          items: [
            {
              id: 'item-7',
              label: 'B',
              items: [
                {
                  id: 'item-8',
                  label: 'Tables & Chairs',
                },
                {
                  id: 'item-9',
                  label: 'Sofas',
                },
                {
                  id: 'item-10',
                  label: 'Occasional Furniture',
                },
              ],
            },
          ],
        },
        {
          id: 'item-11',
          label: 'Curtains & Blinds',
        },
        {
          id: 'item-12',
          label: 'Carpets',
        },
      ],
    },
  ];

  const tree2 = [
    {
      id: 'random-1',
      label: 'Furniture',
      items: [
        {
          id: 'random-2',
          label: 'Tables & Chairs',
        },
        {
          id: 'random-3',
          label: 'Sofas',
        },
        {
          id: 'random-4',
          label: 'Occasional Furniture',
        },
      ],
    },
    {
      id: 'random-5',
      label: 'Decor',
      items: [
        {
          id: 'random-6',
          label: 'Bed Linen',
          items: [
            {
              id: 'random-7',
              label: 'B',
              items: [
                {
                  id: 'random-8',
                  label: 'Tables & Chairs',
                },
                {
                  id: 'random-9',
                  label: 'Sofas',
                },
                {
                  id: 'random-10',
                  label: 'Occasional Furniture',
                },
              ],
            },
          ],
        },
        {
          id: 'random-11',
          label: 'Curtains & Blinds',
        },
        {
          id: 'random-12',
          label: 'Carpets',
        },
      ],
    },
    {
      id: 'random-13',
      label: 'RM1000',
      items: [
        {
          id: 'random-14',
          label: 'Bed Linen',
          items: [
            {
              id: 'random-15',
              label: 'B',
              items: [
                {
                  id: 'random-16',
                  label: 'Tables & Chairs',
                },
                {
                  id: 'random-17',
                  label: 'Sofas',
                },
                {
                  id: 'random-18',
                  label: 'Occasional Furniture',
                },
              ],
            },
          ],
        },
        {
          id: 'random-19',
          label: 'Curtains & Blinds',
        },
        {
          id: 'random-20',
          label: 'Carpets',
        },
      ],
    },
  ];

  const [checkBox, handleChangeSearch, handleCheckAll, handleTransfer] = useTransferCheckBox({
    dataL: leftSourceData,
    dataR: rightSourceData,
    dataCheckBoxTitleL: 'Example Left',
    dataCheckBoxTitleR: 'Example Right',
    paginationL: {
      page: 1,
      pageSize: 10,
      total: 10,
      onPageChange: noop,
      onPageSizeChange: noop,
    },
    paginationR: {
      page: 1,
      pageSize: 10,
      total: 10,
      onPageChange: noop,
      onPageSizeChange: noop,
    },
  });

  const [checkBoxImg, _handleChangeSearch, _handleCheckAll, _handleTransfer] = useTransferCheckBox({
    dataL: leftDataImg,
    dataR: rightDataImg,
    dataCheckBoxTitleL: 'Example Left',
    dataCheckBoxTitleR: 'Example Right',
    isShowAvatar: true,
  });

  const [treeView, handleSearchTreeView, handleCheckAllTreeView, handleTransferTreeView] =
    useTransferTreeView({
      dataL: tree,
      dataR: tree2,
      dataCheckBoxTitleL: 'TreeView Left',
      dataCheckBoxTitleR: 'TreeView Right',
    });

  return (
    <div className='mt-6'>
      <h3 className='mb-6'>Transfer Demo</h3>
      <Transfer
        type='checkbox'
        checkBox={checkBox}
        onSearch={handleChangeSearch}
        onCheckAll={handleCheckAll}
        onTransfer={handleTransfer}
      />
      <div className='mt-6'>
        <Transfer
          type='checkbox'
          checkBox={checkBoxImg}
          onSearch={_handleChangeSearch}
          onCheckAll={_handleCheckAll}
          onTransfer={_handleTransfer}
        />
      </div>
      <div className='mt-6'>
        <Transfer
          type='tree-view'
          treeView={treeView}
          onSearch={handleSearchTreeView}
          onCheckAll={handleCheckAllTreeView}
          onTransfer={handleTransferTreeView}
        />
      </div>
    </div>
  );
};

export default TransferDemo;
