import { useState } from 'react';

import { Breadcrumb } from '@/lib';
export default function BreadcrumbDemo() {
  const breadcrumbs = [
    { id: 'home', name: 'home', url: '/' },
    { id: 'feature', name: 'feature', url: '/feature' },
    { id: 'option', name: 'option', url: '/sth/option' },
    { id: 'hello', name: 'hello', url: '/sth/hello' },
    { id: 'ola', name: 'ola', url: '/sth/ola' },
  ];
  const [, setSelected] = useState<{ id: string; name: string; url: string }>(breadcrumbs[0]);
  const handleOnSelected = (selected: any) => {
    setSelected(selected);
  };

  return (
    <div className='p-8'>
      <h3>Breadcrumb</h3>
      <Breadcrumb onSelected={handleOnSelected} breadcrumbs={breadcrumbs} />
    </div>
  );
}
