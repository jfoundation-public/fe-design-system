import { Status } from '@/lib/components/Status';

export default function StatusDemo() {
  return (
    <div className='p-8'>
      <h3>Status</h3>
      <h5>Indicate specific status</h5>
      <div className='grid grid-cols-6 mt-4 justify-items-center'>
        <Status variant='success' size='lg' stroke>
          Success
        </Status>
        <Status variant='warning' size='lg' iconName='warning' stroke>
          Warning
        </Status>
        <Status variant='error' size='lg' stroke>
          Error
        </Status>
        <Status variant='done' size='lg' stroke>
          Done
        </Status>
        <Status variant='canceled' size='lg' iconName='failed-remove' stroke>
          Canceled
        </Status>
        <Status variant='primary' size='lg' stroke>
          Primary
        </Status>
      </div>

      <div className='grid grid-cols-6 mt-4 justify-items-center'>
        <Status variant='success' size='md' stroke>
          Success
        </Status>
        <Status variant='warning' size='md' stroke>
          Warning
        </Status>
        <Status variant='error' size='md' stroke>
          Error
        </Status>
        <Status variant='done' size='md' stroke>
          Done
        </Status>
        <Status variant='canceled' size='md' stroke>
          Canceled
        </Status>
        <Status variant='primary' size='md' stroke>
          Primary
        </Status>
      </div>

      <div className='grid grid-cols-6 mt-4 justify-items-center'>
        <Status variant='success' size='sm' stroke>
          Success
        </Status>
        <Status variant='warning' size='sm' stroke>
          Warning
        </Status>
        <Status variant='error' size='sm' stroke>
          Error
        </Status>
        <Status variant='done' size='sm' stroke>
          Done
        </Status>
        <Status variant='canceled' size='sm' stroke>
          Canceled
        </Status>
        <Status variant='primary' size='sm' stroke>
          Primary
        </Status>
      </div>

      <div className='grid grid-cols-6 mt-4 justify-items-center'>
        <Status variant='success' size='lg'>
          Success
        </Status>
        <Status variant='warning' size='lg'>
          Warning
        </Status>
        <Status variant='error' size='lg'>
          Error
        </Status>
        <Status variant='done' size='lg'>
          Done
        </Status>
        <Status variant='canceled' size='lg'>
          Canceled
        </Status>
        <Status variant='primary' size='lg'>
          Primary
        </Status>
      </div>

      <div className='grid grid-cols-6 mt-4 justify-items-center'>
        <Status variant='success' size='md'>
          Success
        </Status>
        <Status variant='warning' size='md'>
          Warning
        </Status>
        <Status variant='error' size='md'>
          Error
        </Status>
        <Status variant='done' size='md'>
          Done
        </Status>
        <Status variant='canceled' size='md'>
          Canceled
        </Status>
        <Status variant='primary' size='md'>
          Primary
        </Status>
      </div>

      <div className='grid grid-cols-6 mt-4 justify-items-center'>
        <Status variant='success' size='sm'>
          Success
        </Status>
        <Status variant='warning' size='sm'>
          Warning
        </Status>
        <Status variant='error' size='sm'>
          Error
        </Status>
        <Status variant='done' size='sm'>
          Done
        </Status>
        <Status variant='canceled' size='sm'>
          Canceled
        </Status>
        <Status variant='primary' size='sm'>
          Primary
        </Status>
      </div>
    </div>
  );
}
