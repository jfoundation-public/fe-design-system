import clsx from 'clsx';
import { useMemo, useState } from 'react';

import {
  Checkbox,
  ColumnDef,
  ColumnDisplayed,
  ColumnOrderState,
  DataGrid,
  ExpandedState,
  FieldSearch,
  GridTableType,
  IconButton,
  RowSelectionState,
  SortingState,
  Typography,
  VisibilityState,
} from '@/lib';
import { Popover, usePopover } from '@/lib/components/Popover';
import { SelectItem } from '@/lib/components/SelectItem';

type Data = {
  id: number;
  lastName: string;
  firstName: string | null;
  age: number | null;
  subRows?: Data[];
};

const rawData: Data[] = [
  {
    id: 1,
    lastName: 'Snow',
    firstName: 'Jon',
    age: 35,
    subRows: [
      { id: 2, lastName: 'Snow', firstName: 'Jon', age: 35 },
      {
        id: 3,
        lastName: 'Lannister',
        firstName: 'Cersei',
        age: 42,
        subRows: [
          { id: 4, lastName: 'Lannister', firstName: 'Cersei', age: 42 },
          {
            id: 5,
            lastName: 'Lannister',
            firstName: 'Jaime',
            age: 45,
            subRows: [
              { id: 6, lastName: 'Lannister', firstName: 'Cersei', age: 42 },
              { id: 7, lastName: 'Lannister', firstName: 'Jaime', age: 45 },
              { id: 8, lastName: 'Stark', firstName: 'Arya', age: 16 },
            ],
          },
          { id: 9, lastName: 'Stark', firstName: 'Arya', age: 16 },
        ],
      },
      { id: 10, lastName: 'Lannister', firstName: 'Jaime', age: 45 },
      { id: 11, lastName: 'Stark', firstName: 'Arya', age: 16 },
    ],
  },
  { id: 2, lastName: 'Lannister', firstName: 'Cersei', age: 42 },
  { id: 3, lastName: 'Lannister', firstName: 'Jaime', age: 45 },
  { id: 4, lastName: 'Stark', firstName: 'Arya', age: 16 },
  { id: 5, lastName: 'Targaryen', firstName: 'Daenerys', age: null },
  { id: 6, lastName: 'Melisandre', firstName: null, age: 150 },
  { id: 7, lastName: 'Clifford', firstName: 'Ferrara', age: 44 },
  { id: 8, lastName: 'Frances', firstName: 'Rossini', age: 36 },
  { id: 9, lastName: 'Roxie', firstName: 'Harvey', age: 65 },
];

function MenuAction({
  onSelect,
  rowId,
}: {
  rowId: number;
  onSelect: (rowId: number) => (selected: string) => void;
}) {
  const { onOpen, ...popoverProps } = usePopover();
  const handleSelect = (v: string) => {
    onSelect(rowId)(v);
    popoverProps.onClose();
  };
  return (
    <>
      <IconButton iconName='3dot' isAdornment iconVariant='outline' onClick={onOpen} />
      <Popover {...popoverProps}>
        <div className='paper'>
          {['View', 'Edit', 'Delete'].map((item) => (
            <SelectItem value={item} onSelect={handleSelect} key={item}>
              {item}
            </SelectItem>
          ))}
        </div>
      </Popover>
    </>
  );
}

const HEADER_NAMES = {
  select: 'Select',
  firstName: 'First name',
  id: 'ID',
  lastName: 'Last Name',
  age: 'Age',
  fullName: 'Full name',
  action: 'Action',
};

export default function DataGridDemo() {
  const [rowSelection, setRowSelection] = useState<RowSelectionState>({});
  const [expanded, setExpanded] = useState<ExpandedState>({});
  const [columnVisibility, setColumnVisibility] = useState<VisibilityState>({});
  const [columnOrder, setColumnOrder] = useState<ColumnOrderState>([]);
  // console.log('👌 ~ columnOrder', columnOrder);
  const [sorting, setSorting] = useState<SortingState>([]);

  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);

  const [search, setSearch] = useState('');

  const [table, setTable] = useState<GridTableType<Data>>();
  const [data, setData] = useState<Data[]>(rawData);

  const handleAction = (row: number) => (action: string) => {
    // eslint-disable-next-line no-console
    console.log('>>>>>>', row, action);
  };

  const columns = useMemo<ColumnDef<Data>[]>(() => {
    const baseColumns: ColumnDef<Data>[] = [
      {
        id: 'select',
        header: ({ table }) => (
          <Checkbox
            className='flex items-center'
            checked={table.getIsAllRowsSelected()}
            indeterminate={table.getIsSomeRowsSelected()}
            onChange={table.getToggleAllRowsSelectedHandler()}
          />
        ),
        cell: ({ row }) => (
          <Checkbox
            className='flex items-center'
            checked={row.getIsSelected()}
            indeterminate={row.getIsSomeSelected()}
            onChange={() => row.toggleSelected()}
          />
        ),
        meta: {
          headerClassName: 'w-[56px]',
        },
      },
      {
        id: 'id',
        accessorKey: 'id',
        header: HEADER_NAMES['id'],
      },
      {
        accessorKey: 'firstName',
        header: HEADER_NAMES['firstName'],
        cell: ({ row, getValue }) => {
          return (
            <div className='relative flex items-center'>
              {row.depth >= 1 && (
                <>
                  <p
                    className={clsx(
                      'absolute left-3 h-1 w-1',
                      'before:absolute before:bottom-[8px] before:left-0  before:w-[1px] before:bg-[#9A9DB2]',
                      row.index === 0 && row.depth === 1 ? 'before:h-[1220%]' : 'before:h-[1607%]',
                    )}
                  />
                  <div
                    className={clsx(
                      'relative h-1',
                      row.depth >= 2 &&
                        'before:absolute before:-left-[52px] before:bottom-[2px] before:h-[52px] before:border-l before:border-[#9A9DB2]',
                    )}
                    style={{
                      marginLeft: `${row.depth * 32}px`,
                    }}
                  />
                </>
              )}
              {row.getCanExpand() && (
                <IconButton
                  iconName={row.getIsExpanded() ? 'minus-square' : 'add-square'}
                  iconVariant='outline'
                  isAdornment
                  iconSize={24}
                  className={clsx(
                    'relative mr-2 z-10 bg-white rounded-lg',
                    row.depth >= 1 &&
                      'before:absolute before:-left-5 before:-top-[43px] before:h-[57px] before:w-4 before:rounded-bl-md before:border-b before:border-l before:border-[#9A9DB2]',
                  )}
                  onClick={row.getToggleExpandedHandler()}
                />
              )}
              <Typography
                variant='body'
                className={clsx(
                  !row.getCanExpand() &&
                    row.depth >= 1 &&
                    'relative before:absolute before:-left-5 before:w-4 before:rounded-bl-md before:border-b before:border-l before:border-[#9A9DB2]',
                  row.index === 0
                    ? 'before:-top-[35px] before:h-[200%]'
                    : 'before:-top-[60px] before:h-[310%]',
                )}
              >
                {getValue<string>()}
              </Typography>
            </div>
          );
        },
      },
      {
        id: 'lastName',
        accessorKey: 'lastName',
        header: HEADER_NAMES['lastName'],
        // header: ({ column }) => {
        //   column.columnDef.header = HEADER_NAMES['lastName'];
        //   // header.getContext().setColumnVisibility('lastName', false);
        // },
        cell: ({ row }) => {
          return <>{row.original.lastName}</>;
        },
      },
      {
        accessorKey: 'age',
        header: HEADER_NAMES['age'],
        meta: { align: 'center' },
      },
      {
        id: 'fullName',
        accessorFn: (row) => `${row.firstName} ${row.lastName}`,
        header: HEADER_NAMES['fullName'],
      },
      {
        id: 'action',
        header: () => <>Action</>,
        cell: ({ row }) => <MenuAction rowId={row.original.id} onSelect={handleAction} />,
        meta: { align: 'right', headerClassName: '' },
      },
    ];

    return baseColumns;
  }, []);

  return (
    <div className='p-8'>
      <h3>Data Grid Demo</h3>
      {/* <DataGrid
        data={data}
        columns={columns.slice(1, columns.length)}
        pagination={{
          total: 201,
          page,
          pageSize,
          onPageChange: setPage,
          onPageSizeChange: setPageSize,
        }}
      /> */}
      <h5>With check</h5>
      <div className='flex items-center'>
        <ColumnDisplayed
          columnNames={HEADER_NAMES}
          table={table}
          columnVisibility={columnVisibility}
          onColumnVisibilityChange={setColumnVisibility}
          columnOrder={columnOrder}
          onColumnOrderChange={setColumnOrder}
          hideColumnsId={['action', 'select']}
        />
        <FieldSearch
          value={search}
          onChange={(e) => setSearch(e.target.value)}
          name='search'
          placeholder='Filter'
          noOutline
          className='ml-auto'
        />
      </div>
      <DataGrid
        data={data}
        columns={columns}
        rowSelection={rowSelection}
        expanded={expanded}
        onRowSelectionChange={setRowSelection}
        sorting={sorting}
        onSortingChange={setSorting}
        getSubRows={(row) => row.subRows}
        onExpandedChange={setExpanded}
        columnVisibility={columnVisibility}
        onColumnVisibilityChange={setColumnVisibility}
        columnOrder={columnOrder}
        onColumnOrderChange={setColumnOrder}
        pagination={{
          total: 200,
          page,
          pageSize,
          onPageChange: setPage,
          onPageSizeChange: setPageSize,
        }}
        onTableStateChange={setTable}
        onDataStateChange={setData}
      />
    </div>
  );
}
