import { useFormik } from 'formik';

import { FieldSearch } from '@/lib/components/FieldSearch';

export default function SearchDemo() {
  const { values, handleChange, handleBlur, handleSubmit } = useFormik({
    initialValues: { search1: '', search2: 'typed' },
    onSubmit: (_values) => {
      console.log(_values);
    },
  });
  return (
    <div className='p-8'>
      <div>
        <FieldSearch
          name='search1'
          value={values.search1}
          onChange={handleChange}
          onBlur={handleBlur}
          noOutline
        />
      </div>
      <div className='mt-6'>
        <FieldSearch
          name='search2'
          value={values.search2}
          onChange={handleChange}
          onBlur={handleBlur}
          onKeyDown={(e) => {
            if (e.key === 'Enter') {
              handleSubmit();
            }
          }}
        />
      </div>
    </div>
  );
}
