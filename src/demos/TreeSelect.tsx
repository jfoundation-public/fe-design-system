import { useFormik } from 'formik';

import { TreeSelect } from '@/lib';

import SelectItemCreate from './shared/SelectItemCreate';

const options = [
  {
    id: '1',
    label: 'Furniture',
    items: [
      {
        id: '1.1',
        label: 'Tables & Chairs',
      },
      {
        id: '1.2',
        label: 'Sofas',
      },
      {
        id: '1.3',
        label: 'Occasional Furniture',
      },
    ],
  },
  {
    id: '2',
    label: 'Decor',
    items: [
      {
        id: '2.1',
        label: 'Bed Linen',
        items: [
          {
            id: '2.1.1',
            label: 'B',
            items: [
              {
                id: '2.1.1.1',
                label: 'Tables & Chairs',
              },
              {
                id: '2.1.1.2',
                label: 'Sofas',
              },
              {
                id: '2.1.1.3',
                label: 'Occasional Furniture',
              },
            ],
          },
        ],
      },
      {
        id: '2.2',
        label: 'Curtains & Blinds',
      },
      {
        id: '2.3',
        label: 'Carpets',
      },
    ],
  },
];

export default function TreeSelectDemo() {
  const { values, handleChange } = useFormik({
    initialValues: { something: '2' },
    onSubmit: console.log,
  });

  return (
    <div className='m-8'>
      <h3>Demo TreeSelect</h3>
      <TreeSelect
        label='label demo'
        name='something'
        placeholder='chose some thing'
        options={options}
        value={values.something}
        onChange={handleChange}
        moreSelectItem={({ onClose }) => (
          <SelectItemCreate onClose={onClose} onSelect={() => console.log('>>>>>')} />
        )}
      />
    </div>
  );
}
