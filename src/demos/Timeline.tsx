import { Timeline } from '@/lib/components/Timeline';
import {
  TimeLineBottomInfo,
  TimeLineItems,
  TimeLineTopInfo,
} from '@/lib/components/Timeline/types';

const data: TimeLineItems[] = [
  {
    id: '1',
    icon: 'output-up',
    iconVariant: 'solid',
    bottomInfo: {
      detail: { value: 'something bottom 1' },
      info: { value: 'something info bottom' },
    },
    topInfo: { detail: { value: 'something top' }, time: { value: 'time' } },
  },
  {
    id: '2',
    icon: 'add-circle',
    iconVariant: 'outline',
    iconColor: 'error',
    bottomInfo: {
      detail: { value: 'something bottom 2' },
      info: { value: 'something info bottom' },
    },
    topInfo: { detail: { value: 'something top' }, time: { value: 'time' } },
  },
  {
    id: '3',
    icon: 'eye',
    iconColor: 'information',
    bottomInfo: {
      detail: { value: 'something bottom 3' },
      info: { value: 'something info bottom' },
    },
    topInfo: { detail: { value: 'something top' }, time: { value: 'time' } },
  },
  {
    id: '4',
    icon: 'eye-slash',
    iconColor: 'success',
    bottomInfo: {
      detail: { value: 'something bottom 4' },
      info: { value: 'something info bottom' },
    },
    topInfo: { detail: { value: 'something top' }, time: { value: 'time' } },
  },
];

const TimelineDemo = () => {
  return (
    <div className='p-8'>
      <h3 className='mb-4'>Demo Timeline</h3>
      <Timeline
        hasLastLine
        direction='vertical'
        items={data}
        renderTop={(data: TimeLineTopInfo, _id: string) => {
          // console.log(data, 'top section');
          // console.log(id, 'id');

          return <div>{data.detail.value}</div>;
        }}
        renderBottom={(data: TimeLineBottomInfo, id: string) => {
          // console.log(data, 'bottom section');
          // eslint-disable-next-line no-console
          console.log(id, 'id');

          return <div>{data.detail.value}</div>;
        }}
      />
    </div>
  );
};

export default TimelineDemo;
