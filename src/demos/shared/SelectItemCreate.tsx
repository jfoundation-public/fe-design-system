import { Button, Icon } from '@/lib';

export default function SelectItemCreate({
  onClose,
  onSelect,
}: {
  onClose: () => void;
  onSelect: () => void;
}) {
  return (
    <div>
      <Button
        variant='blank'
        leftIcon={<Icon iconName='add' variant='outline' />}
        onClick={() => {
          onClose();
          onSelect();
        }}
      >
        Create
      </Button>
    </div>
  );
}
