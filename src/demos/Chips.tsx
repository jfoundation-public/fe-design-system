import { Chips } from '@/lib';

export default function ChipsDemo() {
  const handleClick = () => {
    console.log('clicked');
  };
  return (
    <div className='p-8'>
      <h3>Chips</h3>
      <div className='flex mt-4'>
        <Chips tagName='Default' onRemove={handleClick} />
        <Chips tagName='Selected' selected onRemove={handleClick} />
        <Chips tagName='Disable' disabled onRemove={handleClick} />
      </div>
      <div className='flex mt-4'>
        <Chips size='md' tagName='Default' onRemove={handleClick} />
        <Chips size='md' tagName='Selected' selected onRemove={handleClick} />
        <Chips size='md' tagName='Disable' disabled onRemove={handleClick} />
      </div>
      <div className='flex mt-4'>
        <Chips size='lg' tagName='Default' onRemove={handleClick} />
        <Chips size='lg' tagName='Selected' selected onRemove={handleClick} />
        <Chips size='lg' tagName='Disable' disabled onRemove={handleClick} />
      </div>
    </div>
  );
}
