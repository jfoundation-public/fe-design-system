import { useState } from 'react';

import { RenderMarkProps, Slider } from '@/lib';

function RenderMark(props: RenderMarkProps) {
  const { isActive, onClick, value } = props;
  return (
    <div
      role='presentation'
      className={isActive ? 'text-text-primary' : 'text-text-error'}
      onClick={onClick}
    >
      {value}
    </div>
  );
}

const marks = {
  100: RenderMark,
  200: RenderMark,
};

export default function SliderDemo() {
  const [rangeValue, setRangValue] = useState<[number, number]>([100, 250]);
  const [value, setValue] = useState(250);
  return (
    <div className='p-8'>
      <h3>Demo Slider</h3>
      <div className='grid grid-cols-2 gap-4'>
        <Slider
          className='w-[300px]'
          range
          value={rangeValue}
          onChange={(v) => setRangValue(v)}
          max={260}
          step={0.1}
        />
        <Slider
          className='w-[300px]'
          value={value}
          onChange={(v) => setValue(v)}
          max={300}
          min={100}
          marks={marks}
        />
      </div>
    </div>
  );
}
