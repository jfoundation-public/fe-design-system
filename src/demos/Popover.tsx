import { Button } from '@/lib';
import { Popover, usePopover } from '@/lib/components/Popover';

export default function PopoverDemo() {
  const { onOpen: onOpenPopover1, ...popoverProps1 } = usePopover();
  const { onOpen: onOpenPopover2, ...popoverProps2 } = usePopover();
  return (
    <div className='p-8'>
      <h3 className='my-4'>Popover</h3>
      <Button onClick={onOpenPopover1}>Open Popover</Button>
      <Popover {...popoverProps1}>
        <div className='p-4 bg-green-100'>Popover content</div>
      </Popover>
      <Button onClick={onOpenPopover2}>Open Popover 2</Button>
      <Popover {...popoverProps2}>
        <div className='p-4 bg-green-100'>Popover content 2</div>
      </Popover>
    </div>
  );
}
