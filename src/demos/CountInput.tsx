import { ChangeEvent, useState } from 'react';

import { CountInput, noop } from '@/lib';

function useCount(init = 0, step = 1) {
  const [val, setVal] = useState(init);

  function onIncrease() {
    setVal((v) => v + step);
  }

  function onDecrease() {
    setVal((v) => v - step);
  }

  function onChange(e: ChangeEvent<HTMLInputElement>) {
    setVal(+e.target.value);
  }
  return { value: val, onChange, onIncrease, onDecrease };
}

export default function CountInputDemo() {
  const { value: step, onIncrease: onStepIncrease, onDecrease: onStepDescrease } = useCount(1);
  const { value, onIncrease, onDecrease, onChange } = useCount(0, step);

  return (
    <div className='p-8'>
      <h3>Demo Count Input</h3>
      <div className='w-1/2 mt-4'>
        <h5>Step: </h5>
        <CountInput
          value={step}
          onChange={noop}
          decreaseDisabled={step <= 1}
          onIncrease={onStepIncrease}
          onDecrease={onStepDescrease}
          className='w-full'
        />
      </div>
      <div className='max-w-[300px] mt-4'>
        <span>Count: </span>
        <CountInput
          value={value}
          onIncrease={onIncrease}
          onDecrease={onDecrease}
          onChange={onChange}
          className='w-[100px]'
        />
      </div>
    </div>
  );
}
