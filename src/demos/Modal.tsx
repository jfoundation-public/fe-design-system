import { useFormik } from 'formik';

import { Button, DateRangePicker, Icon, useToggle } from '@/lib';
import {
  Modal,
  ModalBody,
  ModalConfirmHeader,
  ModalFooter,
  ModalHeader,
} from '@/lib/components/Modal';
import { Status } from '@/lib/components/Status';

export default function ModalDemo() {
  const [open, onToggle] = useToggle();
  const [openConfirm, onToggleConfirm] = useToggle();
  const { values, handleChange } = useFormik({
    initialValues: {
      date: '',
      time: '',
      dateTime: '',
      dateRange: ['', ''] as [string, string],
      dateRange1: ['2022-01-19', '2022-08-19'] as [string, string],
    },
    onSubmit: console.log,
  });
  return (
    <div className='p-8'>
      <h3>Demo Modal</h3>
      <Button className='mt-6' onClick={onToggle}>
        OpenModal
      </Button>
      <Modal open={open} onClose={onToggle} size='xl'>
        <ModalHeader
          icon={<Icon iconName='profile-user' color='default' />}
          title={<h5>Modal Title</h5>}
          description='This is description for this header'
          status={<Status variant='success'>Success</Status>}
          onClose={onToggle}
        />
        <ModalBody>
          <DateRangePicker
            label='Date range'
            name='dateRange'
            placeholder={['Input from Date', 'input to date']}
            value={values.dateRange}
            onChange={handleChange}
            className='mt-4'
          />

          <div className='min-h-screen'>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Quidem dolorem, iste harum ipsa
            accusamus error facere temporibus quos quaerat nobis! Possimus qui porro veniam eum illo
            voluptates eos nemo molestiae!
          </div>
        </ModalBody>
        <ModalFooter>This is Modal footer content</ModalFooter>
      </Modal>

      <Button className='mt-6' onClick={onToggleConfirm}>
        OpenModal Confirm
      </Button>
      <Modal open={openConfirm} onClose={onToggleConfirm}>
        <ModalConfirmHeader
          variant='delete'
          title='Information something'
          description='This is description for this header'
          status={<Status variant='success'>Success</Status>}
          onClose={onToggleConfirm}
        />
        <ModalBody>This is body content</ModalBody>
        <ModalFooter>This is Modal footer content</ModalFooter>
      </Modal>
    </div>
  );
}
