import axios from 'axios';
import { useRef } from 'react';

import { Icon, PreviewImage, uuid } from '@/lib';
import { Upload, UploadProgress, useUpload } from '@/lib/components/UnstableUpload';

const headers = {
  Authorization:
    'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJXXzlNTTRQcE9kNXhhNnlENGJ0VlpBPT0iLCJleHAiOjE2Njk0Mzc3ODcsImp0aSI6InVtUHZoSm9GV0VNSzFBMGE2YVZSWlk2R1VaYnRBdmZpV1JTWjlIdS1LdG90b1hVTUVjUndJRU0tWjc1V0dyUkhWdWR5LWc9PSJ9.gKzHv6OCIR9jbU9R6sfK2yIvjJocmyXJS4T4HPxn8fw',
  'x-client-key':
    'LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0KTUZrd0V3WUhLb1pJemowQ0FRWUlLb1pJemowREFRY0RRZ0FFMmFBZTZ3WjBWL3hZZ0dHS2t5ZGYrUkRnSkJ0bApNSkFZQzJ2cERRSHlsQ2hiWTNhd0o2d1BUMUt0S01VdWxKWlN3U0RjMmdoQTNYV2Z1RFFnY3ZNTUxBPT0KLS0tLS1FTkQgUFVCTElDIEtFWS0tLS0tCg==',
};

export default function UploadDemo() {
  const signalRef = useRef(new AbortController());
  const handleUpload = (setProgress: React.Dispatch<React.SetStateAction<UploadProgress>>) => {
    return async (file: File | undefined) => {
      if (file) {
        const fileId = file.name;
        try {
          const resp = await axios.post(
            'https://dev.jcore.me/jf-media/api/v1/file/pre-upload',
            {
              name: fileId,
              media_code: file.type.split('/').pop(),
              key: 'jf_seller',
              object_key: 'user_avatar',
            },
            {
              headers: { ...headers, 'Content-Type': 'application/json' },
              signal: signalRef.current.signal,
            },
          );
          const { id, url } = resp.data.data;

          const formData = new FormData();
          formData.append('file', file);
          formData.append('upload_url', url);
          setProgress((prv) => ({ ...prv, [fileId]: 1 }));

          const res = await axios.post(
            `https://dev.jcore.me/jf-media/api/v1/file/upload/${id}`,
            formData,
            {
              headers,
              onUploadProgress: (progressEvent) => {
                let _progress = Math.floor(
                  (progressEvent.loaded * 100) / (progressEvent?.total ?? 1),
                );
                _progress = _progress === 100 ? 99 : _progress;
                setProgress((prv) => ({ ...prv, [fileId]: _progress }));
              },
              signal: signalRef.current.signal,
            },
          );
          setProgress((prv) => ({ ...prv, [fileId]: 100 }));
          if (resp?.data?.error || res?.data?.error) {
            return setErrors((prev) => [
              ...prev,
              {
                id: fileId,
                file,
                errorMsg: resp?.data?.error || res?.data?.error,
                hasSuccessUpload: true,
              },
            ]);
          }

          setPreviews((p) => [
            ...p,
            {
              id: res?.data?.data?.id || uuid(),
              name: file.name,
              url: res?.data?.data?.url || url,
              type: 'image',
            },
          ]);
        } catch (err) {
          // console.log(err, 'err');
        }
      }
    };
  };

  const {
    progress,
    errors,
    previews,
    previewFile,
    setErrors,
    setPreviews,
    handleChange,
    handleDrag,
    cancelRequest,
    handleRemoveFile,
    handleRemoveErrorFile,
    handlePreviewFile,
    handleClosePreviewFile,
  } = useUpload(handleUpload, signalRef);

  if (previewFile) {
    return (
      <div className='flex flex-col'>
        <Icon
          className='cursor-pointer'
          onClick={handleClosePreviewFile}
          iconName='circleCross'
          variant='solid'
        />
        <PreviewImage ratio='1:1' src={previewFile} width={600} />
      </div>
    );
  }

  return (
    <div className='p-8 flex flex-col gap-24'>
      <h3>Demo Upload</h3>
      <div>
        <Upload
          layout='vertical'
          onCancelUpload={cancelRequest}
          previews={previews}
          name='uploadFile'
          progress={progress}
          errors={errors}
          onChange={handleChange}
          onRemoveFile={handleRemoveFile}
          onRemoveErrorFile={handleRemoveErrorFile}
          multiple
        />
      </div>
      <div>
        <Upload
          layout='horizontal'
          onCancelUpload={cancelRequest}
          previews={previews}
          name='uploadFile'
          progress={progress}
          errors={errors}
          onChange={handleChange}
          onRemoveFile={handleRemoveFile}
          onPreviewFile={handlePreviewFile}
          onRemoveErrorFile={handleRemoveErrorFile}
          multiple
          // wrapperClassName='w-1/2'
          className='!w-[300px]'
        />
      </div>
      <div>
        <Upload
          isDrag
          layout='horizontal'
          onCancelUpload={cancelRequest}
          previews={previews}
          errors={errors}
          name='uploadFile'
          progress={progress}
          onRemoveErrorFile={handleRemoveErrorFile}
          onRemoveFile={handleRemoveFile}
          onPreviewFile={handlePreviewFile}
          onDragUpload={handleDrag}
          multiple
        />
      </div>
      <div>
        <Upload
          isDrag
          layout='vertical'
          onCancelUpload={cancelRequest}
          previews={previews}
          errors={errors}
          name='uploadFile'
          progress={progress}
          onRemoveErrorFile={handleRemoveErrorFile}
          onRemoveFile={handleRemoveFile}
          onPreviewFile={handlePreviewFile}
          onDragUpload={handleDrag}
          dragText={{ btnText: 'Button Text', mainText: 'Main Text' }}
          multiple
        />
      </div>
    </div>
  );
}
