export default [
  {
    value: 'lao-cai',
    label: 'Lào Cai',
  },
  {
    value: 'dien-bien',
    label: 'Điện Biên',
  },
  {
    value: 'lai-chau',
    label: 'Lai Châu',
  },
  {
    value: 'son-la',
    label: 'Sơn La',
  },
  {
    value: 'yen-bai',
    label: 'Yên Bái',
  },
  {
    value: 'hoa-binh',
    label: 'Hoà Bình',
  },
  {
    value: 'thai-nguyen',
    label: 'Thái Nguyên',
  },
  {
    value: 'lang-son',
    label: 'Lạng Sơn',
  },
  {
    value: 'quang-ninh',
    label: 'Quảng Ninh',
  },
  {
    value: 'bac-giang',
    label: 'Bắc Giang',
  },
  {
    value: 'phu-tho',
    label: 'Phú Thọ',
  },
  {
    value: 'vinh-phuc',
    label: 'Vĩnh Phúc',
  },
  {
    value: 'bac-ninh',
    label: 'Bắc Ninh',
  },
  {
    value: 'hai-duong',
    label: 'Hải Dương',
  },
  {
    value: 'hai-phong',
    label: 'Hải Phòng',
  },
  {
    value: 'hung-yen',
    label: 'Hưng Yên',
  },
  {
    value: 'thai-binh',
    label: 'Thái Bình',
  },
  {
    value: 'ha-nam',
    label: 'Hà Nam',
  },
  {
    value: 'nam-dinh',
    label: 'Nam Định',
  },
  {
    value: 'ninh-binh',
    label: 'Ninh Bình',
  },
  {
    value: 'thanh-hoa',
    label: 'Thanh Hóa',
  },
  {
    value: 'nghe-an',
    label: 'Nghệ An',
  },
  {
    value: 'ha-tinh',
    label: 'Hà Tĩnh',
  },
  {
    value: 'quang-binh',
    label: 'Quảng Bình',
  },
  {
    value: 'quang-tri',
    label: 'Quảng Trị',
  },
  {
    value: 'thua-thien-hue',
    label: 'Thừa Thiên Huế',
  },
  {
    value: 'da-nang',
    label: 'Đà Nẵng',
  },
  {
    value: 'quang-nam',
    label: 'Quảng Nam',
  },
  {
    value: 'quang-ngai',
    label: 'Quảng Ngãi',
  },
  {
    value: 'binh-dinh',
    label: 'Bình Định',
  },
  {
    value: 'phu-yen',
    label: 'Phú Yên',
  },
  {
    value: 'khanh-hoa',
    label: 'Khánh Hòa',
  },
  {
    value: 'ninh-thuan',
    label: 'Ninh Thuận',
  },
  {
    value: 'binh-thuan',
    label: 'Bình Thuận',
  },
  {
    value: 'kon-tum',
    label: 'Kon Tum',
  },
  {
    value: 'gia-lai',
    label: 'Gia Lai',
  },
  {
    value: 'dak-lak',
    label: 'Đắk Lắk',
  },
  {
    value: 'dak-nong',
    label: 'Đắk Nông',
  },
  {
    value: 'lam-dong',
    label: 'Lâm Đồng',
  },
  {
    value: 'binh-phuoc',
    label: 'Bình Phước',
  },
  {
    value: 'tay-ninh',
    label: 'Tây Ninh',
  },
  {
    value: 'binh-duong',
    label: 'Bình Dương',
  },
  {
    value: 'dong-nai',
    label: 'Đồng Nai',
  },
  {
    value: 'ba-ria-vung-tau',
    label: 'Bà Rịa - Vũng Tàu',
  },
  {
    value: 'ho-chi-minh',
    label: 'Hồ Chí Minh',
  },
  {
    value: 'long-an',
    label: 'Long An',
  },
  {
    value: 'tien-giang',
    label: 'Tiền Giang',
  },
  {
    value: 'ben-tre',
    label: 'Bến Tre',
  },
  {
    value: 'tra-vinh',
    label: 'Trà Vinh',
  },
  {
    value: 'vinh-long',
    label: 'Vĩnh Long',
  },
  {
    value: 'dong-thap',
    label: 'Đồng Tháp',
  },
  {
    value: 'an-giang',
    label: 'An Giang',
  },
  {
    value: 'kien-giang',
    label: 'Kiên Giang',
  },
  {
    value: 'can-tho',
    label: 'Cần Thơ',
  },
  {
    value: 'hau-giang',
    label: 'Hậu Giang',
  },
  {
    value: 'soc-trang',
    label: 'Sóc Trăng',
  },
  {
    value: 'bac-lieu',
    label: 'Bạc Liêu',
  },
  {
    value: 'ca-mau',
    label: 'Cà Mau',
  },
  {
    value: 'ha-noi',
    label: 'Hà Nội',
  },
  {
    value: 'ha-giang',
    label: 'Hà Giang',
  },
  {
    value: 'cao-bang',
    label: 'Cao Bằng',
  },
  {
    value: 'bac-kan',
    label: 'Bắc Kạn',
  },
  {
    value: 'tuyen-quang',
    label: 'Tuyên Quang',
  },
];
