import { Badge } from '@/lib';

const BadgeDemos = () => {
  return (
    <div className='p-8'>
      <h3>Badge</h3>
      <Badge type='dot' value='New' />
      <Badge type='text' value='New' />
      <Badge type='text' value='9' />
      <Badge type='text' value='99+' />
      <Badge>
        <span>999+</span>
      </Badge>
    </div>
  );
};

export default BadgeDemos;
