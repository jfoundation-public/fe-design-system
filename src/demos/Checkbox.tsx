import { useToggle } from '@/lib';
import { Checkbox } from '@/lib/components/Checkbox';

export default function ToggleDemo() {
  const [checked, onToggle] = useToggle();
  return (
    <div className='p-8'>
      <h3>Demo Checkbox</h3>
      <Checkbox className='mt-4' checked={checked} disabled onChange={onToggle}>
        Disable
      </Checkbox>
      <Checkbox className='mt-4' checked={checked} readOnly onChange={onToggle}>
        Read only
      </Checkbox>
      <Checkbox className='mt-4' checked={checked} onChange={onToggle}>
        Controlled
      </Checkbox>
      <Checkbox indeterminate disabled>
        Indeterminate
      </Checkbox>
      <Checkbox indeterminate checked={checked} onChange={onToggle}>
        Indeterminate
      </Checkbox>
    </div>
  );
}
