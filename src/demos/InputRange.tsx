import { useFormik } from 'formik';

import { InputRange, noop } from '@/lib';

export default function InputRangeDemo() {
  const { values, handleChange } = useFormik({
    initialValues: { age: [3, 4], range: [10, 20] },
    onSubmit: noop,
  });

  return (
    <div className='p-8'>
      <h3>Demo Input Range</h3>
      <div className='grid grid-cols-2 space-x-4'>
        <InputRange
          label='Range'
          name='age'
          placeholder={['age from', 'to']}
          value={values.age}
          onChange={handleChange}
        />
        <InputRange
          label='Range'
          isError
          helperText='Error'
          name='age'
          value={values.age}
          onChange={handleChange}
        />
      </div>
    </div>
  );
}
