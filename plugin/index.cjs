const plugin = require('tailwindcss/plugin');

const base = require('./base.cjs');
const utilities = require('./utilities.cjs');

const alert = require('./components/alert.cjs');
const accordion = require('./components/accordion.cjs');
const avatar = require('./components/avatar.cjs');
const button = require('./components/button.cjs');
const breadcrumb = require('./components/breadcrumb.cjs');
const badge = require('./components/badge.cjs');
const checkbox = require('./components/checkbox.cjs');
const chips = require('./components/chips.cjs');
const countInput = require('./components/count-input.cjs');
const dataGrid = require('./components/data-grid.cjs');
const dateTimePicker = require('./components/date-time-picker.cjs');
const fieldContainer = require('./components/field-container.cjs');
const fieldControlled = require('./components/field-controlled.cjs');
const fieldInput = require('./components/field-input.cjs');
const fieldLabel = require('./components/field-label.cjs');
const helperText = require('./components/helper-text.cjs');
const icon = require('./components/icon.cjs');
const inputBase = require('./components/input-base.cjs');
const inputRange = require('./components/input-range.cjs');
const menu = require('./components/menu.cjs');
const modal = require('./components/modal.cjs');
const listPreviewImg = require('./components/list-preview-img.cjs');
const notification = require('./components/notification.cjs');
const otpInput = require('./components/otp-input.cjs');
const progress = require('./components/progress.cjs');
const previewImg = require('./components/previewImage.cjs');
const radio = require('./components/radio.cjs');
const schedules = require('./components/schedules.cjs');
const selectBase = require('./components/select-base.cjs');
const selectItem = require('./components/select-item.cjs');
const sideBar = require('./components/sidebar.cjs');
const slider = require('./components/slider.cjs');
const status = require('./components/status.cjs');
const step = require('./components/step.cjs');
const tab = require('./components/tab.cjs');
const textarea = require('./components/textarea.cjs');
const toggle = require('./components/toggle.cjs');
const toggleBar = require('./components/toggle-bar.cjs');
const tooltip = require('./components/tooltip.cjs');
const treeView = require('./components/tree-view.cjs');
const transfer = require('./components/transfer.cjs');
const timeline = require('./components/timeline.cjs');
const upload = require('./components/upload.cjs');

const boxShadow = require('./theme/boxShadow.cjs');
const colors = require('./theme/colors.cjs');
const fontSize = require('./theme/fontSize.cjs');
const zIndex = require('./theme/zIndex.cjs');

module.exports = plugin(
  ({ addComponents, addBase, addUtilities }) => {
    addBase({ ...base });
    addComponents({
      ...alert,
      ...accordion,
      ...avatar,
      ...button,
      ...breadcrumb,
      ...badge,
      ...checkbox,
      ...chips,
      ...countInput,
      ...dataGrid,
      ...dateTimePicker,
      ...fieldContainer,
      ...fieldControlled,
      ...fieldInput,
      ...fieldLabel,
      ...helperText,
      ...icon,
      ...inputBase,
      ...inputRange,
      ...menu,
      ...modal,
      ...listPreviewImg,
      ...notification,
      ...otpInput,
      ...progress,
      ...previewImg,
      ...radio,
      ...schedules,
      ...selectBase,
      ...selectItem,
      ...sideBar,
      ...slider,
      ...status,
      ...step,
      ...tab,
      ...textarea,
      ...toggle,
      ...toggleBar,
      ...tooltip,
      ...treeView,
      ...transfer,
      ...timeline,
      ...upload,
    });
    addUtilities({ ...utilities });
  },
  { theme: { extend: { boxShadow, colors, fontSize, zIndex } } },
);
