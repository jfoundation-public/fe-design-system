module.exports = {
  h1: ['3.5rem', { letterSpacing: '0', lineHeight: '4.5rem' }],
  h2: ['2.5rem', { letterSpacing: '0', lineHeight: '3rem' }],
  h3: ['2rem', { letterSpacing: '0', lineHeight: '2.5rem' }],
  h4: ['1.5rem', { letterSpacing: '0', lineHeight: '1.875rem' }],
  h5: ['1.125rem', { letterSpacing: '0', lineHeight: '1.625rem' }],
  subHeading: ['0.875rem', { letterSpacing: '0.0175em', lineHeight: '1.375rem' }],
  subtitle1: ['0.875rem', { letterSpacing: '0.00875em', lineHeight: '1.375rem' }],
  subtitle2: ['0.75rem', { letterSpacing: '0.0175em', lineHeight: '1rem' }],
  body: ['0.875rem', { letterSpacing: '0', lineHeight: '1.375rem' }],
  caption1: ['0.75rem', { letterSpacing: '0.00875em', lineHeight: '1rem' }],
  caption2: ['0.75rem', { letterSpacing: '0', lineHeight: '1rem' }],
};
