module.exports = {
  modal:
    '0px 17px 48px rgba(0, 35, 149, 0.04), 0px 5.125px 14.4706px rgba(0, 35, 149, 0.02), 0px 2.12866px 6.01034px rgba(0, 35, 149, 0.02), 0px 0.769896px 2.17382px rgba(0, 35, 149, 0.01)',
  dropdown: '0px 4px 15px rgba(0, 35, 149, 0.04)',
};
