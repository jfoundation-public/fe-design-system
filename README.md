# jf - Design System

## Environment

- Node - v16
- Yarn v1
- VSCode
- Eslint plugin
- Prettier plugin

## To install dependencies

```
yarn
```

## To Start Development

```
yarn dev
```

## Code base Structure

- src/lib/components: contains all React component that is used by the project
- src/lib/hooks: contains common/shared hooks

* Folder src/lib will be exported

- src/demos: contains all demo components
- src/stories: contains all storybook file

- styles/components: contains all css style for components, which be used to parse to cjs file, which will be used to build tailwind plugin
- plugin folder: will be used to build tailwind plugin, for more information, please visit https://tailwindcss.com/docs/plugins

## After completing the development, run:

`yarn parse-css` to parse _.css to _.cjs file,
Then, go to the 'plugin/index.cjs' file, import the component that you've just developed into addComponent section.

## Build Project

```
yarn build
```

# To contribute this package

- Checkout new branch
  `git checkout -b feat/<branch-name>` for new Features, or:
  `git checkout -b fix/<branch-name>` for bug fixes

- Commit, follow commitlint convention https://commitlint.js.org/#/ or using commitlint/prompt-cli
- Create Merge Request to develop branch

### Release new version

- Create Merge Request to `release` branch. The release branch will trigger CI/CD process after change.
- To release new version, you have to commit with prefix ( fix(pencil): , feat(pencil):, perf(pencil):, ...). More info: https://github.com/semantic-release/semantic-release

#### To get gitlab-token, please read the document below

https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html

Then, install the package

`npm install jf-design-system`

or

`yarn add jf-design-system`

- Config Tailwindcss (tailwind.config.js) as below:

```
/** @type {import('tailwindcss').Config} */
const dsPlugin = require("jf-design-system/plugin");
module.exports = {
  content: [
    "node_modules/jf-design-system/**/*.{js,ts,tsx}",
    "src/**/*.{js,ts,tsx}",
  ],
  plugins: [dsPlugin],
};
```
