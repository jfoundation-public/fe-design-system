import fs from 'fs';
import readDir from 'fs-readdir-recursive';
import outputFileSync from 'output-file-sync';
import path from 'path';
import postcss from 'postcss';
import postcssJs from 'postcss-js';

const PARSE_CSS = [
  { source: path.join('styles', 'base.css'), outDir: 'plugin' },
  { source: path.join('styles', 'utilities.css'), outDir: 'plugin' },
  { source: path.join('styles', 'components'), outDir: path.join('plugin', 'components') },
];

doParse(PARSE_CSS);

function getFileNameWithoutExt(filename) {
  return filename.replace(/\.(\w*?)$/, '');
}

function processParse(file, { outDir }) {
  const cssContent = fs.readFileSync(file, 'utf-8');

  const source = file.split(path.sep);
  const name = getFileNameWithoutExt(source.pop());
  const root = postcss.parse(cssContent);
  const parsed = postcssJs.objectify(root);
  const outExt = 'cjs';
  const outName = `${name}.${outExt}`;

  let output = 'module.exports = ' + JSON.stringify(parsed, null, 0) + ';';

  const finalFile = path.join(outDir, outName);
  outputFileSync(finalFile, output);
}

function isDirectory(_path) {
  const stat = fs.statSync(_path);
  return stat.isDirectory();
}

function isCssFile(filename) {
  return filename.split('.').pop() === 'css';
}

function doParse(sources) {
  sources.forEach(({ source: fileOrFolder, outDir }) => {
    if (isDirectory(fileOrFolder)) {
      readDir(fileOrFolder).forEach((fileInDir) => {
        const finalPath = path.join(fileOrFolder, fileInDir);
        if (isCssFile(finalPath)) {
          processParse(finalPath, { outDir });
        }
      });
    } else {
      if (isCssFile(fileOrFolder)) {
        processParse(fileOrFolder, { outDir });
      }
    }
  });
}
